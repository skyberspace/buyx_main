//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class MainAppCartApi {
  MainAppCartApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// add product to cart
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AddToCartRequest] addToCartRequest:
  Future<Response> cartAddToCartPostWithHttpInfo({ AddToCartRequest addToCartRequest }) async {
    // Verify required params are set.

    final path = r'/cart/AddToCart';

    Object postBody = addToCartRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// add product to cart
  ///
  /// Parameters:
  ///
  /// * [AddToCartRequest] addToCartRequest:
  Future<CartItemBR> cartAddToCartPost({ AddToCartRequest addToCartRequest }) async {
    final response = await cartAddToCartPostWithHttpInfo( addToCartRequest: addToCartRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'CartItemBR') as CartItemBR;
        }
    return Future<CartItemBR>.value(null);
  }

  /// CreatePaymentIntent
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [CreateOrderRequest] createOrderRequest:
  Future<Response> cartCreatePaymentIntentPostWithHttpInfo({ CreateOrderRequest createOrderRequest }) async {
    // Verify required params are set.

    final path = r'/cart/CreatePaymentIntent';

    Object postBody = createOrderRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// CreatePaymentIntent
  ///
  /// Parameters:
  ///
  /// * [CreateOrderRequest] createOrderRequest:
  Future<PaymentSheetDataBR> cartCreatePaymentIntentPost({ CreateOrderRequest createOrderRequest }) async {
    final response = await cartCreatePaymentIntentPostWithHttpInfo( createOrderRequest: createOrderRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'PaymentSheetDataBR') as PaymentSheetDataBR;
        }
    return Future<PaymentSheetDataBR>.value(null);
  }

  /// DeleteOrder
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [IdRequest] idRequest:
  Future<Response> cartDeleteOrderPostWithHttpInfo({ IdRequest idRequest }) async {
    // Verify required params are set.

    final path = r'/cart/DeleteOrder';

    Object postBody = idRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// DeleteOrder
  ///
  /// Parameters:
  ///
  /// * [IdRequest] idRequest:
  Future<BR> cartDeleteOrderPost({ IdRequest idRequest }) async {
    final response = await cartDeleteOrderPostWithHttpInfo( idRequest: idRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }

  /// Returns the list of the not completed orders for user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> cartGetActiveOrdersPostWithHttpInfo() async {
    final path = r'/cart/GetActiveOrders';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Returns the list of the not completed orders for user
  Future<OrderListBR> cartGetActiveOrdersPost() async {
    final response = await cartGetActiveOrdersPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderListBR') as OrderListBR;
        }
    return Future<OrderListBR>.value(null);
  }

  /// get current cart items for user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> cartGetCartItemsPostWithHttpInfo() async {
    final path = r'/cart/GetCartItems';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// get current cart items for user
  Future<CartItemListBR> cartGetCartItemsPost() async {
    final response = await cartGetCartItemsPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'CartItemListBR') as CartItemListBR;
        }
    return Future<CartItemListBR>.value(null);
  }

  /// GetCheckoutDetails
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [CheckoutDetailsRequest] checkoutDetailsRequest:
  Future<Response> cartGetCheckoutDetailsPostWithHttpInfo({ CheckoutDetailsRequest checkoutDetailsRequest }) async {
    // Verify required params are set.

    final path = r'/cart/GetCheckoutDetails';

    Object postBody = checkoutDetailsRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// GetCheckoutDetails
  ///
  /// Parameters:
  ///
  /// * [CheckoutDetailsRequest] checkoutDetailsRequest:
  Future<CheckoutDetailsBR> cartGetCheckoutDetailsPost({ CheckoutDetailsRequest checkoutDetailsRequest }) async {
    final response = await cartGetCheckoutDetailsPostWithHttpInfo( checkoutDetailsRequest: checkoutDetailsRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'CheckoutDetailsBR') as CheckoutDetailsBR;
        }
    return Future<CheckoutDetailsBR>.value(null);
  }

  /// GetOrderhistory
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> cartGetOrderHistoryPostWithHttpInfo() async {
    final path = r'/cart/GetOrderHistory';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// GetOrderhistory
  Future<OrderListBR> cartGetOrderHistoryPost() async {
    final response = await cartGetOrderHistoryPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderListBR') as OrderListBR;
        }
    return Future<OrderListBR>.value(null);
  }

  /// GetPublicStripeToken
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> cartGetPublicStripeTokenPostWithHttpInfo() async {
    final path = r'/cart/GetPublicStripeToken';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// GetPublicStripeToken
  Future<StringBR> cartGetPublicStripeTokenPost() async {
    final response = await cartGetPublicStripeTokenPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'StringBR') as StringBR;
        }
    return Future<StringBR>.value(null);
  }

  /// PlaceOrder
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [CreateOrderRequestWithPayment] createOrderRequestWithPayment:
  Future<Response> cartPlaceNonProvisionedOrderPostWithHttpInfo({ CreateOrderRequestWithPayment createOrderRequestWithPayment }) async {
    // Verify required params are set.

    final path = r'/cart/PlaceNonProvisionedOrder';

    Object postBody = createOrderRequestWithPayment;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// PlaceOrder
  ///
  /// Parameters:
  ///
  /// * [CreateOrderRequestWithPayment] createOrderRequestWithPayment:
  Future<OrderBR> cartPlaceNonProvisionedOrderPost({ CreateOrderRequestWithPayment createOrderRequestWithPayment }) async {
    final response = await cartPlaceNonProvisionedOrderPostWithHttpInfo( createOrderRequestWithPayment: createOrderRequestWithPayment );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderBR') as OrderBR;
        }
    return Future<OrderBR>.value(null);
  }

  /// remove product from cart
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [RemoveFromCartRequest] removeFromCartRequest:
  Future<Response> cartRemoveItemFromCartPostWithHttpInfo({ RemoveFromCartRequest removeFromCartRequest }) async {
    // Verify required params are set.

    final path = r'/cart/RemoveItemFromCart';

    Object postBody = removeFromCartRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// remove product from cart
  ///
  /// Parameters:
  ///
  /// * [RemoveFromCartRequest] removeFromCartRequest:
  Future<ObjectBR> cartRemoveItemFromCartPost({ RemoveFromCartRequest removeFromCartRequest }) async {
    final response = await cartRemoveItemFromCartPostWithHttpInfo( removeFromCartRequest: removeFromCartRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ObjectBR') as ObjectBR;
        }
    return Future<ObjectBR>.value(null);
  }

  /// SetOrderStars
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [SetOrderStarsRequest] setOrderStarsRequest:
  Future<Response> cartSetOrderStarsPostWithHttpInfo({ SetOrderStarsRequest setOrderStarsRequest }) async {
    // Verify required params are set.

    final path = r'/cart/SetOrderStars';

    Object postBody = setOrderStarsRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// SetOrderStars
  ///
  /// Parameters:
  ///
  /// * [SetOrderStarsRequest] setOrderStarsRequest:
  Future<BR> cartSetOrderStarsPost({ SetOrderStarsRequest setOrderStarsRequest }) async {
    final response = await cartSetOrderStarsPostWithHttpInfo( setOrderStarsRequest: setOrderStarsRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }
}
