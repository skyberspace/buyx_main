//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class MainAppAuthApi {
  MainAppAuthApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Check if the current token is valid.if not you should call CreateGuestAccount to create guest account
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> authCheckAuthStatusPostWithHttpInfo() async {
    final path = r'/auth/CheckAuthStatus';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Check if the current token is valid.if not you should call CreateGuestAccount to create guest account
  Future<AuthTokenBR> authCheckAuthStatusPost() async {
    final response = await authCheckAuthStatusPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AuthTokenBR') as AuthTokenBR;
        }
    return Future<AuthTokenBR>.value(null);
  }

  /// ForgotPassword
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [ForgotPasswordStep1Request] forgotPasswordStep1Request:
  Future<Response> authForgotPasswordStep1PostWithHttpInfo({ ForgotPasswordStep1Request forgotPasswordStep1Request }) async {
    // Verify required params are set.

    final path = r'/auth/ForgotPasswordStep1';

    Object postBody = forgotPasswordStep1Request;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// ForgotPassword
  ///
  /// Parameters:
  ///
  /// * [ForgotPasswordStep1Request] forgotPasswordStep1Request:
  Future<ForgotPasswordStep1ResultBR> authForgotPasswordStep1Post({ ForgotPasswordStep1Request forgotPasswordStep1Request }) async {
    final response = await authForgotPasswordStep1PostWithHttpInfo( forgotPasswordStep1Request: forgotPasswordStep1Request );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ForgotPasswordStep1ResultBR') as ForgotPasswordStep1ResultBR;
        }
    return Future<ForgotPasswordStep1ResultBR>.value(null);
  }

  /// ForgotPasswordStep2
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [ForgotPasswordStep2Request] forgotPasswordStep2Request:
  Future<Response> authForgotPasswordStep2PostWithHttpInfo({ ForgotPasswordStep2Request forgotPasswordStep2Request }) async {
    // Verify required params are set.

    final path = r'/auth/ForgotPasswordStep2';

    Object postBody = forgotPasswordStep2Request;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// ForgotPasswordStep2
  ///
  /// Parameters:
  ///
  /// * [ForgotPasswordStep2Request] forgotPasswordStep2Request:
  Future<BooleanBR> authForgotPasswordStep2Post({ ForgotPasswordStep2Request forgotPasswordStep2Request }) async {
    final response = await authForgotPasswordStep2PostWithHttpInfo( forgotPasswordStep2Request: forgotPasswordStep2Request );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BooleanBR') as BooleanBR;
        }
    return Future<BooleanBR>.value(null);
  }

  /// used for getting the user details from AuthToken
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> authGetUserFromTokenPostWithHttpInfo() async {
    final path = r'/auth/GetUserFromToken';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// used for getting the user details from AuthToken
  Future<AuthTokenBR> authGetUserFromTokenPost() async {
    final response = await authGetUserFromTokenPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AuthTokenBR') as AuthTokenBR;
        }
    return Future<AuthTokenBR>.value(null);
  }

  /// Login with Authorization basic
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> authLoginBasicPostWithHttpInfo() async {
    final path = r'/auth/LoginBasic';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Login with Authorization basic
  Future<StringBR> authLoginBasicPost() async {
    final response = await authLoginBasicPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'StringBR') as StringBR;
        }
    return Future<StringBR>.value(null);
  }

  /// Login using phone and password, returns auth token
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [LoginRequest] loginRequest:
  Future<Response> authLoginPostWithHttpInfo({ LoginRequest loginRequest }) async {
    // Verify required params are set.

    final path = r'/auth/Login';

    Object postBody = loginRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Login using phone and password, returns auth token
  ///
  /// Parameters:
  ///
  /// * [LoginRequest] loginRequest:
  Future<AuthTokenBR> authLoginPost({ LoginRequest loginRequest }) async {
    final response = await authLoginPostWithHttpInfo( loginRequest: loginRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AuthTokenBR') as AuthTokenBR;
        }
    return Future<AuthTokenBR>.value(null);
  }

  /// Logs out and invalidates given token
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> authLogoutPostWithHttpInfo() async {
    final path = r'/auth/Logout';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Logs out and invalidates given token
  Future<ObjectBR> authLogoutPost() async {
    final response = await authLogoutPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ObjectBR') as ObjectBR;
        }
    return Future<ObjectBR>.value(null);
  }

  /// register new user returns AuthToken
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [RegisterRequest] registerRequest:
  Future<Response> authRegisterPostWithHttpInfo({ RegisterRequest registerRequest }) async {
    // Verify required params are set.

    final path = r'/auth/Register';

    Object postBody = registerRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// register new user returns AuthToken
  ///
  /// Parameters:
  ///
  /// * [RegisterRequest] registerRequest:
  Future<StringBR> authRegisterPost({ RegisterRequest registerRequest }) async {
    final response = await authRegisterPostWithHttpInfo( registerRequest: registerRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'StringBR') as StringBR;
        }
    return Future<StringBR>.value(null);
  }

  /// RegisterOTPStep2
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] code:
  ///
  /// * [String] OTP:
  Future<Response> authRegisterStep2PostWithHttpInfo({ String code, String OTP }) async {
    // Verify required params are set.

    final path = r'/auth/RegisterStep2';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (code != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'Code', code));
    }
    if (OTP != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'OTP', OTP));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// RegisterOTPStep2
  ///
  /// Parameters:
  ///
  /// * [String] code:
  ///
  /// * [String] OTP:
  Future<AuthTokenBR> authRegisterStep2Post({ String code, String OTP }) async {
    final response = await authRegisterStep2PostWithHttpInfo( code: code, OTP: OTP );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AuthTokenBR') as AuthTokenBR;
        }
    return Future<AuthTokenBR>.value(null);
  }
}
