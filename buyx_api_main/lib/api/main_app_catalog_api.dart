//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class MainAppCatalogApi {
  MainAppCatalogApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// get product details
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [ProductDetailsRequest] productDetailsRequest:
  Future<Response> catalogGetProductDetailsPostWithHttpInfo({ ProductDetailsRequest productDetailsRequest }) async {
    // Verify required params are set.

    final path = r'/catalog/GetProductDetails';

    Object postBody = productDetailsRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// get product details
  ///
  /// Parameters:
  ///
  /// * [ProductDetailsRequest] productDetailsRequest:
  Future<ProductBR> catalogGetProductDetailsPost({ ProductDetailsRequest productDetailsRequest }) async {
    final response = await catalogGetProductDetailsPostWithHttpInfo( productDetailsRequest: productDetailsRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ProductBR') as ProductBR;
        }
    return Future<ProductBR>.value(null);
  }

  /// gets the categories for mainapp homepage
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> catalogGetRootCategoriesPostWithHttpInfo() async {
    final path = r'/catalog/GetRootCategories';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// gets the categories for mainapp homepage
  Future<CategoryListBR> catalogGetRootCategoriesPost() async {
    final response = await catalogGetRootCategoriesPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'CategoryListBR') as CategoryListBR;
        }
    return Future<CategoryListBR>.value(null);
  }

  /// returns the categories for mainapp category detail page
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [GetSubCategoriesRequest] getSubCategoriesRequest:
  Future<Response> catalogGetSubCategoriesFromPostWithHttpInfo({ GetSubCategoriesRequest getSubCategoriesRequest }) async {
    // Verify required params are set.

    final path = r'/catalog/GetSubCategoriesFrom';

    Object postBody = getSubCategoriesRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns the categories for mainapp category detail page
  ///
  /// Parameters:
  ///
  /// * [GetSubCategoriesRequest] getSubCategoriesRequest:
  Future<CategoryListBR> catalogGetSubCategoriesFromPost({ GetSubCategoriesRequest getSubCategoriesRequest }) async {
    final response = await catalogGetSubCategoriesFromPostWithHttpInfo( getSubCategoriesRequest: getSubCategoriesRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'CategoryListBR') as CategoryListBR;
        }
    return Future<CategoryListBR>.value(null);
  }

  /// SearchProducts with a text filter
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [StringFilterRequest] stringFilterRequest:
  Future<Response> catalogSearchProductsPostWithHttpInfo({ StringFilterRequest stringFilterRequest }) async {
    // Verify required params are set.

    final path = r'/catalog/SearchProducts';

    Object postBody = stringFilterRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// SearchProducts with a text filter
  ///
  /// Parameters:
  ///
  /// * [StringFilterRequest] stringFilterRequest:
  Future<ProductSearchRankedListBR> catalogSearchProductsPost({ StringFilterRequest stringFilterRequest }) async {
    final response = await catalogSearchProductsPostWithHttpInfo( stringFilterRequest: stringFilterRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ProductSearchRankedListBR') as ProductSearchRankedListBR;
        }
    return Future<ProductSearchRankedListBR>.value(null);
  }
}
