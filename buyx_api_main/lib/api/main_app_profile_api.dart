//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class MainAppProfileApi {
  MainAppProfileApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// adds the product to current users favorites list
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [ProductDetailsRequest] productDetailsRequest:
  Future<Response> profileAddProductToFavoritesPostWithHttpInfo({ ProductDetailsRequest productDetailsRequest }) async {
    // Verify required params are set.

    final path = r'/profile/AddProductToFavorites';

    Object postBody = productDetailsRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// adds the product to current users favorites list
  ///
  /// Parameters:
  ///
  /// * [ProductDetailsRequest] productDetailsRequest:
  Future<UserFavoriteBR> profileAddProductToFavoritesPost({ ProductDetailsRequest productDetailsRequest }) async {
    final response = await profileAddProductToFavoritesPostWithHttpInfo( productDetailsRequest: productDetailsRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UserFavoriteBR') as UserFavoriteBR;
        }
    return Future<UserFavoriteBR>.value(null);
  }

  /// Change email, name,surname Information
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [ChangeInformtionRequest] changeInformtionRequest:
  Future<Response> profileChangeInformationPostWithHttpInfo({ ChangeInformtionRequest changeInformtionRequest }) async {
    // Verify required params are set.

    final path = r'/profile/ChangeInformation';

    Object postBody = changeInformtionRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Change email, name,surname Information
  ///
  /// Parameters:
  ///
  /// * [ChangeInformtionRequest] changeInformtionRequest:
  Future<BR> profileChangeInformationPost({ ChangeInformtionRequest changeInformtionRequest }) async {
    final response = await profileChangeInformationPostWithHttpInfo( changeInformtionRequest: changeInformtionRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }

  /// changes password of the current user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [ChangePasswordRequest] changePasswordRequest:
  Future<Response> profileChangePasswordPostWithHttpInfo({ ChangePasswordRequest changePasswordRequest }) async {
    // Verify required params are set.

    final path = r'/profile/ChangePassword';

    Object postBody = changePasswordRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// changes password of the current user
  ///
  /// Parameters:
  ///
  /// * [ChangePasswordRequest] changePasswordRequest:
  Future<UserBR> profileChangePasswordPost({ ChangePasswordRequest changePasswordRequest }) async {
    final response = await profileChangePasswordPostWithHttpInfo( changePasswordRequest: changePasswordRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UserBR') as UserBR;
        }
    return Future<UserBR>.value(null);
  }

  /// deletes the address from current user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AddressRequest] addressRequest:
  Future<Response> profileDeleteAddressPostWithHttpInfo({ AddressRequest addressRequest }) async {
    // Verify required params are set.

    final path = r'/profile/DeleteAddress';

    Object postBody = addressRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// deletes the address from current user
  ///
  /// Parameters:
  ///
  /// * [AddressRequest] addressRequest:
  Future<BR> profileDeleteAddressPost({ AddressRequest addressRequest }) async {
    final response = await profileDeleteAddressPostWithHttpInfo( addressRequest: addressRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }

  /// returns the default address of current user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> profileGetDefaultAddressPostWithHttpInfo() async {
    final path = r'/profile/GetDefaultAddress';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns the default address of current user
  Future<AddressBR> profileGetDefaultAddressPost() async {
    final response = await profileGetDefaultAddressPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AddressBR') as AddressBR;
        }
    return Future<AddressBR>.value(null);
  }

  /// returns favorites products for the current user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> profileGetFavoritesPostWithHttpInfo() async {
    final path = r'/profile/GetFavorites';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns favorites products for the current user
  Future<UserFavoriteListBR> profileGetFavoritesPost() async {
    final response = await profileGetFavoritesPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UserFavoriteListBR') as UserFavoriteListBR;
        }
    return Future<UserFavoriteListBR>.value(null);
  }

  /// returns current users addresses
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> profileGetUserAddressesPostWithHttpInfo() async {
    final path = r'/profile/GetUserAddresses';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns current users addresses
  Future<AddressListBR> profileGetUserAddressesPost() async {
    final response = await profileGetUserAddressesPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AddressListBR') as AddressListBR;
        }
    return Future<AddressListBR>.value(null);
  }

  /// returns the current user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> profileGetUserPostWithHttpInfo() async {
    final path = r'/profile/GetUser';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns the current user
  Future<UserBR> profileGetUserPost() async {
    final response = await profileGetUserPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UserBR') as UserBR;
        }
    return Future<UserBR>.value(null);
  }

  /// removes the product from current users favorites list
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [UserFavoriteRequest] userFavoriteRequest:
  Future<Response> profileRemoveProductFromFavoritesPostWithHttpInfo({ UserFavoriteRequest userFavoriteRequest }) async {
    // Verify required params are set.

    final path = r'/profile/RemoveProductFromFavorites';

    Object postBody = userFavoriteRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// removes the product from current users favorites list
  ///
  /// Parameters:
  ///
  /// * [UserFavoriteRequest] userFavoriteRequest:
  Future<BR> profileRemoveProductFromFavoritesPost({ UserFavoriteRequest userFavoriteRequest }) async {
    final response = await profileRemoveProductFromFavoritesPostWithHttpInfo( userFavoriteRequest: userFavoriteRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }

  /// saves address details to current user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [SaveAddressRequest] saveAddressRequest:
  Future<Response> profileSaveAddressPostWithHttpInfo({ SaveAddressRequest saveAddressRequest }) async {
    // Verify required params are set.

    final path = r'/profile/SaveAddress';

    Object postBody = saveAddressRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// saves address details to current user
  ///
  /// Parameters:
  ///
  /// * [SaveAddressRequest] saveAddressRequest:
  Future<AddressBR> profileSaveAddressPost({ SaveAddressRequest saveAddressRequest }) async {
    final response = await profileSaveAddressPostWithHttpInfo( saveAddressRequest: saveAddressRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AddressBR') as AddressBR;
        }
    return Future<AddressBR>.value(null);
  }

  /// sets the default address of the user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AddressRequest] addressRequest:
  Future<Response> profileSetDefaultAddressPostWithHttpInfo({ AddressRequest addressRequest }) async {
    // Verify required params are set.

    final path = r'/profile/SetDefaultAddress';

    Object postBody = addressRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// sets the default address of the user
  ///
  /// Parameters:
  ///
  /// * [AddressRequest] addressRequest:
  Future<AddressBR> profileSetDefaultAddressPost({ AddressRequest addressRequest }) async {
    final response = await profileSetDefaultAddressPostWithHttpInfo( addressRequest: addressRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AddressBR') as AddressBR;
        }
    return Future<AddressBR>.value(null);
  }

  /// sets marketing choices for current user  keys are :      MarketingEmails      MarketingNotifications      MarketingSMS      MarketingCalls
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [SetMarketingRequest] setMarketingRequest:
  Future<Response> profileSetMarketingChoicesPostWithHttpInfo({ SetMarketingRequest setMarketingRequest }) async {
    // Verify required params are set.

    final path = r'/profile/SetMarketingChoices';

    Object postBody = setMarketingRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// sets marketing choices for current user  keys are :      MarketingEmails      MarketingNotifications      MarketingSMS      MarketingCalls
  ///
  /// Parameters:
  ///
  /// * [SetMarketingRequest] setMarketingRequest:
  Future<BR> profileSetMarketingChoicesPost({ SetMarketingRequest setMarketingRequest }) async {
    final response = await profileSetMarketingChoicesPostWithHttpInfo( setMarketingRequest: setMarketingRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }

  /// SetUserFCMToken
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [SetFCMTokenRequest] setFCMTokenRequest:
  Future<Response> profileSetUserFCMTokenPostWithHttpInfo({ SetFCMTokenRequest setFCMTokenRequest }) async {
    // Verify required params are set.

    final path = r'/profile/SetUserFCMToken';

    Object postBody = setFCMTokenRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// SetUserFCMToken
  ///
  /// Parameters:
  ///
  /// * [SetFCMTokenRequest] setFCMTokenRequest:
  Future<BooleanBR> profileSetUserFCMTokenPost({ SetFCMTokenRequest setFCMTokenRequest }) async {
    final response = await profileSetUserFCMTokenPostWithHttpInfo( setFCMTokenRequest: setFCMTokenRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BooleanBR') as BooleanBR;
        }
    return Future<BooleanBR>.value(null);
  }
}
