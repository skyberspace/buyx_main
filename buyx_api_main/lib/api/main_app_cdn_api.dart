//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class MainAppCdnApi {
  MainAppCdnApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// returns the given image with guidName and resizes it if hasnt been done before and returns the image in the requested format
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] guidName (required):
  ///   db image name
  ///
  /// * [String] size (required):
  ///   requested size of the image
  ///
  /// * [String] friendlyName (required):
  Future<Response> cdnImageSizeGuidNameFriendlyNameGetWithHttpInfo(String guidName, String size, String friendlyName) async {
    // Verify required params are set.
    if (guidName == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: guidName');
    }
    if (size == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: size');
    }
    if (friendlyName == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: friendlyName');
    }

    final path = r'/cdn/image/{size}/{guidName}/{friendlyName}'
      .replaceAll('{' + 'guidName' + '}', guidName.toString())
      .replaceAll('{' + 'size' + '}', size.toString())
      .replaceAll('{' + 'friendlyName' + '}', friendlyName.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns the given image with guidName and resizes it if hasnt been done before and returns the image in the requested format
  ///
  /// Parameters:
  ///
  /// * [String] guidName (required):
  ///   db image name
  ///
  /// * [String] size (required):
  ///   requested size of the image
  ///
  /// * [String] friendlyName (required):
  Future<void> cdnImageSizeGuidNameFriendlyNameGet(String guidName, String size, String friendlyName) async {
    final response = await cdnImageSizeGuidNameFriendlyNameGetWithHttpInfo(guidName, size, friendlyName);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
  }
}
