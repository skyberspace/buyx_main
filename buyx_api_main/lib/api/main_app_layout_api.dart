//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class MainAppLayoutApi {
  MainAppLayoutApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// returns blog entries from type
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [GetBlogEntriesRequest] getBlogEntriesRequest:
  Future<Response> layoutGetBlogEntriesPostWithHttpInfo({ GetBlogEntriesRequest getBlogEntriesRequest }) async {
    // Verify required params are set.

    final path = r'/layout/GetBlogEntries';

    Object postBody = getBlogEntriesRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns blog entries from type
  ///
  /// Parameters:
  ///
  /// * [GetBlogEntriesRequest] getBlogEntriesRequest:
  Future<BlogEntryListBR> layoutGetBlogEntriesPost({ GetBlogEntriesRequest getBlogEntriesRequest }) async {
    final response = await layoutGetBlogEntriesPostWithHttpInfo( getBlogEntriesRequest: getBlogEntriesRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BlogEntryListBR') as BlogEntryListBR;
        }
    return Future<BlogEntryListBR>.value(null);
  }

  /// returns home slider content
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> layoutGetHomeSliderGetWithHttpInfo() async {
    final path = r'/layout/GetHomeSlider';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns home slider content
  Future<BannerListBR> layoutGetHomeSliderGet() async {
    final response = await layoutGetHomeSliderGetWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BannerListBR') as BannerListBR;
        }
    return Future<BannerListBR>.value(null);
  }
}
