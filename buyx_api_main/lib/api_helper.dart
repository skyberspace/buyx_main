//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class QueryParam {
  const QueryParam(this.name, this.value);

  final String name;
  final String value;

  @override
  String toString() => '${Uri.encodeQueryComponent(name)}=${Uri.encodeQueryComponent(value)}';
}

// Ported from the Java version.
Iterable<QueryParam> _convertParametersForCollectionFormat(
  String collectionFormat,
  String name,
  dynamic value,
) {
  final params = <QueryParam>[];

  // preconditions
  if (name != null && !name.isEmpty && value != null) {
    if (value is List) {
      // get the collection format, default: csv
      collectionFormat = (collectionFormat == null || collectionFormat.isEmpty)
        ? 'csv'
        : collectionFormat;

      if (collectionFormat == 'multi') {
        return value.map((v) => QueryParam(name, parameterToString(v)));
      }

      final delimiter = _delimiters[collectionFormat] ?? ',';

      params.add(QueryParam(name, value.map((v) => parameterToString(v)).join(delimiter)));
    } else {
      params.add(QueryParam(name, parameterToString(value)));
    }
  }

  return params;
}

/// Format the given parameter object into a [String].
String parameterToString(dynamic value) {
  if (value == null) {
    return '';
  }
  if (value is DateTime) {
    return value.toUtc().toIso8601String();
  }
  if (value is BlogType) {
    return BlogTypeTypeTransformer().encode(value).toString();
  }
  if (value is CartCampaignOperation) {
    return CartCampaignOperationTypeTransformer().encode(value).toString();
  }
  if (value is CartCampaignRuleType) {
    return CartCampaignRuleTypeTypeTransformer().encode(value).toString();
  }
  if (value is Dimension) {
    return DimensionTypeTransformer().encode(value).toString();
  }
  if (value is DiscountEffect) {
    return DiscountEffectTypeTransformer().encode(value).toString();
  }
  if (value is DiscountType) {
    return DiscountTypeTypeTransformer().encode(value).toString();
  }
  if (value is OgcGeometryType) {
    return OgcGeometryTypeTypeTransformer().encode(value).toString();
  }
  if (value is OrderState) {
    return OrderStateTypeTransformer().encode(value).toString();
  }
  if (value is OrderType) {
    return OrderTypeTypeTransformer().encode(value).toString();
  }
  if (value is Ordinates) {
    return OrdinatesTypeTransformer().encode(value).toString();
  }
  if (value is PaymentType) {
    return PaymentTypeTypeTransformer().encode(value).toString();
  }
  if (value is Permissions) {
    return PermissionsTypeTransformer().encode(value).toString();
  }
  if (value is PrecisionModels) {
    return PrecisionModelsTypeTransformer().encode(value).toString();
  }
  if (value is WarehouseRole) {
    return WarehouseRoleTypeTransformer().encode(value).toString();
  }
  return value.toString();
}

/// Returns the decoded body as UTF-8 if the given headers indicate an 'application/json'
/// content type. Otherwise, returns the decoded body as decoded by dart:http package.
String _decodeBodyBytes(Response response) {
  final contentType = response.headers['content-type'];
  return contentType != null && contentType.toLowerCase().startsWith('application/json')
    ? response.bodyBytes == null ? null : utf8.decode(response.bodyBytes)
    : response.body;
}
