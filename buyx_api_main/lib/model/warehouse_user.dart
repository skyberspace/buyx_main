//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class WarehouseUser {
  /// Returns a new [WarehouseUser] instance.
  WarehouseUser({
    this.id,
    this.warehouseRole,
    this.firebaseCloudMessagingToken,
    this.canSeeWarehouseOrders,
    this.canSeeRestaurantOrders,
    this.warehouse,
    this.user,
    this.warehouseId,
    this.userId,
    this.imageUrl,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  WarehouseRole warehouseRole;

  String firebaseCloudMessagingToken;

  bool canSeeWarehouseOrders;

  bool canSeeRestaurantOrders;

  Warehouse warehouse;

  User user;

  int warehouseId;

  int userId;

  String imageUrl;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is WarehouseUser &&
     other.id == id &&
     other.warehouseRole == warehouseRole &&
     other.firebaseCloudMessagingToken == firebaseCloudMessagingToken &&
     other.canSeeWarehouseOrders == canSeeWarehouseOrders &&
     other.canSeeRestaurantOrders == canSeeRestaurantOrders &&
     other.warehouse == warehouse &&
     other.user == user &&
     other.warehouseId == warehouseId &&
     other.userId == userId &&
     other.imageUrl == imageUrl &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (warehouseRole == null ? 0 : warehouseRole.hashCode) +
    (firebaseCloudMessagingToken == null ? 0 : firebaseCloudMessagingToken.hashCode) +
    (canSeeWarehouseOrders == null ? 0 : canSeeWarehouseOrders.hashCode) +
    (canSeeRestaurantOrders == null ? 0 : canSeeRestaurantOrders.hashCode) +
    (warehouse == null ? 0 : warehouse.hashCode) +
    (user == null ? 0 : user.hashCode) +
    (warehouseId == null ? 0 : warehouseId.hashCode) +
    (userId == null ? 0 : userId.hashCode) +
    (imageUrl == null ? 0 : imageUrl.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'WarehouseUser[id=$id, warehouseRole=$warehouseRole, firebaseCloudMessagingToken=$firebaseCloudMessagingToken, canSeeWarehouseOrders=$canSeeWarehouseOrders, canSeeRestaurantOrders=$canSeeRestaurantOrders, warehouse=$warehouse, user=$user, warehouseId=$warehouseId, userId=$userId, imageUrl=$imageUrl, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (warehouseRole != null) {
      json[r'warehouseRole'] = warehouseRole;
    }
    if (firebaseCloudMessagingToken != null) {
      json[r'firebaseCloudMessagingToken'] = firebaseCloudMessagingToken;
    }
    if (canSeeWarehouseOrders != null) {
      json[r'canSeeWarehouseOrders'] = canSeeWarehouseOrders;
    }
    if (canSeeRestaurantOrders != null) {
      json[r'canSeeRestaurantOrders'] = canSeeRestaurantOrders;
    }
    if (warehouse != null) {
      json[r'warehouse'] = warehouse;
    }
    if (user != null) {
      json[r'user'] = user;
    }
    if (warehouseId != null) {
      json[r'warehouseId'] = warehouseId;
    }
    if (userId != null) {
      json[r'userId'] = userId;
    }
    if (imageUrl != null) {
      json[r'imageUrl'] = imageUrl;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [WarehouseUser] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static WarehouseUser fromJson(Map<String, dynamic> json) => json == null
    ? null
    : WarehouseUser(
        id: json[r'id'],
        warehouseRole: WarehouseRole.fromJson(json[r'warehouseRole']),
        firebaseCloudMessagingToken: json[r'firebaseCloudMessagingToken'],
        canSeeWarehouseOrders: json[r'canSeeWarehouseOrders'],
        canSeeRestaurantOrders: json[r'canSeeRestaurantOrders'],
        warehouse: Warehouse.fromJson(json[r'warehouse']),
        user: User.fromJson(json[r'user']),
        warehouseId: json[r'warehouseId'],
        userId: json[r'userId'],
        imageUrl: json[r'imageUrl'],
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<WarehouseUser> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <WarehouseUser>[]
      : json.map((v) => WarehouseUser.fromJson(v)).toList(growable: true == growable);

  static Map<String, WarehouseUser> mapFromJson(Map<String, dynamic> json) {
    final map = <String, WarehouseUser>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = WarehouseUser.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of WarehouseUser-objects as value to a dart map
  static Map<String, List<WarehouseUser>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<WarehouseUser>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = WarehouseUser.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

