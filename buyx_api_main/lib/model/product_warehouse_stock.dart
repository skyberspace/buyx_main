//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ProductWarehouseStock {
  /// Returns a new [ProductWarehouseStock] instance.
  ProductWarehouseStock({
    this.id,
    this.quantity,
    this.productId,
    this.warehouseId,
    this.product,
    this.warehouse,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  double quantity;

  int productId;

  int warehouseId;

  Product product;

  Warehouse warehouse;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProductWarehouseStock &&
     other.id == id &&
     other.quantity == quantity &&
     other.productId == productId &&
     other.warehouseId == warehouseId &&
     other.product == product &&
     other.warehouse == warehouse &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (quantity == null ? 0 : quantity.hashCode) +
    (productId == null ? 0 : productId.hashCode) +
    (warehouseId == null ? 0 : warehouseId.hashCode) +
    (product == null ? 0 : product.hashCode) +
    (warehouse == null ? 0 : warehouse.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'ProductWarehouseStock[id=$id, quantity=$quantity, productId=$productId, warehouseId=$warehouseId, product=$product, warehouse=$warehouse, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (quantity != null) {
      json[r'quantity'] = quantity;
    }
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (warehouseId != null) {
      json[r'warehouseId'] = warehouseId;
    }
    if (product != null) {
      json[r'product'] = product;
    }
    if (warehouse != null) {
      json[r'warehouse'] = warehouse;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [ProductWarehouseStock] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ProductWarehouseStock fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ProductWarehouseStock(
        id: json[r'id'],
        quantity: json[r'quantity'],
        productId: json[r'productId'],
        warehouseId: json[r'warehouseId'],
        product: Product.fromJson(json[r'product']),
        warehouse: Warehouse.fromJson(json[r'warehouse']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<ProductWarehouseStock> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ProductWarehouseStock>[]
      : json.map((v) => ProductWarehouseStock.fromJson(v)).toList(growable: true == growable);

  static Map<String, ProductWarehouseStock> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ProductWarehouseStock>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ProductWarehouseStock.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ProductWarehouseStock-objects as value to a dart map
  static Map<String, List<ProductWarehouseStock>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ProductWarehouseStock>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ProductWarehouseStock.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

