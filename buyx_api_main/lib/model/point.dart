//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Point {
  /// Returns a new [Point] instance.
  Point({
    this.coordinateSequence,
    this.coordinates,
    this.numPoints,
    this.isEmpty,
    this.dimension,
    this.boundaryDimension,
    this.x,
    this.y,
    this.coordinate,
    this.geometryType,
    this.ogcGeometryType,
    this.boundary,
    this.z,
    this.m,
    this.factory_,
    this.userData,
    this.srid,
    this.precisionModel,
    this.numGeometries,
    this.isSimple,
    this.isValid,
    this.area,
    this.length,
    this.centroid,
    this.interiorPoint,
    this.pointOnSurface,
    this.envelope,
    this.envelopeInternal,
    this.isRectangle,
  });

  CoordinateSequence coordinateSequence;

  List<Coordinate> coordinates;

  int numPoints;

  bool isEmpty;

  Dimension dimension;

  Dimension boundaryDimension;

  double x;

  double y;

  Coordinate coordinate;

  String geometryType;

  OgcGeometryType ogcGeometryType;

  Geometry boundary;

  double z;

  double m;

  GeometryFactory factory_;

  Object userData;

  int srid;

  PrecisionModel precisionModel;

  int numGeometries;

  bool isSimple;

  bool isValid;

  double area;

  double length;

  Point centroid;

  Point interiorPoint;

  Point pointOnSurface;

  Geometry envelope;

  Envelope envelopeInternal;

  bool isRectangle;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Point &&
     other.coordinateSequence == coordinateSequence &&
     other.coordinates == coordinates &&
     other.numPoints == numPoints &&
     other.isEmpty == isEmpty &&
     other.dimension == dimension &&
     other.boundaryDimension == boundaryDimension &&
     other.x == x &&
     other.y == y &&
     other.coordinate == coordinate &&
     other.geometryType == geometryType &&
     other.ogcGeometryType == ogcGeometryType &&
     other.boundary == boundary &&
     other.z == z &&
     other.m == m &&
     other.factory_ == factory_ &&
     other.userData == userData &&
     other.srid == srid &&
     other.precisionModel == precisionModel &&
     other.numGeometries == numGeometries &&
     other.isSimple == isSimple &&
     other.isValid == isValid &&
     other.area == area &&
     other.length == length &&
     other.centroid == centroid &&
     other.interiorPoint == interiorPoint &&
     other.pointOnSurface == pointOnSurface &&
     other.envelope == envelope &&
     other.envelopeInternal == envelopeInternal &&
     other.isRectangle == isRectangle;

  @override
  int get hashCode =>
    (coordinateSequence == null ? 0 : coordinateSequence.hashCode) +
    (coordinates == null ? 0 : coordinates.hashCode) +
    (numPoints == null ? 0 : numPoints.hashCode) +
    (isEmpty == null ? 0 : isEmpty.hashCode) +
    (dimension == null ? 0 : dimension.hashCode) +
    (boundaryDimension == null ? 0 : boundaryDimension.hashCode) +
    (x == null ? 0 : x.hashCode) +
    (y == null ? 0 : y.hashCode) +
    (coordinate == null ? 0 : coordinate.hashCode) +
    (geometryType == null ? 0 : geometryType.hashCode) +
    (ogcGeometryType == null ? 0 : ogcGeometryType.hashCode) +
    (boundary == null ? 0 : boundary.hashCode) +
    (z == null ? 0 : z.hashCode) +
    (m == null ? 0 : m.hashCode) +
    (factory_ == null ? 0 : factory_.hashCode) +
    (userData == null ? 0 : userData.hashCode) +
    (srid == null ? 0 : srid.hashCode) +
    (precisionModel == null ? 0 : precisionModel.hashCode) +
    (numGeometries == null ? 0 : numGeometries.hashCode) +
    (isSimple == null ? 0 : isSimple.hashCode) +
    (isValid == null ? 0 : isValid.hashCode) +
    (area == null ? 0 : area.hashCode) +
    (length == null ? 0 : length.hashCode) +
    (centroid == null ? 0 : centroid.hashCode) +
    (interiorPoint == null ? 0 : interiorPoint.hashCode) +
    (pointOnSurface == null ? 0 : pointOnSurface.hashCode) +
    (envelope == null ? 0 : envelope.hashCode) +
    (envelopeInternal == null ? 0 : envelopeInternal.hashCode) +
    (isRectangle == null ? 0 : isRectangle.hashCode);

  @override
  String toString() => 'Point[coordinateSequence=$coordinateSequence, coordinates=$coordinates, numPoints=$numPoints, isEmpty=$isEmpty, dimension=$dimension, boundaryDimension=$boundaryDimension, x=$x, y=$y, coordinate=$coordinate, geometryType=$geometryType, ogcGeometryType=$ogcGeometryType, boundary=$boundary, z=$z, m=$m, factory_=$factory_, userData=$userData, srid=$srid, precisionModel=$precisionModel, numGeometries=$numGeometries, isSimple=$isSimple, isValid=$isValid, area=$area, length=$length, centroid=$centroid, interiorPoint=$interiorPoint, pointOnSurface=$pointOnSurface, envelope=$envelope, envelopeInternal=$envelopeInternal, isRectangle=$isRectangle]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (coordinateSequence != null) {
      json[r'coordinateSequence'] = coordinateSequence;
    }
    if (coordinates != null) {
      json[r'coordinates'] = coordinates;
    }
    if (numPoints != null) {
      json[r'numPoints'] = numPoints;
    }
    if (isEmpty != null) {
      json[r'isEmpty'] = isEmpty;
    }
    if (dimension != null) {
      json[r'dimension'] = dimension;
    }
    if (boundaryDimension != null) {
      json[r'boundaryDimension'] = boundaryDimension;
    }
    if (x != null) {
      json[r'x'] = x;
    }
    if (y != null) {
      json[r'y'] = y;
    }
    if (coordinate != null) {
      json[r'coordinate'] = coordinate;
    }
    if (geometryType != null) {
      json[r'geometryType'] = geometryType;
    }
    if (ogcGeometryType != null) {
      json[r'ogcGeometryType'] = ogcGeometryType;
    }
    if (boundary != null) {
      json[r'boundary'] = boundary;
    }
    if (z != null) {
      json[r'z'] = z;
    }
    if (m != null) {
      json[r'm'] = m;
    }
    if (factory_ != null) {
      json[r'factory'] = factory_;
    }
    if (userData != null) {
      json[r'userData'] = userData;
    }
    if (srid != null) {
      json[r'srid'] = srid;
    }
    if (precisionModel != null) {
      json[r'precisionModel'] = precisionModel;
    }
    if (numGeometries != null) {
      json[r'numGeometries'] = numGeometries;
    }
    if (isSimple != null) {
      json[r'isSimple'] = isSimple;
    }
    if (isValid != null) {
      json[r'isValid'] = isValid;
    }
    if (area != null) {
      json[r'area'] = area;
    }
    if (length != null) {
      json[r'length'] = length;
    }
    if (centroid != null) {
      json[r'centroid'] = centroid;
    }
    if (interiorPoint != null) {
      json[r'interiorPoint'] = interiorPoint;
    }
    if (pointOnSurface != null) {
      json[r'pointOnSurface'] = pointOnSurface;
    }
    if (envelope != null) {
      json[r'envelope'] = envelope;
    }
    if (envelopeInternal != null) {
      json[r'envelopeInternal'] = envelopeInternal;
    }
    if (isRectangle != null) {
      json[r'isRectangle'] = isRectangle;
    }
    return json;
  }

  /// Returns a new [Point] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Point fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Point(
        coordinateSequence: CoordinateSequence.fromJson(json[r'coordinateSequence']),
        coordinates: Coordinate.listFromJson(json[r'coordinates']),
        numPoints: json[r'numPoints'],
        isEmpty: json[r'isEmpty'],
        dimension: Dimension.fromJson(json[r'dimension']),
        boundaryDimension: Dimension.fromJson(json[r'boundaryDimension']),
        x: json[r'x'],
        y: json[r'y'],
        coordinate: Coordinate.fromJson(json[r'coordinate']),
        geometryType: json[r'geometryType'],
        ogcGeometryType: OgcGeometryType.fromJson(json[r'ogcGeometryType']),
        boundary: Geometry.fromJson(json[r'boundary']),
        z: json[r'z'],
        m: json[r'm'],
        factory_: GeometryFactory.fromJson(json[r'factory']),
        userData: json[r'userData'],
        srid: json[r'srid'],
        precisionModel: PrecisionModel.fromJson(json[r'precisionModel']),
        numGeometries: json[r'numGeometries'],
        isSimple: json[r'isSimple'],
        isValid: json[r'isValid'],
        area: json[r'area'],
        length: json[r'length'],
        centroid: Point.fromJson(json[r'centroid']),
        interiorPoint: Point.fromJson(json[r'interiorPoint']),
        pointOnSurface: Point.fromJson(json[r'pointOnSurface']),
        envelope: Geometry.fromJson(json[r'envelope']),
        envelopeInternal: Envelope.fromJson(json[r'envelopeInternal']),
        isRectangle: json[r'isRectangle'],
    );

  static List<Point> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Point>[]
      : json.map((v) => Point.fromJson(v)).toList(growable: true == growable);

  static Map<String, Point> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Point>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Point.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Point-objects as value to a dart map
  static Map<String, List<Point>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Point>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Point.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

