//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GeometryFactory {
  /// Returns a new [GeometryFactory] instance.
  GeometryFactory({
    this.precisionModel,
    this.coordinateSequenceFactory,
    this.srid,
  });

  PrecisionModel precisionModel;

  CoordinateSequenceFactory coordinateSequenceFactory;

  int srid;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GeometryFactory &&
     other.precisionModel == precisionModel &&
     other.coordinateSequenceFactory == coordinateSequenceFactory &&
     other.srid == srid;

  @override
  int get hashCode =>
    (precisionModel == null ? 0 : precisionModel.hashCode) +
    (coordinateSequenceFactory == null ? 0 : coordinateSequenceFactory.hashCode) +
    (srid == null ? 0 : srid.hashCode);

  @override
  String toString() => 'GeometryFactory[precisionModel=$precisionModel, coordinateSequenceFactory=$coordinateSequenceFactory, srid=$srid]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (precisionModel != null) {
      json[r'precisionModel'] = precisionModel;
    }
    if (coordinateSequenceFactory != null) {
      json[r'coordinateSequenceFactory'] = coordinateSequenceFactory;
    }
    if (srid != null) {
      json[r'srid'] = srid;
    }
    return json;
  }

  /// Returns a new [GeometryFactory] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static GeometryFactory fromJson(Map<String, dynamic> json) => json == null
    ? null
    : GeometryFactory(
        precisionModel: PrecisionModel.fromJson(json[r'precisionModel']),
        coordinateSequenceFactory: CoordinateSequenceFactory.fromJson(json[r'coordinateSequenceFactory']),
        srid: json[r'srid'],
    );

  static List<GeometryFactory> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <GeometryFactory>[]
      : json.map((v) => GeometryFactory.fromJson(v)).toList(growable: true == growable);

  static Map<String, GeometryFactory> mapFromJson(Map<String, dynamic> json) {
    final map = <String, GeometryFactory>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = GeometryFactory.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of GeometryFactory-objects as value to a dart map
  static Map<String, List<GeometryFactory>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<GeometryFactory>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = GeometryFactory.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

