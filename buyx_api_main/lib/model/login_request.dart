//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class LoginRequest {
  /// Returns a new [LoginRequest] instance.
  LoginRequest({
    this.phone,
    this.password,
  });

  String phone;

  String password;

  @override
  bool operator ==(Object other) => identical(this, other) || other is LoginRequest &&
     other.phone == phone &&
     other.password == password;

  @override
  int get hashCode =>
    (phone == null ? 0 : phone.hashCode) +
    (password == null ? 0 : password.hashCode);

  @override
  String toString() => 'LoginRequest[phone=$phone, password=$password]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (phone != null) {
      json[r'phone'] = phone;
    }
    if (password != null) {
      json[r'password'] = password;
    }
    return json;
  }

  /// Returns a new [LoginRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static LoginRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : LoginRequest(
        phone: json[r'phone'],
        password: json[r'password'],
    );

  static List<LoginRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <LoginRequest>[]
      : json.map((v) => LoginRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, LoginRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, LoginRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = LoginRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of LoginRequest-objects as value to a dart map
  static Map<String, List<LoginRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<LoginRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = LoginRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

