//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class OrderStateHistory {
  /// Returns a new [OrderStateHistory] instance.
  OrderStateHistory({
    this.id,
    this.orderState,
    this.orderId,
    this.isuuerId,
    this.order,
    this.issuer,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  OrderState orderState;

  int orderId;

  int isuuerId;

  Order order;

  User issuer;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is OrderStateHistory &&
     other.id == id &&
     other.orderState == orderState &&
     other.orderId == orderId &&
     other.isuuerId == isuuerId &&
     other.order == order &&
     other.issuer == issuer &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (orderState == null ? 0 : orderState.hashCode) +
    (orderId == null ? 0 : orderId.hashCode) +
    (isuuerId == null ? 0 : isuuerId.hashCode) +
    (order == null ? 0 : order.hashCode) +
    (issuer == null ? 0 : issuer.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'OrderStateHistory[id=$id, orderState=$orderState, orderId=$orderId, isuuerId=$isuuerId, order=$order, issuer=$issuer, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (orderState != null) {
      json[r'orderState'] = orderState;
    }
    if (orderId != null) {
      json[r'orderId'] = orderId;
    }
    if (isuuerId != null) {
      json[r'isuuerId'] = isuuerId;
    }
    if (order != null) {
      json[r'order'] = order;
    }
    if (issuer != null) {
      json[r'issuer'] = issuer;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [OrderStateHistory] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static OrderStateHistory fromJson(Map<String, dynamic> json) => json == null
    ? null
    : OrderStateHistory(
        id: json[r'id'],
        orderState: OrderState.fromJson(json[r'orderState']),
        orderId: json[r'orderId'],
        isuuerId: json[r'isuuerId'],
        order: Order.fromJson(json[r'order']),
        issuer: User.fromJson(json[r'issuer']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<OrderStateHistory> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <OrderStateHistory>[]
      : json.map((v) => OrderStateHistory.fromJson(v)).toList(growable: true == growable);

  static Map<String, OrderStateHistory> mapFromJson(Map<String, dynamic> json) {
    final map = <String, OrderStateHistory>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = OrderStateHistory.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of OrderStateHistory-objects as value to a dart map
  static Map<String, List<OrderStateHistory>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<OrderStateHistory>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = OrderStateHistory.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

