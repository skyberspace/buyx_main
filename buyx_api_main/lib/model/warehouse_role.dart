//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     0 = Supervisor    1 = Packaging    2 = Delivery
class WarehouseRole {
  /// Instantiate a new enum with the provided [value].
  const WarehouseRole._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number0 = WarehouseRole._(0);
  static const number1 = WarehouseRole._(1);
  static const number2 = WarehouseRole._(2);

  /// List of all possible values in this [enum][WarehouseRole].
  static const values = <WarehouseRole>[
    number0,
    number1,
    number2,
  ];

  static WarehouseRole fromJson(dynamic value) =>
    WarehouseRoleTypeTransformer().decode(value);

  static List<WarehouseRole> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <WarehouseRole>[]
      : json
          .map((value) => WarehouseRole.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [WarehouseRole] to int,
/// and [decode] dynamic data back to [WarehouseRole].
class WarehouseRoleTypeTransformer {
  const WarehouseRoleTypeTransformer._();

  factory WarehouseRoleTypeTransformer() => _instance ??= WarehouseRoleTypeTransformer._();

  int encode(WarehouseRole data) => data.value;

  /// Decodes a [dynamic value][data] to a WarehouseRole.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  WarehouseRole decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 0: return WarehouseRole.number0;
      case 1: return WarehouseRole.number1;
      case 2: return WarehouseRole.number2;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [WarehouseRoleTypeTransformer] instance.
  static WarehouseRoleTypeTransformer _instance;
}
