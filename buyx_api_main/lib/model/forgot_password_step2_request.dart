//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ForgotPasswordStep2Request {
  /// Returns a new [ForgotPasswordStep2Request] instance.
  ForgotPasswordStep2Request({
    this.code,
    this.otp,
    this.newPassword,
  });

  String code;

  String otp;

  String newPassword;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ForgotPasswordStep2Request &&
     other.code == code &&
     other.otp == otp &&
     other.newPassword == newPassword;

  @override
  int get hashCode =>
    (code == null ? 0 : code.hashCode) +
    (otp == null ? 0 : otp.hashCode) +
    (newPassword == null ? 0 : newPassword.hashCode);

  @override
  String toString() => 'ForgotPasswordStep2Request[code=$code, otp=$otp, newPassword=$newPassword]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (code != null) {
      json[r'code'] = code;
    }
    if (otp != null) {
      json[r'otp'] = otp;
    }
    if (newPassword != null) {
      json[r'newPassword'] = newPassword;
    }
    return json;
  }

  /// Returns a new [ForgotPasswordStep2Request] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ForgotPasswordStep2Request fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ForgotPasswordStep2Request(
        code: json[r'code'],
        otp: json[r'otp'],
        newPassword: json[r'newPassword'],
    );

  static List<ForgotPasswordStep2Request> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ForgotPasswordStep2Request>[]
      : json.map((v) => ForgotPasswordStep2Request.fromJson(v)).toList(growable: true == growable);

  static Map<String, ForgotPasswordStep2Request> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ForgotPasswordStep2Request>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ForgotPasswordStep2Request.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ForgotPasswordStep2Request-objects as value to a dart map
  static Map<String, List<ForgotPasswordStep2Request>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ForgotPasswordStep2Request>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ForgotPasswordStep2Request.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

