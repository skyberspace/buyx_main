//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AddToCartRequest {
  /// Returns a new [AddToCartRequest] instance.
  AddToCartRequest({
    this.productId,
    this.quantity,
  });

  int productId;

  double quantity;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AddToCartRequest &&
     other.productId == productId &&
     other.quantity == quantity;

  @override
  int get hashCode =>
    (productId == null ? 0 : productId.hashCode) +
    (quantity == null ? 0 : quantity.hashCode);

  @override
  String toString() => 'AddToCartRequest[productId=$productId, quantity=$quantity]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (quantity != null) {
      json[r'quantity'] = quantity;
    }
    return json;
  }

  /// Returns a new [AddToCartRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static AddToCartRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : AddToCartRequest(
        productId: json[r'productId'],
        quantity: json[r'quantity'],
    );

  static List<AddToCartRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <AddToCartRequest>[]
      : json.map((v) => AddToCartRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, AddToCartRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, AddToCartRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = AddToCartRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of AddToCartRequest-objects as value to a dart map
  static Map<String, List<AddToCartRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<AddToCartRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = AddToCartRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

