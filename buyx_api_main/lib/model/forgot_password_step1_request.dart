//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ForgotPasswordStep1Request {
  /// Returns a new [ForgotPasswordStep1Request] instance.
  ForgotPasswordStep1Request({
    this.phone,
  });

  String phone;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ForgotPasswordStep1Request &&
     other.phone == phone;

  @override
  int get hashCode =>
    (phone == null ? 0 : phone.hashCode);

  @override
  String toString() => 'ForgotPasswordStep1Request[phone=$phone]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (phone != null) {
      json[r'phone'] = phone;
    }
    return json;
  }

  /// Returns a new [ForgotPasswordStep1Request] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ForgotPasswordStep1Request fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ForgotPasswordStep1Request(
        phone: json[r'phone'],
    );

  static List<ForgotPasswordStep1Request> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ForgotPasswordStep1Request>[]
      : json.map((v) => ForgotPasswordStep1Request.fromJson(v)).toList(growable: true == growable);

  static Map<String, ForgotPasswordStep1Request> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ForgotPasswordStep1Request>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ForgotPasswordStep1Request.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ForgotPasswordStep1Request-objects as value to a dart map
  static Map<String, List<ForgotPasswordStep1Request>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ForgotPasswordStep1Request>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ForgotPasswordStep1Request.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

