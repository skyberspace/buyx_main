//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ProductSearchRanked {
  /// Returns a new [ProductSearchRanked] instance.
  ProductSearchRanked({
    this.rank,
    this.product,
  });

  double rank;

  Product product;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProductSearchRanked &&
     other.rank == rank &&
     other.product == product;

  @override
  int get hashCode =>
    (rank == null ? 0 : rank.hashCode) +
    (product == null ? 0 : product.hashCode);

  @override
  String toString() => 'ProductSearchRanked[rank=$rank, product=$product]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (rank != null) {
      json[r'rank'] = rank;
    }
    if (product != null) {
      json[r'product'] = product;
    }
    return json;
  }

  /// Returns a new [ProductSearchRanked] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ProductSearchRanked fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ProductSearchRanked(
        rank: json[r'rank'],
        product: Product.fromJson(json[r'product']),
    );

  static List<ProductSearchRanked> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ProductSearchRanked>[]
      : json.map((v) => ProductSearchRanked.fromJson(v)).toList(growable: true == growable);

  static Map<String, ProductSearchRanked> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ProductSearchRanked>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ProductSearchRanked.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ProductSearchRanked-objects as value to a dart map
  static Map<String, List<ProductSearchRanked>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ProductSearchRanked>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ProductSearchRanked.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

