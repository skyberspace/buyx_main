//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class PaymentSheetData {
  /// Returns a new [PaymentSheetData] instance.
  PaymentSheetData({
    this.customerEphemeralKeySecret,
    this.paymentIntentClientSecret,
    this.customerId,
  });

  String customerEphemeralKeySecret;

  String paymentIntentClientSecret;

  String customerId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is PaymentSheetData &&
     other.customerEphemeralKeySecret == customerEphemeralKeySecret &&
     other.paymentIntentClientSecret == paymentIntentClientSecret &&
     other.customerId == customerId;

  @override
  int get hashCode =>
    (customerEphemeralKeySecret == null ? 0 : customerEphemeralKeySecret.hashCode) +
    (paymentIntentClientSecret == null ? 0 : paymentIntentClientSecret.hashCode) +
    (customerId == null ? 0 : customerId.hashCode);

  @override
  String toString() => 'PaymentSheetData[customerEphemeralKeySecret=$customerEphemeralKeySecret, paymentIntentClientSecret=$paymentIntentClientSecret, customerId=$customerId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (customerEphemeralKeySecret != null) {
      json[r'customerEphemeralKeySecret'] = customerEphemeralKeySecret;
    }
    if (paymentIntentClientSecret != null) {
      json[r'paymentIntentClientSecret'] = paymentIntentClientSecret;
    }
    if (customerId != null) {
      json[r'customerId'] = customerId;
    }
    return json;
  }

  /// Returns a new [PaymentSheetData] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static PaymentSheetData fromJson(Map<String, dynamic> json) => json == null
    ? null
    : PaymentSheetData(
        customerEphemeralKeySecret: json[r'customerEphemeralKeySecret'],
        paymentIntentClientSecret: json[r'paymentIntentClientSecret'],
        customerId: json[r'customerId'],
    );

  static List<PaymentSheetData> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <PaymentSheetData>[]
      : json.map((v) => PaymentSheetData.fromJson(v)).toList(growable: true == growable);

  static Map<String, PaymentSheetData> mapFromJson(Map<String, dynamic> json) {
    final map = <String, PaymentSheetData>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = PaymentSheetData.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of PaymentSheetData-objects as value to a dart map
  static Map<String, List<PaymentSheetData>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<PaymentSheetData>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = PaymentSheetData.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

