//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class Ordinates {
  /// Instantiate a new enum with the provided [value].
  const Ordinates._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number0 = Ordinates._(0);
  static const number1 = Ordinates._(1);
  static const number2 = Ordinates._(2);
  static const number3 = Ordinates._(3);
  static const number4 = Ordinates._(4);
  static const number7 = Ordinates._(7);
  static const number8 = Ordinates._(8);
  static const number16 = Ordinates._(16);
  static const number32 = Ordinates._(32);
  static const number64 = Ordinates._(64);
  static const number128 = Ordinates._(128);
  static const number256 = Ordinates._(256);
  static const number512 = Ordinates._(512);
  static const number1024 = Ordinates._(1024);
  static const number2048 = Ordinates._(2048);
  static const number4096 = Ordinates._(4096);
  static const number8192 = Ordinates._(8192);
  static const number16384 = Ordinates._(16384);
  static const number32768 = Ordinates._(32768);
  static const number65535 = Ordinates._(65535);
  static const number65536 = Ordinates._(65536);
  static const number65539 = Ordinates._(65539);
  static const number65543 = Ordinates._(65543);
  static const number131072 = Ordinates._(131072);
  static const number262144 = Ordinates._(262144);
  static const number524288 = Ordinates._(524288);
  static const number1048576 = Ordinates._(1048576);
  static const number2097152 = Ordinates._(2097152);
  static const number4194304 = Ordinates._(4194304);
  static const number8388608 = Ordinates._(8388608);
  static const number16777216 = Ordinates._(16777216);
  static const number33554432 = Ordinates._(33554432);
  static const number67108864 = Ordinates._(67108864);
  static const number134217728 = Ordinates._(134217728);
  static const number268435456 = Ordinates._(268435456);
  static const number536870912 = Ordinates._(536870912);
  static const number1073741824 = Ordinates._(1073741824);
  static const numberNegative2147483648 = Ordinates._(-2147483648);
  static const numberNegative65536 = Ordinates._(-65536);
  static const numberNegative1 = Ordinates._(-1);

  /// List of all possible values in this [enum][Ordinates].
  static const values = <Ordinates>[
    number0,
    number1,
    number2,
    number3,
    number4,
    number7,
    number8,
    number16,
    number32,
    number64,
    number128,
    number256,
    number512,
    number1024,
    number2048,
    number4096,
    number8192,
    number16384,
    number32768,
    number65535,
    number65536,
    number65539,
    number65543,
    number131072,
    number262144,
    number524288,
    number1048576,
    number2097152,
    number4194304,
    number8388608,
    number16777216,
    number33554432,
    number67108864,
    number134217728,
    number268435456,
    number536870912,
    number1073741824,
    numberNegative2147483648,
    numberNegative65536,
    numberNegative1,
  ];

  static Ordinates fromJson(dynamic value) =>
    OrdinatesTypeTransformer().decode(value);

  static List<Ordinates> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Ordinates>[]
      : json
          .map((value) => Ordinates.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [Ordinates] to int,
/// and [decode] dynamic data back to [Ordinates].
class OrdinatesTypeTransformer {
  const OrdinatesTypeTransformer._();

  factory OrdinatesTypeTransformer() => _instance ??= OrdinatesTypeTransformer._();

  int encode(Ordinates data) => data.value;

  /// Decodes a [dynamic value][data] to a Ordinates.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  Ordinates decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 0: return Ordinates.number0;
      case 1: return Ordinates.number1;
      case 2: return Ordinates.number2;
      case 3: return Ordinates.number3;
      case 4: return Ordinates.number4;
      case 7: return Ordinates.number7;
      case 8: return Ordinates.number8;
      case 16: return Ordinates.number16;
      case 32: return Ordinates.number32;
      case 64: return Ordinates.number64;
      case 128: return Ordinates.number128;
      case 256: return Ordinates.number256;
      case 512: return Ordinates.number512;
      case 1024: return Ordinates.number1024;
      case 2048: return Ordinates.number2048;
      case 4096: return Ordinates.number4096;
      case 8192: return Ordinates.number8192;
      case 16384: return Ordinates.number16384;
      case 32768: return Ordinates.number32768;
      case 65535: return Ordinates.number65535;
      case 65536: return Ordinates.number65536;
      case 65539: return Ordinates.number65539;
      case 65543: return Ordinates.number65543;
      case 131072: return Ordinates.number131072;
      case 262144: return Ordinates.number262144;
      case 524288: return Ordinates.number524288;
      case 1048576: return Ordinates.number1048576;
      case 2097152: return Ordinates.number2097152;
      case 4194304: return Ordinates.number4194304;
      case 8388608: return Ordinates.number8388608;
      case 16777216: return Ordinates.number16777216;
      case 33554432: return Ordinates.number33554432;
      case 67108864: return Ordinates.number67108864;
      case 134217728: return Ordinates.number134217728;
      case 268435456: return Ordinates.number268435456;
      case 536870912: return Ordinates.number536870912;
      case 1073741824: return Ordinates.number1073741824;
      case -2147483648: return Ordinates.numberNegative2147483648;
      case -65536: return Ordinates.numberNegative65536;
      case -1: return Ordinates.numberNegative1;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [OrdinatesTypeTransformer] instance.
  static OrdinatesTypeTransformer _instance;
}
