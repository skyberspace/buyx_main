//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class OrderLine {
  /// Returns a new [OrderLine] instance.
  OrderLine({
    this.id,
    this.productNameCache,
    this.productBarcodeCache,
    this.price,
    this.tax,
    this.returned,
    this.canceled,
    this.quantity,
    this.productId,
    this.orderId,
    this.productWarehouseStockId,
    this.order,
    this.product,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String productNameCache;

  String productBarcodeCache;

  double price;

  double tax;

  bool returned;

  bool canceled;

  double quantity;

  int productId;

  int orderId;

  int productWarehouseStockId;

  Order order;

  Product product;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is OrderLine &&
     other.id == id &&
     other.productNameCache == productNameCache &&
     other.productBarcodeCache == productBarcodeCache &&
     other.price == price &&
     other.tax == tax &&
     other.returned == returned &&
     other.canceled == canceled &&
     other.quantity == quantity &&
     other.productId == productId &&
     other.orderId == orderId &&
     other.productWarehouseStockId == productWarehouseStockId &&
     other.order == order &&
     other.product == product &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (productNameCache == null ? 0 : productNameCache.hashCode) +
    (productBarcodeCache == null ? 0 : productBarcodeCache.hashCode) +
    (price == null ? 0 : price.hashCode) +
    (tax == null ? 0 : tax.hashCode) +
    (returned == null ? 0 : returned.hashCode) +
    (canceled == null ? 0 : canceled.hashCode) +
    (quantity == null ? 0 : quantity.hashCode) +
    (productId == null ? 0 : productId.hashCode) +
    (orderId == null ? 0 : orderId.hashCode) +
    (productWarehouseStockId == null ? 0 : productWarehouseStockId.hashCode) +
    (order == null ? 0 : order.hashCode) +
    (product == null ? 0 : product.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'OrderLine[id=$id, productNameCache=$productNameCache, productBarcodeCache=$productBarcodeCache, price=$price, tax=$tax, returned=$returned, canceled=$canceled, quantity=$quantity, productId=$productId, orderId=$orderId, productWarehouseStockId=$productWarehouseStockId, order=$order, product=$product, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (productNameCache != null) {
      json[r'productNameCache'] = productNameCache;
    }
    if (productBarcodeCache != null) {
      json[r'productBarcodeCache'] = productBarcodeCache;
    }
    if (price != null) {
      json[r'price'] = price;
    }
    if (tax != null) {
      json[r'tax'] = tax;
    }
    if (returned != null) {
      json[r'returned'] = returned;
    }
    if (canceled != null) {
      json[r'canceled'] = canceled;
    }
    if (quantity != null) {
      json[r'quantity'] = quantity;
    }
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (orderId != null) {
      json[r'orderId'] = orderId;
    }
    if (productWarehouseStockId != null) {
      json[r'productWarehouseStockId'] = productWarehouseStockId;
    }
    if (order != null) {
      json[r'order'] = order;
    }
    if (product != null) {
      json[r'product'] = product;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [OrderLine] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static OrderLine fromJson(Map<String, dynamic> json) => json == null
    ? null
    : OrderLine(
        id: json[r'id'],
        productNameCache: json[r'productNameCache'],
        productBarcodeCache: json[r'productBarcodeCache'],
        price: json[r'price'],
        tax: json[r'tax'],
        returned: json[r'returned'],
        canceled: json[r'canceled'],
        quantity: json[r'quantity'],
        productId: json[r'productId'],
        orderId: json[r'orderId'],
        productWarehouseStockId: json[r'productWarehouseStockId'],
        order: Order.fromJson(json[r'order']),
        product: Product.fromJson(json[r'product']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<OrderLine> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <OrderLine>[]
      : json.map((v) => OrderLine.fromJson(v)).toList(growable: true == growable);

  static Map<String, OrderLine> mapFromJson(Map<String, dynamic> json) {
    final map = <String, OrderLine>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = OrderLine.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of OrderLine-objects as value to a dart map
  static Map<String, List<OrderLine>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<OrderLine>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = OrderLine.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

