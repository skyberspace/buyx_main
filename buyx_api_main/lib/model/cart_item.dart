//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CartItem {
  /// Returns a new [CartItem] instance.
  CartItem({
    this.id,
    this.quantitiy,
    this.userId,
    this.productWarehouseStockId,
    this.warehouseId,
    this.user,
    this.productWarehouseStock,
    this.warehouse,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  double quantitiy;

  int userId;

  int productWarehouseStockId;

  int warehouseId;

  User user;

  ProductWarehouseStock productWarehouseStock;

  Warehouse warehouse;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CartItem &&
     other.id == id &&
     other.quantitiy == quantitiy &&
     other.userId == userId &&
     other.productWarehouseStockId == productWarehouseStockId &&
     other.warehouseId == warehouseId &&
     other.user == user &&
     other.productWarehouseStock == productWarehouseStock &&
     other.warehouse == warehouse &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (quantitiy == null ? 0 : quantitiy.hashCode) +
    (userId == null ? 0 : userId.hashCode) +
    (productWarehouseStockId == null ? 0 : productWarehouseStockId.hashCode) +
    (warehouseId == null ? 0 : warehouseId.hashCode) +
    (user == null ? 0 : user.hashCode) +
    (productWarehouseStock == null ? 0 : productWarehouseStock.hashCode) +
    (warehouse == null ? 0 : warehouse.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'CartItem[id=$id, quantitiy=$quantitiy, userId=$userId, productWarehouseStockId=$productWarehouseStockId, warehouseId=$warehouseId, user=$user, productWarehouseStock=$productWarehouseStock, warehouse=$warehouse, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (quantitiy != null) {
      json[r'quantitiy'] = quantitiy;
    }
    if (userId != null) {
      json[r'userId'] = userId;
    }
    if (productWarehouseStockId != null) {
      json[r'productWarehouseStockId'] = productWarehouseStockId;
    }
    if (warehouseId != null) {
      json[r'warehouseId'] = warehouseId;
    }
    if (user != null) {
      json[r'user'] = user;
    }
    if (productWarehouseStock != null) {
      json[r'productWarehouseStock'] = productWarehouseStock;
    }
    if (warehouse != null) {
      json[r'warehouse'] = warehouse;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [CartItem] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CartItem fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CartItem(
        id: json[r'id'],
        quantitiy: json[r'quantitiy'],
        userId: json[r'userId'],
        productWarehouseStockId: json[r'productWarehouseStockId'],
        warehouseId: json[r'warehouseId'],
        user: User.fromJson(json[r'user']),
        productWarehouseStock: ProductWarehouseStock.fromJson(json[r'productWarehouseStock']),
        warehouse: Warehouse.fromJson(json[r'warehouse']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<CartItem> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CartItem>[]
      : json.map((v) => CartItem.fromJson(v)).toList(growable: true == growable);

  static Map<String, CartItem> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CartItem>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CartItem.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CartItem-objects as value to a dart map
  static Map<String, List<CartItem>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CartItem>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CartItem.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

