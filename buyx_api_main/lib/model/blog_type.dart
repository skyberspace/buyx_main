//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     1 = Campaign    2 = Announcement
class BlogType {
  /// Instantiate a new enum with the provided [value].
  const BlogType._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number1 = BlogType._(1);
  static const number2 = BlogType._(2);

  /// List of all possible values in this [enum][BlogType].
  static const values = <BlogType>[
    number1,
    number2,
  ];

  static BlogType fromJson(dynamic value) =>
    BlogTypeTypeTransformer().decode(value);

  static List<BlogType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <BlogType>[]
      : json
          .map((value) => BlogType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [BlogType] to int,
/// and [decode] dynamic data back to [BlogType].
class BlogTypeTypeTransformer {
  const BlogTypeTypeTransformer._();

  factory BlogTypeTypeTransformer() => _instance ??= BlogTypeTypeTransformer._();

  int encode(BlogType data) => data.value;

  /// Decodes a [dynamic value][data] to a BlogType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  BlogType decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 1: return BlogType.number1;
      case 2: return BlogType.number2;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [BlogTypeTypeTransformer] instance.
  static BlogTypeTypeTransformer _instance;
}
