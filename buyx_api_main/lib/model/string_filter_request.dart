//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class StringFilterRequest {
  /// Returns a new [StringFilterRequest] instance.
  StringFilterRequest({
    this.filter,
  });

  String filter;

  @override
  bool operator ==(Object other) => identical(this, other) || other is StringFilterRequest &&
     other.filter == filter;

  @override
  int get hashCode =>
    (filter == null ? 0 : filter.hashCode);

  @override
  String toString() => 'StringFilterRequest[filter=$filter]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (filter != null) {
      json[r'filter'] = filter;
    }
    return json;
  }

  /// Returns a new [StringFilterRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static StringFilterRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : StringFilterRequest(
        filter: json[r'filter'],
    );

  static List<StringFilterRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <StringFilterRequest>[]
      : json.map((v) => StringFilterRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, StringFilterRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, StringFilterRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = StringFilterRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of StringFilterRequest-objects as value to a dart map
  static Map<String, List<StringFilterRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<StringFilterRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = StringFilterRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

