//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class PaymentLog {
  /// Returns a new [PaymentLog] instance.
  PaymentLog({
    this.id,
    this.orderId,
    this.order,
    this.event,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  int orderId;

  Order order;

  String event;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is PaymentLog &&
     other.id == id &&
     other.orderId == orderId &&
     other.order == order &&
     other.event == event &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (orderId == null ? 0 : orderId.hashCode) +
    (order == null ? 0 : order.hashCode) +
    (event == null ? 0 : event.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'PaymentLog[id=$id, orderId=$orderId, order=$order, event=$event, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (orderId != null) {
      json[r'orderId'] = orderId;
    }
    if (order != null) {
      json[r'order'] = order;
    }
    if (event != null) {
      json[r'event'] = event;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [PaymentLog] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static PaymentLog fromJson(Map<String, dynamic> json) => json == null
    ? null
    : PaymentLog(
        id: json[r'id'],
        orderId: json[r'orderId'],
        order: Order.fromJson(json[r'order']),
        event: json[r'event'],
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<PaymentLog> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <PaymentLog>[]
      : json.map((v) => PaymentLog.fromJson(v)).toList(growable: true == growable);

  static Map<String, PaymentLog> mapFromJson(Map<String, dynamic> json) {
    final map = <String, PaymentLog>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = PaymentLog.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of PaymentLog-objects as value to a dart map
  static Map<String, List<PaymentLog>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<PaymentLog>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = PaymentLog.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

