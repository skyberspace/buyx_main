//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ProductImage {
  /// Returns a new [ProductImage] instance.
  ProductImage({
    this.id,
    this.path,
    this.name,
    this.isPrimary,
    this.productId,
    this.product,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String path;

  String name;

  bool isPrimary;

  int productId;

  Product product;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProductImage &&
     other.id == id &&
     other.path == path &&
     other.name == name &&
     other.isPrimary == isPrimary &&
     other.productId == productId &&
     other.product == product &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (path == null ? 0 : path.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (isPrimary == null ? 0 : isPrimary.hashCode) +
    (productId == null ? 0 : productId.hashCode) +
    (product == null ? 0 : product.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'ProductImage[id=$id, path=$path, name=$name, isPrimary=$isPrimary, productId=$productId, product=$product, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (path != null) {
      json[r'path'] = path;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (isPrimary != null) {
      json[r'isPrimary'] = isPrimary;
    }
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (product != null) {
      json[r'product'] = product;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [ProductImage] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ProductImage fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ProductImage(
        id: json[r'id'],
        path: json[r'path'],
        name: json[r'name'],
        isPrimary: json[r'isPrimary'],
        productId: json[r'productId'],
        product: Product.fromJson(json[r'product']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<ProductImage> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ProductImage>[]
      : json.map((v) => ProductImage.fromJson(v)).toList(growable: true == growable);

  static Map<String, ProductImage> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ProductImage>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ProductImage.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ProductImage-objects as value to a dart map
  static Map<String, List<ProductImage>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ProductImage>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ProductImage.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

