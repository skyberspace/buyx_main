//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class RemoveFromCartRequest {
  /// Returns a new [RemoveFromCartRequest] instance.
  RemoveFromCartRequest({
    this.cartItemId,
  });

  int cartItemId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is RemoveFromCartRequest &&
     other.cartItemId == cartItemId;

  @override
  int get hashCode =>
    (cartItemId == null ? 0 : cartItemId.hashCode);

  @override
  String toString() => 'RemoveFromCartRequest[cartItemId=$cartItemId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (cartItemId != null) {
      json[r'cartItemId'] = cartItemId;
    }
    return json;
  }

  /// Returns a new [RemoveFromCartRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static RemoveFromCartRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : RemoveFromCartRequest(
        cartItemId: json[r'cartItemId'],
    );

  static List<RemoveFromCartRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <RemoveFromCartRequest>[]
      : json.map((v) => RemoveFromCartRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, RemoveFromCartRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, RemoveFromCartRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = RemoveFromCartRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of RemoveFromCartRequest-objects as value to a dart map
  static Map<String, List<RemoveFromCartRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<RemoveFromCartRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = RemoveFromCartRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

