//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Coordinate {
  /// Returns a new [Coordinate] instance.
  Coordinate({
    this.x,
    this.y,
    this.z,
    this.m,
    this.coordinateValue,
  });

  double x;

  double y;

  double z;

  double m;

  Coordinate coordinateValue;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Coordinate &&
     other.x == x &&
     other.y == y &&
     other.z == z &&
     other.m == m &&
     other.coordinateValue == coordinateValue;

  @override
  int get hashCode =>
    (x == null ? 0 : x.hashCode) +
    (y == null ? 0 : y.hashCode) +
    (z == null ? 0 : z.hashCode) +
    (m == null ? 0 : m.hashCode) +
    (coordinateValue == null ? 0 : coordinateValue.hashCode);

  @override
  String toString() => 'Coordinate[x=$x, y=$y, z=$z, m=$m, coordinateValue=$coordinateValue]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (x != null) {
      json[r'x'] = x;
    }
    if (y != null) {
      json[r'y'] = y;
    }
    if (z != null) {
      json[r'z'] = z;
    }
    if (m != null) {
      json[r'm'] = m;
    }
    if (coordinateValue != null) {
      json[r'coordinateValue'] = coordinateValue;
    }
    return json;
  }

  /// Returns a new [Coordinate] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Coordinate fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Coordinate(
        x: json[r'x'],
        y: json[r'y'],
        z: json[r'z'],
        m: json[r'm'],
        coordinateValue: Coordinate.fromJson(json[r'coordinateValue']),
    );

  static List<Coordinate> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Coordinate>[]
      : json.map((v) => Coordinate.fromJson(v)).toList(growable: true == growable);

  static Map<String, Coordinate> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Coordinate>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Coordinate.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Coordinate-objects as value to a dart map
  static Map<String, List<Coordinate>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Coordinate>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Coordinate.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

