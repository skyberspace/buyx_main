//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     1 = Point    2 = LineString    3 = Polygon    4 = MultiPoint    5 = MultiLineString    6 = MultiPolygon    7 = GeometryCollection    8 = CircularString    9 = CompoundCurve    10 = CurvePolygon    11 = MultiCurve    12 = MultiSurface    13 = Curve    14 = Surface    15 = PolyhedralSurface    16 = TIN
class OgcGeometryType {
  /// Instantiate a new enum with the provided [value].
  const OgcGeometryType._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number1 = OgcGeometryType._(1);
  static const number2 = OgcGeometryType._(2);
  static const number3 = OgcGeometryType._(3);
  static const number4 = OgcGeometryType._(4);
  static const number5 = OgcGeometryType._(5);
  static const number6 = OgcGeometryType._(6);
  static const number7 = OgcGeometryType._(7);
  static const number8 = OgcGeometryType._(8);
  static const number9 = OgcGeometryType._(9);
  static const number10 = OgcGeometryType._(10);
  static const number11 = OgcGeometryType._(11);
  static const number12 = OgcGeometryType._(12);
  static const number13 = OgcGeometryType._(13);
  static const number14 = OgcGeometryType._(14);
  static const number15 = OgcGeometryType._(15);
  static const number16 = OgcGeometryType._(16);

  /// List of all possible values in this [enum][OgcGeometryType].
  static const values = <OgcGeometryType>[
    number1,
    number2,
    number3,
    number4,
    number5,
    number6,
    number7,
    number8,
    number9,
    number10,
    number11,
    number12,
    number13,
    number14,
    number15,
    number16,
  ];

  static OgcGeometryType fromJson(dynamic value) =>
    OgcGeometryTypeTypeTransformer().decode(value);

  static List<OgcGeometryType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <OgcGeometryType>[]
      : json
          .map((value) => OgcGeometryType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [OgcGeometryType] to int,
/// and [decode] dynamic data back to [OgcGeometryType].
class OgcGeometryTypeTypeTransformer {
  const OgcGeometryTypeTypeTransformer._();

  factory OgcGeometryTypeTypeTransformer() => _instance ??= OgcGeometryTypeTypeTransformer._();

  int encode(OgcGeometryType data) => data.value;

  /// Decodes a [dynamic value][data] to a OgcGeometryType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  OgcGeometryType decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 1: return OgcGeometryType.number1;
      case 2: return OgcGeometryType.number2;
      case 3: return OgcGeometryType.number3;
      case 4: return OgcGeometryType.number4;
      case 5: return OgcGeometryType.number5;
      case 6: return OgcGeometryType.number6;
      case 7: return OgcGeometryType.number7;
      case 8: return OgcGeometryType.number8;
      case 9: return OgcGeometryType.number9;
      case 10: return OgcGeometryType.number10;
      case 11: return OgcGeometryType.number11;
      case 12: return OgcGeometryType.number12;
      case 13: return OgcGeometryType.number13;
      case 14: return OgcGeometryType.number14;
      case 15: return OgcGeometryType.number15;
      case 16: return OgcGeometryType.number16;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [OgcGeometryTypeTypeTransformer] instance.
  static OgcGeometryTypeTypeTransformer _instance;
}
