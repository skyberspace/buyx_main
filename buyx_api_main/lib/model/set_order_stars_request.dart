//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SetOrderStarsRequest {
  /// Returns a new [SetOrderStarsRequest] instance.
  SetOrderStarsRequest({
    this.orderId,
    this.stars,
  });

  int orderId;

  double stars;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SetOrderStarsRequest &&
     other.orderId == orderId &&
     other.stars == stars;

  @override
  int get hashCode =>
    (orderId == null ? 0 : orderId.hashCode) +
    (stars == null ? 0 : stars.hashCode);

  @override
  String toString() => 'SetOrderStarsRequest[orderId=$orderId, stars=$stars]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (orderId != null) {
      json[r'orderId'] = orderId;
    }
    if (stars != null) {
      json[r'stars'] = stars;
    }
    return json;
  }

  /// Returns a new [SetOrderStarsRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SetOrderStarsRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : SetOrderStarsRequest(
        orderId: json[r'orderId'],
        stars: json[r'stars'],
    );

  static List<SetOrderStarsRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <SetOrderStarsRequest>[]
      : json.map((v) => SetOrderStarsRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, SetOrderStarsRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SetOrderStarsRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = SetOrderStarsRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of SetOrderStarsRequest-objects as value to a dart map
  static Map<String, List<SetOrderStarsRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<SetOrderStarsRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = SetOrderStarsRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

