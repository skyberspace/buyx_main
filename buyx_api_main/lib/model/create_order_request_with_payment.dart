//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CreateOrderRequestWithPayment {
  /// Returns a new [CreateOrderRequestWithPayment] instance.
  CreateOrderRequestWithPayment({
    this.campaignId,
    this.note,
    this.paymentType,
  });

  int campaignId;

  String note;

  PaymentType paymentType;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CreateOrderRequestWithPayment &&
     other.campaignId == campaignId &&
     other.note == note &&
     other.paymentType == paymentType;

  @override
  int get hashCode =>
    (campaignId == null ? 0 : campaignId.hashCode) +
    (note == null ? 0 : note.hashCode) +
    (paymentType == null ? 0 : paymentType.hashCode);

  @override
  String toString() => 'CreateOrderRequestWithPayment[campaignId=$campaignId, note=$note, paymentType=$paymentType]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (campaignId != null) {
      json[r'campaignId'] = campaignId;
    }
    if (note != null) {
      json[r'note'] = note;
    }
    if (paymentType != null) {
      json[r'paymentType'] = paymentType;
    }
    return json;
  }

  /// Returns a new [CreateOrderRequestWithPayment] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CreateOrderRequestWithPayment fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CreateOrderRequestWithPayment(
        campaignId: json[r'campaignId'],
        note: json[r'note'],
        paymentType: PaymentType.fromJson(json[r'paymentType']),
    );

  static List<CreateOrderRequestWithPayment> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CreateOrderRequestWithPayment>[]
      : json.map((v) => CreateOrderRequestWithPayment.fromJson(v)).toList(growable: true == growable);

  static Map<String, CreateOrderRequestWithPayment> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CreateOrderRequestWithPayment>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CreateOrderRequestWithPayment.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CreateOrderRequestWithPayment-objects as value to a dart map
  static Map<String, List<CreateOrderRequestWithPayment>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CreateOrderRequestWithPayment>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CreateOrderRequestWithPayment.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

