//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ProductCategory {
  /// Returns a new [ProductCategory] instance.
  ProductCategory({
    this.id,
    this.order,
    this.productId,
    this.categoryId,
    this.product,
    this.category,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  int order;

  int productId;

  int categoryId;

  Product product;

  Category category;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProductCategory &&
     other.id == id &&
     other.order == order &&
     other.productId == productId &&
     other.categoryId == categoryId &&
     other.product == product &&
     other.category == category &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (order == null ? 0 : order.hashCode) +
    (productId == null ? 0 : productId.hashCode) +
    (categoryId == null ? 0 : categoryId.hashCode) +
    (product == null ? 0 : product.hashCode) +
    (category == null ? 0 : category.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'ProductCategory[id=$id, order=$order, productId=$productId, categoryId=$categoryId, product=$product, category=$category, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (order != null) {
      json[r'order'] = order;
    }
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (categoryId != null) {
      json[r'categoryId'] = categoryId;
    }
    if (product != null) {
      json[r'product'] = product;
    }
    if (category != null) {
      json[r'category'] = category;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [ProductCategory] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ProductCategory fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ProductCategory(
        id: json[r'id'],
        order: json[r'order'],
        productId: json[r'productId'],
        categoryId: json[r'categoryId'],
        product: Product.fromJson(json[r'product']),
        category: Category.fromJson(json[r'category']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<ProductCategory> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ProductCategory>[]
      : json.map((v) => ProductCategory.fromJson(v)).toList(growable: true == growable);

  static Map<String, ProductCategory> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ProductCategory>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ProductCategory.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ProductCategory-objects as value to a dart map
  static Map<String, List<ProductCategory>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ProductCategory>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ProductCategory.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

