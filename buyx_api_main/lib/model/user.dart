//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class User {
  /// Returns a new [User] instance.
  User({
    this.setPassword,
    this.id,
    this.email,
    this.name,
    this.surname,
    this.phone,
    this.phoneVerified,
    this.marketingEmails,
    this.marketingNotifications,
    this.marketingSMS,
    this.marketingCalls,
    this.isGuest,
    this.canPlaceNonProvisionedPaymentMethods,
    this.stripeUserId,
    this.adminUser,
    this.warehouseUser,
    this.addresses,
    this.favorites,
    this.fcmToken,
    this.tenantRoleGroup,
    this.isAdmin,
    this.isWarehouseUser,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  String setPassword;

  int id;

  String email;

  String name;

  String surname;

  String phone;

  bool phoneVerified;

  bool marketingEmails;

  bool marketingNotifications;

  bool marketingSMS;

  bool marketingCalls;

  bool isGuest;

  bool canPlaceNonProvisionedPaymentMethods;

  String stripeUserId;

  AdminUser adminUser;

  WarehouseUser warehouseUser;

  List<Address> addresses;

  List<UserFavorite> favorites;

  String fcmToken;

  /// used for multitenant module. If set, this automatically manages what this user can see and do  represents the tenant group Id for the user
  int tenantRoleGroup;

  bool isAdmin;

  bool isWarehouseUser;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is User &&
     other.setPassword == setPassword &&
     other.id == id &&
     other.email == email &&
     other.name == name &&
     other.surname == surname &&
     other.phone == phone &&
     other.phoneVerified == phoneVerified &&
     other.marketingEmails == marketingEmails &&
     other.marketingNotifications == marketingNotifications &&
     other.marketingSMS == marketingSMS &&
     other.marketingCalls == marketingCalls &&
     other.isGuest == isGuest &&
     other.canPlaceNonProvisionedPaymentMethods == canPlaceNonProvisionedPaymentMethods &&
     other.stripeUserId == stripeUserId &&
     other.adminUser == adminUser &&
     other.warehouseUser == warehouseUser &&
     other.addresses == addresses &&
     other.favorites == favorites &&
     other.fcmToken == fcmToken &&
     other.tenantRoleGroup == tenantRoleGroup &&
     other.isAdmin == isAdmin &&
     other.isWarehouseUser == isWarehouseUser &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (setPassword == null ? 0 : setPassword.hashCode) +
    (id == null ? 0 : id.hashCode) +
    (email == null ? 0 : email.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (surname == null ? 0 : surname.hashCode) +
    (phone == null ? 0 : phone.hashCode) +
    (phoneVerified == null ? 0 : phoneVerified.hashCode) +
    (marketingEmails == null ? 0 : marketingEmails.hashCode) +
    (marketingNotifications == null ? 0 : marketingNotifications.hashCode) +
    (marketingSMS == null ? 0 : marketingSMS.hashCode) +
    (marketingCalls == null ? 0 : marketingCalls.hashCode) +
    (isGuest == null ? 0 : isGuest.hashCode) +
    (canPlaceNonProvisionedPaymentMethods == null ? 0 : canPlaceNonProvisionedPaymentMethods.hashCode) +
    (stripeUserId == null ? 0 : stripeUserId.hashCode) +
    (adminUser == null ? 0 : adminUser.hashCode) +
    (warehouseUser == null ? 0 : warehouseUser.hashCode) +
    (addresses == null ? 0 : addresses.hashCode) +
    (favorites == null ? 0 : favorites.hashCode) +
    (fcmToken == null ? 0 : fcmToken.hashCode) +
    (tenantRoleGroup == null ? 0 : tenantRoleGroup.hashCode) +
    (isAdmin == null ? 0 : isAdmin.hashCode) +
    (isWarehouseUser == null ? 0 : isWarehouseUser.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'User[setPassword=$setPassword, id=$id, email=$email, name=$name, surname=$surname, phone=$phone, phoneVerified=$phoneVerified, marketingEmails=$marketingEmails, marketingNotifications=$marketingNotifications, marketingSMS=$marketingSMS, marketingCalls=$marketingCalls, isGuest=$isGuest, canPlaceNonProvisionedPaymentMethods=$canPlaceNonProvisionedPaymentMethods, stripeUserId=$stripeUserId, adminUser=$adminUser, warehouseUser=$warehouseUser, addresses=$addresses, favorites=$favorites, fcmToken=$fcmToken, tenantRoleGroup=$tenantRoleGroup, isAdmin=$isAdmin, isWarehouseUser=$isWarehouseUser, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (setPassword != null) {
      json[r'set_password'] = setPassword;
    }
    if (id != null) {
      json[r'id'] = id;
    }
    if (email != null) {
      json[r'email'] = email;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (surname != null) {
      json[r'surname'] = surname;
    }
    if (phone != null) {
      json[r'phone'] = phone;
    }
    if (phoneVerified != null) {
      json[r'phoneVerified'] = phoneVerified;
    }
    if (marketingEmails != null) {
      json[r'marketingEmails'] = marketingEmails;
    }
    if (marketingNotifications != null) {
      json[r'marketingNotifications'] = marketingNotifications;
    }
    if (marketingSMS != null) {
      json[r'marketingSMS'] = marketingSMS;
    }
    if (marketingCalls != null) {
      json[r'marketingCalls'] = marketingCalls;
    }
    if (isGuest != null) {
      json[r'isGuest'] = isGuest;
    }
    if (canPlaceNonProvisionedPaymentMethods != null) {
      json[r'canPlaceNonProvisionedPaymentMethods'] = canPlaceNonProvisionedPaymentMethods;
    }
    if (stripeUserId != null) {
      json[r'stripeUserId'] = stripeUserId;
    }
    if (adminUser != null) {
      json[r'adminUser'] = adminUser;
    }
    if (warehouseUser != null) {
      json[r'warehouseUser'] = warehouseUser;
    }
    if (addresses != null) {
      json[r'addresses'] = addresses;
    }
    if (favorites != null) {
      json[r'favorites'] = favorites;
    }
    if (fcmToken != null) {
      json[r'fcmToken'] = fcmToken;
    }
    if (tenantRoleGroup != null) {
      json[r'tenantRoleGroup'] = tenantRoleGroup;
    }
    if (isAdmin != null) {
      json[r'isAdmin'] = isAdmin;
    }
    if (isWarehouseUser != null) {
      json[r'isWarehouseUser'] = isWarehouseUser;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [User] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static User fromJson(Map<String, dynamic> json) => json == null
    ? null
    : User(
        setPassword: json[r'set_password'],
        id: json[r'id'],
        email: json[r'email'],
        name: json[r'name'],
        surname: json[r'surname'],
        phone: json[r'phone'],
        phoneVerified: json[r'phoneVerified'],
        marketingEmails: json[r'marketingEmails'],
        marketingNotifications: json[r'marketingNotifications'],
        marketingSMS: json[r'marketingSMS'],
        marketingCalls: json[r'marketingCalls'],
        isGuest: json[r'isGuest'],
        canPlaceNonProvisionedPaymentMethods: json[r'canPlaceNonProvisionedPaymentMethods'],
        stripeUserId: json[r'stripeUserId'],
        adminUser: AdminUser.fromJson(json[r'adminUser']),
        warehouseUser: WarehouseUser.fromJson(json[r'warehouseUser']),
        addresses: Address.listFromJson(json[r'addresses']),
        favorites: UserFavorite.listFromJson(json[r'favorites']),
        fcmToken: json[r'fcmToken'],
        tenantRoleGroup: json[r'tenantRoleGroup'],
        isAdmin: json[r'isAdmin'],
        isWarehouseUser: json[r'isWarehouseUser'],
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<User> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <User>[]
      : json.map((v) => User.fromJson(v)).toList(growable: true == growable);

  static Map<String, User> mapFromJson(Map<String, dynamic> json) {
    final map = <String, User>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = User.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of User-objects as value to a dart map
  static Map<String, List<User>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<User>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = User.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

