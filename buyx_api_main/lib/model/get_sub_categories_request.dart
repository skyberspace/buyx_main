//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GetSubCategoriesRequest {
  /// Returns a new [GetSubCategoriesRequest] instance.
  GetSubCategoriesRequest({
    this.parentCategoryId,
  });

  int parentCategoryId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GetSubCategoriesRequest &&
     other.parentCategoryId == parentCategoryId;

  @override
  int get hashCode =>
    (parentCategoryId == null ? 0 : parentCategoryId.hashCode);

  @override
  String toString() => 'GetSubCategoriesRequest[parentCategoryId=$parentCategoryId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (parentCategoryId != null) {
      json[r'parentCategoryId'] = parentCategoryId;
    }
    return json;
  }

  /// Returns a new [GetSubCategoriesRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static GetSubCategoriesRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : GetSubCategoriesRequest(
        parentCategoryId: json[r'parentCategoryId'],
    );

  static List<GetSubCategoriesRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <GetSubCategoriesRequest>[]
      : json.map((v) => GetSubCategoriesRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, GetSubCategoriesRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, GetSubCategoriesRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = GetSubCategoriesRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of GetSubCategoriesRequest-objects as value to a dart map
  static Map<String, List<GetSubCategoriesRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<GetSubCategoriesRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = GetSubCategoriesRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

