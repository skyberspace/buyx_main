//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Order {
  /// Returns a new [Order] instance.
  Order({
    this.id,
    this.total,
    this.subTotal,
    this.deliveryFee,
    this.tax,
    this.orderState,
    this.returned,
    this.note,
    this.stripeIntentId,
    this.orderType,
    this.ratedStars,
    this.rated,
    this.paymentType,
    this.deliveryLastLatitude,
    this.deliveryLastLongitude,
    this.userId,
    this.warehouseId,
    this.orderAddressId,
    this.pickupAddressId,
    this.packagingAgentId,
    this.deliveryAgentId,
    this.campaignId,
    this.deliveryAgentImageUrl,
    this.deliveryAgentName,
    this.packagableAt,
    this.assignedToPackagingAt,
    this.packagedAt,
    this.deliverableAt,
    this.assignedToDeliveryAt,
    this.deliveredAt,
    this.packagingAgent,
    this.deliveryAgent,
    this.orderAddress,
    this.pickupAddress,
    this.user,
    this.warehouse,
    this.lines,
    this.orderStateHistory,
    this.paymentLogs,
    this.campaign,
    this.deletedAt,
    this.isDeleted = false,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  /// Grand Total of the order
  double total;

  /// total of order before delivery fee and tax
  double subTotal;

  double deliveryFee;

  /// total tax calculated when order is placed
  double tax;

  OrderState orderState;

  bool returned;

  String note;

  String stripeIntentId;

  OrderType orderType;

  double ratedStars;

  bool rated;

  PaymentType paymentType;

  double deliveryLastLatitude;

  double deliveryLastLongitude;

  int userId;

  int warehouseId;

  int orderAddressId;

  int pickupAddressId;

  int packagingAgentId;

  int deliveryAgentId;

  int campaignId;

  String deliveryAgentImageUrl;

  String deliveryAgentName;

  DateTime packagableAt;

  DateTime assignedToPackagingAt;

  DateTime packagedAt;

  DateTime deliverableAt;

  DateTime assignedToDeliveryAt;

  DateTime deliveredAt;

  WarehouseUser packagingAgent;

  WarehouseUser deliveryAgent;

  OrderAddress orderAddress;

  OrderAddress pickupAddress;

  User user;

  Warehouse warehouse;

  /// order details
  List<OrderLine> lines;

  /// history of order state changes and timestamps and issuer user ids
  List<OrderStateHistory> orderStateHistory;

  List<PaymentLog> paymentLogs;

  CartCampaign campaign;

  DateTime deletedAt;

  bool isDeleted;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Order &&
     other.id == id &&
     other.total == total &&
     other.subTotal == subTotal &&
     other.deliveryFee == deliveryFee &&
     other.tax == tax &&
     other.orderState == orderState &&
     other.returned == returned &&
     other.note == note &&
     other.stripeIntentId == stripeIntentId &&
     other.orderType == orderType &&
     other.ratedStars == ratedStars &&
     other.rated == rated &&
     other.paymentType == paymentType &&
     other.deliveryLastLatitude == deliveryLastLatitude &&
     other.deliveryLastLongitude == deliveryLastLongitude &&
     other.userId == userId &&
     other.warehouseId == warehouseId &&
     other.orderAddressId == orderAddressId &&
     other.pickupAddressId == pickupAddressId &&
     other.packagingAgentId == packagingAgentId &&
     other.deliveryAgentId == deliveryAgentId &&
     other.campaignId == campaignId &&
     other.deliveryAgentImageUrl == deliveryAgentImageUrl &&
     other.deliveryAgentName == deliveryAgentName &&
     other.packagableAt == packagableAt &&
     other.assignedToPackagingAt == assignedToPackagingAt &&
     other.packagedAt == packagedAt &&
     other.deliverableAt == deliverableAt &&
     other.assignedToDeliveryAt == assignedToDeliveryAt &&
     other.deliveredAt == deliveredAt &&
     other.packagingAgent == packagingAgent &&
     other.deliveryAgent == deliveryAgent &&
     other.orderAddress == orderAddress &&
     other.pickupAddress == pickupAddress &&
     other.user == user &&
     other.warehouse == warehouse &&
     other.lines == lines &&
     other.orderStateHistory == orderStateHistory &&
     other.paymentLogs == paymentLogs &&
     other.campaign == campaign &&
     other.deletedAt == deletedAt &&
     other.isDeleted == isDeleted &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (total == null ? 0 : total.hashCode) +
    (subTotal == null ? 0 : subTotal.hashCode) +
    (deliveryFee == null ? 0 : deliveryFee.hashCode) +
    (tax == null ? 0 : tax.hashCode) +
    (orderState == null ? 0 : orderState.hashCode) +
    (returned == null ? 0 : returned.hashCode) +
    (note == null ? 0 : note.hashCode) +
    (stripeIntentId == null ? 0 : stripeIntentId.hashCode) +
    (orderType == null ? 0 : orderType.hashCode) +
    (ratedStars == null ? 0 : ratedStars.hashCode) +
    (rated == null ? 0 : rated.hashCode) +
    (paymentType == null ? 0 : paymentType.hashCode) +
    (deliveryLastLatitude == null ? 0 : deliveryLastLatitude.hashCode) +
    (deliveryLastLongitude == null ? 0 : deliveryLastLongitude.hashCode) +
    (userId == null ? 0 : userId.hashCode) +
    (warehouseId == null ? 0 : warehouseId.hashCode) +
    (orderAddressId == null ? 0 : orderAddressId.hashCode) +
    (pickupAddressId == null ? 0 : pickupAddressId.hashCode) +
    (packagingAgentId == null ? 0 : packagingAgentId.hashCode) +
    (deliveryAgentId == null ? 0 : deliveryAgentId.hashCode) +
    (campaignId == null ? 0 : campaignId.hashCode) +
    (deliveryAgentImageUrl == null ? 0 : deliveryAgentImageUrl.hashCode) +
    (deliveryAgentName == null ? 0 : deliveryAgentName.hashCode) +
    (packagableAt == null ? 0 : packagableAt.hashCode) +
    (assignedToPackagingAt == null ? 0 : assignedToPackagingAt.hashCode) +
    (packagedAt == null ? 0 : packagedAt.hashCode) +
    (deliverableAt == null ? 0 : deliverableAt.hashCode) +
    (assignedToDeliveryAt == null ? 0 : assignedToDeliveryAt.hashCode) +
    (deliveredAt == null ? 0 : deliveredAt.hashCode) +
    (packagingAgent == null ? 0 : packagingAgent.hashCode) +
    (deliveryAgent == null ? 0 : deliveryAgent.hashCode) +
    (orderAddress == null ? 0 : orderAddress.hashCode) +
    (pickupAddress == null ? 0 : pickupAddress.hashCode) +
    (user == null ? 0 : user.hashCode) +
    (warehouse == null ? 0 : warehouse.hashCode) +
    (lines == null ? 0 : lines.hashCode) +
    (orderStateHistory == null ? 0 : orderStateHistory.hashCode) +
    (paymentLogs == null ? 0 : paymentLogs.hashCode) +
    (campaign == null ? 0 : campaign.hashCode) +
    (deletedAt == null ? 0 : deletedAt.hashCode) +
    (isDeleted == null ? 0 : isDeleted.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'Order[id=$id, total=$total, subTotal=$subTotal, deliveryFee=$deliveryFee, tax=$tax, orderState=$orderState, returned=$returned, note=$note, stripeIntentId=$stripeIntentId, orderType=$orderType, ratedStars=$ratedStars, rated=$rated, paymentType=$paymentType, deliveryLastLatitude=$deliveryLastLatitude, deliveryLastLongitude=$deliveryLastLongitude, userId=$userId, warehouseId=$warehouseId, orderAddressId=$orderAddressId, pickupAddressId=$pickupAddressId, packagingAgentId=$packagingAgentId, deliveryAgentId=$deliveryAgentId, campaignId=$campaignId, deliveryAgentImageUrl=$deliveryAgentImageUrl, deliveryAgentName=$deliveryAgentName, packagableAt=$packagableAt, assignedToPackagingAt=$assignedToPackagingAt, packagedAt=$packagedAt, deliverableAt=$deliverableAt, assignedToDeliveryAt=$assignedToDeliveryAt, deliveredAt=$deliveredAt, packagingAgent=$packagingAgent, deliveryAgent=$deliveryAgent, orderAddress=$orderAddress, pickupAddress=$pickupAddress, user=$user, warehouse=$warehouse, lines=$lines, orderStateHistory=$orderStateHistory, paymentLogs=$paymentLogs, campaign=$campaign, deletedAt=$deletedAt, isDeleted=$isDeleted, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (total != null) {
      json[r'total'] = total;
    }
    if (subTotal != null) {
      json[r'subTotal'] = subTotal;
    }
    if (deliveryFee != null) {
      json[r'deliveryFee'] = deliveryFee;
    }
    if (tax != null) {
      json[r'tax'] = tax;
    }
    if (orderState != null) {
      json[r'orderState'] = orderState;
    }
    if (returned != null) {
      json[r'returned'] = returned;
    }
    if (note != null) {
      json[r'note'] = note;
    }
    if (stripeIntentId != null) {
      json[r'stripeIntentId'] = stripeIntentId;
    }
    if (orderType != null) {
      json[r'orderType'] = orderType;
    }
    if (ratedStars != null) {
      json[r'ratedStars'] = ratedStars;
    }
    if (rated != null) {
      json[r'rated'] = rated;
    }
    if (paymentType != null) {
      json[r'paymentType'] = paymentType;
    }
    if (deliveryLastLatitude != null) {
      json[r'deliveryLastLatitude'] = deliveryLastLatitude;
    }
    if (deliveryLastLongitude != null) {
      json[r'deliveryLastLongitude'] = deliveryLastLongitude;
    }
    if (userId != null) {
      json[r'userId'] = userId;
    }
    if (warehouseId != null) {
      json[r'warehouseId'] = warehouseId;
    }
    if (orderAddressId != null) {
      json[r'orderAddressId'] = orderAddressId;
    }
    if (pickupAddressId != null) {
      json[r'pickupAddressId'] = pickupAddressId;
    }
    if (packagingAgentId != null) {
      json[r'packagingAgentId'] = packagingAgentId;
    }
    if (deliveryAgentId != null) {
      json[r'deliveryAgentId'] = deliveryAgentId;
    }
    if (campaignId != null) {
      json[r'campaignId'] = campaignId;
    }
    if (deliveryAgentImageUrl != null) {
      json[r'deliveryAgentImageUrl'] = deliveryAgentImageUrl;
    }
    if (deliveryAgentName != null) {
      json[r'deliveryAgentName'] = deliveryAgentName;
    }
    if (packagableAt != null) {
      json[r'packagableAt'] = packagableAt.toUtc().toIso8601String();
    }
    if (assignedToPackagingAt != null) {
      json[r'assignedToPackagingAt'] = assignedToPackagingAt.toUtc().toIso8601String();
    }
    if (packagedAt != null) {
      json[r'packagedAt'] = packagedAt.toUtc().toIso8601String();
    }
    if (deliverableAt != null) {
      json[r'deliverableAt'] = deliverableAt.toUtc().toIso8601String();
    }
    if (assignedToDeliveryAt != null) {
      json[r'assignedToDeliveryAt'] = assignedToDeliveryAt.toUtc().toIso8601String();
    }
    if (deliveredAt != null) {
      json[r'deliveredAt'] = deliveredAt.toUtc().toIso8601String();
    }
    if (packagingAgent != null) {
      json[r'packagingAgent'] = packagingAgent;
    }
    if (deliveryAgent != null) {
      json[r'deliveryAgent'] = deliveryAgent;
    }
    if (orderAddress != null) {
      json[r'orderAddress'] = orderAddress;
    }
    if (pickupAddress != null) {
      json[r'pickupAddress'] = pickupAddress;
    }
    if (user != null) {
      json[r'user'] = user;
    }
    if (warehouse != null) {
      json[r'warehouse'] = warehouse;
    }
    if (lines != null) {
      json[r'lines'] = lines;
    }
    if (orderStateHistory != null) {
      json[r'orderStateHistory'] = orderStateHistory;
    }
    if (paymentLogs != null) {
      json[r'paymentLogs'] = paymentLogs;
    }
    if (campaign != null) {
      json[r'campaign'] = campaign;
    }
    if (deletedAt != null) {
      json[r'deletedAt'] = deletedAt.toUtc().toIso8601String();
    }
    if (isDeleted != null) {
      json[r'isDeleted'] = isDeleted;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [Order] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Order fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Order(
        id: json[r'id'],
        total: json[r'total'],
        subTotal: json[r'subTotal'],
        deliveryFee: json[r'deliveryFee'],
        tax: json[r'tax'],
        orderState: OrderState.fromJson(json[r'orderState']),
        returned: json[r'returned'],
        note: json[r'note'],
        stripeIntentId: json[r'stripeIntentId'],
        orderType: OrderType.fromJson(json[r'orderType']),
        ratedStars: json[r'ratedStars'],
        rated: json[r'rated'],
        paymentType: PaymentType.fromJson(json[r'paymentType']),
        deliveryLastLatitude: json[r'deliveryLastLatitude'],
        deliveryLastLongitude: json[r'deliveryLastLongitude'],
        userId: json[r'userId'],
        warehouseId: json[r'warehouseId'],
        orderAddressId: json[r'orderAddressId'],
        pickupAddressId: json[r'pickupAddressId'],
        packagingAgentId: json[r'packagingAgentId'],
        deliveryAgentId: json[r'deliveryAgentId'],
        campaignId: json[r'campaignId'],
        deliveryAgentImageUrl: json[r'deliveryAgentImageUrl'],
        deliveryAgentName: json[r'deliveryAgentName'],
        packagableAt: json[r'packagableAt'] == null
          ? null
          : DateTime.parse(json[r'packagableAt']),
        assignedToPackagingAt: json[r'assignedToPackagingAt'] == null
          ? null
          : DateTime.parse(json[r'assignedToPackagingAt']),
        packagedAt: json[r'packagedAt'] == null
          ? null
          : DateTime.parse(json[r'packagedAt']),
        deliverableAt: json[r'deliverableAt'] == null
          ? null
          : DateTime.parse(json[r'deliverableAt']),
        assignedToDeliveryAt: json[r'assignedToDeliveryAt'] == null
          ? null
          : DateTime.parse(json[r'assignedToDeliveryAt']),
        deliveredAt: json[r'deliveredAt'] == null
          ? null
          : DateTime.parse(json[r'deliveredAt']),
        packagingAgent: WarehouseUser.fromJson(json[r'packagingAgent']),
        deliveryAgent: WarehouseUser.fromJson(json[r'deliveryAgent']),
        orderAddress: OrderAddress.fromJson(json[r'orderAddress']),
        pickupAddress: OrderAddress.fromJson(json[r'pickupAddress']),
        user: User.fromJson(json[r'user']),
        warehouse: Warehouse.fromJson(json[r'warehouse']),
        lines: OrderLine.listFromJson(json[r'lines']),
        orderStateHistory: OrderStateHistory.listFromJson(json[r'orderStateHistory']),
        paymentLogs: PaymentLog.listFromJson(json[r'paymentLogs']),
        campaign: CartCampaign.fromJson(json[r'campaign']),
        deletedAt: json[r'deletedAt'] == null
          ? null
          : DateTime.parse(json[r'deletedAt']),
        isDeleted: json[r'isDeleted'],
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<Order> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Order>[]
      : json.map((v) => Order.fromJson(v)).toList(growable: true == growable);

  static Map<String, Order> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Order>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Order.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Order-objects as value to a dart map
  static Map<String, List<Order>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Order>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Order.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

