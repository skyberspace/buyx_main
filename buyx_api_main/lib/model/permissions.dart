//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     1 = CanViewProducts    2 = CanEditProducts    3 = ProductManagement    4 = CanViewCustomers    8 = CanEditCustomers    12 = CustomerManagement    16 = CanViewLayouts    32 = CanEditLayouts    48 = LayoutManagement    64 = CanViewCarts    128 = CanEditCarts    192 = CartManagement    256 = CanViewCategories    512 = CanEditCategories    768 = CategoryManagement    1024 = CanViewWarehouses    2048 = CanEditWarehouses    3072 = InventoryManagement    3843 = CatalogManagement    3891 = DesignManagement    4096 = CanViewAdministritiveSettings    8192 = CanEditAdministritiveSettings    12288 = AdministritiveOperations    16384 = CanViewOrders    32768 = CanEditOrders    49152 = OrderManagement    49356 = OperationManagement    65535 = Admin
class Permissions {
  /// Instantiate a new enum with the provided [value].
  const Permissions._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number1 = Permissions._(1);
  static const number2 = Permissions._(2);
  static const number3 = Permissions._(3);
  static const number4 = Permissions._(4);
  static const number8 = Permissions._(8);
  static const number12 = Permissions._(12);
  static const number16 = Permissions._(16);
  static const number32 = Permissions._(32);
  static const number48 = Permissions._(48);
  static const number64 = Permissions._(64);
  static const number128 = Permissions._(128);
  static const number192 = Permissions._(192);
  static const number256 = Permissions._(256);
  static const number512 = Permissions._(512);
  static const number768 = Permissions._(768);
  static const number1024 = Permissions._(1024);
  static const number2048 = Permissions._(2048);
  static const number3072 = Permissions._(3072);
  static const number3843 = Permissions._(3843);
  static const number3891 = Permissions._(3891);
  static const number4096 = Permissions._(4096);
  static const number8192 = Permissions._(8192);
  static const number12288 = Permissions._(12288);
  static const number16384 = Permissions._(16384);
  static const number32768 = Permissions._(32768);
  static const number49152 = Permissions._(49152);
  static const number49356 = Permissions._(49356);
  static const number65535 = Permissions._(65535);

  /// List of all possible values in this [enum][Permissions].
  static const values = <Permissions>[
    number1,
    number2,
    number3,
    number4,
    number8,
    number12,
    number16,
    number32,
    number48,
    number64,
    number128,
    number192,
    number256,
    number512,
    number768,
    number1024,
    number2048,
    number3072,
    number3843,
    number3891,
    number4096,
    number8192,
    number12288,
    number16384,
    number32768,
    number49152,
    number49356,
    number65535,
  ];

  static Permissions fromJson(dynamic value) =>
    PermissionsTypeTransformer().decode(value);

  static List<Permissions> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Permissions>[]
      : json
          .map((value) => Permissions.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [Permissions] to int,
/// and [decode] dynamic data back to [Permissions].
class PermissionsTypeTransformer {
  const PermissionsTypeTransformer._();

  factory PermissionsTypeTransformer() => _instance ??= PermissionsTypeTransformer._();

  int encode(Permissions data) => data.value;

  /// Decodes a [dynamic value][data] to a Permissions.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  Permissions decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 1: return Permissions.number1;
      case 2: return Permissions.number2;
      case 3: return Permissions.number3;
      case 4: return Permissions.number4;
      case 8: return Permissions.number8;
      case 12: return Permissions.number12;
      case 16: return Permissions.number16;
      case 32: return Permissions.number32;
      case 48: return Permissions.number48;
      case 64: return Permissions.number64;
      case 128: return Permissions.number128;
      case 192: return Permissions.number192;
      case 256: return Permissions.number256;
      case 512: return Permissions.number512;
      case 768: return Permissions.number768;
      case 1024: return Permissions.number1024;
      case 2048: return Permissions.number2048;
      case 3072: return Permissions.number3072;
      case 3843: return Permissions.number3843;
      case 3891: return Permissions.number3891;
      case 4096: return Permissions.number4096;
      case 8192: return Permissions.number8192;
      case 12288: return Permissions.number12288;
      case 16384: return Permissions.number16384;
      case 32768: return Permissions.number32768;
      case 49152: return Permissions.number49152;
      case 49356: return Permissions.number49356;
      case 65535: return Permissions.number65535;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [PermissionsTypeTransformer] instance.
  static PermissionsTypeTransformer _instance;
}
