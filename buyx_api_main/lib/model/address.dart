//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Address {
  /// Returns a new [Address] instance.
  Address({
    this.id,
    this.addressName,
    this.addressLine1,
    this.zipCode,
    this.isDefault,
    this.buildingNo,
    this.floor,
    this.flat,
    this.directions,
    this.latitude,
    this.longitude,
    this.userId,
    this.user,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String addressName;

  String addressLine1;

  String zipCode;

  bool isDefault;

  String buildingNo;

  String floor;

  String flat;

  String directions;

  double latitude;

  double longitude;

  int userId;

  User user;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Address &&
     other.id == id &&
     other.addressName == addressName &&
     other.addressLine1 == addressLine1 &&
     other.zipCode == zipCode &&
     other.isDefault == isDefault &&
     other.buildingNo == buildingNo &&
     other.floor == floor &&
     other.flat == flat &&
     other.directions == directions &&
     other.latitude == latitude &&
     other.longitude == longitude &&
     other.userId == userId &&
     other.user == user &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (addressName == null ? 0 : addressName.hashCode) +
    (addressLine1 == null ? 0 : addressLine1.hashCode) +
    (zipCode == null ? 0 : zipCode.hashCode) +
    (isDefault == null ? 0 : isDefault.hashCode) +
    (buildingNo == null ? 0 : buildingNo.hashCode) +
    (floor == null ? 0 : floor.hashCode) +
    (flat == null ? 0 : flat.hashCode) +
    (directions == null ? 0 : directions.hashCode) +
    (latitude == null ? 0 : latitude.hashCode) +
    (longitude == null ? 0 : longitude.hashCode) +
    (userId == null ? 0 : userId.hashCode) +
    (user == null ? 0 : user.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'Address[id=$id, addressName=$addressName, addressLine1=$addressLine1, zipCode=$zipCode, isDefault=$isDefault, buildingNo=$buildingNo, floor=$floor, flat=$flat, directions=$directions, latitude=$latitude, longitude=$longitude, userId=$userId, user=$user, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (addressName != null) {
      json[r'addressName'] = addressName;
    }
    if (addressLine1 != null) {
      json[r'addressLine1'] = addressLine1;
    }
    if (zipCode != null) {
      json[r'zipCode'] = zipCode;
    }
    if (isDefault != null) {
      json[r'isDefault'] = isDefault;
    }
    if (buildingNo != null) {
      json[r'buildingNo'] = buildingNo;
    }
    if (floor != null) {
      json[r'floor'] = floor;
    }
    if (flat != null) {
      json[r'flat'] = flat;
    }
    if (directions != null) {
      json[r'directions'] = directions;
    }
    if (latitude != null) {
      json[r'latitude'] = latitude;
    }
    if (longitude != null) {
      json[r'longitude'] = longitude;
    }
    if (userId != null) {
      json[r'userId'] = userId;
    }
    if (user != null) {
      json[r'user'] = user;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [Address] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Address fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Address(
        id: json[r'id'],
        addressName: json[r'addressName'],
        addressLine1: json[r'addressLine1'],
        zipCode: json[r'zipCode'],
        isDefault: json[r'isDefault'],
        buildingNo: json[r'buildingNo'],
        floor: json[r'floor'],
        flat: json[r'flat'],
        directions: json[r'directions'],
        latitude: json[r'latitude'],
        longitude: json[r'longitude'],
        userId: json[r'userId'],
        user: User.fromJson(json[r'user']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<Address> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Address>[]
      : json.map((v) => Address.fromJson(v)).toList(growable: true == growable);

  static Map<String, Address> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Address>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Address.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Address-objects as value to a dart map
  static Map<String, List<Address>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Address>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Address.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

