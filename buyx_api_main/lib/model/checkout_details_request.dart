//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CheckoutDetailsRequest {
  /// Returns a new [CheckoutDetailsRequest] instance.
  CheckoutDetailsRequest({
    this.campaignId,
  });

  int campaignId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CheckoutDetailsRequest &&
     other.campaignId == campaignId;

  @override
  int get hashCode =>
    (campaignId == null ? 0 : campaignId.hashCode);

  @override
  String toString() => 'CheckoutDetailsRequest[campaignId=$campaignId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (campaignId != null) {
      json[r'campaignId'] = campaignId;
    }
    return json;
  }

  /// Returns a new [CheckoutDetailsRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CheckoutDetailsRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CheckoutDetailsRequest(
        campaignId: json[r'campaignId'],
    );

  static List<CheckoutDetailsRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CheckoutDetailsRequest>[]
      : json.map((v) => CheckoutDetailsRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, CheckoutDetailsRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CheckoutDetailsRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CheckoutDetailsRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CheckoutDetailsRequest-objects as value to a dart map
  static Map<String, List<CheckoutDetailsRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CheckoutDetailsRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CheckoutDetailsRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

