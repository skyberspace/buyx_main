//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Geometry {
  /// Returns a new [Geometry] instance.
  Geometry({
    this.factory_,
    this.userData,
    this.srid,
    this.geometryType,
    this.ogcGeometryType,
    this.precisionModel,
    this.coordinate,
    this.coordinates,
    this.numPoints,
    this.numGeometries,
    this.isSimple,
    this.isValid,
    this.isEmpty,
    this.area,
    this.length,
    this.centroid,
    this.interiorPoint,
    this.pointOnSurface,
    this.dimension,
    this.boundary,
    this.boundaryDimension,
    this.envelope,
    this.envelopeInternal,
    this.isRectangle,
  });

  GeometryFactory factory_;

  Object userData;

  int srid;

  String geometryType;

  OgcGeometryType ogcGeometryType;

  PrecisionModel precisionModel;

  Coordinate coordinate;

  List<Coordinate> coordinates;

  int numPoints;

  int numGeometries;

  bool isSimple;

  bool isValid;

  bool isEmpty;

  double area;

  double length;

  Point centroid;

  Point interiorPoint;

  Point pointOnSurface;

  Dimension dimension;

  Geometry boundary;

  Dimension boundaryDimension;

  Geometry envelope;

  Envelope envelopeInternal;

  bool isRectangle;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Geometry &&
     other.factory_ == factory_ &&
     other.userData == userData &&
     other.srid == srid &&
     other.geometryType == geometryType &&
     other.ogcGeometryType == ogcGeometryType &&
     other.precisionModel == precisionModel &&
     other.coordinate == coordinate &&
     other.coordinates == coordinates &&
     other.numPoints == numPoints &&
     other.numGeometries == numGeometries &&
     other.isSimple == isSimple &&
     other.isValid == isValid &&
     other.isEmpty == isEmpty &&
     other.area == area &&
     other.length == length &&
     other.centroid == centroid &&
     other.interiorPoint == interiorPoint &&
     other.pointOnSurface == pointOnSurface &&
     other.dimension == dimension &&
     other.boundary == boundary &&
     other.boundaryDimension == boundaryDimension &&
     other.envelope == envelope &&
     other.envelopeInternal == envelopeInternal &&
     other.isRectangle == isRectangle;

  @override
  int get hashCode =>
    (factory_ == null ? 0 : factory_.hashCode) +
    (userData == null ? 0 : userData.hashCode) +
    (srid == null ? 0 : srid.hashCode) +
    (geometryType == null ? 0 : geometryType.hashCode) +
    (ogcGeometryType == null ? 0 : ogcGeometryType.hashCode) +
    (precisionModel == null ? 0 : precisionModel.hashCode) +
    (coordinate == null ? 0 : coordinate.hashCode) +
    (coordinates == null ? 0 : coordinates.hashCode) +
    (numPoints == null ? 0 : numPoints.hashCode) +
    (numGeometries == null ? 0 : numGeometries.hashCode) +
    (isSimple == null ? 0 : isSimple.hashCode) +
    (isValid == null ? 0 : isValid.hashCode) +
    (isEmpty == null ? 0 : isEmpty.hashCode) +
    (area == null ? 0 : area.hashCode) +
    (length == null ? 0 : length.hashCode) +
    (centroid == null ? 0 : centroid.hashCode) +
    (interiorPoint == null ? 0 : interiorPoint.hashCode) +
    (pointOnSurface == null ? 0 : pointOnSurface.hashCode) +
    (dimension == null ? 0 : dimension.hashCode) +
    (boundary == null ? 0 : boundary.hashCode) +
    (boundaryDimension == null ? 0 : boundaryDimension.hashCode) +
    (envelope == null ? 0 : envelope.hashCode) +
    (envelopeInternal == null ? 0 : envelopeInternal.hashCode) +
    (isRectangle == null ? 0 : isRectangle.hashCode);

  @override
  String toString() => 'Geometry[factory_=$factory_, userData=$userData, srid=$srid, geometryType=$geometryType, ogcGeometryType=$ogcGeometryType, precisionModel=$precisionModel, coordinate=$coordinate, coordinates=$coordinates, numPoints=$numPoints, numGeometries=$numGeometries, isSimple=$isSimple, isValid=$isValid, isEmpty=$isEmpty, area=$area, length=$length, centroid=$centroid, interiorPoint=$interiorPoint, pointOnSurface=$pointOnSurface, dimension=$dimension, boundary=$boundary, boundaryDimension=$boundaryDimension, envelope=$envelope, envelopeInternal=$envelopeInternal, isRectangle=$isRectangle]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (factory_ != null) {
      json[r'factory'] = factory_;
    }
    if (userData != null) {
      json[r'userData'] = userData;
    }
    if (srid != null) {
      json[r'srid'] = srid;
    }
    if (geometryType != null) {
      json[r'geometryType'] = geometryType;
    }
    if (ogcGeometryType != null) {
      json[r'ogcGeometryType'] = ogcGeometryType;
    }
    if (precisionModel != null) {
      json[r'precisionModel'] = precisionModel;
    }
    if (coordinate != null) {
      json[r'coordinate'] = coordinate;
    }
    if (coordinates != null) {
      json[r'coordinates'] = coordinates;
    }
    if (numPoints != null) {
      json[r'numPoints'] = numPoints;
    }
    if (numGeometries != null) {
      json[r'numGeometries'] = numGeometries;
    }
    if (isSimple != null) {
      json[r'isSimple'] = isSimple;
    }
    if (isValid != null) {
      json[r'isValid'] = isValid;
    }
    if (isEmpty != null) {
      json[r'isEmpty'] = isEmpty;
    }
    if (area != null) {
      json[r'area'] = area;
    }
    if (length != null) {
      json[r'length'] = length;
    }
    if (centroid != null) {
      json[r'centroid'] = centroid;
    }
    if (interiorPoint != null) {
      json[r'interiorPoint'] = interiorPoint;
    }
    if (pointOnSurface != null) {
      json[r'pointOnSurface'] = pointOnSurface;
    }
    if (dimension != null) {
      json[r'dimension'] = dimension;
    }
    if (boundary != null) {
      json[r'boundary'] = boundary;
    }
    if (boundaryDimension != null) {
      json[r'boundaryDimension'] = boundaryDimension;
    }
    if (envelope != null) {
      json[r'envelope'] = envelope;
    }
    if (envelopeInternal != null) {
      json[r'envelopeInternal'] = envelopeInternal;
    }
    if (isRectangle != null) {
      json[r'isRectangle'] = isRectangle;
    }
    return json;
  }

  /// Returns a new [Geometry] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Geometry fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Geometry(
        factory_: GeometryFactory.fromJson(json[r'factory']),
        userData: json[r'userData'],
        srid: json[r'srid'],
        geometryType: json[r'geometryType'],
        ogcGeometryType: OgcGeometryType.fromJson(json[r'ogcGeometryType']),
        precisionModel: PrecisionModel.fromJson(json[r'precisionModel']),
        coordinate: Coordinate.fromJson(json[r'coordinate']),
        coordinates: Coordinate.listFromJson(json[r'coordinates']),
        numPoints: json[r'numPoints'],
        numGeometries: json[r'numGeometries'],
        isSimple: json[r'isSimple'],
        isValid: json[r'isValid'],
        isEmpty: json[r'isEmpty'],
        area: json[r'area'],
        length: json[r'length'],
        centroid: Point.fromJson(json[r'centroid']),
        interiorPoint: Point.fromJson(json[r'interiorPoint']),
        pointOnSurface: Point.fromJson(json[r'pointOnSurface']),
        dimension: Dimension.fromJson(json[r'dimension']),
        boundary: Geometry.fromJson(json[r'boundary']),
        boundaryDimension: Dimension.fromJson(json[r'boundaryDimension']),
        envelope: Geometry.fromJson(json[r'envelope']),
        envelopeInternal: Envelope.fromJson(json[r'envelopeInternal']),
        isRectangle: json[r'isRectangle'],
    );

  static List<Geometry> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Geometry>[]
      : json.map((v) => Geometry.fromJson(v)).toList(growable: true == growable);

  static Map<String, Geometry> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Geometry>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Geometry.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Geometry-objects as value to a dart map
  static Map<String, List<Geometry>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Geometry>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Geometry.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

