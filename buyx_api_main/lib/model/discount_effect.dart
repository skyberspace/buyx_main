//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     1 = AllCart    2 = OnlyRulefiltered
class DiscountEffect {
  /// Instantiate a new enum with the provided [value].
  const DiscountEffect._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number1 = DiscountEffect._(1);
  static const number2 = DiscountEffect._(2);

  /// List of all possible values in this [enum][DiscountEffect].
  static const values = <DiscountEffect>[
    number1,
    number2,
  ];

  static DiscountEffect fromJson(dynamic value) =>
    DiscountEffectTypeTransformer().decode(value);

  static List<DiscountEffect> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <DiscountEffect>[]
      : json
          .map((value) => DiscountEffect.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DiscountEffect] to int,
/// and [decode] dynamic data back to [DiscountEffect].
class DiscountEffectTypeTransformer {
  const DiscountEffectTypeTransformer._();

  factory DiscountEffectTypeTransformer() => _instance ??= DiscountEffectTypeTransformer._();

  int encode(DiscountEffect data) => data.value;

  /// Decodes a [dynamic value][data] to a DiscountEffect.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DiscountEffect decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 1: return DiscountEffect.number1;
      case 2: return DiscountEffect.number2;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [DiscountEffectTypeTransformer] instance.
  static DiscountEffectTypeTransformer _instance;
}
