//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CheckoutDetails {
  /// Returns a new [CheckoutDetails] instance.
  CheckoutDetails({
    this.cartTotal,
    this.deliveryFee,
    this.campaignDiscount,
    this.total,
    this.cartItems,
    this.selectedCampaign,
    this.availableCampaigns,
  });

  double cartTotal;

  double deliveryFee;

  double campaignDiscount;

  double total;

  List<CartItem> cartItems;

  CartCampaign selectedCampaign;

  List<CartCampaign> availableCampaigns;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CheckoutDetails &&
     other.cartTotal == cartTotal &&
     other.deliveryFee == deliveryFee &&
     other.campaignDiscount == campaignDiscount &&
     other.total == total &&
     other.cartItems == cartItems &&
     other.selectedCampaign == selectedCampaign &&
     other.availableCampaigns == availableCampaigns;

  @override
  int get hashCode =>
    (cartTotal == null ? 0 : cartTotal.hashCode) +
    (deliveryFee == null ? 0 : deliveryFee.hashCode) +
    (campaignDiscount == null ? 0 : campaignDiscount.hashCode) +
    (total == null ? 0 : total.hashCode) +
    (cartItems == null ? 0 : cartItems.hashCode) +
    (selectedCampaign == null ? 0 : selectedCampaign.hashCode) +
    (availableCampaigns == null ? 0 : availableCampaigns.hashCode);

  @override
  String toString() => 'CheckoutDetails[cartTotal=$cartTotal, deliveryFee=$deliveryFee, campaignDiscount=$campaignDiscount, total=$total, cartItems=$cartItems, selectedCampaign=$selectedCampaign, availableCampaigns=$availableCampaigns]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (cartTotal != null) {
      json[r'cartTotal'] = cartTotal;
    }
    if (deliveryFee != null) {
      json[r'deliveryFee'] = deliveryFee;
    }
    if (campaignDiscount != null) {
      json[r'campaignDiscount'] = campaignDiscount;
    }
    if (total != null) {
      json[r'total'] = total;
    }
    if (cartItems != null) {
      json[r'cartItems'] = cartItems;
    }
    if (selectedCampaign != null) {
      json[r'selectedCampaign'] = selectedCampaign;
    }
    if (availableCampaigns != null) {
      json[r'availableCampaigns'] = availableCampaigns;
    }
    return json;
  }

  /// Returns a new [CheckoutDetails] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CheckoutDetails fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CheckoutDetails(
        cartTotal: json[r'cartTotal'],
        deliveryFee: json[r'deliveryFee'],
        campaignDiscount: json[r'campaignDiscount'],
        total: json[r'total'],
        cartItems: CartItem.listFromJson(json[r'cartItems']),
        selectedCampaign: CartCampaign.fromJson(json[r'selectedCampaign']),
        availableCampaigns: CartCampaign.listFromJson(json[r'availableCampaigns']),
    );

  static List<CheckoutDetails> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CheckoutDetails>[]
      : json.map((v) => CheckoutDetails.fromJson(v)).toList(growable: true == growable);

  static Map<String, CheckoutDetails> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CheckoutDetails>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CheckoutDetails.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CheckoutDetails-objects as value to a dart map
  static Map<String, List<CheckoutDetails>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CheckoutDetails>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CheckoutDetails.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

