//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Banner {
  /// Returns a new [Banner] instance.
  Banner({
    this.id,
    this.name,
    this.imagePath,
    this.order,
    this.moduleId,
    this.module,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String name;

  String imagePath;

  int order;

  int moduleId;

  BannerModule module;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Banner &&
     other.id == id &&
     other.name == name &&
     other.imagePath == imagePath &&
     other.order == order &&
     other.moduleId == moduleId &&
     other.module == module &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (imagePath == null ? 0 : imagePath.hashCode) +
    (order == null ? 0 : order.hashCode) +
    (moduleId == null ? 0 : moduleId.hashCode) +
    (module == null ? 0 : module.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'Banner[id=$id, name=$name, imagePath=$imagePath, order=$order, moduleId=$moduleId, module=$module, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (imagePath != null) {
      json[r'imagePath'] = imagePath;
    }
    if (order != null) {
      json[r'order'] = order;
    }
    if (moduleId != null) {
      json[r'moduleId'] = moduleId;
    }
    if (module != null) {
      json[r'module'] = module;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [Banner] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Banner fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Banner(
        id: json[r'id'],
        name: json[r'name'],
        imagePath: json[r'imagePath'],
        order: json[r'order'],
        moduleId: json[r'moduleId'],
        module: BannerModule.fromJson(json[r'module']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<Banner> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Banner>[]
      : json.map((v) => Banner.fromJson(v)).toList(growable: true == growable);

  static Map<String, Banner> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Banner>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Banner.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Banner-objects as value to a dart map
  static Map<String, List<Banner>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Banner>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Banner.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

