//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ChangeInformtionRequest {
  /// Returns a new [ChangeInformtionRequest] instance.
  ChangeInformtionRequest({
    this.email,
    this.name,
    this.surname,
  });

  String email;

  String name;

  String surname;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ChangeInformtionRequest &&
     other.email == email &&
     other.name == name &&
     other.surname == surname;

  @override
  int get hashCode =>
    (email == null ? 0 : email.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (surname == null ? 0 : surname.hashCode);

  @override
  String toString() => 'ChangeInformtionRequest[email=$email, name=$name, surname=$surname]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (email != null) {
      json[r'email'] = email;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (surname != null) {
      json[r'surname'] = surname;
    }
    return json;
  }

  /// Returns a new [ChangeInformtionRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ChangeInformtionRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ChangeInformtionRequest(
        email: json[r'email'],
        name: json[r'name'],
        surname: json[r'surname'],
    );

  static List<ChangeInformtionRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ChangeInformtionRequest>[]
      : json.map((v) => ChangeInformtionRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, ChangeInformtionRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ChangeInformtionRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ChangeInformtionRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ChangeInformtionRequest-objects as value to a dart map
  static Map<String, List<ChangeInformtionRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ChangeInformtionRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ChangeInformtionRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

