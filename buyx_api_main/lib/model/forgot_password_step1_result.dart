//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ForgotPasswordStep1Result {
  /// Returns a new [ForgotPasswordStep1Result] instance.
  ForgotPasswordStep1Result({
    this.passwordResetToken,
  });

  String passwordResetToken;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ForgotPasswordStep1Result &&
     other.passwordResetToken == passwordResetToken;

  @override
  int get hashCode =>
    (passwordResetToken == null ? 0 : passwordResetToken.hashCode);

  @override
  String toString() => 'ForgotPasswordStep1Result[passwordResetToken=$passwordResetToken]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (passwordResetToken != null) {
      json[r'passwordResetToken'] = passwordResetToken;
    }
    return json;
  }

  /// Returns a new [ForgotPasswordStep1Result] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ForgotPasswordStep1Result fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ForgotPasswordStep1Result(
        passwordResetToken: json[r'passwordResetToken'],
    );

  static List<ForgotPasswordStep1Result> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ForgotPasswordStep1Result>[]
      : json.map((v) => ForgotPasswordStep1Result.fromJson(v)).toList(growable: true == growable);

  static Map<String, ForgotPasswordStep1Result> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ForgotPasswordStep1Result>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ForgotPasswordStep1Result.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ForgotPasswordStep1Result-objects as value to a dart map
  static Map<String, List<ForgotPasswordStep1Result>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ForgotPasswordStep1Result>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ForgotPasswordStep1Result.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

