//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class UserFavoriteRequest {
  /// Returns a new [UserFavoriteRequest] instance.
  UserFavoriteRequest({
    this.favoriteId,
  });

  int favoriteId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is UserFavoriteRequest &&
     other.favoriteId == favoriteId;

  @override
  int get hashCode =>
    (favoriteId == null ? 0 : favoriteId.hashCode);

  @override
  String toString() => 'UserFavoriteRequest[favoriteId=$favoriteId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (favoriteId != null) {
      json[r'favoriteId'] = favoriteId;
    }
    return json;
  }

  /// Returns a new [UserFavoriteRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static UserFavoriteRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : UserFavoriteRequest(
        favoriteId: json[r'favoriteId'],
    );

  static List<UserFavoriteRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <UserFavoriteRequest>[]
      : json.map((v) => UserFavoriteRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, UserFavoriteRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, UserFavoriteRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = UserFavoriteRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of UserFavoriteRequest-objects as value to a dart map
  static Map<String, List<UserFavoriteRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<UserFavoriteRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = UserFavoriteRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

