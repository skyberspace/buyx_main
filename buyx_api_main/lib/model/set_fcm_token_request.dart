//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SetFCMTokenRequest {
  /// Returns a new [SetFCMTokenRequest] instance.
  SetFCMTokenRequest({
    this.fcmToken,
  });

  String fcmToken;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SetFCMTokenRequest &&
     other.fcmToken == fcmToken;

  @override
  int get hashCode =>
    (fcmToken == null ? 0 : fcmToken.hashCode);

  @override
  String toString() => 'SetFCMTokenRequest[fcmToken=$fcmToken]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (fcmToken != null) {
      json[r'fcmToken'] = fcmToken;
    }
    return json;
  }

  /// Returns a new [SetFCMTokenRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SetFCMTokenRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : SetFCMTokenRequest(
        fcmToken: json[r'fcmToken'],
    );

  static List<SetFCMTokenRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <SetFCMTokenRequest>[]
      : json.map((v) => SetFCMTokenRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, SetFCMTokenRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SetFCMTokenRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = SetFCMTokenRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of SetFCMTokenRequest-objects as value to a dart map
  static Map<String, List<SetFCMTokenRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<SetFCMTokenRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = SetFCMTokenRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

