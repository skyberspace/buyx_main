//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     0 = Stripe    1 = CashOnDelivery
class PaymentType {
  /// Instantiate a new enum with the provided [value].
  const PaymentType._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number0 = PaymentType._(0);
  static const number1 = PaymentType._(1);

  /// List of all possible values in this [enum][PaymentType].
  static const values = <PaymentType>[
    number0,
    number1,
  ];

  static PaymentType fromJson(dynamic value) =>
    PaymentTypeTypeTransformer().decode(value);

  static List<PaymentType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <PaymentType>[]
      : json
          .map((value) => PaymentType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [PaymentType] to int,
/// and [decode] dynamic data back to [PaymentType].
class PaymentTypeTypeTransformer {
  const PaymentTypeTypeTransformer._();

  factory PaymentTypeTypeTransformer() => _instance ??= PaymentTypeTypeTransformer._();

  int encode(PaymentType data) => data.value;

  /// Decodes a [dynamic value][data] to a PaymentType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  PaymentType decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 0: return PaymentType.number0;
      case 1: return PaymentType.number1;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [PaymentTypeTypeTransformer] instance.
  static PaymentTypeTypeTransformer _instance;
}
