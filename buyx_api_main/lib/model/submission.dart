//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Submission {
  /// Returns a new [Submission] instance.
  Submission({
    this.id,
    this.date,
  });

  String id;

  DateTime date;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Submission &&
     other.id == id &&
     other.date == date;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (date == null ? 0 : date.hashCode);

  @override
  String toString() => 'Submission[id=$id, date=$date]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (date != null) {
      json[r'date'] = date.toUtc().toIso8601String();
    }
    return json;
  }

  /// Returns a new [Submission] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Submission fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Submission(
        id: json[r'id'],
        date: json[r'date'] == null
          ? null
          : DateTime.parse(json[r'date']),
    );

  static List<Submission> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Submission>[]
      : json.map((v) => Submission.fromJson(v)).toList(growable: true == growable);

  static Map<String, Submission> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Submission>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Submission.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Submission-objects as value to a dart map
  static Map<String, List<Submission>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Submission>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Submission.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

