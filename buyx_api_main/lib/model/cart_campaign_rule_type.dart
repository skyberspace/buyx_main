//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     1 = FlatAmountCheck    2 = ProductQuantityCheck
class CartCampaignRuleType {
  /// Instantiate a new enum with the provided [value].
  const CartCampaignRuleType._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number1 = CartCampaignRuleType._(1);
  static const number2 = CartCampaignRuleType._(2);

  /// List of all possible values in this [enum][CartCampaignRuleType].
  static const values = <CartCampaignRuleType>[
    number1,
    number2,
  ];

  static CartCampaignRuleType fromJson(dynamic value) =>
    CartCampaignRuleTypeTypeTransformer().decode(value);

  static List<CartCampaignRuleType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CartCampaignRuleType>[]
      : json
          .map((value) => CartCampaignRuleType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [CartCampaignRuleType] to int,
/// and [decode] dynamic data back to [CartCampaignRuleType].
class CartCampaignRuleTypeTypeTransformer {
  const CartCampaignRuleTypeTypeTransformer._();

  factory CartCampaignRuleTypeTypeTransformer() => _instance ??= CartCampaignRuleTypeTypeTransformer._();

  int encode(CartCampaignRuleType data) => data.value;

  /// Decodes a [dynamic value][data] to a CartCampaignRuleType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  CartCampaignRuleType decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 1: return CartCampaignRuleType.number1;
      case 2: return CartCampaignRuleType.number2;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [CartCampaignRuleTypeTypeTransformer] instance.
  static CartCampaignRuleTypeTypeTransformer _instance;
}
