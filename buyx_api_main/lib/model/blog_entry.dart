//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class BlogEntry {
  /// Returns a new [BlogEntry] instance.
  BlogEntry({
    this.id,
    this.title,
    this.shortDescription,
    this.description,
    this.leadingImage,
    this.blogType,
    this.order,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String title;

  String shortDescription;

  String description;

  String leadingImage;

  BlogType blogType;

  int order;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is BlogEntry &&
     other.id == id &&
     other.title == title &&
     other.shortDescription == shortDescription &&
     other.description == description &&
     other.leadingImage == leadingImage &&
     other.blogType == blogType &&
     other.order == order &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (title == null ? 0 : title.hashCode) +
    (shortDescription == null ? 0 : shortDescription.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (leadingImage == null ? 0 : leadingImage.hashCode) +
    (blogType == null ? 0 : blogType.hashCode) +
    (order == null ? 0 : order.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'BlogEntry[id=$id, title=$title, shortDescription=$shortDescription, description=$description, leadingImage=$leadingImage, blogType=$blogType, order=$order, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (title != null) {
      json[r'title'] = title;
    }
    if (shortDescription != null) {
      json[r'shortDescription'] = shortDescription;
    }
    if (description != null) {
      json[r'description'] = description;
    }
    if (leadingImage != null) {
      json[r'leadingImage'] = leadingImage;
    }
    if (blogType != null) {
      json[r'blogType'] = blogType;
    }
    if (order != null) {
      json[r'order'] = order;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [BlogEntry] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static BlogEntry fromJson(Map<String, dynamic> json) => json == null
    ? null
    : BlogEntry(
        id: json[r'id'],
        title: json[r'title'],
        shortDescription: json[r'shortDescription'],
        description: json[r'description'],
        leadingImage: json[r'leadingImage'],
        blogType: BlogType.fromJson(json[r'blogType']),
        order: json[r'order'],
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<BlogEntry> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <BlogEntry>[]
      : json.map((v) => BlogEntry.fromJson(v)).toList(growable: true == growable);

  static Map<String, BlogEntry> mapFromJson(Map<String, dynamic> json) {
    final map = <String, BlogEntry>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = BlogEntry.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of BlogEntry-objects as value to a dart map
  static Map<String, List<BlogEntry>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<BlogEntry>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = BlogEntry.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

