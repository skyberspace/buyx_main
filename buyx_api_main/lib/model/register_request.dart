//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class RegisterRequest {
  /// Returns a new [RegisterRequest] instance.
  RegisterRequest({
    this.email,
    this.phone,
    this.password,
    this.name,
    this.surname,
  });

  String email;

  String phone;

  String password;

  String name;

  String surname;

  @override
  bool operator ==(Object other) => identical(this, other) || other is RegisterRequest &&
     other.email == email &&
     other.phone == phone &&
     other.password == password &&
     other.name == name &&
     other.surname == surname;

  @override
  int get hashCode =>
    (email == null ? 0 : email.hashCode) +
    (phone == null ? 0 : phone.hashCode) +
    (password == null ? 0 : password.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (surname == null ? 0 : surname.hashCode);

  @override
  String toString() => 'RegisterRequest[email=$email, phone=$phone, password=$password, name=$name, surname=$surname]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (email != null) {
      json[r'email'] = email;
    }
    if (phone != null) {
      json[r'phone'] = phone;
    }
    if (password != null) {
      json[r'password'] = password;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (surname != null) {
      json[r'surname'] = surname;
    }
    return json;
  }

  /// Returns a new [RegisterRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static RegisterRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : RegisterRequest(
        email: json[r'email'],
        phone: json[r'phone'],
        password: json[r'password'],
        name: json[r'name'],
        surname: json[r'surname'],
    );

  static List<RegisterRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <RegisterRequest>[]
      : json.map((v) => RegisterRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, RegisterRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, RegisterRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = RegisterRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of RegisterRequest-objects as value to a dart map
  static Map<String, List<RegisterRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<RegisterRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = RegisterRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

