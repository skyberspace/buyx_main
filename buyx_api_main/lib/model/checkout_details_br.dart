//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CheckoutDetailsBR {
  /// Returns a new [CheckoutDetailsBR] instance.
  CheckoutDetailsBR({
    this.success,
    this.errors,
    this.infos,
    this.data,
  });

  bool success;

  List<String> errors;

  List<String> infos;

  CheckoutDetails data;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CheckoutDetailsBR &&
     other.success == success &&
     other.errors == errors &&
     other.infos == infos &&
     other.data == data;

  @override
  int get hashCode =>
    (success == null ? 0 : success.hashCode) +
    (errors == null ? 0 : errors.hashCode) +
    (infos == null ? 0 : infos.hashCode) +
    (data == null ? 0 : data.hashCode);

  @override
  String toString() => 'CheckoutDetailsBR[success=$success, errors=$errors, infos=$infos, data=$data]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (success != null) {
      json[r'success'] = success;
    }
    if (errors != null) {
      json[r'errors'] = errors;
    }
    if (infos != null) {
      json[r'infos'] = infos;
    }
    if (data != null) {
      json[r'data'] = data;
    }
    return json;
  }

  /// Returns a new [CheckoutDetailsBR] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CheckoutDetailsBR fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CheckoutDetailsBR(
        success: json[r'success'],
        errors: json[r'errors'] == null
          ? null
          : (json[r'errors'] as List).cast<String>(),
        infos: json[r'infos'] == null
          ? null
          : (json[r'infos'] as List).cast<String>(),
        data: CheckoutDetails.fromJson(json[r'data']),
    );

  static List<CheckoutDetailsBR> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CheckoutDetailsBR>[]
      : json.map((v) => CheckoutDetailsBR.fromJson(v)).toList(growable: true == growable);

  static Map<String, CheckoutDetailsBR> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CheckoutDetailsBR>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CheckoutDetailsBR.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CheckoutDetailsBR-objects as value to a dart map
  static Map<String, List<CheckoutDetailsBR>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CheckoutDetailsBR>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CheckoutDetailsBR.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

