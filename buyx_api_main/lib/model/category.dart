//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Category {
  /// Returns a new [Category] instance.
  Category({
    this.id,
    this.name,
    this.shortDesc,
    this.imagePath,
    this.active,
    this.order,
    this.parentCategoryId,
    this.parentCategory,
    this.products,
    this.subCategories,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String name;

  String shortDesc;

  String imagePath;

  bool active;

  int order;

  int parentCategoryId;

  Category parentCategory;

  List<ProductCategory> products;

  List<Category> subCategories;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Category &&
     other.id == id &&
     other.name == name &&
     other.shortDesc == shortDesc &&
     other.imagePath == imagePath &&
     other.active == active &&
     other.order == order &&
     other.parentCategoryId == parentCategoryId &&
     other.parentCategory == parentCategory &&
     other.products == products &&
     other.subCategories == subCategories &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (shortDesc == null ? 0 : shortDesc.hashCode) +
    (imagePath == null ? 0 : imagePath.hashCode) +
    (active == null ? 0 : active.hashCode) +
    (order == null ? 0 : order.hashCode) +
    (parentCategoryId == null ? 0 : parentCategoryId.hashCode) +
    (parentCategory == null ? 0 : parentCategory.hashCode) +
    (products == null ? 0 : products.hashCode) +
    (subCategories == null ? 0 : subCategories.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'Category[id=$id, name=$name, shortDesc=$shortDesc, imagePath=$imagePath, active=$active, order=$order, parentCategoryId=$parentCategoryId, parentCategory=$parentCategory, products=$products, subCategories=$subCategories, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (shortDesc != null) {
      json[r'shortDesc'] = shortDesc;
    }
    if (imagePath != null) {
      json[r'imagePath'] = imagePath;
    }
    if (active != null) {
      json[r'active'] = active;
    }
    if (order != null) {
      json[r'order'] = order;
    }
    if (parentCategoryId != null) {
      json[r'parentCategoryId'] = parentCategoryId;
    }
    if (parentCategory != null) {
      json[r'parentCategory'] = parentCategory;
    }
    if (products != null) {
      json[r'products'] = products;
    }
    if (subCategories != null) {
      json[r'subCategories'] = subCategories;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [Category] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Category fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Category(
        id: json[r'id'],
        name: json[r'name'],
        shortDesc: json[r'shortDesc'],
        imagePath: json[r'imagePath'],
        active: json[r'active'],
        order: json[r'order'],
        parentCategoryId: json[r'parentCategoryId'],
        parentCategory: Category.fromJson(json[r'parentCategory']),
        products: ProductCategory.listFromJson(json[r'products']),
        subCategories: Category.listFromJson(json[r'subCategories']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<Category> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Category>[]
      : json.map((v) => Category.fromJson(v)).toList(growable: true == growable);

  static Map<String, Category> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Category>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Category.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Category-objects as value to a dart map
  static Map<String, List<Category>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Category>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Category.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

