//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AddressRequest {
  /// Returns a new [AddressRequest] instance.
  AddressRequest({
    this.addressId,
  });

  int addressId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AddressRequest &&
     other.addressId == addressId;

  @override
  int get hashCode =>
    (addressId == null ? 0 : addressId.hashCode);

  @override
  String toString() => 'AddressRequest[addressId=$addressId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (addressId != null) {
      json[r'addressId'] = addressId;
    }
    return json;
  }

  /// Returns a new [AddressRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static AddressRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : AddressRequest(
        addressId: json[r'addressId'],
    );

  static List<AddressRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <AddressRequest>[]
      : json.map((v) => AddressRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, AddressRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, AddressRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = AddressRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of AddressRequest-objects as value to a dart map
  static Map<String, List<AddressRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<AddressRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = AddressRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

