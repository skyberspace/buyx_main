//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     0 = Floating    1 = FloatingSingle    2 = Fixed
class PrecisionModels {
  /// Instantiate a new enum with the provided [value].
  const PrecisionModels._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number0 = PrecisionModels._(0);
  static const number1 = PrecisionModels._(1);
  static const number2 = PrecisionModels._(2);

  /// List of all possible values in this [enum][PrecisionModels].
  static const values = <PrecisionModels>[
    number0,
    number1,
    number2,
  ];

  static PrecisionModels fromJson(dynamic value) =>
    PrecisionModelsTypeTransformer().decode(value);

  static List<PrecisionModels> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <PrecisionModels>[]
      : json
          .map((value) => PrecisionModels.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [PrecisionModels] to int,
/// and [decode] dynamic data back to [PrecisionModels].
class PrecisionModelsTypeTransformer {
  const PrecisionModelsTypeTransformer._();

  factory PrecisionModelsTypeTransformer() => _instance ??= PrecisionModelsTypeTransformer._();

  int encode(PrecisionModels data) => data.value;

  /// Decodes a [dynamic value][data] to a PrecisionModels.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  PrecisionModels decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 0: return PrecisionModels.number0;
      case 1: return PrecisionModels.number1;
      case 2: return PrecisionModels.number2;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [PrecisionModelsTypeTransformer] instance.
  static PrecisionModelsTypeTransformer _instance;
}
