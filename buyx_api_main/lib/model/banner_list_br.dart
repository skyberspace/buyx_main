//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class BannerListBR {
  /// Returns a new [BannerListBR] instance.
  BannerListBR({
    this.success,
    this.errors,
    this.infos,
    this.data,
  });

  bool success;

  List<String> errors;

  List<String> infos;

  List<Banner> data;

  @override
  bool operator ==(Object other) => identical(this, other) || other is BannerListBR &&
     other.success == success &&
     other.errors == errors &&
     other.infos == infos &&
     other.data == data;

  @override
  int get hashCode =>
    (success == null ? 0 : success.hashCode) +
    (errors == null ? 0 : errors.hashCode) +
    (infos == null ? 0 : infos.hashCode) +
    (data == null ? 0 : data.hashCode);

  @override
  String toString() => 'BannerListBR[success=$success, errors=$errors, infos=$infos, data=$data]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (success != null) {
      json[r'success'] = success;
    }
    if (errors != null) {
      json[r'errors'] = errors;
    }
    if (infos != null) {
      json[r'infos'] = infos;
    }
    if (data != null) {
      json[r'data'] = data;
    }
    return json;
  }

  /// Returns a new [BannerListBR] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static BannerListBR fromJson(Map<String, dynamic> json) => json == null
    ? null
    : BannerListBR(
        success: json[r'success'],
        errors: json[r'errors'] == null
          ? null
          : (json[r'errors'] as List).cast<String>(),
        infos: json[r'infos'] == null
          ? null
          : (json[r'infos'] as List).cast<String>(),
        data: Banner.listFromJson(json[r'data']),
    );

  static List<BannerListBR> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <BannerListBR>[]
      : json.map((v) => BannerListBR.fromJson(v)).toList(growable: true == growable);

  static Map<String, BannerListBR> mapFromJson(Map<String, dynamic> json) {
    final map = <String, BannerListBR>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = BannerListBR.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of BannerListBR-objects as value to a dart map
  static Map<String, List<BannerListBR>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<BannerListBR>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = BannerListBR.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

