//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CoordinateSequenceFactory {
  /// Returns a new [CoordinateSequenceFactory] instance.
  CoordinateSequenceFactory({
    this.ordinates,
  });

  Ordinates ordinates;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CoordinateSequenceFactory &&
     other.ordinates == ordinates;

  @override
  int get hashCode =>
    (ordinates == null ? 0 : ordinates.hashCode);

  @override
  String toString() => 'CoordinateSequenceFactory[ordinates=$ordinates]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (ordinates != null) {
      json[r'ordinates'] = ordinates;
    }
    return json;
  }

  /// Returns a new [CoordinateSequenceFactory] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CoordinateSequenceFactory fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CoordinateSequenceFactory(
        ordinates: Ordinates.fromJson(json[r'ordinates']),
    );

  static List<CoordinateSequenceFactory> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CoordinateSequenceFactory>[]
      : json.map((v) => CoordinateSequenceFactory.fromJson(v)).toList(growable: true == growable);

  static Map<String, CoordinateSequenceFactory> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CoordinateSequenceFactory>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CoordinateSequenceFactory.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CoordinateSequenceFactory-objects as value to a dart map
  static Map<String, List<CoordinateSequenceFactory>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CoordinateSequenceFactory>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CoordinateSequenceFactory.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

