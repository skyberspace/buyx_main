//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     0 = All    1 = Placed    2 = Preparing    3 = Prepared    4 = Delivering    5 = Complete    6 = Canceled    -2 = Fail    -1 = NotPaidFor
class OrderState {
  /// Instantiate a new enum with the provided [value].
  const OrderState._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number0 = OrderState._(0);
  static const number1 = OrderState._(1);
  static const number2 = OrderState._(2);
  static const number3 = OrderState._(3);
  static const number4 = OrderState._(4);
  static const number5 = OrderState._(5);
  static const number6 = OrderState._(6);
  static const numberNegative2 = OrderState._(-2);
  static const numberNegative1 = OrderState._(-1);

  /// List of all possible values in this [enum][OrderState].
  static const values = <OrderState>[
    number0,
    number1,
    number2,
    number3,
    number4,
    number5,
    number6,
    numberNegative2,
    numberNegative1,
  ];

  static OrderState fromJson(dynamic value) =>
    OrderStateTypeTransformer().decode(value);

  static List<OrderState> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <OrderState>[]
      : json
          .map((value) => OrderState.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [OrderState] to int,
/// and [decode] dynamic data back to [OrderState].
class OrderStateTypeTransformer {
  const OrderStateTypeTransformer._();

  factory OrderStateTypeTransformer() => _instance ??= OrderStateTypeTransformer._();

  int encode(OrderState data) => data.value;

  /// Decodes a [dynamic value][data] to a OrderState.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  OrderState decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 0: return OrderState.number0;
      case 1: return OrderState.number1;
      case 2: return OrderState.number2;
      case 3: return OrderState.number3;
      case 4: return OrderState.number4;
      case 5: return OrderState.number5;
      case 6: return OrderState.number6;
      case -2: return OrderState.numberNegative2;
      case -1: return OrderState.numberNegative1;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [OrderStateTypeTransformer] instance.
  static OrderStateTypeTransformer _instance;
}
