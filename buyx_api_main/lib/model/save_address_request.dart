//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SaveAddressRequest {
  /// Returns a new [SaveAddressRequest] instance.
  SaveAddressRequest({
    this.address,
    this.setLocation,
  });

  Address address;

  FrontendPoint setLocation;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SaveAddressRequest &&
     other.address == address &&
     other.setLocation == setLocation;

  @override
  int get hashCode =>
    (address == null ? 0 : address.hashCode) +
    (setLocation == null ? 0 : setLocation.hashCode);

  @override
  String toString() => 'SaveAddressRequest[address=$address, setLocation=$setLocation]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (address != null) {
      json[r'address'] = address;
    }
    if (setLocation != null) {
      json[r'setLocation'] = setLocation;
    }
    return json;
  }

  /// Returns a new [SaveAddressRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SaveAddressRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : SaveAddressRequest(
        address: Address.fromJson(json[r'address']),
        setLocation: FrontendPoint.fromJson(json[r'setLocation']),
    );

  static List<SaveAddressRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <SaveAddressRequest>[]
      : json.map((v) => SaveAddressRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, SaveAddressRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SaveAddressRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = SaveAddressRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of SaveAddressRequest-objects as value to a dart map
  static Map<String, List<SaveAddressRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<SaveAddressRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = SaveAddressRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

