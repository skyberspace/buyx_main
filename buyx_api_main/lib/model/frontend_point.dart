//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class FrontendPoint {
  /// Returns a new [FrontendPoint] instance.
  FrontendPoint({
    this.latitude,
    this.longitude,
  });

  double latitude;

  double longitude;

  @override
  bool operator ==(Object other) => identical(this, other) || other is FrontendPoint &&
     other.latitude == latitude &&
     other.longitude == longitude;

  @override
  int get hashCode =>
    (latitude == null ? 0 : latitude.hashCode) +
    (longitude == null ? 0 : longitude.hashCode);

  @override
  String toString() => 'FrontendPoint[latitude=$latitude, longitude=$longitude]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (latitude != null) {
      json[r'latitude'] = latitude;
    }
    if (longitude != null) {
      json[r'longitude'] = longitude;
    }
    return json;
  }

  /// Returns a new [FrontendPoint] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static FrontendPoint fromJson(Map<String, dynamic> json) => json == null
    ? null
    : FrontendPoint(
        latitude: json[r'latitude'],
        longitude: json[r'longitude'],
    );

  static List<FrontendPoint> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <FrontendPoint>[]
      : json.map((v) => FrontendPoint.fromJson(v)).toList(growable: true == growable);

  static Map<String, FrontendPoint> mapFromJson(Map<String, dynamic> json) {
    final map = <String, FrontendPoint>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = FrontendPoint.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of FrontendPoint-objects as value to a dart map
  static Map<String, List<FrontendPoint>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<FrontendPoint>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = FrontendPoint.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

