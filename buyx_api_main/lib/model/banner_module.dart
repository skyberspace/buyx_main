//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class BannerModule {
  /// Returns a new [BannerModule] instance.
  BannerModule({
    this.id,
    this.moduleName,
    this.banners,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String moduleName;

  List<Banner> banners;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is BannerModule &&
     other.id == id &&
     other.moduleName == moduleName &&
     other.banners == banners &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (moduleName == null ? 0 : moduleName.hashCode) +
    (banners == null ? 0 : banners.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'BannerModule[id=$id, moduleName=$moduleName, banners=$banners, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (moduleName != null) {
      json[r'moduleName'] = moduleName;
    }
    if (banners != null) {
      json[r'banners'] = banners;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [BannerModule] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static BannerModule fromJson(Map<String, dynamic> json) => json == null
    ? null
    : BannerModule(
        id: json[r'id'],
        moduleName: json[r'moduleName'],
        banners: Banner.listFromJson(json[r'banners']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<BannerModule> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <BannerModule>[]
      : json.map((v) => BannerModule.fromJson(v)).toList(growable: true == growable);

  static Map<String, BannerModule> mapFromJson(Map<String, dynamic> json) {
    final map = <String, BannerModule>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = BannerModule.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of BannerModule-objects as value to a dart map
  static Map<String, List<BannerModule>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<BannerModule>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = BannerModule.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

