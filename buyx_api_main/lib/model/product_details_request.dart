//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ProductDetailsRequest {
  /// Returns a new [ProductDetailsRequest] instance.
  ProductDetailsRequest({
    this.productId,
  });

  int productId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProductDetailsRequest &&
     other.productId == productId;

  @override
  int get hashCode =>
    (productId == null ? 0 : productId.hashCode);

  @override
  String toString() => 'ProductDetailsRequest[productId=$productId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (productId != null) {
      json[r'productId'] = productId;
    }
    return json;
  }

  /// Returns a new [ProductDetailsRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ProductDetailsRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ProductDetailsRequest(
        productId: json[r'productId'],
    );

  static List<ProductDetailsRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ProductDetailsRequest>[]
      : json.map((v) => ProductDetailsRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, ProductDetailsRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ProductDetailsRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ProductDetailsRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ProductDetailsRequest-objects as value to a dart map
  static Map<String, List<ProductDetailsRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ProductDetailsRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ProductDetailsRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

