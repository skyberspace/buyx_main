//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CreateOrderRequest {
  /// Returns a new [CreateOrderRequest] instance.
  CreateOrderRequest({
    this.campaignId,
    this.note,
  });

  int campaignId;

  String note;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CreateOrderRequest &&
     other.campaignId == campaignId &&
     other.note == note;

  @override
  int get hashCode =>
    (campaignId == null ? 0 : campaignId.hashCode) +
    (note == null ? 0 : note.hashCode);

  @override
  String toString() => 'CreateOrderRequest[campaignId=$campaignId, note=$note]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (campaignId != null) {
      json[r'campaignId'] = campaignId;
    }
    if (note != null) {
      json[r'note'] = note;
    }
    return json;
  }

  /// Returns a new [CreateOrderRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CreateOrderRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CreateOrderRequest(
        campaignId: json[r'campaignId'],
        note: json[r'note'],
    );

  static List<CreateOrderRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CreateOrderRequest>[]
      : json.map((v) => CreateOrderRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, CreateOrderRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CreateOrderRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CreateOrderRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CreateOrderRequest-objects as value to a dart map
  static Map<String, List<CreateOrderRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CreateOrderRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CreateOrderRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

