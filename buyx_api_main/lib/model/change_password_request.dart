//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ChangePasswordRequest {
  /// Returns a new [ChangePasswordRequest] instance.
  ChangePasswordRequest({
    this.oldPassword,
    this.newPassword,
  });

  String oldPassword;

  String newPassword;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ChangePasswordRequest &&
     other.oldPassword == oldPassword &&
     other.newPassword == newPassword;

  @override
  int get hashCode =>
    (oldPassword == null ? 0 : oldPassword.hashCode) +
    (newPassword == null ? 0 : newPassword.hashCode);

  @override
  String toString() => 'ChangePasswordRequest[oldPassword=$oldPassword, newPassword=$newPassword]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (oldPassword != null) {
      json[r'oldPassword'] = oldPassword;
    }
    if (newPassword != null) {
      json[r'newPassword'] = newPassword;
    }
    return json;
  }

  /// Returns a new [ChangePasswordRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ChangePasswordRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ChangePasswordRequest(
        oldPassword: json[r'oldPassword'],
        newPassword: json[r'newPassword'],
    );

  static List<ChangePasswordRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ChangePasswordRequest>[]
      : json.map((v) => ChangePasswordRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, ChangePasswordRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ChangePasswordRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ChangePasswordRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ChangePasswordRequest-objects as value to a dart map
  static Map<String, List<ChangePasswordRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ChangePasswordRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ChangePasswordRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

