//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     0 = Point    1 = Curve    2 = Surface    -3 = Dontcare    -2 = True    -1 = False
class Dimension {
  /// Instantiate a new enum with the provided [value].
  const Dimension._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number0 = Dimension._(0);
  static const number1 = Dimension._(1);
  static const number2 = Dimension._(2);
  static const numberNegative3 = Dimension._(-3);
  static const numberNegative2 = Dimension._(-2);
  static const numberNegative1 = Dimension._(-1);

  /// List of all possible values in this [enum][Dimension].
  static const values = <Dimension>[
    number0,
    number1,
    number2,
    numberNegative3,
    numberNegative2,
    numberNegative1,
  ];

  static Dimension fromJson(dynamic value) =>
    DimensionTypeTransformer().decode(value);

  static List<Dimension> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Dimension>[]
      : json
          .map((value) => Dimension.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [Dimension] to int,
/// and [decode] dynamic data back to [Dimension].
class DimensionTypeTransformer {
  const DimensionTypeTransformer._();

  factory DimensionTypeTransformer() => _instance ??= DimensionTypeTransformer._();

  int encode(Dimension data) => data.value;

  /// Decodes a [dynamic value][data] to a Dimension.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  Dimension decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 0: return Dimension.number0;
      case 1: return Dimension.number1;
      case 2: return Dimension.number2;
      case -3: return Dimension.numberNegative3;
      case -2: return Dimension.numberNegative2;
      case -1: return Dimension.numberNegative1;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [DimensionTypeTransformer] instance.
  static DimensionTypeTransformer _instance;
}
