//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class WebHookReceivedMessage {
  /// Returns a new [WebHookReceivedMessage] instance.
  WebHookReceivedMessage({
    this.id,
    this.type,
    this.from,
    this.to,
    this.body,
    this.encoding,
    this.protocolId,
    this.messageClass,
    this.numberOfParts,
    this.creditCost,
    this.submission,
    this.status,
    this.relatedSentMessageId,
    this.userSuppliedId,
  });

  String id;

  String type;

  String from;

  String to;

  String body;

  String encoding;

  int protocolId;

  int messageClass;

  int numberOfParts;

  int creditCost;

  Submission submission;

  Status status;

  String relatedSentMessageId;

  String userSuppliedId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is WebHookReceivedMessage &&
     other.id == id &&
     other.type == type &&
     other.from == from &&
     other.to == to &&
     other.body == body &&
     other.encoding == encoding &&
     other.protocolId == protocolId &&
     other.messageClass == messageClass &&
     other.numberOfParts == numberOfParts &&
     other.creditCost == creditCost &&
     other.submission == submission &&
     other.status == status &&
     other.relatedSentMessageId == relatedSentMessageId &&
     other.userSuppliedId == userSuppliedId;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (type == null ? 0 : type.hashCode) +
    (from == null ? 0 : from.hashCode) +
    (to == null ? 0 : to.hashCode) +
    (body == null ? 0 : body.hashCode) +
    (encoding == null ? 0 : encoding.hashCode) +
    (protocolId == null ? 0 : protocolId.hashCode) +
    (messageClass == null ? 0 : messageClass.hashCode) +
    (numberOfParts == null ? 0 : numberOfParts.hashCode) +
    (creditCost == null ? 0 : creditCost.hashCode) +
    (submission == null ? 0 : submission.hashCode) +
    (status == null ? 0 : status.hashCode) +
    (relatedSentMessageId == null ? 0 : relatedSentMessageId.hashCode) +
    (userSuppliedId == null ? 0 : userSuppliedId.hashCode);

  @override
  String toString() => 'WebHookReceivedMessage[id=$id, type=$type, from=$from, to=$to, body=$body, encoding=$encoding, protocolId=$protocolId, messageClass=$messageClass, numberOfParts=$numberOfParts, creditCost=$creditCost, submission=$submission, status=$status, relatedSentMessageId=$relatedSentMessageId, userSuppliedId=$userSuppliedId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (type != null) {
      json[r'type'] = type;
    }
    if (from != null) {
      json[r'from'] = from;
    }
    if (to != null) {
      json[r'to'] = to;
    }
    if (body != null) {
      json[r'body'] = body;
    }
    if (encoding != null) {
      json[r'encoding'] = encoding;
    }
    if (protocolId != null) {
      json[r'protocolId'] = protocolId;
    }
    if (messageClass != null) {
      json[r'messageClass'] = messageClass;
    }
    if (numberOfParts != null) {
      json[r'numberOfParts'] = numberOfParts;
    }
    if (creditCost != null) {
      json[r'creditCost'] = creditCost;
    }
    if (submission != null) {
      json[r'submission'] = submission;
    }
    if (status != null) {
      json[r'status'] = status;
    }
    if (relatedSentMessageId != null) {
      json[r'relatedSentMessageId'] = relatedSentMessageId;
    }
    if (userSuppliedId != null) {
      json[r'userSuppliedId'] = userSuppliedId;
    }
    return json;
  }

  /// Returns a new [WebHookReceivedMessage] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static WebHookReceivedMessage fromJson(Map<String, dynamic> json) => json == null
    ? null
    : WebHookReceivedMessage(
        id: json[r'id'],
        type: json[r'type'],
        from: json[r'from'],
        to: json[r'to'],
        body: json[r'body'],
        encoding: json[r'encoding'],
        protocolId: json[r'protocolId'],
        messageClass: json[r'messageClass'],
        numberOfParts: json[r'numberOfParts'],
        creditCost: json[r'creditCost'],
        submission: Submission.fromJson(json[r'submission']),
        status: Status.fromJson(json[r'status']),
        relatedSentMessageId: json[r'relatedSentMessageId'],
        userSuppliedId: json[r'userSuppliedId'],
    );

  static List<WebHookReceivedMessage> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <WebHookReceivedMessage>[]
      : json.map((v) => WebHookReceivedMessage.fromJson(v)).toList(growable: true == growable);

  static Map<String, WebHookReceivedMessage> mapFromJson(Map<String, dynamic> json) {
    final map = <String, WebHookReceivedMessage>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = WebHookReceivedMessage.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of WebHookReceivedMessage-objects as value to a dart map
  static Map<String, List<WebHookReceivedMessage>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<WebHookReceivedMessage>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = WebHookReceivedMessage.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

