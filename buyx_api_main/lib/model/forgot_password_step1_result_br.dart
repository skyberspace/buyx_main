//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ForgotPasswordStep1ResultBR {
  /// Returns a new [ForgotPasswordStep1ResultBR] instance.
  ForgotPasswordStep1ResultBR({
    this.success,
    this.errors,
    this.infos,
    this.data,
  });

  bool success;

  List<String> errors;

  List<String> infos;

  ForgotPasswordStep1Result data;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ForgotPasswordStep1ResultBR &&
     other.success == success &&
     other.errors == errors &&
     other.infos == infos &&
     other.data == data;

  @override
  int get hashCode =>
    (success == null ? 0 : success.hashCode) +
    (errors == null ? 0 : errors.hashCode) +
    (infos == null ? 0 : infos.hashCode) +
    (data == null ? 0 : data.hashCode);

  @override
  String toString() => 'ForgotPasswordStep1ResultBR[success=$success, errors=$errors, infos=$infos, data=$data]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (success != null) {
      json[r'success'] = success;
    }
    if (errors != null) {
      json[r'errors'] = errors;
    }
    if (infos != null) {
      json[r'infos'] = infos;
    }
    if (data != null) {
      json[r'data'] = data;
    }
    return json;
  }

  /// Returns a new [ForgotPasswordStep1ResultBR] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ForgotPasswordStep1ResultBR fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ForgotPasswordStep1ResultBR(
        success: json[r'success'],
        errors: json[r'errors'] == null
          ? null
          : (json[r'errors'] as List).cast<String>(),
        infos: json[r'infos'] == null
          ? null
          : (json[r'infos'] as List).cast<String>(),
        data: ForgotPasswordStep1Result.fromJson(json[r'data']),
    );

  static List<ForgotPasswordStep1ResultBR> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ForgotPasswordStep1ResultBR>[]
      : json.map((v) => ForgotPasswordStep1ResultBR.fromJson(v)).toList(growable: true == growable);

  static Map<String, ForgotPasswordStep1ResultBR> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ForgotPasswordStep1ResultBR>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ForgotPasswordStep1ResultBR.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ForgotPasswordStep1ResultBR-objects as value to a dart map
  static Map<String, List<ForgotPasswordStep1ResultBR>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ForgotPasswordStep1ResultBR>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ForgotPasswordStep1ResultBR.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

