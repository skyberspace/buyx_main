//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class GetBlogEntriesRequest {
  /// Returns a new [GetBlogEntriesRequest] instance.
  GetBlogEntriesRequest({
    this.blogType,
  });

  BlogType blogType;

  @override
  bool operator ==(Object other) => identical(this, other) || other is GetBlogEntriesRequest &&
     other.blogType == blogType;

  @override
  int get hashCode =>
    (blogType == null ? 0 : blogType.hashCode);

  @override
  String toString() => 'GetBlogEntriesRequest[blogType=$blogType]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (blogType != null) {
      json[r'blogType'] = blogType;
    }
    return json;
  }

  /// Returns a new [GetBlogEntriesRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static GetBlogEntriesRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : GetBlogEntriesRequest(
        blogType: BlogType.fromJson(json[r'blogType']),
    );

  static List<GetBlogEntriesRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <GetBlogEntriesRequest>[]
      : json.map((v) => GetBlogEntriesRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, GetBlogEntriesRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, GetBlogEntriesRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = GetBlogEntriesRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of GetBlogEntriesRequest-objects as value to a dart map
  static Map<String, List<GetBlogEntriesRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<GetBlogEntriesRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = GetBlogEntriesRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

