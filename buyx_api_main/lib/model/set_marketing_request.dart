//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class SetMarketingRequest {
  /// Returns a new [SetMarketingRequest] instance.
  SetMarketingRequest({
    this.key,
    this.value,
  });

  String key;

  bool value;

  @override
  bool operator ==(Object other) => identical(this, other) || other is SetMarketingRequest &&
     other.key == key &&
     other.value == value;

  @override
  int get hashCode =>
    (key == null ? 0 : key.hashCode) +
    (value == null ? 0 : value.hashCode);

  @override
  String toString() => 'SetMarketingRequest[key=$key, value=$value]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (key != null) {
      json[r'key'] = key;
    }
    if (value != null) {
      json[r'value'] = value;
    }
    return json;
  }

  /// Returns a new [SetMarketingRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static SetMarketingRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : SetMarketingRequest(
        key: json[r'key'],
        value: json[r'value'],
    );

  static List<SetMarketingRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <SetMarketingRequest>[]
      : json.map((v) => SetMarketingRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, SetMarketingRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, SetMarketingRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = SetMarketingRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of SetMarketingRequest-objects as value to a dart map
  static Map<String, List<SetMarketingRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<SetMarketingRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = SetMarketingRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

