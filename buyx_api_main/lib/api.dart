//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

library openapi.api;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:intl/intl.dart';

import 'package:meta/meta.dart';

part 'api_client.dart';
part 'api_helper.dart';
part 'api_exception.dart';
part 'auth/authentication.dart';
part 'auth/api_key_auth.dart';
part 'auth/oauth.dart';
part 'auth/http_basic_auth.dart';
part 'auth/http_bearer_auth.dart';

part 'api/main_app_auth_api.dart';
part 'api/main_app_cart_api.dart';
part 'api/main_app_catalog_api.dart';
part 'api/main_app_cdn_api.dart';
part 'api/main_app_layout_api.dart';
part 'api/main_app_profile_api.dart';

part 'model/add_to_cart_request.dart';
part 'model/address.dart';
part 'model/address_br.dart';
part 'model/address_list_br.dart';
part 'model/address_request.dart';
part 'model/admin_user.dart';
part 'model/auth_token.dart';
part 'model/auth_token_br.dart';
part 'model/br.dart';
part 'model/banner.dart';
part 'model/banner_list_br.dart';
part 'model/banner_module.dart';
part 'model/blog_entry.dart';
part 'model/blog_entry_list_br.dart';
part 'model/blog_type.dart';
part 'model/boolean_br.dart';
part 'model/cart_campaign.dart';
part 'model/cart_campaign_operation.dart';
part 'model/cart_campaign_rule.dart';
part 'model/cart_campaign_rule_type.dart';
part 'model/cart_item.dart';
part 'model/cart_item_br.dart';
part 'model/cart_item_list_br.dart';
part 'model/category.dart';
part 'model/category_list_br.dart';
part 'model/change_informtion_request.dart';
part 'model/change_password_request.dart';
part 'model/checkout_details.dart';
part 'model/checkout_details_br.dart';
part 'model/checkout_details_request.dart';
part 'model/coordinate.dart';
part 'model/coordinate_sequence.dart';
part 'model/coordinate_sequence_factory.dart';
part 'model/create_order_request.dart';
part 'model/create_order_request_with_payment.dart';
part 'model/dimension.dart';
part 'model/discount_effect.dart';
part 'model/discount_type.dart';
part 'model/envelope.dart';
part 'model/forgot_password_step1_request.dart';
part 'model/forgot_password_step1_result.dart';
part 'model/forgot_password_step1_result_br.dart';
part 'model/forgot_password_step2_request.dart';
part 'model/frontend_point.dart';
part 'model/geometry.dart';
part 'model/geometry_factory.dart';
part 'model/get_blog_entries_request.dart';
part 'model/get_sub_categories_request.dart';
part 'model/id_request.dart';
part 'model/login_request.dart';
part 'model/object_br.dart';
part 'model/ogc_geometry_type.dart';
part 'model/order.dart';
part 'model/order_address.dart';
part 'model/order_br.dart';
part 'model/order_line.dart';
part 'model/order_list_br.dart';
part 'model/order_state.dart';
part 'model/order_state_history.dart';
part 'model/order_type.dart';
part 'model/ordinates.dart';
part 'model/payment_log.dart';
part 'model/payment_sheet_data.dart';
part 'model/payment_sheet_data_br.dart';
part 'model/payment_type.dart';
part 'model/permissions.dart';
part 'model/point.dart';
part 'model/precision_model.dart';
part 'model/precision_models.dart';
part 'model/product.dart';
part 'model/product_br.dart';
part 'model/product_category.dart';
part 'model/product_details_request.dart';
part 'model/product_image.dart';
part 'model/product_search_ranked.dart';
part 'model/product_search_ranked_list_br.dart';
part 'model/product_warehouse_stock.dart';
part 'model/register_request.dart';
part 'model/remove_from_cart_request.dart';
part 'model/save_address_request.dart';
part 'model/set_fcm_token_request.dart';
part 'model/set_marketing_request.dart';
part 'model/set_order_stars_request.dart';
part 'model/status.dart';
part 'model/string_br.dart';
part 'model/string_filter_request.dart';
part 'model/submission.dart';
part 'model/user.dart';
part 'model/user_br.dart';
part 'model/user_favorite.dart';
part 'model/user_favorite_br.dart';
part 'model/user_favorite_list_br.dart';
part 'model/user_favorite_request.dart';
part 'model/warehouse.dart';
part 'model/warehouse_role.dart';
part 'model/warehouse_user.dart';
part 'model/web_hook_received_message.dart';


const _delimiters = {'csv': ',', 'ssv': ' ', 'tsv': '\t', 'pipes': '|'};
const _dateEpochMarker = 'epoch';
final _dateFormatter = DateFormat('yyyy-MM-dd');
final _regList = RegExp(r'^List<(.*)>$');
final _regSet = RegExp(r'^Set<(.*)>$');
final _regMap = RegExp(r'^Map<String,(.*)>$');

ApiClient defaultApiClient = ApiClient();
