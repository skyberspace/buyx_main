# buyx_api_main.model.WebHookReceivedMessage

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**from** | **String** |  | [optional] 
**to** | **String** |  | [optional] 
**body** | **String** |  | [optional] 
**encoding** | **String** |  | [optional] 
**protocolId** | **int** |  | [optional] 
**messageClass** | **int** |  | [optional] 
**numberOfParts** | **int** |  | [optional] 
**creditCost** | **int** |  | [optional] 
**submission** | [**Submission**](Submission.md) |  | [optional] 
**status** | [**Status**](Status.md) |  | [optional] 
**relatedSentMessageId** | **String** |  | [optional] 
**userSuppliedId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


