# buyx_api_main.model.OrderStateHistory

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**orderState** | [**OrderState**](OrderState.md) |  | [optional] 
**orderId** | **int** |  | [optional] 
**isuuerId** | **int** |  | [optional] 
**order** | [**Order**](Order.md) |  | [optional] 
**issuer** | [**User**](User.md) |  | [optional] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


