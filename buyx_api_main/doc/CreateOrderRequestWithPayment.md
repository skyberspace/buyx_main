# buyx_api_main.model.CreateOrderRequestWithPayment

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**campaignId** | **int** |  | [optional] 
**note** | **String** |  | [optional] 
**paymentType** | [**PaymentType**](PaymentType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


