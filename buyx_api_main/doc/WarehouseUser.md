# buyx_api_main.model.WarehouseUser

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**warehouseRole** | [**WarehouseRole**](WarehouseRole.md) |  | [optional] 
**firebaseCloudMessagingToken** | **String** |  | [optional] 
**canSeeWarehouseOrders** | **bool** |  | [optional] 
**canSeeRestaurantOrders** | **bool** |  | [optional] 
**warehouse** | [**Warehouse**](Warehouse.md) |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**warehouseId** | **int** |  | [optional] 
**userId** | **int** |  | [optional] 
**imageUrl** | **String** |  | [optional] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


