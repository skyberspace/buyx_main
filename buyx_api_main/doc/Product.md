# buyx_api_main.model.Product

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**price** | **double** |  | [optional] 
**shortDescription** | **String** |  | [optional] 
**minCartCount** | **double** |  | [optional] 
**maxCartCount** | **double** |  | [optional] 
**active** | **bool** |  | [optional] 
**barcode** | **String** |  | [optional] 
**metaTags** | **String** |  | [optional] 
**metaDescription** | **String** |  | [optional] 
**images** | [**List<ProductImage>**](ProductImage.md) |  | [optional] [default to const []]
**stocks** | [**List<ProductWarehouseStock>**](ProductWarehouseStock.md) |  | [optional] [default to const []]
**categories** | [**List<ProductCategory>**](ProductCategory.md) |  | [optional] [default to const []]
**deletedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**isDeleted** | **bool** |  | [optional] [default to false]
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


