# buyx_api_main.api.MainAppCdnApi

## Load the API package
```dart
import 'package:buyx_api_main/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cdnImageSizeGuidNameFriendlyNameGet**](MainAppCdnApi.md#cdnimagesizeguidnamefriendlynameget) | **GET** /cdn/image/{size}/{guidName}/{friendlyName} | returns the given image with guidName and resizes it if hasnt been done before and returns the image in the requested format


# **cdnImageSizeGuidNameFriendlyNameGet**
> cdnImageSizeGuidNameFriendlyNameGet(guidName, size, friendlyName)

returns the given image with guidName and resizes it if hasnt been done before and returns the image in the requested format

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppCdnApi();
final guidName = guidName_example; // String | db image name
final size = size_example; // String | requested size of the image
final friendlyName = friendlyName_example; // String | 

try { 
    api_instance.cdnImageSizeGuidNameFriendlyNameGet(guidName, size, friendlyName);
} catch (e) {
    print('Exception when calling MainAppCdnApi->cdnImageSizeGuidNameFriendlyNameGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guidName** | **String**| db image name | 
 **size** | **String**| requested size of the image | 
 **friendlyName** | **String**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

