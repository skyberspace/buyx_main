# buyx_api_main.api.MainAppCartApi

## Load the API package
```dart
import 'package:buyx_api_main/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cartAddToCartPost**](MainAppCartApi.md#cartaddtocartpost) | **POST** /cart/AddToCart | add product to cart
[**cartCreatePaymentIntentPost**](MainAppCartApi.md#cartcreatepaymentintentpost) | **POST** /cart/CreatePaymentIntent | CreatePaymentIntent
[**cartDeleteOrderPost**](MainAppCartApi.md#cartdeleteorderpost) | **POST** /cart/DeleteOrder | DeleteOrder
[**cartGetActiveOrdersPost**](MainAppCartApi.md#cartgetactiveorderspost) | **POST** /cart/GetActiveOrders | Returns the list of the not completed orders for user
[**cartGetCartItemsPost**](MainAppCartApi.md#cartgetcartitemspost) | **POST** /cart/GetCartItems | get current cart items for user
[**cartGetCheckoutDetailsPost**](MainAppCartApi.md#cartgetcheckoutdetailspost) | **POST** /cart/GetCheckoutDetails | GetCheckoutDetails
[**cartGetOrderHistoryPost**](MainAppCartApi.md#cartgetorderhistorypost) | **POST** /cart/GetOrderHistory | GetOrderhistory
[**cartGetPublicStripeTokenPost**](MainAppCartApi.md#cartgetpublicstripetokenpost) | **POST** /cart/GetPublicStripeToken | GetPublicStripeToken
[**cartPlaceNonProvisionedOrderPost**](MainAppCartApi.md#cartplacenonprovisionedorderpost) | **POST** /cart/PlaceNonProvisionedOrder | PlaceOrder
[**cartRemoveItemFromCartPost**](MainAppCartApi.md#cartremoveitemfromcartpost) | **POST** /cart/RemoveItemFromCart | remove product from cart
[**cartSetOrderStarsPost**](MainAppCartApi.md#cartsetorderstarspost) | **POST** /cart/SetOrderStars | SetOrderStars


# **cartAddToCartPost**
> CartItemBR cartAddToCartPost(addToCartRequest)

add product to cart

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();
final addToCartRequest = AddToCartRequest(); // AddToCartRequest | 

try { 
    final result = api_instance.cartAddToCartPost(addToCartRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartAddToCartPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addToCartRequest** | [**AddToCartRequest**](AddToCartRequest.md)|  | [optional] 

### Return type

[**CartItemBR**](CartItemBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartCreatePaymentIntentPost**
> PaymentSheetDataBR cartCreatePaymentIntentPost(createOrderRequest)

CreatePaymentIntent

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();
final createOrderRequest = CreateOrderRequest(); // CreateOrderRequest | 

try { 
    final result = api_instance.cartCreatePaymentIntentPost(createOrderRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartCreatePaymentIntentPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createOrderRequest** | [**CreateOrderRequest**](CreateOrderRequest.md)|  | [optional] 

### Return type

[**PaymentSheetDataBR**](PaymentSheetDataBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartDeleteOrderPost**
> BR cartDeleteOrderPost(idRequest)

DeleteOrder

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();
final idRequest = IdRequest(); // IdRequest | 

try { 
    final result = api_instance.cartDeleteOrderPost(idRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartDeleteOrderPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRequest** | [**IdRequest**](IdRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartGetActiveOrdersPost**
> OrderListBR cartGetActiveOrdersPost()

Returns the list of the not completed orders for user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();

try { 
    final result = api_instance.cartGetActiveOrdersPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartGetActiveOrdersPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OrderListBR**](OrderListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartGetCartItemsPost**
> CartItemListBR cartGetCartItemsPost()

get current cart items for user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();

try { 
    final result = api_instance.cartGetCartItemsPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartGetCartItemsPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CartItemListBR**](CartItemListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartGetCheckoutDetailsPost**
> CheckoutDetailsBR cartGetCheckoutDetailsPost(checkoutDetailsRequest)

GetCheckoutDetails

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();
final checkoutDetailsRequest = CheckoutDetailsRequest(); // CheckoutDetailsRequest | 

try { 
    final result = api_instance.cartGetCheckoutDetailsPost(checkoutDetailsRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartGetCheckoutDetailsPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkoutDetailsRequest** | [**CheckoutDetailsRequest**](CheckoutDetailsRequest.md)|  | [optional] 

### Return type

[**CheckoutDetailsBR**](CheckoutDetailsBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartGetOrderHistoryPost**
> OrderListBR cartGetOrderHistoryPost()

GetOrderhistory

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();

try { 
    final result = api_instance.cartGetOrderHistoryPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartGetOrderHistoryPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OrderListBR**](OrderListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartGetPublicStripeTokenPost**
> StringBR cartGetPublicStripeTokenPost()

GetPublicStripeToken

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();

try { 
    final result = api_instance.cartGetPublicStripeTokenPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartGetPublicStripeTokenPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StringBR**](StringBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartPlaceNonProvisionedOrderPost**
> OrderBR cartPlaceNonProvisionedOrderPost(createOrderRequestWithPayment)

PlaceOrder

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();
final createOrderRequestWithPayment = CreateOrderRequestWithPayment(); // CreateOrderRequestWithPayment | 

try { 
    final result = api_instance.cartPlaceNonProvisionedOrderPost(createOrderRequestWithPayment);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartPlaceNonProvisionedOrderPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createOrderRequestWithPayment** | [**CreateOrderRequestWithPayment**](CreateOrderRequestWithPayment.md)|  | [optional] 

### Return type

[**OrderBR**](OrderBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartRemoveItemFromCartPost**
> ObjectBR cartRemoveItemFromCartPost(removeFromCartRequest)

remove product from cart

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();
final removeFromCartRequest = RemoveFromCartRequest(); // RemoveFromCartRequest | 

try { 
    final result = api_instance.cartRemoveItemFromCartPost(removeFromCartRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartRemoveItemFromCartPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **removeFromCartRequest** | [**RemoveFromCartRequest**](RemoveFromCartRequest.md)|  | [optional] 

### Return type

[**ObjectBR**](ObjectBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cartSetOrderStarsPost**
> BR cartSetOrderStarsPost(setOrderStarsRequest)

SetOrderStars

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppCartApi();
final setOrderStarsRequest = SetOrderStarsRequest(); // SetOrderStarsRequest | 

try { 
    final result = api_instance.cartSetOrderStarsPost(setOrderStarsRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCartApi->cartSetOrderStarsPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setOrderStarsRequest** | [**SetOrderStarsRequest**](SetOrderStarsRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

