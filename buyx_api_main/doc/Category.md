# buyx_api_main.model.Category

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **String** |  | [optional] 
**shortDesc** | **String** |  | [optional] 
**imagePath** | **String** |  | [optional] 
**active** | **bool** |  | [optional] 
**order** | **int** |  | [optional] 
**parentCategoryId** | **int** |  | [optional] 
**parentCategory** | [**Category**](Category.md) |  | [optional] 
**products** | [**List<ProductCategory>**](ProductCategory.md) |  | [optional] [default to const []]
**subCategories** | [**List<Category>**](Category.md) |  | [optional] [default to const []]
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


