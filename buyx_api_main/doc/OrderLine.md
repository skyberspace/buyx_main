# buyx_api_main.model.OrderLine

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**productNameCache** | **String** |  | [optional] 
**productBarcodeCache** | **String** |  | [optional] 
**price** | **double** |  | [optional] 
**tax** | **double** |  | [optional] 
**returned** | **bool** |  | [optional] 
**canceled** | **bool** |  | [optional] 
**quantity** | **double** |  | [optional] 
**productId** | **int** |  | [optional] 
**orderId** | **int** |  | [optional] 
**productWarehouseStockId** | **int** |  | [optional] 
**order** | [**Order**](Order.md) |  | [optional] 
**product** | [**Product**](Product.md) |  | [optional] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


