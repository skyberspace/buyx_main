# buyx_api_main.model.Coordinate

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x** | **double** |  | [optional] 
**y** | **double** |  | [optional] 
**z** | **double** |  | [optional] 
**m** | **double** |  | [optional] 
**coordinateValue** | [**Coordinate**](Coordinate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


