# buyx_api_main.model.CartCampaignRule

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entityId** | **int** |  | [optional] 
**ruleType** | [**CartCampaignRuleType**](CartCampaignRuleType.md) |  | [optional] 
**operation** | [**CartCampaignOperation**](CartCampaignOperation.md) |  | [optional] 
**checkAgainst** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


