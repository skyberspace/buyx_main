# buyx_api_main.model.User

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**setPassword** | **String** |  | [optional] 
**id** | **int** |  | [optional] 
**email** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**phoneVerified** | **bool** |  | [optional] 
**marketingEmails** | **bool** |  | [optional] 
**marketingNotifications** | **bool** |  | [optional] 
**marketingSMS** | **bool** |  | [optional] 
**marketingCalls** | **bool** |  | [optional] 
**isGuest** | **bool** |  | [optional] 
**canPlaceNonProvisionedPaymentMethods** | **bool** |  | [optional] 
**stripeUserId** | **String** |  | [optional] 
**adminUser** | [**AdminUser**](AdminUser.md) |  | [optional] 
**warehouseUser** | [**WarehouseUser**](WarehouseUser.md) |  | [optional] 
**addresses** | [**List<Address>**](Address.md) |  | [optional] [default to const []]
**favorites** | [**List<UserFavorite>**](UserFavorite.md) |  | [optional] [default to const []]
**fcmToken** | **String** |  | [optional] 
**tenantRoleGroup** | **int** | used for multitenant module. If set, this automatically manages what this user can see and do  represents the tenant group Id for the user | [optional] 
**isAdmin** | **bool** |  | [optional] [readonly] 
**isWarehouseUser** | **bool** |  | [optional] [readonly] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


