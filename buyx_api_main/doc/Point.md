# buyx_api_main.model.Point

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**coordinateSequence** | [**CoordinateSequence**](CoordinateSequence.md) |  | [optional] 
**coordinates** | [**List<Coordinate>**](Coordinate.md) |  | [optional] [default to const []]
**numPoints** | **int** |  | [optional] [readonly] 
**isEmpty** | **bool** |  | [optional] [readonly] 
**dimension** | [**Dimension**](Dimension.md) |  | [optional] 
**boundaryDimension** | [**Dimension**](Dimension.md) |  | [optional] 
**x** | **double** |  | [optional] 
**y** | **double** |  | [optional] 
**coordinate** | [**Coordinate**](Coordinate.md) |  | [optional] 
**geometryType** | **String** |  | [optional] [readonly] 
**ogcGeometryType** | [**OgcGeometryType**](OgcGeometryType.md) |  | [optional] 
**boundary** | [**Geometry**](Geometry.md) |  | [optional] 
**z** | **double** |  | [optional] 
**m** | **double** |  | [optional] 
**factory_** | [**GeometryFactory**](GeometryFactory.md) |  | [optional] 
**userData** | [**Object**](.md) |  | [optional] 
**srid** | **int** |  | [optional] 
**precisionModel** | [**PrecisionModel**](PrecisionModel.md) |  | [optional] 
**numGeometries** | **int** |  | [optional] [readonly] 
**isSimple** | **bool** |  | [optional] [readonly] 
**isValid** | **bool** |  | [optional] [readonly] 
**area** | **double** |  | [optional] [readonly] 
**length** | **double** |  | [optional] [readonly] 
**centroid** | [**Point**](Point.md) |  | [optional] 
**interiorPoint** | [**Point**](Point.md) |  | [optional] 
**pointOnSurface** | [**Point**](Point.md) |  | [optional] 
**envelope** | [**Geometry**](Geometry.md) |  | [optional] 
**envelopeInternal** | [**Envelope**](Envelope.md) |  | [optional] 
**isRectangle** | **bool** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


