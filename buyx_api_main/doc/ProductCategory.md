# buyx_api_main.model.ProductCategory

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**order** | **int** |  | [optional] 
**productId** | **int** |  | [optional] 
**categoryId** | **int** |  | [optional] 
**product** | [**Product**](Product.md) |  | [optional] 
**category** | [**Category**](Category.md) |  | [optional] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


