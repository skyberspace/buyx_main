# buyx_api_main.model.Address

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**addressName** | **String** |  | [optional] 
**addressLine1** | **String** |  | [optional] 
**zipCode** | **String** |  | [optional] 
**isDefault** | **bool** |  | [optional] 
**buildingNo** | **String** |  | [optional] 
**floor** | **String** |  | [optional] 
**flat** | **String** |  | [optional] 
**directions** | **String** |  | [optional] 
**latitude** | **double** |  | [optional] 
**longitude** | **double** |  | [optional] 
**userId** | **int** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


