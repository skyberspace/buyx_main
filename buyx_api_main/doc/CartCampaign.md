# buyx_api_main.model.CartCampaign

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **String** |  | [optional] 
**discountType** | [**DiscountType**](DiscountType.md) |  | [optional] 
**discountEffect** | [**DiscountEffect**](DiscountEffect.md) |  | [optional] 
**discountAmount** | **double** |  | [optional] 
**totalUsed** | **int** |  | [optional] 
**maxUseCount** | **int** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**cartCampaignRules** | [**List<CartCampaignRule>**](CartCampaignRule.md) |  | [optional] [default to const []]
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


