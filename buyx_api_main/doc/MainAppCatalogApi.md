# buyx_api_main.api.MainAppCatalogApi

## Load the API package
```dart
import 'package:buyx_api_main/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogGetProductDetailsPost**](MainAppCatalogApi.md#cataloggetproductdetailspost) | **POST** /catalog/GetProductDetails | get product details
[**catalogGetRootCategoriesPost**](MainAppCatalogApi.md#cataloggetrootcategoriespost) | **POST** /catalog/GetRootCategories | gets the categories for mainapp homepage
[**catalogGetSubCategoriesFromPost**](MainAppCatalogApi.md#cataloggetsubcategoriesfrompost) | **POST** /catalog/GetSubCategoriesFrom | returns the categories for mainapp category detail page
[**catalogSearchProductsPost**](MainAppCatalogApi.md#catalogsearchproductspost) | **POST** /catalog/SearchProducts | SearchProducts with a text filter


# **catalogGetProductDetailsPost**
> ProductBR catalogGetProductDetailsPost(productDetailsRequest)

get product details

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppCatalogApi();
final productDetailsRequest = ProductDetailsRequest(); // ProductDetailsRequest | 

try { 
    final result = api_instance.catalogGetProductDetailsPost(productDetailsRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCatalogApi->catalogGetProductDetailsPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productDetailsRequest** | [**ProductDetailsRequest**](ProductDetailsRequest.md)|  | [optional] 

### Return type

[**ProductBR**](ProductBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogGetRootCategoriesPost**
> CategoryListBR catalogGetRootCategoriesPost()

gets the categories for mainapp homepage

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppCatalogApi();

try { 
    final result = api_instance.catalogGetRootCategoriesPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppCatalogApi->catalogGetRootCategoriesPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CategoryListBR**](CategoryListBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogGetSubCategoriesFromPost**
> CategoryListBR catalogGetSubCategoriesFromPost(getSubCategoriesRequest)

returns the categories for mainapp category detail page

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppCatalogApi();
final getSubCategoriesRequest = GetSubCategoriesRequest(); // GetSubCategoriesRequest | 

try { 
    final result = api_instance.catalogGetSubCategoriesFromPost(getSubCategoriesRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCatalogApi->catalogGetSubCategoriesFromPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getSubCategoriesRequest** | [**GetSubCategoriesRequest**](GetSubCategoriesRequest.md)|  | [optional] 

### Return type

[**CategoryListBR**](CategoryListBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogSearchProductsPost**
> ProductSearchRankedListBR catalogSearchProductsPost(stringFilterRequest)

SearchProducts with a text filter

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppCatalogApi();
final stringFilterRequest = StringFilterRequest(); // StringFilterRequest | 

try { 
    final result = api_instance.catalogSearchProductsPost(stringFilterRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppCatalogApi->catalogSearchProductsPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stringFilterRequest** | [**StringFilterRequest**](StringFilterRequest.md)|  | [optional] 

### Return type

[**ProductSearchRankedListBR**](ProductSearchRankedListBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

