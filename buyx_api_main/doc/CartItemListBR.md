# buyx_api_main.model.CartItemListBR

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** |  | [optional] 
**errors** | **List<String>** |  | [optional] [default to const []]
**infos** | **List<String>** |  | [optional] [default to const []]
**data** | [**List<CartItem>**](CartItem.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


