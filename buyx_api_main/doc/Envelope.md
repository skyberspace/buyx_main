# buyx_api_main.model.Envelope

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isNull** | **bool** |  | [optional] [readonly] 
**width** | **double** |  | [optional] [readonly] 
**height** | **double** |  | [optional] [readonly] 
**minX** | **double** |  | [optional] [readonly] 
**maxX** | **double** |  | [optional] [readonly] 
**minY** | **double** |  | [optional] [readonly] 
**maxY** | **double** |  | [optional] [readonly] 
**area** | **double** |  | [optional] [readonly] 
**minExtent** | **double** |  | [optional] [readonly] 
**maxExtent** | **double** |  | [optional] [readonly] 
**centre** | [**Coordinate**](Coordinate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


