# buyx_api_main.api.MainAppProfileApi

## Load the API package
```dart
import 'package:buyx_api_main/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**profileAddProductToFavoritesPost**](MainAppProfileApi.md#profileaddproducttofavoritespost) | **POST** /profile/AddProductToFavorites | adds the product to current users favorites list
[**profileChangeInformationPost**](MainAppProfileApi.md#profilechangeinformationpost) | **POST** /profile/ChangeInformation | Change email, name,surname Information
[**profileChangePasswordPost**](MainAppProfileApi.md#profilechangepasswordpost) | **POST** /profile/ChangePassword | changes password of the current user
[**profileDeleteAddressPost**](MainAppProfileApi.md#profiledeleteaddresspost) | **POST** /profile/DeleteAddress | deletes the address from current user
[**profileGetDefaultAddressPost**](MainAppProfileApi.md#profilegetdefaultaddresspost) | **POST** /profile/GetDefaultAddress | returns the default address of current user
[**profileGetFavoritesPost**](MainAppProfileApi.md#profilegetfavoritespost) | **POST** /profile/GetFavorites | returns favorites products for the current user
[**profileGetUserAddressesPost**](MainAppProfileApi.md#profilegetuseraddressespost) | **POST** /profile/GetUserAddresses | returns current users addresses
[**profileGetUserPost**](MainAppProfileApi.md#profilegetuserpost) | **POST** /profile/GetUser | returns the current user
[**profileRemoveProductFromFavoritesPost**](MainAppProfileApi.md#profileremoveproductfromfavoritespost) | **POST** /profile/RemoveProductFromFavorites | removes the product from current users favorites list
[**profileSaveAddressPost**](MainAppProfileApi.md#profilesaveaddresspost) | **POST** /profile/SaveAddress | saves address details to current user
[**profileSetDefaultAddressPost**](MainAppProfileApi.md#profilesetdefaultaddresspost) | **POST** /profile/SetDefaultAddress | sets the default address of the user
[**profileSetMarketingChoicesPost**](MainAppProfileApi.md#profilesetmarketingchoicespost) | **POST** /profile/SetMarketingChoices | sets marketing choices for current user  keys are :      MarketingEmails      MarketingNotifications      MarketingSMS      MarketingCalls
[**profileSetUserFCMTokenPost**](MainAppProfileApi.md#profilesetuserfcmtokenpost) | **POST** /profile/SetUserFCMToken | SetUserFCMToken


# **profileAddProductToFavoritesPost**
> UserFavoriteBR profileAddProductToFavoritesPost(productDetailsRequest)

adds the product to current users favorites list

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final productDetailsRequest = ProductDetailsRequest(); // ProductDetailsRequest | 

try { 
    final result = api_instance.profileAddProductToFavoritesPost(productDetailsRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileAddProductToFavoritesPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productDetailsRequest** | [**ProductDetailsRequest**](ProductDetailsRequest.md)|  | [optional] 

### Return type

[**UserFavoriteBR**](UserFavoriteBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileChangeInformationPost**
> BR profileChangeInformationPost(changeInformtionRequest)

Change email, name,surname Information

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final changeInformtionRequest = ChangeInformtionRequest(); // ChangeInformtionRequest | 

try { 
    final result = api_instance.profileChangeInformationPost(changeInformtionRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileChangeInformationPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeInformtionRequest** | [**ChangeInformtionRequest**](ChangeInformtionRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileChangePasswordPost**
> UserBR profileChangePasswordPost(changePasswordRequest)

changes password of the current user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final changePasswordRequest = ChangePasswordRequest(); // ChangePasswordRequest | 

try { 
    final result = api_instance.profileChangePasswordPost(changePasswordRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileChangePasswordPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changePasswordRequest** | [**ChangePasswordRequest**](ChangePasswordRequest.md)|  | [optional] 

### Return type

[**UserBR**](UserBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileDeleteAddressPost**
> BR profileDeleteAddressPost(addressRequest)

deletes the address from current user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final addressRequest = AddressRequest(); // AddressRequest | 

try { 
    final result = api_instance.profileDeleteAddressPost(addressRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileDeleteAddressPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addressRequest** | [**AddressRequest**](AddressRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileGetDefaultAddressPost**
> AddressBR profileGetDefaultAddressPost()

returns the default address of current user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();

try { 
    final result = api_instance.profileGetDefaultAddressPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileGetDefaultAddressPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AddressBR**](AddressBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileGetFavoritesPost**
> UserFavoriteListBR profileGetFavoritesPost()

returns favorites products for the current user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();

try { 
    final result = api_instance.profileGetFavoritesPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileGetFavoritesPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserFavoriteListBR**](UserFavoriteListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileGetUserAddressesPost**
> AddressListBR profileGetUserAddressesPost()

returns current users addresses

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();

try { 
    final result = api_instance.profileGetUserAddressesPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileGetUserAddressesPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AddressListBR**](AddressListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileGetUserPost**
> UserBR profileGetUserPost()

returns the current user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();

try { 
    final result = api_instance.profileGetUserPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileGetUserPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserBR**](UserBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileRemoveProductFromFavoritesPost**
> BR profileRemoveProductFromFavoritesPost(userFavoriteRequest)

removes the product from current users favorites list

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final userFavoriteRequest = UserFavoriteRequest(); // UserFavoriteRequest | 

try { 
    final result = api_instance.profileRemoveProductFromFavoritesPost(userFavoriteRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileRemoveProductFromFavoritesPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userFavoriteRequest** | [**UserFavoriteRequest**](UserFavoriteRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileSaveAddressPost**
> AddressBR profileSaveAddressPost(saveAddressRequest)

saves address details to current user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final saveAddressRequest = SaveAddressRequest(); // SaveAddressRequest | 

try { 
    final result = api_instance.profileSaveAddressPost(saveAddressRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileSaveAddressPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saveAddressRequest** | [**SaveAddressRequest**](SaveAddressRequest.md)|  | [optional] 

### Return type

[**AddressBR**](AddressBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileSetDefaultAddressPost**
> AddressBR profileSetDefaultAddressPost(addressRequest)

sets the default address of the user

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final addressRequest = AddressRequest(); // AddressRequest | 

try { 
    final result = api_instance.profileSetDefaultAddressPost(addressRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileSetDefaultAddressPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addressRequest** | [**AddressRequest**](AddressRequest.md)|  | [optional] 

### Return type

[**AddressBR**](AddressBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileSetMarketingChoicesPost**
> BR profileSetMarketingChoicesPost(setMarketingRequest)

sets marketing choices for current user  keys are :      MarketingEmails      MarketingNotifications      MarketingSMS      MarketingCalls

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final setMarketingRequest = SetMarketingRequest(); // SetMarketingRequest | 

try { 
    final result = api_instance.profileSetMarketingChoicesPost(setMarketingRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileSetMarketingChoicesPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setMarketingRequest** | [**SetMarketingRequest**](SetMarketingRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profileSetUserFCMTokenPost**
> BooleanBR profileSetUserFCMTokenPost(setFCMTokenRequest)

SetUserFCMToken

### Example 
```dart
import 'package:buyx_api_main/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = MainAppProfileApi();
final setFCMTokenRequest = SetFCMTokenRequest(); // SetFCMTokenRequest | 

try { 
    final result = api_instance.profileSetUserFCMTokenPost(setFCMTokenRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppProfileApi->profileSetUserFCMTokenPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **setFCMTokenRequest** | [**SetFCMTokenRequest**](SetFCMTokenRequest.md)|  | [optional] 

### Return type

[**BooleanBR**](BooleanBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

