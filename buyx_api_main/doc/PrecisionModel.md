# buyx_api_main.model.PrecisionModel

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isFloating** | **bool** |  | [optional] [readonly] 
**maximumSignificantDigits** | **int** |  | [optional] [readonly] 
**scale** | **double** |  | [optional] 
**precisionModelType** | [**PrecisionModels**](PrecisionModels.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


