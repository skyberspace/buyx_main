# buyx_api_main.model.CartItem

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**quantitiy** | **double** |  | [optional] 
**userId** | **int** |  | [optional] 
**productWarehouseStockId** | **int** |  | [optional] 
**warehouseId** | **int** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**productWarehouseStock** | [**ProductWarehouseStock**](ProductWarehouseStock.md) |  | [optional] 
**warehouse** | [**Warehouse**](Warehouse.md) |  | [optional] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


