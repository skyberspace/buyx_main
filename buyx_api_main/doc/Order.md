# buyx_api_main.model.Order

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**total** | **double** | Grand Total of the order | [optional] 
**subTotal** | **double** | total of order before delivery fee and tax | [optional] 
**deliveryFee** | **double** |  | [optional] 
**tax** | **double** | total tax calculated when order is placed | [optional] 
**orderState** | [**OrderState**](OrderState.md) |  | [optional] 
**returned** | **bool** |  | [optional] 
**note** | **String** |  | [optional] 
**stripeIntentId** | **String** |  | [optional] 
**orderType** | [**OrderType**](OrderType.md) |  | [optional] 
**ratedStars** | **double** |  | [optional] 
**rated** | **bool** |  | [optional] 
**paymentType** | [**PaymentType**](PaymentType.md) |  | [optional] 
**deliveryLastLatitude** | **double** |  | [optional] 
**deliveryLastLongitude** | **double** |  | [optional] 
**userId** | **int** |  | [optional] 
**warehouseId** | **int** |  | [optional] 
**orderAddressId** | **int** |  | [optional] 
**pickupAddressId** | **int** |  | [optional] 
**packagingAgentId** | **int** |  | [optional] 
**deliveryAgentId** | **int** |  | [optional] 
**campaignId** | **int** |  | [optional] 
**deliveryAgentImageUrl** | **String** |  | [optional] 
**deliveryAgentName** | **String** |  | [optional] 
**packagableAt** | [**DateTime**](DateTime.md) |  | [optional] 
**assignedToPackagingAt** | [**DateTime**](DateTime.md) |  | [optional] 
**packagedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**deliverableAt** | [**DateTime**](DateTime.md) |  | [optional] 
**assignedToDeliveryAt** | [**DateTime**](DateTime.md) |  | [optional] 
**deliveredAt** | [**DateTime**](DateTime.md) |  | [optional] 
**packagingAgent** | [**WarehouseUser**](WarehouseUser.md) |  | [optional] 
**deliveryAgent** | [**WarehouseUser**](WarehouseUser.md) |  | [optional] 
**orderAddress** | [**OrderAddress**](OrderAddress.md) |  | [optional] 
**pickupAddress** | [**OrderAddress**](OrderAddress.md) |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**warehouse** | [**Warehouse**](Warehouse.md) |  | [optional] 
**lines** | [**List<OrderLine>**](OrderLine.md) | order details | [optional] [default to const []]
**orderStateHistory** | [**List<OrderStateHistory>**](OrderStateHistory.md) | history of order state changes and timestamps and issuer user ids | [optional] [default to const []]
**paymentLogs** | [**List<PaymentLog>**](PaymentLog.md) |  | [optional] [default to const []]
**campaign** | [**CartCampaign**](CartCampaign.md) |  | [optional] 
**deletedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**isDeleted** | **bool** |  | [optional] [default to false]
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


