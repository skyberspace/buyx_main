# buyx_api_main.model.CheckoutDetails

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cartTotal** | **double** |  | [optional] 
**deliveryFee** | **double** |  | [optional] 
**campaignDiscount** | **double** |  | [optional] 
**total** | **double** |  | [optional] [readonly] 
**cartItems** | [**List<CartItem>**](CartItem.md) |  | [optional] [default to const []]
**selectedCampaign** | [**CartCampaign**](CartCampaign.md) |  | [optional] 
**availableCampaigns** | [**List<CartCampaign>**](CartCampaign.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


