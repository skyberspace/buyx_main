# buyx_api_main.api.MainAppAuthApi

## Load the API package
```dart
import 'package:buyx_api_main/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authCheckAuthStatusPost**](MainAppAuthApi.md#authcheckauthstatuspost) | **POST** /auth/CheckAuthStatus | Check if the current token is valid.if not you should call CreateGuestAccount to create guest account
[**authForgotPasswordStep1Post**](MainAppAuthApi.md#authforgotpasswordstep1post) | **POST** /auth/ForgotPasswordStep1 | ForgotPassword
[**authForgotPasswordStep2Post**](MainAppAuthApi.md#authforgotpasswordstep2post) | **POST** /auth/ForgotPasswordStep2 | ForgotPasswordStep2
[**authGetUserFromTokenPost**](MainAppAuthApi.md#authgetuserfromtokenpost) | **POST** /auth/GetUserFromToken | used for getting the user details from AuthToken
[**authLoginBasicPost**](MainAppAuthApi.md#authloginbasicpost) | **POST** /auth/LoginBasic | Login with Authorization basic
[**authLoginPost**](MainAppAuthApi.md#authloginpost) | **POST** /auth/Login | Login using phone and password, returns auth token
[**authLogoutPost**](MainAppAuthApi.md#authlogoutpost) | **POST** /auth/Logout | Logs out and invalidates given token
[**authRegisterPost**](MainAppAuthApi.md#authregisterpost) | **POST** /auth/Register | register new user returns AuthToken
[**authRegisterStep2Post**](MainAppAuthApi.md#authregisterstep2post) | **POST** /auth/RegisterStep2 | RegisterOTPStep2


# **authCheckAuthStatusPost**
> AuthTokenBR authCheckAuthStatusPost()

Check if the current token is valid.if not you should call CreateGuestAccount to create guest account

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();

try { 
    final result = api_instance.authCheckAuthStatusPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authCheckAuthStatusPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthTokenBR**](AuthTokenBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authForgotPasswordStep1Post**
> ForgotPasswordStep1ResultBR authForgotPasswordStep1Post(forgotPasswordStep1Request)

ForgotPassword

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();
final forgotPasswordStep1Request = ForgotPasswordStep1Request(); // ForgotPasswordStep1Request | 

try { 
    final result = api_instance.authForgotPasswordStep1Post(forgotPasswordStep1Request);
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authForgotPasswordStep1Post: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forgotPasswordStep1Request** | [**ForgotPasswordStep1Request**](ForgotPasswordStep1Request.md)|  | [optional] 

### Return type

[**ForgotPasswordStep1ResultBR**](ForgotPasswordStep1ResultBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authForgotPasswordStep2Post**
> BooleanBR authForgotPasswordStep2Post(forgotPasswordStep2Request)

ForgotPasswordStep2

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();
final forgotPasswordStep2Request = ForgotPasswordStep2Request(); // ForgotPasswordStep2Request | 

try { 
    final result = api_instance.authForgotPasswordStep2Post(forgotPasswordStep2Request);
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authForgotPasswordStep2Post: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forgotPasswordStep2Request** | [**ForgotPasswordStep2Request**](ForgotPasswordStep2Request.md)|  | [optional] 

### Return type

[**BooleanBR**](BooleanBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authGetUserFromTokenPost**
> AuthTokenBR authGetUserFromTokenPost()

used for getting the user details from AuthToken

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();

try { 
    final result = api_instance.authGetUserFromTokenPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authGetUserFromTokenPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthTokenBR**](AuthTokenBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authLoginBasicPost**
> StringBR authLoginBasicPost()

Login with Authorization basic

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();

try { 
    final result = api_instance.authLoginBasicPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authLoginBasicPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StringBR**](StringBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authLoginPost**
> AuthTokenBR authLoginPost(loginRequest)

Login using phone and password, returns auth token

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();
final loginRequest = LoginRequest(); // LoginRequest | 

try { 
    final result = api_instance.authLoginPost(loginRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authLoginPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loginRequest** | [**LoginRequest**](LoginRequest.md)|  | [optional] 

### Return type

[**AuthTokenBR**](AuthTokenBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authLogoutPost**
> ObjectBR authLogoutPost()

Logs out and invalidates given token

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();

try { 
    final result = api_instance.authLogoutPost();
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authLogoutPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ObjectBR**](ObjectBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authRegisterPost**
> StringBR authRegisterPost(registerRequest)

register new user returns AuthToken

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();
final registerRequest = RegisterRequest(); // RegisterRequest | 

try { 
    final result = api_instance.authRegisterPost(registerRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authRegisterPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerRequest** | [**RegisterRequest**](RegisterRequest.md)|  | [optional] 

### Return type

[**StringBR**](StringBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authRegisterStep2Post**
> AuthTokenBR authRegisterStep2Post(code, OTP)

RegisterOTPStep2

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppAuthApi();
final code = code_example; // String | 
final OTP = OTP_example; // String | 

try { 
    final result = api_instance.authRegisterStep2Post(code, OTP);
    print(result);
} catch (e) {
    print('Exception when calling MainAppAuthApi->authRegisterStep2Post: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**|  | [optional] 
 **OTP** | **String**|  | [optional] 

### Return type

[**AuthTokenBR**](AuthTokenBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

