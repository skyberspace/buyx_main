# buyx_api_main.api.MainAppLayoutApi

## Load the API package
```dart
import 'package:buyx_api_main/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**layoutGetBlogEntriesPost**](MainAppLayoutApi.md#layoutgetblogentriespost) | **POST** /layout/GetBlogEntries | returns blog entries from type
[**layoutGetHomeSliderGet**](MainAppLayoutApi.md#layoutgethomesliderget) | **GET** /layout/GetHomeSlider | returns home slider content


# **layoutGetBlogEntriesPost**
> BlogEntryListBR layoutGetBlogEntriesPost(getBlogEntriesRequest)

returns blog entries from type

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppLayoutApi();
final getBlogEntriesRequest = GetBlogEntriesRequest(); // GetBlogEntriesRequest | 

try { 
    final result = api_instance.layoutGetBlogEntriesPost(getBlogEntriesRequest);
    print(result);
} catch (e) {
    print('Exception when calling MainAppLayoutApi->layoutGetBlogEntriesPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getBlogEntriesRequest** | [**GetBlogEntriesRequest**](GetBlogEntriesRequest.md)|  | [optional] 

### Return type

[**BlogEntryListBR**](BlogEntryListBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **layoutGetHomeSliderGet**
> BannerListBR layoutGetHomeSliderGet()

returns home slider content

### Example 
```dart
import 'package:buyx_api_main/api.dart';

final api_instance = MainAppLayoutApi();

try { 
    final result = api_instance.layoutGetHomeSliderGet();
    print(result);
} catch (e) {
    print('Exception when calling MainAppLayoutApi->layoutGetHomeSliderGet: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BannerListBR**](BannerListBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

