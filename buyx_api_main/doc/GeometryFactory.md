# buyx_api_main.model.GeometryFactory

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**precisionModel** | [**PrecisionModel**](PrecisionModel.md) |  | [optional] 
**coordinateSequenceFactory** | [**CoordinateSequenceFactory**](CoordinateSequenceFactory.md) |  | [optional] 
**srid** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


