# buyx_api_main.model.Geometry

## Load the model package
```dart
import 'package:buyx_api_main/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**factory_** | [**GeometryFactory**](GeometryFactory.md) |  | [optional] 
**userData** | [**Object**](.md) |  | [optional] 
**srid** | **int** |  | [optional] 
**geometryType** | **String** |  | [optional] [readonly] 
**ogcGeometryType** | [**OgcGeometryType**](OgcGeometryType.md) |  | [optional] 
**precisionModel** | [**PrecisionModel**](PrecisionModel.md) |  | [optional] 
**coordinate** | [**Coordinate**](Coordinate.md) |  | [optional] 
**coordinates** | [**List<Coordinate>**](Coordinate.md) |  | [optional] [readonly] [default to const []]
**numPoints** | **int** |  | [optional] [readonly] 
**numGeometries** | **int** |  | [optional] [readonly] 
**isSimple** | **bool** |  | [optional] [readonly] 
**isValid** | **bool** |  | [optional] [readonly] 
**isEmpty** | **bool** |  | [optional] [readonly] 
**area** | **double** |  | [optional] [readonly] 
**length** | **double** |  | [optional] [readonly] 
**centroid** | [**Point**](Point.md) |  | [optional] 
**interiorPoint** | [**Point**](Point.md) |  | [optional] 
**pointOnSurface** | [**Point**](Point.md) |  | [optional] 
**dimension** | [**Dimension**](Dimension.md) |  | [optional] 
**boundary** | [**Geometry**](Geometry.md) |  | [optional] 
**boundaryDimension** | [**Dimension**](Dimension.md) |  | [optional] 
**envelope** | [**Geometry**](Geometry.md) |  | [optional] 
**envelopeInternal** | [**Envelope**](Envelope.md) |  | [optional] 
**isRectangle** | **bool** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


