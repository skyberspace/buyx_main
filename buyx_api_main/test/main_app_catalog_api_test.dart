//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';


/// tests for MainAppCatalogApi
void main() {
  final instance = MainAppCatalogApi();

  group('tests for MainAppCatalogApi', () {
    // get product details
    //
    //Future<ProductBR> catalogGetProductDetailsPost({ ProductDetailsRequest productDetailsRequest }) async
    test('test catalogGetProductDetailsPost', () async {
      // TODO
    });

    // gets the categories for mainapp homepage
    //
    //Future<CategoryListBR> catalogGetRootCategoriesPost() async
    test('test catalogGetRootCategoriesPost', () async {
      // TODO
    });

    // returns the categories for mainapp category detail page
    //
    //Future<CategoryListBR> catalogGetSubCategoriesFromPost({ GetSubCategoriesRequest getSubCategoriesRequest }) async
    test('test catalogGetSubCategoriesFromPost', () async {
      // TODO
    });

    // SearchProducts with a text filter
    //
    //Future<ProductSearchRankedListBR> catalogSearchProductsPost({ StringFilterRequest stringFilterRequest }) async
    test('test catalogSearchProductsPost', () async {
      // TODO
    });

  });
}
