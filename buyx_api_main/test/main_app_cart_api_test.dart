//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';


/// tests for MainAppCartApi
void main() {
  final instance = MainAppCartApi();

  group('tests for MainAppCartApi', () {
    // add product to cart
    //
    //Future<CartItemBR> cartAddToCartPost({ AddToCartRequest addToCartRequest }) async
    test('test cartAddToCartPost', () async {
      // TODO
    });

    // CreatePaymentIntent
    //
    //Future<PaymentSheetDataBR> cartCreatePaymentIntentPost({ CreateOrderRequest createOrderRequest }) async
    test('test cartCreatePaymentIntentPost', () async {
      // TODO
    });

    // DeleteOrder
    //
    //Future<BR> cartDeleteOrderPost({ IdRequest idRequest }) async
    test('test cartDeleteOrderPost', () async {
      // TODO
    });

    // Returns the list of the not completed orders for user
    //
    //Future<OrderListBR> cartGetActiveOrdersPost() async
    test('test cartGetActiveOrdersPost', () async {
      // TODO
    });

    // get current cart items for user
    //
    //Future<CartItemListBR> cartGetCartItemsPost() async
    test('test cartGetCartItemsPost', () async {
      // TODO
    });

    // GetCheckoutDetails
    //
    //Future<CheckoutDetailsBR> cartGetCheckoutDetailsPost({ CheckoutDetailsRequest checkoutDetailsRequest }) async
    test('test cartGetCheckoutDetailsPost', () async {
      // TODO
    });

    // GetOrderhistory
    //
    //Future<OrderListBR> cartGetOrderHistoryPost() async
    test('test cartGetOrderHistoryPost', () async {
      // TODO
    });

    // GetPublicStripeToken
    //
    //Future<StringBR> cartGetPublicStripeTokenPost() async
    test('test cartGetPublicStripeTokenPost', () async {
      // TODO
    });

    // PlaceOrder
    //
    //Future<OrderBR> cartPlaceNonProvisionedOrderPost({ CreateOrderRequestWithPayment createOrderRequestWithPayment }) async
    test('test cartPlaceNonProvisionedOrderPost', () async {
      // TODO
    });

    // remove product from cart
    //
    //Future<ObjectBR> cartRemoveItemFromCartPost({ RemoveFromCartRequest removeFromCartRequest }) async
    test('test cartRemoveItemFromCartPost', () async {
      // TODO
    });

    // SetOrderStars
    //
    //Future<BR> cartSetOrderStarsPost({ SetOrderStarsRequest setOrderStarsRequest }) async
    test('test cartSetOrderStarsPost', () async {
      // TODO
    });

  });
}
