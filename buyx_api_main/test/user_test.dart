//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for User
void main() {
  final instance = User();

  group('test User', () {
    // String setPassword
    test('to test the property `setPassword`', () async {
      // TODO
    });

    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // String email
    test('to test the property `email`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String surname
    test('to test the property `surname`', () async {
      // TODO
    });

    // String phone
    test('to test the property `phone`', () async {
      // TODO
    });

    // bool phoneVerified
    test('to test the property `phoneVerified`', () async {
      // TODO
    });

    // bool marketingEmails
    test('to test the property `marketingEmails`', () async {
      // TODO
    });

    // bool marketingNotifications
    test('to test the property `marketingNotifications`', () async {
      // TODO
    });

    // bool marketingSMS
    test('to test the property `marketingSMS`', () async {
      // TODO
    });

    // bool marketingCalls
    test('to test the property `marketingCalls`', () async {
      // TODO
    });

    // bool isGuest
    test('to test the property `isGuest`', () async {
      // TODO
    });

    // bool canPlaceNonProvisionedPaymentMethods
    test('to test the property `canPlaceNonProvisionedPaymentMethods`', () async {
      // TODO
    });

    // String stripeUserId
    test('to test the property `stripeUserId`', () async {
      // TODO
    });

    // AdminUser adminUser
    test('to test the property `adminUser`', () async {
      // TODO
    });

    // WarehouseUser warehouseUser
    test('to test the property `warehouseUser`', () async {
      // TODO
    });

    // List<Address> addresses (default value: const [])
    test('to test the property `addresses`', () async {
      // TODO
    });

    // List<UserFavorite> favorites (default value: const [])
    test('to test the property `favorites`', () async {
      // TODO
    });

    // String fcmToken
    test('to test the property `fcmToken`', () async {
      // TODO
    });

    // used for multitenant module. If set, this automatically manages what this user can see and do  represents the tenant group Id for the user
    // int tenantRoleGroup
    test('to test the property `tenantRoleGroup`', () async {
      // TODO
    });

    // bool isAdmin
    test('to test the property `isAdmin`', () async {
      // TODO
    });

    // bool isWarehouseUser
    test('to test the property `isWarehouseUser`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // DateTime updatedAt
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // bool delete
    test('to test the property `delete`', () async {
      // TODO
    });


  });

}
