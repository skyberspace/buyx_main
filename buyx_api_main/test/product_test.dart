//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for Product
void main() {
  final instance = Product();

  group('test Product', () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // double price
    test('to test the property `price`', () async {
      // TODO
    });

    // String shortDescription
    test('to test the property `shortDescription`', () async {
      // TODO
    });

    // double minCartCount
    test('to test the property `minCartCount`', () async {
      // TODO
    });

    // double maxCartCount
    test('to test the property `maxCartCount`', () async {
      // TODO
    });

    // bool active
    test('to test the property `active`', () async {
      // TODO
    });

    // String barcode
    test('to test the property `barcode`', () async {
      // TODO
    });

    // String metaTags
    test('to test the property `metaTags`', () async {
      // TODO
    });

    // String metaDescription
    test('to test the property `metaDescription`', () async {
      // TODO
    });

    // List<ProductImage> images (default value: const [])
    test('to test the property `images`', () async {
      // TODO
    });

    // List<ProductWarehouseStock> stocks (default value: const [])
    test('to test the property `stocks`', () async {
      // TODO
    });

    // List<ProductCategory> categories (default value: const [])
    test('to test the property `categories`', () async {
      // TODO
    });

    // DateTime deletedAt
    test('to test the property `deletedAt`', () async {
      // TODO
    });

    // bool isDeleted (default value: false)
    test('to test the property `isDeleted`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // DateTime updatedAt
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // bool delete
    test('to test the property `delete`', () async {
      // TODO
    });


  });

}
