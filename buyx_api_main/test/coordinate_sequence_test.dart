//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for CoordinateSequence
void main() {
  final instance = CoordinateSequence();

  group('test CoordinateSequence', () {
    // int dimension
    test('to test the property `dimension`', () async {
      // TODO
    });

    // int measures
    test('to test the property `measures`', () async {
      // TODO
    });

    // int spatial
    test('to test the property `spatial`', () async {
      // TODO
    });

    // Ordinates ordinates
    test('to test the property `ordinates`', () async {
      // TODO
    });

    // bool hasZ
    test('to test the property `hasZ`', () async {
      // TODO
    });

    // bool hasM
    test('to test the property `hasM`', () async {
      // TODO
    });

    // int zOrdinateIndex
    test('to test the property `zOrdinateIndex`', () async {
      // TODO
    });

    // int mOrdinateIndex
    test('to test the property `mOrdinateIndex`', () async {
      // TODO
    });

    // int count
    test('to test the property `count`', () async {
      // TODO
    });


  });

}
