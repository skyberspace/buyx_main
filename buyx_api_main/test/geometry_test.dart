//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for Geometry
void main() {
  final instance = Geometry();

  group('test Geometry', () {
    // GeometryFactory factory_
    test('to test the property `factory_`', () async {
      // TODO
    });

    // Object userData
    test('to test the property `userData`', () async {
      // TODO
    });

    // int srid
    test('to test the property `srid`', () async {
      // TODO
    });

    // String geometryType
    test('to test the property `geometryType`', () async {
      // TODO
    });

    // OgcGeometryType ogcGeometryType
    test('to test the property `ogcGeometryType`', () async {
      // TODO
    });

    // PrecisionModel precisionModel
    test('to test the property `precisionModel`', () async {
      // TODO
    });

    // Coordinate coordinate
    test('to test the property `coordinate`', () async {
      // TODO
    });

    // List<Coordinate> coordinates (default value: const [])
    test('to test the property `coordinates`', () async {
      // TODO
    });

    // int numPoints
    test('to test the property `numPoints`', () async {
      // TODO
    });

    // int numGeometries
    test('to test the property `numGeometries`', () async {
      // TODO
    });

    // bool isSimple
    test('to test the property `isSimple`', () async {
      // TODO
    });

    // bool isValid
    test('to test the property `isValid`', () async {
      // TODO
    });

    // bool isEmpty
    test('to test the property `isEmpty`', () async {
      // TODO
    });

    // double area
    test('to test the property `area`', () async {
      // TODO
    });

    // double length
    test('to test the property `length`', () async {
      // TODO
    });

    // Point centroid
    test('to test the property `centroid`', () async {
      // TODO
    });

    // Point interiorPoint
    test('to test the property `interiorPoint`', () async {
      // TODO
    });

    // Point pointOnSurface
    test('to test the property `pointOnSurface`', () async {
      // TODO
    });

    // Dimension dimension
    test('to test the property `dimension`', () async {
      // TODO
    });

    // Geometry boundary
    test('to test the property `boundary`', () async {
      // TODO
    });

    // Dimension boundaryDimension
    test('to test the property `boundaryDimension`', () async {
      // TODO
    });

    // Geometry envelope
    test('to test the property `envelope`', () async {
      // TODO
    });

    // Envelope envelopeInternal
    test('to test the property `envelopeInternal`', () async {
      // TODO
    });

    // bool isRectangle
    test('to test the property `isRectangle`', () async {
      // TODO
    });


  });

}
