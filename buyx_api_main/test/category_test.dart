//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for Category
void main() {
  final instance = Category();

  group('test Category', () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String shortDesc
    test('to test the property `shortDesc`', () async {
      // TODO
    });

    // String imagePath
    test('to test the property `imagePath`', () async {
      // TODO
    });

    // bool active
    test('to test the property `active`', () async {
      // TODO
    });

    // int order
    test('to test the property `order`', () async {
      // TODO
    });

    // int parentCategoryId
    test('to test the property `parentCategoryId`', () async {
      // TODO
    });

    // Category parentCategory
    test('to test the property `parentCategory`', () async {
      // TODO
    });

    // List<ProductCategory> products (default value: const [])
    test('to test the property `products`', () async {
      // TODO
    });

    // List<Category> subCategories (default value: const [])
    test('to test the property `subCategories`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // DateTime updatedAt
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // bool delete
    test('to test the property `delete`', () async {
      // TODO
    });


  });

}
