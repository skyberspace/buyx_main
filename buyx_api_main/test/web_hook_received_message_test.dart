//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for WebHookReceivedMessage
void main() {
  final instance = WebHookReceivedMessage();

  group('test WebHookReceivedMessage', () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // String type
    test('to test the property `type`', () async {
      // TODO
    });

    // String from
    test('to test the property `from`', () async {
      // TODO
    });

    // String to
    test('to test the property `to`', () async {
      // TODO
    });

    // String body
    test('to test the property `body`', () async {
      // TODO
    });

    // String encoding
    test('to test the property `encoding`', () async {
      // TODO
    });

    // int protocolId
    test('to test the property `protocolId`', () async {
      // TODO
    });

    // int messageClass
    test('to test the property `messageClass`', () async {
      // TODO
    });

    // int numberOfParts
    test('to test the property `numberOfParts`', () async {
      // TODO
    });

    // int creditCost
    test('to test the property `creditCost`', () async {
      // TODO
    });

    // Submission submission
    test('to test the property `submission`', () async {
      // TODO
    });

    // Status status
    test('to test the property `status`', () async {
      // TODO
    });

    // String relatedSentMessageId
    test('to test the property `relatedSentMessageId`', () async {
      // TODO
    });

    // String userSuppliedId
    test('to test the property `userSuppliedId`', () async {
      // TODO
    });


  });

}
