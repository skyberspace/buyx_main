//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';


/// tests for MainAppAuthApi
void main() {
  final instance = MainAppAuthApi();

  group('tests for MainAppAuthApi', () {
    // Check if the current token is valid.if not you should call CreateGuestAccount to create guest account
    //
    //Future<AuthTokenBR> authCheckAuthStatusPost() async
    test('test authCheckAuthStatusPost', () async {
      // TODO
    });

    // ForgotPassword
    //
    //Future<ForgotPasswordStep1ResultBR> authForgotPasswordStep1Post({ ForgotPasswordStep1Request forgotPasswordStep1Request }) async
    test('test authForgotPasswordStep1Post', () async {
      // TODO
    });

    // ForgotPasswordStep2
    //
    //Future<BooleanBR> authForgotPasswordStep2Post({ ForgotPasswordStep2Request forgotPasswordStep2Request }) async
    test('test authForgotPasswordStep2Post', () async {
      // TODO
    });

    // used for getting the user details from AuthToken
    //
    //Future<AuthTokenBR> authGetUserFromTokenPost() async
    test('test authGetUserFromTokenPost', () async {
      // TODO
    });

    // Login with Authorization basic
    //
    //Future<StringBR> authLoginBasicPost() async
    test('test authLoginBasicPost', () async {
      // TODO
    });

    // Login using phone and password, returns auth token
    //
    //Future<AuthTokenBR> authLoginPost({ LoginRequest loginRequest }) async
    test('test authLoginPost', () async {
      // TODO
    });

    // Logs out and invalidates given token
    //
    //Future<ObjectBR> authLogoutPost() async
    test('test authLogoutPost', () async {
      // TODO
    });

    // register new user returns AuthToken
    //
    //Future<StringBR> authRegisterPost({ RegisterRequest registerRequest }) async
    test('test authRegisterPost', () async {
      // TODO
    });

    // RegisterOTPStep2
    //
    //Future<AuthTokenBR> authRegisterStep2Post({ String code, String OTP }) async
    test('test authRegisterStep2Post', () async {
      // TODO
    });

  });
}
