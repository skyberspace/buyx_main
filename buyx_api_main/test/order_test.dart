//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for Order
void main() {
  final instance = Order();

  group('test Order', () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // Grand Total of the order
    // double total
    test('to test the property `total`', () async {
      // TODO
    });

    // total of order before delivery fee and tax
    // double subTotal
    test('to test the property `subTotal`', () async {
      // TODO
    });

    // double deliveryFee
    test('to test the property `deliveryFee`', () async {
      // TODO
    });

    // total tax calculated when order is placed
    // double tax
    test('to test the property `tax`', () async {
      // TODO
    });

    // OrderState orderState
    test('to test the property `orderState`', () async {
      // TODO
    });

    // bool returned
    test('to test the property `returned`', () async {
      // TODO
    });

    // String note
    test('to test the property `note`', () async {
      // TODO
    });

    // String stripeIntentId
    test('to test the property `stripeIntentId`', () async {
      // TODO
    });

    // OrderType orderType
    test('to test the property `orderType`', () async {
      // TODO
    });

    // double ratedStars
    test('to test the property `ratedStars`', () async {
      // TODO
    });

    // bool rated
    test('to test the property `rated`', () async {
      // TODO
    });

    // PaymentType paymentType
    test('to test the property `paymentType`', () async {
      // TODO
    });

    // double deliveryLastLatitude
    test('to test the property `deliveryLastLatitude`', () async {
      // TODO
    });

    // double deliveryLastLongitude
    test('to test the property `deliveryLastLongitude`', () async {
      // TODO
    });

    // int userId
    test('to test the property `userId`', () async {
      // TODO
    });

    // int warehouseId
    test('to test the property `warehouseId`', () async {
      // TODO
    });

    // int orderAddressId
    test('to test the property `orderAddressId`', () async {
      // TODO
    });

    // int pickupAddressId
    test('to test the property `pickupAddressId`', () async {
      // TODO
    });

    // int packagingAgentId
    test('to test the property `packagingAgentId`', () async {
      // TODO
    });

    // int deliveryAgentId
    test('to test the property `deliveryAgentId`', () async {
      // TODO
    });

    // int campaignId
    test('to test the property `campaignId`', () async {
      // TODO
    });

    // String deliveryAgentImageUrl
    test('to test the property `deliveryAgentImageUrl`', () async {
      // TODO
    });

    // String deliveryAgentName
    test('to test the property `deliveryAgentName`', () async {
      // TODO
    });

    // DateTime packagableAt
    test('to test the property `packagableAt`', () async {
      // TODO
    });

    // DateTime assignedToPackagingAt
    test('to test the property `assignedToPackagingAt`', () async {
      // TODO
    });

    // DateTime packagedAt
    test('to test the property `packagedAt`', () async {
      // TODO
    });

    // DateTime deliverableAt
    test('to test the property `deliverableAt`', () async {
      // TODO
    });

    // DateTime assignedToDeliveryAt
    test('to test the property `assignedToDeliveryAt`', () async {
      // TODO
    });

    // DateTime deliveredAt
    test('to test the property `deliveredAt`', () async {
      // TODO
    });

    // WarehouseUser packagingAgent
    test('to test the property `packagingAgent`', () async {
      // TODO
    });

    // WarehouseUser deliveryAgent
    test('to test the property `deliveryAgent`', () async {
      // TODO
    });

    // OrderAddress orderAddress
    test('to test the property `orderAddress`', () async {
      // TODO
    });

    // OrderAddress pickupAddress
    test('to test the property `pickupAddress`', () async {
      // TODO
    });

    // User user
    test('to test the property `user`', () async {
      // TODO
    });

    // Warehouse warehouse
    test('to test the property `warehouse`', () async {
      // TODO
    });

    // order details
    // List<OrderLine> lines (default value: const [])
    test('to test the property `lines`', () async {
      // TODO
    });

    // history of order state changes and timestamps and issuer user ids
    // List<OrderStateHistory> orderStateHistory (default value: const [])
    test('to test the property `orderStateHistory`', () async {
      // TODO
    });

    // List<PaymentLog> paymentLogs (default value: const [])
    test('to test the property `paymentLogs`', () async {
      // TODO
    });

    // CartCampaign campaign
    test('to test the property `campaign`', () async {
      // TODO
    });

    // DateTime deletedAt
    test('to test the property `deletedAt`', () async {
      // TODO
    });

    // bool isDeleted (default value: false)
    test('to test the property `isDeleted`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // DateTime updatedAt
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // bool delete
    test('to test the property `delete`', () async {
      // TODO
    });


  });

}
