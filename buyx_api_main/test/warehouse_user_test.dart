//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';

// tests for WarehouseUser
void main() {
  final instance = WarehouseUser();

  group('test WarehouseUser', () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // WarehouseRole warehouseRole
    test('to test the property `warehouseRole`', () async {
      // TODO
    });

    // String firebaseCloudMessagingToken
    test('to test the property `firebaseCloudMessagingToken`', () async {
      // TODO
    });

    // bool canSeeWarehouseOrders
    test('to test the property `canSeeWarehouseOrders`', () async {
      // TODO
    });

    // bool canSeeRestaurantOrders
    test('to test the property `canSeeRestaurantOrders`', () async {
      // TODO
    });

    // Warehouse warehouse
    test('to test the property `warehouse`', () async {
      // TODO
    });

    // User user
    test('to test the property `user`', () async {
      // TODO
    });

    // int warehouseId
    test('to test the property `warehouseId`', () async {
      // TODO
    });

    // int userId
    test('to test the property `userId`', () async {
      // TODO
    });

    // String imageUrl
    test('to test the property `imageUrl`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // DateTime updatedAt
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // bool delete
    test('to test the property `delete`', () async {
      // TODO
    });


  });

}
