//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_main/api.dart';
import 'package:test/test.dart';


/// tests for MainAppProfileApi
void main() {
  final instance = MainAppProfileApi();

  group('tests for MainAppProfileApi', () {
    // adds the product to current users favorites list
    //
    //Future<UserFavoriteBR> profileAddProductToFavoritesPost({ ProductDetailsRequest productDetailsRequest }) async
    test('test profileAddProductToFavoritesPost', () async {
      // TODO
    });

    // Change email, name,surname Information
    //
    //Future<BR> profileChangeInformationPost({ ChangeInformtionRequest changeInformtionRequest }) async
    test('test profileChangeInformationPost', () async {
      // TODO
    });

    // changes password of the current user
    //
    //Future<UserBR> profileChangePasswordPost({ ChangePasswordRequest changePasswordRequest }) async
    test('test profileChangePasswordPost', () async {
      // TODO
    });

    // deletes the address from current user
    //
    //Future<BR> profileDeleteAddressPost({ AddressRequest addressRequest }) async
    test('test profileDeleteAddressPost', () async {
      // TODO
    });

    // returns the default address of current user
    //
    //Future<AddressBR> profileGetDefaultAddressPost() async
    test('test profileGetDefaultAddressPost', () async {
      // TODO
    });

    // returns favorites products for the current user
    //
    //Future<UserFavoriteListBR> profileGetFavoritesPost() async
    test('test profileGetFavoritesPost', () async {
      // TODO
    });

    // returns current users addresses
    //
    //Future<AddressListBR> profileGetUserAddressesPost() async
    test('test profileGetUserAddressesPost', () async {
      // TODO
    });

    // returns the current user
    //
    //Future<UserBR> profileGetUserPost() async
    test('test profileGetUserPost', () async {
      // TODO
    });

    // removes the product from current users favorites list
    //
    //Future<BR> profileRemoveProductFromFavoritesPost({ UserFavoriteRequest userFavoriteRequest }) async
    test('test profileRemoveProductFromFavoritesPost', () async {
      // TODO
    });

    // saves address details to current user
    //
    //Future<AddressBR> profileSaveAddressPost({ SaveAddressRequest saveAddressRequest }) async
    test('test profileSaveAddressPost', () async {
      // TODO
    });

    // sets the default address of the user
    //
    //Future<AddressBR> profileSetDefaultAddressPost({ AddressRequest addressRequest }) async
    test('test profileSetDefaultAddressPost', () async {
      // TODO
    });

    // sets marketing choices for current user  keys are :      MarketingEmails      MarketingNotifications      MarketingSMS      MarketingCalls
    //
    //Future<BR> profileSetMarketingChoicesPost({ SetMarketingRequest setMarketingRequest }) async
    test('test profileSetMarketingChoicesPost', () async {
      // TODO
    });

    // SetUserFCMToken
    //
    //Future<BooleanBR> profileSetUserFCMTokenPost({ SetFCMTokenRequest setFCMTokenRequest }) async
    test('test profileSetUserFCMTokenPost', () async {
      // TODO
    });

  });
}
