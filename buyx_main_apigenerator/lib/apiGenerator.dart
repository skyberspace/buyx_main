import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'buyx_api_main', pubAuthor: 'Skyberspace '),
    inputSpecFile: 'swagger.json',
    alwaysRun: true,
    generatorName: Generator.dart,
    outputDirectory: '../buyx_api_main')
class Example extends OpenapiGeneratorConfig {}
