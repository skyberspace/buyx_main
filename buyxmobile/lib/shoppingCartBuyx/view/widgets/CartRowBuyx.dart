import 'package:buyx/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';

class CartRowBuyx extends StatefulWidget {
  const CartRowBuyx(
      {@required this.itemDescription,
      this.itemName,
      this.itemPrice,
      this.itemCount,
      this.itemId,
      this.itemImagePath});

  final String itemName;
  final String itemDescription;
  final double itemPrice;
  final int itemCount;
  final int itemId;
  final String itemImagePath;

  @override
  _CartRowBuyxState createState() => _CartRowBuyxState();
}

class _CartRowBuyxState extends State<CartRowBuyx> {
  String currency = "£ ";

  @override
  Widget build(BuildContext context) {
    var cartState = Provider.of<CartBuyxState>(context);
    return Consumer<CartBuyxState>(
      builder: (context, state, widget) => Container(
        padding: EdgeInsets.only(left: 10, top: 10, right: 0),
        child: Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 7.2, // 52
              height: MediaQuery.of(context).size.height / 15.6, // 52
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                image: DecorationImage(
                  image: NetworkImage(
                    //TODO : 150*150 düzeltilecek.
                    this.widget.itemImagePath != null
                        ? Api.instance.client.basePath +
                            "/cdn/image/original" +
                            this.widget.itemImagePath
                        : Api.instance.client.basePath +
                            "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                    //fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  this.widget.itemName != null
                      ? Text(
                          this.widget.itemName,
                          style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold, fontSize: 20),
                          textScaleFactor: 1,
                        )
                      : Text(""),
                  this.widget.itemDescription != null
                      ? Html(
                          data: this.widget.itemDescription,
                        )
                      : Text(""),
                  this.widget.itemPrice != null
                      ? Text(
                          currency + this.widget.itemPrice.toStringAsFixed(2),
                          style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold,
                              color: kBuyxRedColor,
                              fontSize: 16),
                          textScaleFactor: 1,
                        )
                      : Text(""),
                ],
              ),
            ),
            SizedBox(
              width: 1,
            ),
            Expanded(
              flex: 1,
              child: Container(
                  child: ButtonBar(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      state.addToCart(this.widget.itemId, context);
                    },
                    child: Text(
                      '+',
                      style: GoogleFonts.comfortaa(
                          color: kBuyxRedColor, fontWeight: FontWeight.bold),
                      textScaleFactor: 1,
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kBuyxRedColor),
                    ),
                    onPressed: null,
                    child: this.widget.itemCount != null
                        ? Text(
                            this.widget.itemCount.toString(),
                            style: GoogleFonts.comfortaa(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                            textScaleFactor: 1,
                            softWrap: false,
                            overflow: TextOverflow.visible,
                          )
                        : Text(""),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      state.decrementItem(this.widget.itemId, context);
                    },
                    child: Text(
                      '-',
                      style: GoogleFonts.comfortaa(
                          color: kBuyxRedColor, fontWeight: FontWeight.bold),
                      textScaleFactor: 1,
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                  ),
                ],
              )),
            )
          ],
        ),
      ),
    );
  }
}
