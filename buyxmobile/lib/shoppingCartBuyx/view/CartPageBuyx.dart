import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/BuyxPage/view/widgets/BuyxPay.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx/shoppingCartBuyx/view/widgets/CartRowBuyx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class CartPageBuyx extends StatefulWidget {
  @override
  _CartPageBuyxState createState() => _CartPageBuyxState();
}

class _CartPageBuyxState extends State<CartPageBuyx> {
  @override
  Widget build(BuildContext context) {
    return Consumer<CartBuyxState>(
      builder: (context, state, widget) => Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Cart",
            style: GoogleFonts.comfortaa(
              color: Colors.white,
            ),
            textScaleFactor: 1,
          ),
          backgroundColor: kBuyxRedColor,
          elevation: 0,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  if (state.itemsList.isNotEmpty) {
                    return AwesomeDialog(
                      buttonsTextStyle: GoogleFonts.comfortaa(),
                      context: context,
                      dialogType: DialogType.QUESTION,
                      animType: AnimType.BOTTOMSLIDE,
                      title: 'Are you sure you want to empty your cart?',
                      desc:
                          "If you empty your cart, all your items will be lost.",
                      btnCancelOnPress: () {},
                      btnOkOnPress: () {
                        Navigator.of(context).pop();
                        state.deleteAll(context);
                      },
                    )..show();
                  } else {}
                })
          ],
        ),
        body: (state.itemsList.isNotEmpty)
            ? Stack(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(
                        child: Material(
                          elevation: 10,
                          child: ListView.separated(
                              separatorBuilder:
                                  (BuildContext context, int index) => Divider(
                                        thickness: 1,
                                      ),
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemCount: state.itemsList.length,
                              itemBuilder: (_, index) => CartRowBuyx(
                                    itemImagePath: state
                                        .itemsList[index]
                                        .productWarehouseStock
                                        .product
                                        .images
                                        .first
                                        .path,
                                    itemName: state.itemsList[index]
                                        .productWarehouseStock.product.name,
                                    itemDescription: state
                                        .itemsList[index]
                                        .productWarehouseStock
                                        .product
                                        .shortDescription,
                                    itemPrice: state.itemsList[index]
                                        .productWarehouseStock.product.price,
                                    itemCount: state.itemsList[index].quantitiy
                                        .toInt(),
                                    itemId: state.itemsList[index]
                                        .productWarehouseStock.productId,
                                  )),
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      decoration: new BoxDecoration(
                        border: new Border.all(
                            width: 0.1,
                            color: Colors
                                .transparent), //color is transparent so that it does nßot blend with the actual color specified
                        color: Colors.white60,
                      ),
                      height: MediaQuery.of(context).size.height / 8,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: 25, bottom: 25, left: 10, right: 10),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 6,
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => BuyxPay()));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: kBuyxRedColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Continue',
                                      style: GoogleFonts.comfortaa(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                      textScaleFactor: 1,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Container(
                                color: Colors.white,
                                child: Center(
                                  child: state.sumOfAll != null
                                      ? Text(
                                          "£ " +
                                              state.sumOfAll.toStringAsFixed(2),
                                          style: GoogleFonts.comfortaa(
                                              color: kBuyxRedColor,
                                              fontSize: 24,
                                              fontWeight: FontWeight.bold),
                                          textScaleFactor: 1,
                                        )
                                      : Text(""),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              )
            : Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'You can go to the homepage to add something to your cart.',
                    style: GoogleFonts.comfortaa(color: kBuyxBlueColor),
                    textScaleFactor: 1,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
      ),
    );
  }
}
