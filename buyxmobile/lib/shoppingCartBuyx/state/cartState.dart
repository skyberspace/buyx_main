import 'dart:convert';
import 'dart:ffi';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/constants.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';

class CartBuyxState with ChangeNotifier {
  //List<itemModal> _items = [];
  double sumOfAll = 0;

  List<UserFavorite> favorites = [];

  UserFavoriteListBR userFavorites;

  List<CartItem> itemsList = [];

  OrderListBR orderHistoryResult;

  List<Order> orderHistory;

  //List<itemModal> get items => _items;

  getFavorites() async {
    userFavorites = await Api.instance.profile.profileGetFavoritesPost();
    this.favorites = userFavorites.data;
    notifyListeners();
  }

  setFavorites() {}

  Future<CartItemBR> addToCart(int itemId, BuildContext context,
      {double quantity = 1, bool setLiteral = false}) async {
    var search = itemsList
        .where((product) => product.productWarehouseStock.product.id == itemId);
    var newQuantity = quantity;
    if (!setLiteral) {
      if (search.length > 0) {
        var cartItem = search.first;
        newQuantity = cartItem.quantitiy + quantity;
      }
    }
    CartItemBR addedItem = await Api.instance.cart.cartAddToCartPost(
        addToCartRequest:
            AddToCartRequest(productId: itemId, quantity: newQuantity));

    if (!addedItem.success) {
      AwesomeDialog(
        btnOkColor: kBuyxBlueColor,
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: /*'Dialog description here.............'*/ addedItem.errors[0],
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    }

    await loadCart();

    return addedItem;
  }

  Future<CartItemBR> decrementItem(int itemId, BuildContext context,
      {double quantity = 1}) async {
    // *1 ile çarp yanı eksilt
    return await this.addToCart(
      itemId,
      context,
      quantity: quantity * -1,
    );
  }

  loadCart({BuildContext context}) async {
    var res = await Api.instance.cart.cartGetCartItemsPost();
    if (!res.success) {
      /*     AwesomeDialog(
        btnOkColor: kBuyxBlueColor,
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: /*'Dialog description here.............'*/ res.errors[0],
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();*/
    }

    itemsList = res.data;
    calculateSum();
    notifyListeners();
  }

  bool checkExist(int itemId) {
    return itemsList
        .any((element) => element.productWarehouseStock.product.id == itemId);
  }

  int getItemCount(int itemId) {
    var search = itemsList
        .where((element) => element.productWarehouseStock.productId == itemId);
    if (search.length == 0) return 0;
    return search.first.quantitiy.toInt();
  }

  deleteItem(int itemId, BuildContext context) async {
    await this.addToCart(
      itemId,
      context,
      quantity: 0,
      setLiteral: true,
    );
  }

  calculateSum() {
    double sum = 0;
    for (int i = 0; i < itemsList.length; i++) {
      sum = (sum +
          itemsList[i].productWarehouseStock.product.price *
              itemsList[i].quantitiy);
    }
    sumOfAll = sum;
  }

  void deleteAll(BuildContext context) async {
    for (var item in itemsList) {
      await addToCart(item.productWarehouseStock.productId, context,
          quantity: 0, setLiteral: true);
    }
  }

  // -- Order History --
  getOrderHistory() async {
    orderHistoryResult = await Api.instance.cart.cartGetOrderHistoryPost();
    orderHistory = orderHistoryResult.data;
    notifyListeners();
  }
}
