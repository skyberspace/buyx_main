import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/profilePage/view/widgets/marketingPreferences/marketingPreferences.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:flutter/material.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/api.dart';
import 'package:buyx/buyxActiveOrder/view/widgets/activeOrderDetails.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/logInAndSignUp/view/LogIn/logIn.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/profilePage/view/widgets/adress/adressPage.dart';
import 'package:buyx/profilePage/view/widgets/changeInformation/changeInformation.dart';
import 'package:buyx/profilePage/view/widgets/favoriteItems/favoriteItemsPage.dart';
import 'package:buyx/profilePage/view/widgets/lastOrders/lastOrders.dart';
import 'package:buyx/profilePage/view/widgets/passwordChange/passwordChange.dart';
import 'package:buyx/profilePage/view/widgets/paymentMethods.dart/paymentMethods.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'widgets/photoAndName/PhotoAndName.dart';
import 'widgets/profilePageIconRow.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Profile",
                style: GoogleFonts.comfortaa(
                  color: Colors.white,
                ),
                textScaleFactor: 1,
              ),
            ],
          ),
          backgroundColor: kBuyxRedColor,
          elevation: 0,
        ),
        body: loggedState.isLoggedIn
            ? SingleChildScrollView(
                child: SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Stack(
                          children: [
                            Card(
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 5, bottom: 5),
                                    child: PhotoAndName(
                                      givenName: /*"Umut Yeşildal"*/ loggedState
                                                      .user.name !=
                                                  null &&
                                              loggedState.user.surname != null
                                          ? loggedState.user.name +
                                              " " +
                                              loggedState.user.surname
                                          : " ",
                                    ),
                                  ),
                                  Divider(),
                                  ProfilePageIconRow(
                                    givenIcon: Icon(
                                      Icons.mail,
                                      color: kBuyxRedColor,
                                    ),
                                    givenText: /*"umutyesid@gmail.com"*/ loggedState
                                                .user.email !=
                                            null
                                        ? loggedState.user.email
                                        : " ",
                                  ),
                                  Divider(),
                                  ProfilePageIconRow(
                                    givenIcon: Icon(
                                      Icons.phone,
                                      color: kBuyxRedColor,
                                    ),
                                    givenText: /*"055555555"*/ loggedState
                                                .user.phone !=
                                            null
                                        ? loggedState.user.phone
                                        : " ",
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ChangeInformation(
                                                updateCallback: () {
                                                  loggedState.updateUser();
                                                },
                                              )));
                                },
                                child: Card(
                                  elevation: 8,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Icon(Icons.edit),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Card(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            AdressPage(loggedState.user)));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.location_on,
                                  color: kBuyxRedColor,
                                ),
                                givenText: "Address",
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            FavoriteItemsPage()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.favorite,
                                  color: kBuyxRedColor,
                                ),
                                givenText: "Favorite Products",
                              ),
                            ),
                            Divider(),
                            /*GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            PaymentMethods()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.credit_card,
                                  color: kPurpleColor,
                                ),
                                givenText: "Payment Methods",
                              ),
                            ),
                            Divider(),*/
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LastOrders()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.shopping_bag,
                                  color: kBuyxRedColor,
                                ),
                                givenText: "Previous Orders",
                              ),
                            ),
                            /*Divider(),
                            ProfilePageIconRow(
                              givenIcon: Icon(
                                Icons.receipt,
                                color: kPurpleColor,
                              ),
                              givenText: "Billing Information",
                            ),*/
                            Divider(),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            PasswordChangePage()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.lock,
                                  color: kBuyxRedColor,
                                ),
                                givenText: "Change Password",
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            MarketingPreferences()));
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.notifications,
                                  color: kBuyxRedColor,
                                ),
                                givenText: "Marketing Preferences",
                              ),
                            ),
                            Divider(),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () async {
                                try {
                                  AwesomeDialog(
                                    buttonsTextStyle: GoogleFonts.comfortaa(),
                                    context: context,
                                    dialogType: DialogType.QUESTION,
                                    animType: AnimType.BOTTOMSLIDE,
                                    title: 'Dear ${loggedState.user.name}',
                                    desc: "Do you really want to log out ?",
                                    btnCancelOnPress: () {},
                                    btnOkOnPress: () async {
                                      var logOutResult = await Api
                                          .instance.authApi
                                          .authLogoutPost();
                                      var loginProvider =
                                          Provider.of<LoggedInState>(context,
                                              listen: false);
                                      loginProvider.setUser(null);
                                      loginProvider.userDefaultAddress = null;
                                      await loginProvider.storage.deleteAll();
                                      Api.instance.client.defaultHeaderMap
                                          .clear();
                                      Provider.of<CartBuyxState>(context,
                                              listen: false)
                                          .itemsList = [];
                                      loginProvider.changed();
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  NavigationBuyx()),
                                          (Route<dynamic> route) => false);
                                    },
                                  )..show();
                                } catch (e) {
                                  AwesomeDialog(
                                    buttonsTextStyle: GoogleFonts.comfortaa(),
                                    context: context,
                                    dialogType: DialogType.INFO,
                                    animType: AnimType.BOTTOMSLIDE,
                                    //title: 'Dialog Title',
                                    desc:
                                        "Connection problem. Please try again later",
                                    btnCancelOnPress: () {},
                                    btnOkOnPress: () {},
                                  )..show();
                                }
                              },
                              child: ProfilePageIconRow(
                                givenIcon: Icon(
                                  Icons.sensor_door,
                                  color: kBuyxRedColor,
                                ),
                                givenText: "Log Out",
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Language",
                          style: GoogleFonts.comfortaa(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                          textScaleFactor: 1,
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.language,
                            color: kBuyxRedColor,
                          ),
                          givenText: "English",
                        ),
                      ),
                      /*Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Version",
                          style: GoogleFonts.comfortaa(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                          textScaleFactor: 1,
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.verified,
                            color: kBuyxRedColor,
                          ),
                          givenText: "Version",
                        ),
                      )*/
                    ],
                  ),
                ),
              )
            : SingleChildScrollView(
                child: SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20,
                        ),
                        child: Card(
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LogInPage()));
                                },
                                child: ProfilePageIconRow(
                                  givenIcon: Icon(
                                    Icons.person,
                                    color: kBuyxRedColor,
                                  ),
                                  givenText: "Login",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Language",
                          style: GoogleFonts.comfortaa(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                          textScaleFactor: 1,
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.language,
                            color: kBuyxRedColor,
                          ),
                          givenText: "English",
                        ),
                      ),
                      /*Container(
                        padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                        child: Text(
                          "Version",
                          style: GoogleFonts.comfortaa(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          ),
                          textScaleFactor: 1,
                        ),
                      ),
                      Card(
                        child: ProfilePageIconRow(
                          givenIcon: Icon(
                            Icons.verified,
                            color: kPurpleColor,
                          ),
                          givenText: "Version",
                        ),
                      )*/
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
