import 'package:flutter/material.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfilePageIconRow extends StatelessWidget {
  const ProfilePageIconRow({this.givenText, this.givenIcon});
  final String givenText;
  final Icon givenIcon;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        children: [
          Expanded(flex: 2, child: givenIcon),
          Expanded(
            flex: 8,
            child: Text(
              givenText,
              style: GoogleFonts.comfortaa(
                color: kBuyxBlueColor,
                fontWeight: FontWeight.w900,
              ),
              textScaleFactor: 1,
            ),
          ),
        ],
      ),
    );
  }
}
