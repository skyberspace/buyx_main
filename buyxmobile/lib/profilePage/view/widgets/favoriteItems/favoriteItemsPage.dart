import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/BuyxPage/itemTabs/widgets/BuyxItem.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';

class FavoriteItemsPage extends StatefulWidget {
  @override
  _FavoriteItemsPageState createState() => _FavoriteItemsPageState();
}

class _FavoriteItemsPageState extends State<FavoriteItemsPage> {
  bool loading = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    try {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        Provider.of<CartBuyxState>(context, listen: false).getFavorites();
        loading = false;
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    var loginProvider = Provider.of<LoggedInState>(context);
    var cartprovider = Provider.of<CartBuyxState>(context);
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    //print(cartprovider.favorites.first);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Favorite Products",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              if (cartprovider.favorites.length > 0) ...{
                Material(
                  elevation: 10,
                  child: GridView.count(
                      primary: false,
                      shrinkWrap: true,
                      crossAxisCount: 3,
                      physics: NeverScrollableScrollPhysics(),
                      childAspectRatio: MediaQuery.of(context).size.width /
                          4 /
                          (MediaQuery.of(context).size.height / 5),
                      children: List.generate(
                        cartprovider.favorites.length,
                        (index) => BuyxItem(
                          product: cartprovider.favorites[index].product,
                        ),
                      )),
                ),
                SizedBox(
                  height: 10,
                ),
              } else /*if (cartprovider.favorites.length == 0)*/ ...{
                SizedBox(
                  height: MediaQuery.of(context).size.height / 4,
                ),
                Icon(
                  Icons.favorite_border_rounded,
                  color: kBuyxBlueColor,
                  size: 150,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "You haven't add any product to favorites.",
                  style: GoogleFonts.comfortaa(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: kBuyxBlueColor,
                  ),
                  textScaleFactor: 1,
                  textAlign: TextAlign.center,
                ),
                /*SizedBox(
                  height: 3,
                ),
                Text(
                  "You haven't add any product to favorites.",
                  style: GoogleFonts.comfortaa(
                    fontSize: 10,
                    color: Color(0x00FD3525),
                  ),
                ),*/
                /*SizedBox(
                    height: MediaQuery.of(context).size.height / 4,
                  ),*/
              }
            ],
          ),
        ),
      ),
    );
  }
}
