import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/profilePage/view/profilePage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:provider/provider.dart';

class ChangeInformation extends StatefulWidget {
  Function updateCallback;

  ChangeInformation({this.updateCallback});
  @override
  _ChangeInformationState createState() => _ChangeInformationState();
}

class _ChangeInformationState extends State<ChangeInformation> {
  TextEditingController controllerMail = TextEditingController();
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerSurname = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    try {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        var loginProvider = Provider.of<LoggedInState>(context, listen: false);
        controllerMail = TextEditingController(text: loginProvider.user.email);
        controllerName = TextEditingController(text: loginProvider.user.name);
        controllerSurname =
            TextEditingController(text: loginProvider.user.surname);
        controllerPhone = TextEditingController(text: loginProvider.user.phone);
        setState(() {});
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    var loginProvider = Provider.of<LoggedInState>(context, listen: false);
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Change Information",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: Form(
        key: _formKey,
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.only(top: 5, bottom: 5),
                child: Container(
                  //padding: EdgeInsets.only(left: 15, top: 10, right: 10),
                  child: Row(
                    children: [
                      /*Container(
                        width: MediaQuery.of(context).size.width / 8, // 52
                        height: MediaQuery.of(context).size.height / 15.6, // 52
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7),
                            /*  image: DecorationImage(
        image: AssetImage("images/facebookSymbol.png"),
        fit: BoxFit.fill),*/
                            color: kBuyxBlueColor),
                      ),*/
                      Expanded(
                        flex: 2,
                        child: CircleAvatar(
                          radius: 25,
                          child: Image.asset("assets/circleLogo.png"),
                        ),
                      ),
                      Expanded(
                        flex: 8,
                        child: Column(
                          children: [
                            IntrinsicHeight(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        "Name",
                                        style: GoogleFonts.comfortaa(
                                            fontWeight: FontWeight.bold),
                                        textScaleFactor: 1,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        child: TextFormField(
                                          keyboardType: TextInputType.name,
                                          controller: controllerName,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Cant be empty';
                                            }
                                            return null;
                                          },
                                          style: GoogleFonts.comfortaa(
                                            color: kBuyxBlueColor,
                                            fontWeight: FontWeight.w900,
                                          ),
                                          cursorColor: kBuyxBlueColor,
                                          decoration: new InputDecoration(
                                            border: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  VerticalDivider(
                                    color: Colors.grey,
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        "Surname",
                                        style: GoogleFonts.comfortaa(
                                            fontWeight: FontWeight.bold),
                                        textScaleFactor: 1,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                3,
                                        child: TextFormField(
                                          keyboardType: TextInputType.name,
                                          controller: controllerSurname,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Cant be empty';
                                            }
                                            return null;
                                          },
                                          style: GoogleFonts.comfortaa(
                                            color: kBuyxBlueColor,
                                            fontWeight: FontWeight.w900,
                                          ),
                                          cursorColor: kBuyxBlueColor,
                                          decoration: new InputDecoration(
                                            border: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Divider(),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  children: [
                    Text("Email",
                        textScaleFactor: 1,
                        style:
                            GoogleFonts.comfortaa(fontWeight: FontWeight.bold)),
                    Row(
                      children: [
                        Expanded(
                            flex: 2,
                            child: Icon(
                              Icons.mail,
                              color: kBuyxRedColor,
                            )),
                        Expanded(
                          flex: 8,
                          child: TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            controller: controllerMail,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Cant be empty';
                              } else if (!RegExp(
                                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                  .hasMatch(value)) {
                                return 'Please enter a valid E-mail';
                              } else {
                                return null;
                              }
                            },
                            style: GoogleFonts.comfortaa(
                              color: kBuyxBlueColor,
                              fontWeight: FontWeight.w900,
                            ),
                            cursorColor: kBuyxBlueColor,
                            decoration: new InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              /*Divider(),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Icon(
                          Icons.phone,
                          color: kPurpleColor,
                        )),
                    Expanded(
                      flex: 8,
                      child:
                          /*Text(
                        '5555555555555',
                        style: GoogleFonts.comfortaa(
                          color: kGreyColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),*/
                          IntlPhoneField(
                        textAlign: TextAlign.center,
                        style: GoogleFonts.comfortaa(color: kBuyxBlueColor),
                        decoration: InputDecoration(
                          labelStyle:
                              GoogleFonts.comfortaa(color: kYellowColor.withOpacity(0.5)),
                          counterStyle: GoogleFonts.comfortaa(color: Colors.red),
                          prefixStyle: GoogleFonts.comfortaa(color: kBuyxBlueColor),
                          labelText: 'Phone Number',
                          border: OutlineInputBorder(
                            borderSide: BorderSide(),
                          ),
                        ),
                        initialCountryCode: 'GB',
                        onChanged: (phone) {
                          this.controllerPhone.text = phone.completeNumber;
                          print(phone
                              .completeNumber); // When sending message from otp, use the phone.completeNumber
                        },
                      ),
                    ),
                  ],
                ),
              ),*/
              /*SizedBox(
                height: 10,
              ),*/
              Divider(),
              ElevatedButton(
                  style: ButtonStyle(
                      textStyle: MaterialStateProperty.all<TextStyle>(
                        GoogleFonts.comfortaa(color: Colors.white),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kBuyxRedColor)),
                  onPressed: () async {
                    try {
                      var changeInfoResult = await Api.instance.profile
                          .profileChangeInformationPost(
                              changeInformtionRequest: ChangeInformtionRequest(
                                  email: this.controllerMail.text,
                                  name: this.controllerName.text,
                                  surname: this.controllerSurname.text));
                      if (changeInfoResult.success)
                        Provider.of<LoggedInState>(context, listen: false)
                            .user
                            .email = this.controllerMail.text;
                      Provider.of<LoggedInState>(context, listen: false)
                          .user
                          .name = this.controllerName.text;
                      Provider.of<LoggedInState>(context, listen: false)
                          .user
                          .surname = this.controllerSurname.text;
                      print(changeInfoResult.success);
                      this.widget.updateCallback?.call();
                      Navigator.pop(context);
                    } catch (e) {
                      AwesomeDialog(
                        buttonsTextStyle: GoogleFonts.comfortaa(),
                        context: context,
                        dialogType: DialogType.INFO,
                        animType: AnimType.BOTTOMSLIDE,
                        //title: 'Dialog Title',
                        desc: "Connection problem. Please try again later",
                        btnCancelOnPress: () {},
                        btnOkOnPress: () {
                          Navigator.pop(context);
                        },
                      )..show();
                    }
                  },
                  child: Text(
                    'Save',
                    style: GoogleFonts.comfortaa(),
                    textScaleFactor: 1,
                  )),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
