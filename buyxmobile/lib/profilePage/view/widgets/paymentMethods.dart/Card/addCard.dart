import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class AddCard extends StatefulWidget {
  @override
  _AddCardState createState() => _AddCardState();
}

class _AddCardState extends State<AddCard> {
  bool checkedValue = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Kart Ekle",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 3.6, // 52
                      height: MediaQuery.of(context).size.height / 7.8, // 52
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Security",
                            style: GoogleFonts.comfortaa(
                              color: kBuyxRedColor,
                              fontWeight: FontWeight.bold,
                            ),
                            textScaleFactor: 1,
                          ),
                          Text(
                            "Ödeme altyapımız MasterCard uygulaması olan MasterPass tarafından sağlanmaktadır ve işlem güvenliğimiz MasterCard güvencesi altındadır.",
                            style: GoogleFonts.comfortaa(),
                            textScaleFactor: 1,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(12),
                child: TextFormField(
                  style: GoogleFonts.comfortaa(),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      hintStyle: GoogleFonts.comfortaa(),
                      helperText: '',
                      hintText: "Karta İsim Ver (Kişisel, İş vb.",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            style: BorderStyle.solid,
                          ))),
                ),
              ),
              Container(
                padding: EdgeInsets.all(12),
                child: TextFormField(
                  style: GoogleFonts.comfortaa(),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Cant be empty';
                    } else if (!RegExp(r'[0-9]').hasMatch(value)) {
                      return 'Please enter a valid Card';
                    } else {
                      return null;
                    }
                  },
                  decoration: InputDecoration(
                      hintStyle: GoogleFonts.comfortaa(),
                      helperText: '',
                      hintText: "Kart Numarası",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            style: BorderStyle.solid,
                          ))),
                ),
              ),
              Container(
                margin: EdgeInsets.all(12),
                child: TextFormField(
                  style: GoogleFonts.comfortaa(),
                  keyboardType: TextInputType.datetime,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    helperText: '',
                    hintStyle: GoogleFonts.comfortaa(),
                    hintText: "Kartın Son Kullanım Tarihi",
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                ),
              ),
              CheckboxListTile(
                checkColor: Colors.white,
                activeColor: kBuyxRedColor,
                title: RichText(
                  textScaleFactor: 1,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Kullanım Koşullarını",
                        style: GoogleFonts.comfortaa(
                            color: kBuyxRedColor, fontSize: 14),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => print('Tap Here onTap'),
                      ),
                      TextSpan(
                          text: "'nı okudum ve kabul ediyorum.",
                          style: GoogleFonts.comfortaa(
                              color: Colors.black, fontSize: 14)),
                    ],
                  ),
                ),
                value: checkedValue,
                onChanged: (newValue) {
                  try {
                    setState(() {
                      checkedValue = newValue;
                    });
                    print(checkedValue);
                  } catch (e) {
                    AwesomeDialog(
                      buttonsTextStyle: GoogleFonts.comfortaa(),
                      context: context,
                      dialogType: DialogType.INFO,
                      animType: AnimType.BOTTOMSLIDE,
                      //title: 'Dialog Title',
                      desc: "Connection problem. Please try again later",
                      btnCancelOnPress: () {},
                      btnOkOnPress: () {
                        Navigator.pop(context);
                      },
                    )..show();
                  }
                },
                controlAffinity:
                    ListTileControlAffinity.leading, //  <-- leading Checkbox
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kBuyxRedColor),
                    ),
                    onPressed: () {
                      _formKey.currentState.validate();
                    },
                    child: Text(
                      "Continue",
                      style: GoogleFonts.comfortaa(
                        color: Colors.white,
                      ),
                      textScaleFactor: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
