import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class PasswordChangePage extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  TextEditingController oldPassword = TextEditingController();
  TextEditingController newPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Change Password",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: EdgeInsets.all(12),
                  child: TextFormField(
                    style: GoogleFonts.comfortaa(),
                    controller: oldPassword,
                    /*validator: (value) {
                      if (value.isEmpty) {
                        return 'Cant be Empty';
                      } else if (value.length <= 8)
                        return 'The password you enter must be at least 8 characters!';
                    },*/
                    decoration: InputDecoration(
                        helperText: '',
                        hintText: "Old Password",
                        hintStyle: GoogleFonts.comfortaa(),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              style: BorderStyle.solid,
                            ))),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(12),
                  child: TextFormField(
                    style: GoogleFonts.comfortaa(),
                    controller: newPassword,
                    /*validator: (value) {
                      if (value.isEmpty) {
                        return 'Cant be Empty';
                      } else if (value.length <= 8)
                        return 'The password you enter must be at least 8 characters!';
                    },*/
                    decoration: InputDecoration(
                      hintStyle: GoogleFonts.comfortaa(),
                      helperText: '',
                      hintText: "New Password (at least 8 characters)",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          style: BorderStyle.solid,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(kBuyxRedColor),
                      ),
                      onPressed: () async {
                        try {
                          var changePasswordResult = await Api.instance.profile
                              .profileChangePasswordPost(
                                  changePasswordRequest: ChangePasswordRequest(
                                      oldPassword: oldPassword.text,
                                      newPassword: newPassword.text));
                          //Provider.of<LoggedInState>(context).user = changePasswordResult.
                          if (changePasswordResult.success) {
                            AwesomeDialog(
                              buttonsTextStyle: GoogleFonts.comfortaa(),
                              context: context,
                              dialogType: DialogType.SUCCES,
                              animType: AnimType.BOTTOMSLIDE,
                              //title: 'Dialog Title',
                              desc: "Your password has been changed",
                              btnCancelOnPress: () {},
                              btnOkOnPress: () {
                                Navigator.pop(context);
                              },
                            )..show();
                            //TODO: Show dialog that change success or false
                            Navigator.of(context).pop();
                          } else {
                            AwesomeDialog(
                              buttonsTextStyle: GoogleFonts.comfortaa(),
                              context: context,
                              dialogType: DialogType.WARNING,
                              animType: AnimType.BOTTOMSLIDE,
                              //title: 'Dialog Title',
                              desc: changePasswordResult.errors[0],
                              btnCancelOnPress: () {},
                              btnOkOnPress: () {
                                Navigator.pop(context);
                              },
                            )..show();
                          }
                        } catch (e) {
                          AwesomeDialog(
                            buttonsTextStyle: GoogleFonts.comfortaa(),
                            context: context,
                            dialogType: DialogType.INFO,
                            animType: AnimType.BOTTOMSLIDE,
                            //title: 'Dialog Title',
                            desc: "Connection problem. Please try again later",
                            btnCancelOnPress: () {},
                            btnOkOnPress: () {
                              Navigator.pop(context);
                            },
                          )..show();
                        }
                      },
                      child: Text(
                        "Save",
                        style: GoogleFonts.comfortaa(
                          color: Colors.white,
                        ),
                        textScaleFactor: 1,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
