import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class AddAdressPage extends StatefulWidget {
  AddAdressPage({@required this.adress});
  var adress;

  @override
  _AddAdressPageState createState() => _AddAdressPageState();
}

class _AddAdressPageState extends State<AddAdressPage> {
  String tagDropdownValue = 'Home';
  TextEditingController titleEdit = TextEditingController();
  TextEditingController addressEdit = TextEditingController();
  final buildingNoEdit = TextEditingController();
  final floorEdit = TextEditingController();
  final apartmentNoEdit = TextEditingController();
  final directionsEdit = TextEditingController();

  @override
  void initState() {
    super.initState();
    addressEdit = TextEditingController(
        text: widget.adress != null &&
                widget.adress.first != null &&
                widget.adress.first.addressLine != null
            ? widget.adress.first.addressLine
            : Text(""));
    titleEdit = TextEditingController(text: "Home");
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.adress == null ||
          widget.adress.first == null ||
          widget.adress.first.addressLine == null) Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    var userProvider = Provider.of<LoggedInState>(context);
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Add New Address",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Material(
          elevation: 10,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(12)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(6),
                  child: Row(
                    children: [
                      DropdownButton<String>(
                        value: tagDropdownValue,
                        dropdownColor: Colors.white,
                        icon: Icon(
                          Icons.arrow_drop_down_outlined,
                          color: Colors.black,
                        ),
                        style: GoogleFonts.comfortaa(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w400,
                          color: Colors.black26,
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            tagDropdownValue = newValue;
                            titleEdit.text = newValue;
                          });
                        },
                        items: <String>[
                          'Home',
                          'Work',
                          'School',
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                              value,
                              style: GoogleFonts.comfortaa(color: Colors.black),
                              textScaleFactor: 1,
                            ),
                          );
                        }).toList(),
                      ),
                      Expanded(flex: 1, child: SizedBox()),
                      Expanded(
                        flex: 6,
                        child: Container(
                          padding: EdgeInsets.all(6),
                          child: TextFormField(
                            controller: titleEdit,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              labelText: "Title",
                              labelStyle: GoogleFonts.comfortaa(
                                color: Colors.black45,
                              ),
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(6),
                  child: TextFormField(
                    controller: addressEdit,
                    //initialValue: widget.adress.first.addressLine,
                    cursorColor: Colors.black,
                    decoration: InputDecoration(
                      labelText: "Address",
                      labelStyle: GoogleFonts.comfortaa(
                        color: Colors.black45,
                      ),
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: new BorderSide(
                          color: kBuyxSecondRedColor,
                          style: BorderStyle.solid,
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                        borderSide: new BorderSide(
                          color: kBuyxSecondRedColor,
                          style: BorderStyle.solid,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 14,
                        child: Container(
                          padding: EdgeInsets.all(6),
                          child: TextFormField(
                            controller: buildingNoEdit,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              labelText: "Building No",
                              labelStyle: GoogleFonts.comfortaa(
                                color: Colors.black45,
                              ),
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(flex: 1, child: SizedBox()),
                      Expanded(
                        flex: 14,
                        child: Container(
                          padding: EdgeInsets.all(6),
                          child: TextFormField(
                            controller: floorEdit,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              labelText: "Floor",
                              labelStyle: GoogleFonts.comfortaa(
                                color: Colors.black45,
                              ),
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(flex: 1, child: SizedBox()),
                      Expanded(
                        flex: 14,
                        child: Container(
                          padding: EdgeInsets.all(6),
                          child: TextFormField(
                            controller: apartmentNoEdit,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              labelText: "Apartment Number",
                              labelStyle: GoogleFonts.comfortaa(
                                color: Colors.black45,
                              ),
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(
                                  color: kBuyxSecondRedColor,
                                  style: BorderStyle.solid,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(6),
                  child: TextFormField(
                    controller: directionsEdit,
                    cursorColor: Colors.black,
                    decoration: InputDecoration(
                      labelText: "Address Directions",
                      labelStyle: GoogleFonts.comfortaa(
                        color: Colors.black45,
                      ),
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: new BorderSide(
                          color: kBuyxSecondRedColor,
                          style: BorderStyle.solid,
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                        borderSide: new BorderSide(
                          color: kBuyxSecondRedColor,
                          style: BorderStyle.solid,
                        ),
                      ),
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () async {
                    AddressBR saveResult;
                    try {
                      saveResult =
                          await Api.instance.profile.profileSaveAddressPost(
                        saveAddressRequest: SaveAddressRequest(
                          setLocation: FrontendPoint(
                              latitude:
                                  widget.adress.first.coordinates.latitude,
                              longitude:
                                  widget.adress.first.coordinates.longitude),
                          address: Address(
                            //id: 1,
                            addressLine1: addressEdit.text,
                            addressName: titleEdit.text,
                            buildingNo: buildingNoEdit.text,
                            //createdAt: DateTime.now().toUtc(),
                            directions: directionsEdit.text,
                            //countryNameCache: widget.adress.first.countryName,
                            createdAt: DateTime.now().toUtc(),
                            latitude: widget.adress.first.coordinates.latitude,
                            longitude:
                                widget.adress.first.coordinates.longitude,
                            flat: apartmentNoEdit.text,
                            //updatedAt: DateTime.now().toUtc(),
                            floor: floorEdit.text,
                            updatedAt: DateTime.now().toUtc(),
                            zipCode: widget.adress.first.postalCode.toString(),
                            //user: LoggedInState().user.data,
                            user: userProvider.user,
                            userId: userProvider.user.id,
                            //userId: LoggedInState().user.data.id,
                          ),
                        ),
                      );
                      if (saveResult.success)
                        Provider.of<CartBuyxState>(context, listen: false)
                            .itemsList = [];
                      if (!saveResult.success) {
                        return AwesomeDialog(
                          buttonsTextStyle: GoogleFonts.comfortaa(),
                          context: context,
                          dialogType: DialogType.INFO,
                          animType: AnimType.BOTTOMSLIDE,
                          //title: 'Dialog Title',
                          desc: saveResult.errors[0],
                          btnCancelOnPress: () {},
                          btnOkOnPress: () {
                            Navigator.pop(context);
                          },
                        )..show();
                      }
                      userProvider.userDefaultAddress = await Api
                          .instance.profile
                          .profileGetDefaultAddressPost();
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NavigationBuyx()),
                        (Route<dynamic> route) => false,
                      );
                    } catch (e) {
                      AwesomeDialog(
                        buttonsTextStyle: GoogleFonts.comfortaa(),
                        context: context,
                        dialogType: DialogType.INFO,
                        animType: AnimType.BOTTOMSLIDE,
                        //title: 'Dialog Title',
                        desc: "Connection problem. Please try again later",
                        btnCancelOnPress: () {},
                        btnOkOnPress: () {
                          Navigator.pop(context);
                        },
                      )..show();
                    }
                  },
                  child: Text(
                    'Save',
                    style: GoogleFonts.comfortaa(),
                    textScaleFactor: 1,
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(kBuyxRedColor),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
