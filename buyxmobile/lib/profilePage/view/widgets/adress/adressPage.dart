import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/BuyxPage/BuyxPage.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/profilePage/view/widgets/adress/addAdressPage.dart';
import 'package:buyx/profilePage/view/widgets/adress/findAdressPage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';

class AdressPage extends StatefulWidget {
  AdressPage(this.user);
  User user;
  bool loading = true;
  @override
  _AdressPageState createState() => _AdressPageState();
}

class _AdressPageState extends State<AdressPage> {
  AddressListBR addressesResult;

  List<Address> addresses;

  //String defaultAddress;

  AddressBR defaultAddressResult;

  Address defaultAddress;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    load();
  }

  load() async {
    try {
      this.widget.loading = true;
      addressesResult =
          await Api.instance.profile.profileGetUserAddressesPost();
      addresses = addressesResult.data;
      if (addresses.isNotEmpty) {
        defaultAddressResult =
            await Api.instance.profile.profileGetDefaultAddressPost();
        if (defaultAddressResult != null)
          defaultAddress = defaultAddressResult.data;
      }

      setState(() {
        this.widget.loading = false;
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (this.widget.loading)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    else
      return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Address",
            style: GoogleFonts.comfortaa(
              color: Colors.white,
            ),
            textScaleFactor: 1,
          ),
          backgroundColor: kBuyxRedColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (addresses.length >= 0) ...{
                  Material(
                    elevation: 5,
                    child: ListView.separated(
                      itemCount: /*adresses.length*/ addresses.length,
                      primary: false,
                      shrinkWrap: true,
                      separatorBuilder: (BuildContext context, int index) =>
                          Divider(),
                      itemBuilder: (context, index) {
                        return AdressRow(
                          adressId: addresses[index].id,
                          icon: /*adresses[index].icon*/ defaultAddress.id ==
                                  addresses[index].id
                              ? Icon(Icons.toggle_on)
                              : Icon(Icons.toggle_off),
                          adress: /*adresses[index].adress*/
                              addresses[index].addressLine1 != null
                                  ? addresses[index].addressLine1
                                  : "",
                          adressHeader: /*adresses[index].adressHeader*/ addresses[
                                          index]
                                      .addressName !=
                                  null
                              ? addresses[index].addressName
                              : "",
                        );
                      },
                    ),
                  ),
                },
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Add address",
                          style: GoogleFonts.comfortaa(
                              color: kBuyxBlueColor,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                          textScaleFactor: 1,
                        ),
                      ]),
                ),
                Material(
                  elevation: 5,
                  child: Container(
                    color: Colors.white,
                    child: Column(
                      children: [
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FindAdressPage()));
                          },
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(8),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.place,
                                      color: kBuyxRedColor,
                                    ),
                                    VerticalDivider(),
                                    Text(
                                      'Add Address',
                                      style: GoogleFonts.comfortaa(
                                          fontWeight: FontWeight.bold),
                                      textScaleFactor: 1,
                                    ),
                                    Spacer(),
                                    Icon(
                                      Icons.add,
                                      color: kBuyxRedColor,
                                    )
                                  ],
                                ),
                              ),
                              /*Divider(),
                              Container(
                                padding: EdgeInsets.all(8),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.business,
                                      color: kPurpleColor,
                                    ),
                                    VerticalDivider(),
                                    Text(
                                      'Add Business Address',
                                      style: GoogleFonts.comfortaa(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Spacer(),
                                    Icon(
                                      Icons.add,
                                      color: kPurpleColor,
                                    )
                                  ],
                                ),
                              ),
                              Divider(),
                              Container(
                                padding: EdgeInsets.all(8),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.location_on,
                                      color: kPurpleColor,
                                    ),
                                    VerticalDivider(),
                                    Text(
                                      'Add Another Address',
                                      style: GoogleFonts.comfortaa(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Spacer(),
                                    Icon(
                                      Icons.add,
                                      color: kPurpleColor,
                                    )
                                  ],
                                ),
                              ),*/
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
  }
}

class AdressRow extends StatefulWidget {
  AdressRow({this.icon, this.adress, this.adressHeader, this.adressId});

  Icon icon;
  final String adressHeader;
  final String adress;
  final int adressId;

  @override
  _AdressRowState createState() => _AdressRowState();
}

class _AdressRowState extends State<AdressRow> {
  bool deleted = false;
  @override
  Widget build(BuildContext context) {
    if (deleted) return Container();
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 18,
          child: GestureDetector(
            onTap: () async {
              try {
                var setDefaultResult = await Api.instance.profile
                    .profileSetDefaultAddressPost(
                        addressRequest:
                            AddressRequest(addressId: widget.adressId));
                if (setDefaultResult.success) {
                  Provider.of<LoggedInState>(context, listen: false)
                          .userDefaultAddress =
                      await Api.instance.profile.profileGetDefaultAddressPost();
                  var cartProvider =
                      Provider.of<CartBuyxState>(context, listen: false);
                  cartProvider.loadCart(context: context);
                  /*var loadCartResult = cartProvider.loadCart();
                  if (loadCartResult != 1)
                    AwesomeDialog(
                      buttonsTextStyle: GoogleFonts.comfortaa(),
                      context: context,
                      dialogType: DialogType.INFO,
                      animType: AnimType.BOTTOMSLIDE,
                      //title: 'Dialog Title',
                      desc: loadCartResult,
                      btnCancelOnPress: () {},
                      btnOkOnPress: () {
                        Navigator.pop(context);
                      },
                    )..show();*/
                  //widget.icon = Icon(Icons.toggle_on);
                  Navigator.pop(context);
                  /*Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => BuyxPage()));*/
                  //(Route<dynamic> route) => false);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AdressPage(
                              /*loginProvider.user*/ Provider.of<LoggedInState>(
                                      context)
                                  .user)));
                }
              } catch (e) {
                AwesomeDialog(
                  buttonsTextStyle: GoogleFonts.comfortaa(),
                  context: context,
                  dialogType: DialogType.INFO,
                  animType: AnimType.BOTTOMSLIDE,
                  //title: 'Dialog Title',
                  desc: "Connection problem. Please try again later",
                  btnCancelOnPress: () {},
                  btnOkOnPress: () {
                    Navigator.pop(context);
                  },
                )..show();
              }
            },
            child: Row(
              children: [
                IconTheme(
                    child: widget.icon,
                    data: widget.icon.icon == Icons.toggle_on
                        ? IconThemeData(
                            color: kBuyxRedColor,
                          )
                        : IconThemeData(
                            color: Colors.grey,
                          )),
                VerticalDivider(),
                widget.adressHeader != null
                    ? Text(
                        widget.adressHeader,
                        style:
                            GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                      )
                    : Text(""),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: widget.adress == null
                      ? Text("")
                      : widget.adress.length > 0
                          ? Text(
                              widget.adress,
                              style: GoogleFonts.comfortaa(color: Colors.grey),
                              textScaleFactor: 1,
                              textAlign: TextAlign.justify,
                            )
                          : Text(
                              widget.adress,
                              style: GoogleFonts.comfortaa(color: Colors.grey),
                              textScaleFactor: 1,
                            ),
                ),
                SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () async {
                    try {
                      var deleteAddressResult = await Api.instance.profile
                          .profileDeleteAddressPost(
                              addressRequest: AddressRequest(
                                  addressId: this.widget.adressId));
                      if (deleteAddressResult.success) print("Adres silindi");
                      setState(() {
                        deleted = true;
                      });
                    } catch (e) {
                      AwesomeDialog(
                        buttonsTextStyle: GoogleFonts.comfortaa(),
                        context: context,
                        dialogType: DialogType.INFO,
                        animType: AnimType.BOTTOMSLIDE,
                        //title: 'Dialog Title',
                        desc: "Connection problem. Please try again later",
                        btnCancelOnPress: () {},
                        btnOkOnPress: () {
                          Navigator.pop(context);
                        },
                      )..show();
                    }
                  },
                  child: Icon(
                    Icons.delete,
                    color: kBuyxRedColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
