import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/profilePage/view/widgets/adress/addAdressPage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_places_flutter/google_places_flutter.dart';
import 'package:google_places_flutter/model/prediction.dart';
import 'package:permission_handler/permission_handler.dart';

class FindAdressPage extends StatefulWidget {
  @override
  _FindAdressPageState createState() => _FindAdressPageState();
}

class _FindAdressPageState extends State<FindAdressPage> {
  double longtitudeData = -0.08775623704433322;
  double latitudeData = 51.50799845867318;
  bool isDone = false;

  TextEditingController searchTextController = TextEditingController();
  GoogleMapController googleMapController;
  bool refreshControl = true;
  List<Address> adress;

  List<Address> currentAdress;

  Object redrawObject;
  @override
  void initState() {
    super.initState();
    _getCurrentLocation().then((value) =>
        _getCurrentAdress(Coordinates(latitudeData, longtitudeData)));
    print(isDone);
  }

  getLocation() async {}

  Future _getCurrentLocation() async {
    try {
      final geoPosition = await Geolocator.getCurrentPosition(
              desiredAccuracy: LocationAccuracy.best)
          .timeout(Duration(seconds: 10), onTimeout: () async {
        return await Geolocator.getLastKnownPosition()
            .timeout(Duration(seconds: 10));
      });

      longtitudeData = geoPosition.longitude;
      latitudeData = geoPosition.latitude;
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Please check location and connection settings!",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
    /*Future.delayed(Duration(seconds: 1), () {
      setState(() {
        longtitudeData = geoPosition.longitude;
        latitudeData = geoPosition.latitude;
      });
      print(longtitudeData);
      print(latitudeData);
    });*/
  }

  _getCurrentAdress(Coordinates coordinates) async {
    //locatily is city ----  sublocality is districtnamecache --- thoroughfare is street name -----
    /*List<Address>*/ try {
      adress = await Geocoder.local.findAddressesFromCoordinates(coordinates);
      Future.delayed(Duration(seconds: 2), () {
        setState(() {
          isDone = true;
          print(adress.first.addressLine);
          currentAdress = adress;
        });
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  Set<Marker> _markers = {};

  void _onMapCreated(googleMapController) {
    try {
      setState(() {
        _markers.add(Marker(
            draggable: true,
            markerId: MarkerId('id1'),
            position: LatLng(latitudeData, longtitudeData),
            infoWindow: InfoWindow(title: 'Your Current Location'),
            onDragEnd: ((newPosition) {
              setState(() {
                _getCurrentAdress(
                    Coordinates(newPosition.latitude, newPosition.longitude));
              });
            })));
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  /*GoogleMapsPlaces _places =
      GoogleMapsPlaces(apiKey: "AIzaSyDZZ95Gu67iSeQWI_yNKkl9uiiHj-dytac");*/
  @override
  Widget build(BuildContext context) {
    if (isDone == false) {
      return Scaffold(body: Center(child: CircularProgressIndicator()));
    } else {
      if (currentAdress == 'North Atlantic Ocean') {
        //_getCurrentAdress(Coordinates(latitudeData, longtitudeData));
      }
      return Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
            title: Text(
              "Add New Address",
              style: GoogleFonts.comfortaa(
                color: Colors.white,
              ),
              textScaleFactor: 1,
            ),
            backgroundColor: kBuyxRedColor,
            elevation: 0,
          ),
          body: Stack(
            children: [
              if (refreshControl)
                GoogleMap(
                  key: ValueKey<Object>(redrawObject),
                  onMapCreated: _onMapCreated,
                  markers: _markers,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(latitudeData, longtitudeData),
                    zoom: 17,
                  ),
                  myLocationButtonEnabled: false,
                ),
              Center(
                child: Column(
                  children: [
                    /*Container(
                      width: MediaQuery.of(context).size.width,
                      color: kYellowColor,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                        child: Text(
                          "Search Another address",
                          style: GoogleFonts.comfortaa(color: Colors.white),
                        ),
                      ),
                    ),*/
                    Container(
                      height: MediaQuery.of(context).size.height / 20,
                      color: Colors.white,
                      child: Center(
                        child: GooglePlaceAutoCompleteTextField(
                            textStyle: GoogleFonts.comfortaa(),
                            textEditingController: searchTextController,
                            googleAPIKey:
                                "AIzaSyDZZ95Gu67iSeQWI_yNKkl9uiiHj-dytac",
                            inputDecoration: InputDecoration(
                                hintStyle: GoogleFonts.comfortaa(),
                                contentPadding:
                                    EdgeInsets.fromLTRB(15, 0, 15, 0),
                                hintText: "Search Another Address"),
                            debounceTime: 800, // default 600 ms,
                            countries: [
                              "uk",
                              //"fr"
                            ], // optional by default null is set
                            isLatLngRequired:
                                true, // if you required coordinates from place detail
                            getPlaceDetailWithLatLng: (Prediction prediction) {
                              // this method will return latlng with place detail
                              print("placeDetails" + prediction.lng.toString());
                              latitudeData = double.parse(prediction.lat);
                              longtitudeData = double.parse(prediction.lng);

                              setState(() {
                                _getCurrentAdress(
                                    Coordinates(latitudeData, longtitudeData));
                                redrawObject = Object();
                                print(currentAdress.first.coordinates.latitude);
                              });
                            }, // this callback is called when isLatLngRequired is true
                            itmClick: (Prediction prediction) {
                              searchTextController.text =
                                  prediction.description;
                              searchTextController.selection =
                                  TextSelection.fromPosition(TextPosition(
                                      offset: prediction.description.length));
                            }),
                      ),
                    ),
                    /*Container(
                        alignment: Alignment.center,
                        child: RaisedButton(
                          onPressed: () async {
                            /*
                            Prediction p = await PlacesAutocomplete.show(
                                context: context,
                                apiKey:
                                    "AIzaSyDZZ95Gu67iSeQWI_yNKkl9uiiHj-dytac",
                                mode: Mode.overlay, // Mode.fullscreen
                                language: "en",
                                components: [
                                  new Component(Component.country, "uk")
                                ]);*/
                          },
                          child: Text('Find address'),
                        )),*/
                    Material(
                      elevation: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Row(
                          children: [
                            Icon(
                              Icons.edit_location,
                              color: kBuyxRedColor,
                            ),
                            Expanded(
                              child: TextButton(
                                onPressed: () => _getCurrentAdress(
                                    Coordinates(latitudeData, longtitudeData)),
                                child: Text(
                                  !(currentAdress == 'North Atlantic Ocean')
                                      ? currentAdress.first.addressLine
                                      : '',
                                  style: GoogleFonts.comfortaa(
                                    fontSize: 14,
                                    color: Colors.black,
                                  ),
                                  textScaleFactor: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.8,
                      height: MediaQuery.of(context).size.height / 15,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40)),
                      margin: EdgeInsets.all(24),
                      child: ElevatedButton(
                        onPressed: () {
                          AwesomeDialog(
                            buttonsTextStyle: GoogleFonts.comfortaa(),
                            context: context,
                            dialogType: DialogType.INFO,
                            animType: AnimType.BOTTOMSLIDE,
                            title:
                                'Are you sure you entered the address correctly?',
                            desc:
                                "In the next step, you will enter your address in detail.",
                            btnCancelOnPress: () {},
                            btnOkOnPress: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AddAdressPage(
                                      adress: currentAdress,
                                    ),
                                  ));
                            },
                          )..show();
                        },
                        child: Text(
                          'Use this address',
                          style: GoogleFonts.comfortaa(),
                          textScaleFactor: 1,
                        ),
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(kBuyxRedColor),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ));
    }
  }

  /*Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      this.adress = await Geocoder.local.findAddressesFromQuery(p.description);
      latitudeData = lat;
      longtitudeData = lng;
      _getCurrentAdress(Coordinates(latitudeData, longtitudeData));
      setState(() {});
    }
  }*/
}
