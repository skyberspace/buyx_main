import 'package:buyx/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PhotoAndName extends StatelessWidget {
  const PhotoAndName({
    @required this.givenName,
  });

  final String givenName;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 0, top: 10, right: 0),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: CircleAvatar(
              radius: 25,
              child: Image.asset("assets/circleLogo.png"),
            ),
          ),
          /*Container(
            width: MediaQuery.of(context).size.width / 7.2, // 52
            height: MediaQuery.of(context).size.height / 15.6, // 52
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                image: DecorationImage(
                    image: AssetImage("assets/circleLogo.png"),
                    fit: BoxFit.fill),
                color: Colors.black),
          ),*/

          Expanded(
            flex: 8,
            child: Container(
              width: MediaQuery.of(context).size.width / 1.4,
              child: Text(
                givenName,
                style: GoogleFonts.comfortaa(
                  color: kBuyxBlueColor,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
