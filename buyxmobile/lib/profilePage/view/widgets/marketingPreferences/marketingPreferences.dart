import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';

class MarketingPreferences extends StatefulWidget {
  const MarketingPreferences({Key key}) : super(key: key);

  @override
  _MarketingPreferencesState createState() => _MarketingPreferencesState();
}

class _MarketingPreferencesState extends State<MarketingPreferences> {
  bool marketingCalls;

  bool marketingEmails;

  bool marketingNotifications;

  bool marketingSMS;

  //var marketingList;

  bool loading = true;

  void marketingList1;

  List<Map<String, Object>> marketingList;

  @override
  void initState() {
    super.initState();
    load();
  }

  load() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      marketingCalls = Provider.of<LoggedInState>(context, listen: false)
          .user
          .marketingCalls;
      marketingEmails = Provider.of<LoggedInState>(context, listen: false)
          .user
          .marketingEmails;
      marketingNotifications =
          Provider.of<LoggedInState>(context, listen: false)
              .user
              .marketingNotifications;
      marketingSMS =
          Provider.of<LoggedInState>(context, listen: false).user.marketingSMS;
      /*marketingList.add({
        "name": "Call",
        "description": "I give permission to receive marketing calls from buyx",
        "choice": marketingCalls,
        "key": "marketingCalls"
      });
      marketingList.add({
        "name": "Email",
        "description":
            "I give permission to receive marketing emails from buyx",
        "choice": marketingEmails,
        "key": "marketingEmails"
      });
      marketingList.add({
        "name": "Notification",
        "description":
            "I give permission to receive marketing notifications from buyx",
        "choice": marketingNotifications,
        "key": "marketingNotifications"
      });
      marketingList.add({
        "name": "SMS",
        "description":
            "I give permission to receive marketing messages from buyx",
        "choice": marketingSMS,
        "key": "marketingSMS"
      });*/
      marketingList = [
        {
          "name": "Call",
          "description":
              "I give permission to receive marketing calls from buyx",
          "choice": marketingCalls,
          "key": "MarketingCalls"
        },
        {
          "name": "Email",
          "description":
              "I give permission to receive marketing emails from buyx",
          "choice": marketingEmails,
          "key": "MarketingEmails"
        },
        {
          "name": "Notification",
          "description":
              "I give permission to receive marketing notifications from buyx",
          "choice": marketingNotifications,
          "key": "MarketingNotifications"
        },
        {
          "name": "SMS",
          "description":
              "I give permission to receive marketing messages from buyx",
          "choice": marketingSMS,
          "key": "MarketingSMS"
        },
      ];
      if (marketingList.isNotEmpty) {
        setState(() {
          loading = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Marketing Preferences",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: ListView.separated(
        itemCount: marketingList.length,
        separatorBuilder: (BuildContext context, int index) => Divider(),
        primary: false,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              height: 60,
              child: Center(
                child: Card(
                    semanticContainer: false,
                    elevation: 20,
                    child: SwitchListTile(
                      onChanged: (value) async {
                        var choiceChangeResult = await Api.instance.profile
                            .profileSetMarketingChoicesPost(
                                setMarketingRequest: SetMarketingRequest(
                          key: marketingList[index]["key"],
                          value: !marketingList[index]["choice"],
                        ));
                        if (choiceChangeResult.success) {
                          var userProvider = Provider.of<LoggedInState>(context,
                              listen: false);
                          switch (marketingList[index]["key"]) {
                            case "MarketingEmails":
                              userProvider.user.marketingEmails =
                                  !marketingList[index]["choice"];
                              break;
                            case "MarketingCalls":
                              userProvider.user.marketingCalls =
                                  !marketingList[index]["choice"];
                              break;
                            case "MarketingSMS":
                              userProvider.user.marketingSMS =
                                  !marketingList[index]["choice"];
                              break;
                            case "MarketingNotifications":
                              userProvider.user.marketingNotifications =
                                  !marketingList[index]["choice"];
                              break;
                          }
                          userProvider.changed();
                          setState(() {
                            load();
                          });
                        }
                      },
                      shape: CircleBorder(),
                      activeColor: kBuyxRedColor,
                      dense: true,
                      value: marketingList[index]["choice"],
                      title: Text(
                        marketingList[index]["name"],
                        style: GoogleFonts.comfortaa(),
                        textScaleFactor: 1,
                      ),
                      subtitle: Text(
                        marketingList[index]["description"],
                        style: GoogleFonts.comfortaa(),
                        textScaleFactor: 1,
                      ),
                    )),
              ));
        },
      ),
    );
  }
}
