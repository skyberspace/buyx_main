import 'package:buyx/api.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/campaignsAndAnnouncements/widgets/ReusableCard.dart';
import 'package:buyx/profilePage/view/widgets/paymentMethods.dart/Card/cardRow.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../../constants.dart';

class OrderDetails extends StatefulWidget {
  OrderDetails({@required this.order, this.callback});

  final Order order;
  Function callback;

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  bool campaignUsed = false;

  Color star1Color = kBuyxBlueColor;

  Color star2Color = kBuyxBlueColor;

  Color star3Color = kBuyxBlueColor;

  Color star4Color = kBuyxBlueColor;

  Color star5Color = kBuyxBlueColor;

  ratedResultMethod(double number) {
    if (number == 5) {
      setState(() {
        star1Color = kBuyxRedColor;
        star2Color = kBuyxRedColor;
        star3Color = kBuyxRedColor;
        star4Color = kBuyxRedColor;
        star5Color = kBuyxRedColor;
      });
      return;
    }
    if (number == 4) {
      setState(() {
        star1Color = kBuyxRedColor;
        star2Color = kBuyxRedColor;
        star3Color = kBuyxRedColor;
        star4Color = kBuyxRedColor;
      });
      return;
    }
    if (number == 3) {
      setState(() {
        star1Color = kBuyxRedColor;
        star2Color = kBuyxRedColor;
        star3Color = kBuyxRedColor;
      });
      return;
    }
    if (number == 2) {
      setState(() {
        star1Color = kBuyxRedColor;
        star2Color = kBuyxRedColor;
      });
      return;
    }
    if (number == 1) {
      setState(() {
        star1Color = kBuyxRedColor;
      });
      return;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.order.rated) ratedResultMethod(widget.order.ratedStars);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    widget.callback.call();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Order Details",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 8.0,
                    bottom: 8,
                  ),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 8,
                        ),
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(Icons.moped_rounded),
                            )),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            DateFormat("dd-MM-yyyy kk:mm")
                                .format(widget.order.createdAt.toLocal()),
                            style: GoogleFonts.comfortaa(
                              color: kBuyxSecondRedColor,
                            ),
                            textScaleFactor: 1,
                          ),
                          widget.order.orderAddress.addressLine1 != null
                              ? Text(
                                  widget.order.orderAddress.addressLine1,
                                  style: GoogleFonts.comfortaa(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  softWrap: false,
                                  overflow: TextOverflow.ellipsis,
                                  textScaleFactor: 1,
                                )
                              : Text(""),
                        ],
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: kBuyxRedColor,
                              borderRadius: BorderRadius.circular(4)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(Icons.do_not_touch_outlined,
                                color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Courier",
                style: GoogleFonts.comfortaa(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
                textScaleFactor: 1,
              ),
            ),
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                  //TODO : 150*150 düzeltilecek.
                                  widget.order.deliveryAgentImageUrl != null
                                      ? Api.instance.client.basePath +
                                          "/cdn/image/original" +
                                          widget.order.deliveryAgentImageUrl
                                      : Api.instance.client.basePath +
                                          "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                                  //fit: BoxFit.contain,
                                ),
                              ),
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            width: double.infinity,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: SizedBox(),
                      ),
                      Expanded(
                        flex: 7,
                        child: widget.order.deliveryAgentName != null
                            ? Text(
                                widget.order.deliveryAgentName,
                                style: GoogleFonts.comfortaa(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                                textScaleFactor: 1,
                              )
                            : Text(""),
                      ),
                      Expanded(
                        flex: 1,
                        child: SizedBox(),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "How Was Your Buyx Experience?",
                style: GoogleFonts.comfortaa(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
                textScaleFactor: 1,
              ),
            ),
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                  child: Row(
                    children: [
                      Expanded(
                          child: Row(
                        children: [
                          GestureDetector(
                            onTap: () async {
                              if (widget.order.rated) return;
                              var rateResult = await Api.instance.cart
                                  .cartSetOrderStarsPost(
                                      setOrderStarsRequest:
                                          SetOrderStarsRequest(
                                orderId: this.widget.order.id,
                                stars: 1,
                              ));
                              if (rateResult.success) {
                                setState(() {
                                  star1Color = kBuyxRedColor;
                                });
                              }
                            },
                            child: Icon(
                              Icons.star,
                              color: star1Color,
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              if (widget.order.rated) return;
                              var rateResult = await Api.instance.cart
                                  .cartSetOrderStarsPost(
                                      setOrderStarsRequest:
                                          SetOrderStarsRequest(
                                orderId: this.widget.order.id,
                                stars: 2,
                              ));
                              if (rateResult.success) {
                                setState(() {
                                  star1Color = kBuyxRedColor;
                                  star2Color = kBuyxRedColor;
                                });
                              }
                            },
                            child: Icon(
                              Icons.star,
                              color: star2Color,
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              if (widget.order.rated) return;
                              var rateResult = await Api.instance.cart
                                  .cartSetOrderStarsPost(
                                      setOrderStarsRequest:
                                          SetOrderStarsRequest(
                                orderId: this.widget.order.id,
                                stars: 3,
                              ));
                              if (rateResult.success) {
                                setState(() {
                                  star1Color = kBuyxRedColor;
                                  star2Color = kBuyxRedColor;
                                  star3Color = kBuyxRedColor;
                                });
                              }
                            },
                            child: Icon(
                              Icons.star,
                              color: star3Color,
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              if (widget.order.rated) return;
                              var rateResult = await Api.instance.cart
                                  .cartSetOrderStarsPost(
                                      setOrderStarsRequest:
                                          SetOrderStarsRequest(
                                orderId: this.widget.order.id,
                                stars: 4,
                              ));
                              if (rateResult.success) {
                                setState(() {
                                  star1Color = kBuyxRedColor;
                                  star2Color = kBuyxRedColor;
                                  star3Color = kBuyxRedColor;
                                  star4Color = kBuyxRedColor;
                                });
                              }
                            },
                            child: Icon(
                              Icons.star,
                              color: star4Color,
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              if (widget.order.rated) return;
                              var rateResult = await Api.instance.cart
                                  .cartSetOrderStarsPost(
                                      setOrderStarsRequest:
                                          SetOrderStarsRequest(
                                orderId: this.widget.order.id,
                                stars: 5,
                              ));
                              if (rateResult.success) {
                                setState(() {
                                  star1Color = kBuyxRedColor;
                                  star2Color = kBuyxRedColor;
                                  star3Color = kBuyxRedColor;
                                  star4Color = kBuyxRedColor;
                                  star5Color = kBuyxRedColor;
                                });
                              }
                            },
                            child: Icon(
                              Icons.star,
                              color: star5Color,
                            ),
                          ),
                        ],
                      )),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Cart",
                style: GoogleFonts.comfortaa(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
                textScaleFactor: 1,
              ),
            ),
            Material(
              elevation: 8,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, left: 20),
                  child: Column(
                    children: [
                      ListView.separated(
                        separatorBuilder: (context, index) => Divider(),
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        primary: true,
                        itemCount: widget.order.lines.length,
                        itemBuilder: (context, index) {
                          return ItemRow(item: widget.order.lines[index]);
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
              child: Text(
                "Payment Details",
                style: GoogleFonts.comfortaa(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
                textScaleFactor: 1,
              ),
            ),
            Material(
              elevation: 10,
              child: Padding(
                padding:
                    EdgeInsets.only(top: 10, left: 20, bottom: 10, right: 10),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Cart Amount',
                            style: GoogleFonts.comfortaa(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                            textScaleFactor: 1,
                          ),
                          widget.order.subTotal != null
                              ? Text(
                                  "£ " +
                                      widget.order.subTotal.toStringAsFixed(2),
                                  style: GoogleFonts.comfortaa(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                )
                              : Text(""),
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Delivery',
                            style: GoogleFonts.comfortaa(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                            textScaleFactor: 1,
                          ),
                          widget.order.deliveryFee != null
                              ? Text(
                                  "£ " +
                                      widget.order.deliveryFee
                                          .toStringAsFixed(2),
                                  style: GoogleFonts.comfortaa(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                )
                              : Text(""),
                        ],
                      ),
                      /*Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Tax',
                            style: GoogleFonts.comfortaa(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                            textScaleFactor: 1,
                          ),
                          widget.order.tax != null
                              ? Text(
                                  widget.order.tax.toString(),
                                  style: GoogleFonts.comfortaa(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                )
                              : Text(""),
                        ],
                      ),*/
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Paid Amount',
                            style: GoogleFonts.comfortaa(
                                color: kBuyxRedColor,
                                fontWeight: FontWeight.bold),
                            textScaleFactor: 1,
                          ),
                          widget.order.total != null
                              ? Text(
                                  "£ " + widget.order.total.toStringAsFixed(2),
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxRedColor,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                )
                              : Text(""),
                        ],
                      ),
                      /*Divider(),
                      Row(
                        children: [
                          Icon(Icons.credit_card),
                          SizedBox(
                            width: 10,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                order.orderUsedCard.cardName.toUpperCase(),
                                style: GoogleFonts.comfortaa(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                order.orderUsedCard.cardNumber.substring(0, 6) +
                                    '*******',
                                style: GoogleFonts.comfortaa(
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),*/
                    ],
                  ),
                ),
              ),
            ),
            if (campaignUsed != false) ...{
              Container(
                  padding: EdgeInsets.only(top: 20, left: 20, bottom: 10),
                  child: Text(
                    "Campaign Used",
                    style: GoogleFonts.comfortaa(
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                    ),
                    textScaleFactor: 1,
                  )),
              ReusableCard(
                header: 'Sıradaki siparişine özel 20 TL hediye!',
                subHeader:
                    'Sıradaki siparişinze özel, 30TL ve üzeri siparişinize 20 tl indirim uygulanır.',
              )
            }
          ],
        ),
      ),
    );
  }
}

class ItemRow extends StatelessWidget {
  const ItemRow({
    Key key,
    @required this.item,
  }) : super(key: key);

  final OrderLine item;

  @override
  Widget build(BuildContext context) {
    if (item.product == null)
      return Container();
    else
      return Row(
        children: [
          Expanded(
            flex: 2,
            child: AspectRatio(
              aspectRatio: 1,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                      //TODO : 150*150 düzeltilecek.
                      item.product.images != null
                          ? Api.instance.client.basePath +
                              "/cdn/image/original" +
                              item.product.images.first.path
                          : Api.instance.client.basePath +
                              "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                      //fit: BoxFit.contain,
                    ),
                  ),
                  color: Colors.white,
                  border: Border.all(width: 2, color: kBrightBuyxRedColor),
                  borderRadius: BorderRadius.circular(8),
                ),
                width: double.infinity,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: SizedBox(),
          ),
          Expanded(
            flex: 5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                item.price != null
                    ? Text(
                        "£ " + item.price.toString(),
                        style: GoogleFonts.comfortaa(
                          color: kBuyxRedColor,
                          fontWeight: FontWeight.bold,
                        ),
                        textScaleFactor: 1,
                      )
                    : Text(""),
                SizedBox(
                  height: 10,
                ),
                item.productNameCache != null
                    ? Text(
                        item.productNameCache,
                        style: GoogleFonts.comfortaa(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                        textScaleFactor: 1,
                      )
                    : Text(""),
                /*item.product.shortDescription != null
                  ? Html(
                      data: item.product.shortDescription,
                    )
                  : Text(""),*/
                SizedBox(
                  height: 25,
                )
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: SizedBox(),
          ),
          Expanded(
            flex: 2,
            child: AspectRatio(
              aspectRatio: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: kBrightBuyxRedColor,
                  ),
                  child: Center(
                    child: item.quantity != null
                        ? Text(
                            item.quantity.toStringAsFixed(0),
                            style: GoogleFonts.comfortaa(
                                color: kBuyxRedColor,
                                fontWeight: FontWeight.bold),
                            textScaleFactor: 1,
                          )
                        : Text(""),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
  }
}
