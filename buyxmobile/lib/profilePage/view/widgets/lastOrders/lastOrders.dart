import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/profilePage/view/widgets/lastOrders/orderDetails.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:rive/rive.dart';
import '../../../../constants.dart';

class LastOrders extends StatefulWidget {
  @override
  _LastOrdersState createState() => _LastOrdersState();
}

class _LastOrdersState extends State<LastOrders> {
  List<Order> orders;
  bool loading;

  loadLastOrders() {
    try {
      loading = true;
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        var cartProvider = Provider.of<CartBuyxState>(context, listen: false);
        cartProvider.getOrderHistory();
        //orders = cartProvider.orderHistory;
        loading = false;
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Check internet connection",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  void initState() {
    super.initState();
    loadLastOrders();
  }

  @override
  Widget build(BuildContext context) {
    var cartProvider = Provider.of<CartBuyxState>(context);
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );

    return cartProvider.orderHistory.isNotEmpty
        ? Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(
                color: Colors.white,
              ),
              title: Text(
                "Previous Orders",
                style: GoogleFonts.comfortaa(
                  color: Colors.white,
                ),
                textScaleFactor: 1,
              ),
              backgroundColor: kBuyxRedColor,
              elevation: 0,
            ),
            body: ListView.separated(
              itemCount: cartProvider.orderHistory.length,
              separatorBuilder: (BuildContext context, int index) => Divider(),
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OrderDetails(
                                  order: cartProvider.orderHistory[index],
                                  callback: loadLastOrders,
                                )));
                  },
                  child: OrderRow(
                    orderDate:
                        "${DateFormat("dd-MM-yyyy kk:mm").format(cartProvider.orderHistory[index].deliveredAt.toLocal())}",
                    orderAdress: cartProvider
                        .orderHistory[index].orderAddress.addressLine1,
                    orderPrice: cartProvider.orderHistory[index].total,
                    orderId: cartProvider.orderHistory[index].id,
                  ),
                );
              },
            ),
          )
        : Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(
                color: Colors.white,
              ),
              title: Text(
                "Previous Orders",
                style: GoogleFonts.comfortaa(
                  color: Colors.white,
                ),
                textScaleFactor: 1,
              ),
              backgroundColor: kBuyxRedColor,
              elevation: 0,
            ),
            body: Center(
              child: Text(
                'You have not placed any orders before.',
                style: GoogleFonts.comfortaa(
                    color: kBuyxBlueColor, fontWeight: FontWeight.w900),
                textScaleFactor: 1,
              ),
            ),
          );
  }
}

class OrderRow extends StatefulWidget {
  const OrderRow({
    @required this.orderAdress,
    this.orderDate,
    this.orderPrice,
    this.orderId,
  });

  final String orderDate;
  final String orderAdress;
  final double orderPrice;
  final int orderId;

  @override
  _OrderRowState createState() => _OrderRowState();
}

class _OrderRowState extends State<OrderRow> {
  bool deleted = false;

  @override
  Widget build(BuildContext context) {
    if (deleted == true)
      return Padding(
        padding: EdgeInsets.all(0),
      );
    if (deleted == false)
      return Padding(
        padding: const EdgeInsets.only(left: 4, top: 4, bottom: 4),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Center(
                    child: Icon(
                      Icons.delivery_dining_rounded,
                      color: kBuyxBlueColor,
                    ),
                  )),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.orderDate,
                    style: GoogleFonts.comfortaa(
                      color: kBuyxSecondRedColor,
                    ),
                    textScaleFactor: 1,
                  ),
                  Text(
                    widget.orderAdress,
                    style: GoogleFonts.comfortaa(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                    textScaleFactor: 1,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () async {
                            try {
                              AwesomeDialog(
                                buttonsTextStyle: GoogleFonts.comfortaa(),
                                context: context,
                                dialogType: DialogType.QUESTION,
                                animType: AnimType.BOTTOMSLIDE,
                                //title: 'Dialog Title',
                                desc:
                                    "Do you really want to delete this previous order?",
                                btnCancelOnPress: () {},
                                btnOkOnPress: () async {
                                  var deleteResult = await Api.instance.cart
                                      .cartDeleteOrderPost(
                                          idRequest:
                                              IdRequest(id: widget.orderId));
                                  if (deleteResult.success)
                                    setState(() {
                                      deleted = true;
                                    });
                                },
                              )..show();
                            } catch (e) {
                              AwesomeDialog(
                                buttonsTextStyle: GoogleFonts.comfortaa(),
                                context: context,
                                dialogType: DialogType.INFO,
                                animType: AnimType.BOTTOMSLIDE,
                                //title: 'Dialog Title',
                                desc:
                                    "Connection problem. Please try again later",
                                btnCancelOnPress: () {},
                                btnOkOnPress: () {
                                  Navigator.pop(context);
                                },
                              )..show();
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Icon(
                              Icons.delete,
                              color: kBuyxRedColor,
                            ),
                          ),
                        )),
                    Container(
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(4),
                            topLeft: Radius.circular(4)),
                        color: Colors.white,
                      ),
                      child: Center(
                        child: Icon(
                          Icons.shopping_basket,
                          color: kBuyxRedColor,
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 7,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(4),
                          topRight: Radius.circular(4),
                          topLeft: Radius.circular(4),
                          bottomLeft: Radius.circular(4),
                        ),
                        color: kBrightBuyxRedColor,
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(4),
                        child: Center(
                          child: widget.orderPrice != null
                              ? Text(
                                  "£" + widget.orderPrice.toStringAsFixed(2),
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxRedColor),
                                  textScaleFactor: 1,
                                )
                              : Text(""),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                )),
          ],
        ),
      );
  }
}
