import 'dart:async';
import 'dart:math';
import 'dart:typed_data' as type;
import 'package:buyx/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:ui' as ui;
import 'package:flutter/services.dart' show rootBundle;
import 'package:rive/rive.dart';
import 'dart:convert';
import '../../../constants.dart';
import 'aOrderProgress.dart';
import 'dart:math' as Math;
import 'package:maps_toolkit/maps_toolkit.dart' as toolkit;

class ActiveOrderDetails extends StatefulWidget {
  ActiveOrderDetails({this.order, this.callback});
  Order order;
  Function callback;
  @override
  _ActiveOrderDetailsState createState() => _ActiveOrderDetailsState();
}

class _ActiveOrderDetailsState extends State<ActiveOrderDetails> {
  double _currentZoom = 14;
  String _mapStyle;
  bool loading = true;
  Object redrawObject;
  Object redrawProgress;
  double coordFlightDistanceKm;

  Circle cember;

  LatLng targetBounds;

  toolkit.LatLng targetNorthEast;

  toolkit.LatLng targetSouthWest;

  Timer _timer;

  courierLocationUpdate() async {
    _timer = Timer.periodic(new Duration(seconds: 10), (timer) async {
      var activeOrderResult = await Api.instance.cart.cartGetActiveOrdersPost();
      var search = activeOrderResult.data
          .where((element) => element.id == this.widget.order.id);
      if (search.isEmpty) {
        Navigator.pop(context);
        return;
      }
      /**/
      if (!mounted) {
        return;
      }

      setState(() {
        this.widget.order = search.first;
        markers.clear();
        _addMarker(search.first);
        _initCameraPosition(this.widget.order);
        //redrawObject = Object();
        redrawProgress = Object();
      });

      /*
      */
    });
  }

  double deg2rad(deg) {
    return deg * (Math.pi / 180);
  }

  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(48.858093, 2.294694),
    zoom: 12,
  ); // paris eifell coordinates
  Set<Marker> markers = {};
  GoogleMapController _controller;
  _load() async {
    try {
      await _initIcons();
      await _add();

      /* var max_lat = max(this.widget.order.pickupAddress.latitude,
          this.widget.order.orderAddress.latitude);

      var min_lat = min(this.widget.order.pickupAddress.latitude,
          this.widget.order.orderAddress.latitude);

      var dist_lat = max_lat - min_lat;

      var max_lon = max(this.widget.order.pickupAddress.longitude,
          this.widget.order.orderAddress.longitude);

      var min_lon = min(this.widget.order.pickupAddress.longitude,
          this.widget.order.orderAddress.longitude);

      var dist_lon = max_lon - min_lon;

      var is_order_max = max_lat == this.widget.order.orderAddress.latitude &&
          max_lon == this.widget.order.orderAddress.longitude;

      var bias = .5;
      if (is_order_max) {
        bias = 0.3;
      } else {
        bias = 0.7;
      }
*/
      _kGooglePlex = CameraPosition(
        target: LatLng(this.widget.order.pickupAddress.latitude,
            this.widget.order.pickupAddress.longitude),
        zoom: _currentZoom,
      ); // paris coordinates
      Future.delayed(const Duration(seconds: 10), courierLocationUpdate);
      rootBundle.loadString('assets/map_style.txt').then((string) {
        _mapStyle = string;
      });
    } catch (e) {
      snackBar(
        "Unknown Problem. Please try again",
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  void dispose() {
    _timer.cancel();
    widget.callback.call();
    super.dispose();
  }

  _initIcons() async {
    try {
      final type.Uint8List markerIcon =
          await getBytesFromAsset('assets/marker1.png', 160);
      _iconHome = BitmapDescriptor.fromBytes(markerIcon);
      final type.Uint8List markerIcon2 =
          await getBytesFromAsset('assets/marker2.png', 160);
      _iconDest = BitmapDescriptor.fromBytes(markerIcon2);
      setState(() {
        loading = false;
      });
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  Future<type.Uint8List> getBytesFromAsset(String path, int width) async {
    try {
      type.ByteData data = await rootBundle.load(path);
      ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
          targetWidth: width);
      ui.FrameInfo fi = await codec.getNextFrame();
      return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
          .buffer
          .asUint8List();
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Order Details",
          style: GoogleFonts.comfortaa().copyWith(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 3,
            child: _map(),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 16, 8, 0),
            child: Text(
              "Order Status",
              textAlign: TextAlign.center,
              style: GoogleFonts.comfortaa(
                color: Colors.blueGrey,
                fontWeight: FontWeight.w900,
              ),
              textScaleFactor: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(50, 5, 50, 0),
            child: Divider(
              thickness: 5,
            ),
          ),
          Container(
            child: OrderProgress(
              key: ValueKey<Object>(redrawProgress),
              order: this.widget.order,
            ),
          ),
        ],
      ),
    );
  }

  _map() {
    try {
      return GoogleMap(
          key: ValueKey<Object>(redrawObject),
          liteModeEnabled: false,
          mapType: MapType.normal,
          zoomGesturesEnabled: true,
          zoomControlsEnabled:
              true, // Whether to show zoom controls (only applicable for Android).
          myLocationEnabled:
              true, // For showing your current location on the map with a blue dot.
          myLocationButtonEnabled:
              false, // This button is used to bring the user location to the center of the camera view.
          initialCameraPosition: _kGooglePlex,
          onCameraMove: (CameraPosition cameraPosition) {
            _currentZoom = cameraPosition.zoom;
          },
          onTap: (LatLng pos) {},
          onLongPress: (LatLng pos) {},
          markers: markers != null ? Set<Marker>.from(markers) : null,
          onMapCreated: (GoogleMapController controller) {
            _controller = controller;
            //if (false) _controller.setMapStyle(_mapStyle);
          });
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  Future<void> _add() async {
    try {
      _addMarker(this.widget.order);
      _initCameraPosition(this.widget.order);
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  MarkerId _lastMarkerId;

  var _iconHome;
  var _iconDest;

  _addMarker(Order order) {
    try {
      print("add marker ${order.id}");
      _lastMarkerId = MarkerId("addr1${order.id}");
      final marker = Marker(
          markerId: _lastMarkerId,
          icon: _iconHome,
          position: order.deliveryLastLatitude == 0
              ? LatLng(
                  order.pickupAddress.latitude,
                  order.pickupAddress.longitude,
                )
              : LatLng(
                  order.deliveryLastLatitude,
                  order.deliveryLastLongitude,
                ),
          onTap: () {});
      markers.add(marker);

      _lastMarkerId = MarkerId("addr2${order.id}");
      final marker2 = Marker(
          markerId: _lastMarkerId,
          icon: _iconDest,
          position: LatLng(
            order.orderAddress.latitude,
            order.orderAddress.longitude,
          ),
          onTap: () {});
      markers.add(marker2);
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _initCameraPosition(
    Order order,
    /*LatLng coord*/
  ) async {
    // calculate zoom
    try {
      LatLng latLng_1 = LatLng(
        order.pickupAddress.latitude,
        order.pickupAddress.longitude,
      );
      LatLng latLng_2 =
          LatLng(order.orderAddress.latitude, order.orderAddress.longitude);
      dprint("latLng_1 = $latLng_1");
      dprint("latLng_2 = $latLng_2");

      var lat1 = latLng_1.latitude; // широта
      var lat2 = latLng_2.latitude;
      if (latLng_1.latitude > latLng_2.latitude) {
        lat1 = latLng_2.latitude;
        lat2 = latLng_1.latitude;
      }
      var lng1 = latLng_1.longitude;
      var lng2 = latLng_2.longitude;
      if (latLng_1.longitude > latLng_2.longitude) {
        lng1 = latLng_2.longitude;
        lng2 = latLng_1.longitude;
      }
      dprint("lat1 = $lat1, lat2 = $lat2");
      dprint("lng1 = $lng1, lng2 = $lng2");
      LatLngBounds bound = LatLngBounds(
          southwest: LatLng(lat1, lng1),
          northeast: LatLng(lat2, lng2)); // юго-запад - северо-восток
      dprint(bound.toString());

      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 60);
      if (_controller != null)
        _controller.animateCamera(u2).then((void v) {
          // check(u2,_controller);
        });
      //setState(() {});
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: GoogleFonts.comfortaa(),
          textScaleFactor: 1,
        ),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }
}

dprint(String str) {
  if (!kReleaseMode) print(str);
}

/* ------- Kullanılmayan map fonksiyonları -------------

coordFlightDistance(
        lat1: this.widget.order.orderAddress.latitude,
        lat2: this.widget.order.deliveryLastLatitude,
        lon1: this.widget.order.orderAddress.longitude,
        lon2: this.widget.order.deliveryLastLongitude,
      );
      cember = Circle(
        radius: coordFlightDistanceKm / 2,
        circleId: CircleId("cemberId"),
        center: LatLng(
          min_lat + (dist_lat * bias),
          min_lon + (dist_lat * bias),
        ),
      );
      targetNorthEast = toolkit.SphericalUtil.computeOffset(
          toolkit.LatLng(cember.center.latitude, cember.center.longitude),
          cember.radius * Math.sqrt(2),
          45);
      targetSouthWest = toolkit.SphericalUtil.computeOffset(
          toolkit.LatLng(cember.center.latitude, cember.center.longitude),
          cember.radius * Math.sqrt(2),
          225);
      targetBounds = LatLng(
          toolkit.SphericalUtil.computeOffset(
                  toolkit.LatLng(
                      cember.center.latitude, cember.center.longitude),
                  cember.radius * Math.sqrt(2),
                  45)
              .latitude,
          toolkit.SphericalUtil.computeOffset(
                  toolkit.LatLng(
                      cember.center.latitude, cember.center.longitude),
                  cember.radius * Math.sqrt(2),
                  225)
              .longitude);

--------------- Distance Between Two Coordinates ---------------

coordFlightDistance({double lat1, double lon1, double lat2, double lon2}) {
    var R = 6371 * 1000; // Radius of the earth in m
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) *
            Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) *
            Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km

    coordFlightDistanceKm = d;
  }
-----------------------------------------------------------------
  coordFlightDistance(
        lat1: search.first.orderAddress.latitude,
        lat2: search.first.deliveryLastLatitude,
        lon1: search.first.orderAddress.longitude,
        lon2: search.first.deliveryLastLongitude,
      );
      _addMarker(search.first);
      var max_lat = max(search.first.pickupAddress.latitude,
          this.widget.order.orderAddress.latitude);

      var min_lat = min(search.first.pickupAddress.latitude,
          this.widget.order.orderAddress.latitude);

      var dist_lat = max_lat - min_lat;

      var max_lon = max(search.first.pickupAddress.longitude,
          this.widget.order.orderAddress.longitude);

      var min_lon = min(search.first.pickupAddress.longitude,
          this.widget.order.orderAddress.longitude);

      var dist_lon = max_lon - min_lon;

      var is_order_max = max_lat == search.first.orderAddress.latitude &&
          max_lon == this.widget.order.orderAddress.longitude;

      var bias = .5;
      if (is_order_max) {
        bias = 0.3;
      } else {
        bias = 0.7;
      }
      cember = Circle(
          radius: coordFlightDistanceKm / 2,
          circleId: CircleId("cemberId"),
          center:
              LatLng(min_lat + (dist_lat * bias), min_lon + (dist_lat * bias)));

      /*_kGooglePlex = CameraPosition(
        target:
            LatLng(min_lat + (dist_lat * bias), min_lon + (dist_lat * bias)),
        zoom: _currentZoom,
      );*/

      targetNorthEast = toolkit.SphericalUtil.computeOffset(
          toolkit.LatLng(cember.center.latitude, cember.center.longitude),
          cember.radius * Math.sqrt(2),
          45);
      targetSouthWest = toolkit.SphericalUtil.computeOffset(
          toolkit.LatLng(cember.center.latitude, cember.center.longitude),
          cember.radius * Math.sqrt(2),
          225);

  ----------------------Animate camera beetween two corners------------------------------
  _controller.animateCamera(CameraUpdate.newLatLngBounds(
          /*_bounds(markers)*/ LatLngBounds(
              southwest:
                  LatLng(targetSouthWest.latitude, targetSouthWest.longitude),
              northeast:
                  LatLng(targetNorthEast.latitude, targetNorthEast.longitude)),
          50));

 -----------------------------------------------------------------------------
 */