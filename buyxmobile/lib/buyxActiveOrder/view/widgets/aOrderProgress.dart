import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/api.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/constants.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderProgress extends StatefulWidget {
  Order order;

  OrderProgress({this.order, Key key}) : super(key: key);

  @override
  _OrderProgressState createState() => _OrderProgressState();
}

class _OrderProgressState extends State<OrderProgress> {
  bool loadingPackaging = true;
  bool loadingOnTheWay = true;
  bool loadingdelivered = true;

  //List<Order> activeOrders;

  //Order activeOrder;

  @override
  void initState() {
    super.initState();
    loadPackageCondition();
  }

  loadPackageCondition() async {
    try {
      /*var activeOrderResultBR =
          await Api.instance.cart.cartGetActiveOrdersPost();
      activeOrders = activeOrderResultBR.data;
      activeOrder =
          activeOrders.where((order) => order.id == widget.orderId).first;*/
      if (widget.order.orderState.value < 3) return;
      if (widget.order.orderState.value < 5) {
        setState(() {
          loadingPackaging = false;
        });
        return;
      }
      if (widget.order.orderState.value < 6) {
        setState(() {
          loadingPackaging = false;
          loadingOnTheWay = false;
          loadingdelivered = false;
        });
      }
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Column(
        children: [
          SizedBox(
            height: 15,
          ),
          Row(children: [
            Icon(
              Icons.check_circle_outline_outlined,
              color: Colors.blue,
              size: MediaQuery.of(context).size.width / 7,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 5,
            ),
            Icon(
              Icons.library_books_outlined,
              color: Colors.orange[800],
              size: MediaQuery.of(context).size.width / 7,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 8,
            ),
            Text(
              "Order Placed",
              style: GoogleFonts.comfortaa(
                  color: kBuyxBlueColor, fontWeight: FontWeight.w900),
              textScaleFactor: 1,
            )
          ]),
          SizedBox(
            height: 10,
          ),
          Row(children: [
            if (loadingPackaging == true) ...{
              SpinKitPulse(
                duration: Duration(seconds: 2),
                size: MediaQuery.of(context).size.width / 7,
                color: kBuyxRedColor,
              ),
            } else ...{
              Icon(
                Icons.check_circle_outline_outlined,
                color: Colors.blue,
                size: MediaQuery.of(context).size.width / 7,
              ),
            },
            /*Icon(
            Icons.check,
            size: MediaQuery.of(context).size.width / 7,
          ),*/
            SizedBox(
              width: MediaQuery.of(context).size.width / 5,
            ),
            Icon(
              Icons.inventory_2_outlined,
              color: Colors.amber[800],
              size: MediaQuery.of(context).size.width / 7,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 8,
            ),
            Text(
              "Order Packaging",
              style: GoogleFonts.comfortaa(
                  color: kBuyxBlueColor, fontWeight: FontWeight.w900),
              textScaleFactor: 1,
            )
          ]),
          SizedBox(
            height: 10,
          ),
          Row(children: [
            if (loadingOnTheWay == true) ...{
              SpinKitPulse(
                size: MediaQuery.of(context).size.width / 7,
                color: kBuyxRedColor,
                duration: Duration(seconds: 2),
              ),
            } else ...{
              Icon(
                Icons.check_circle_outline_outlined,
                color: Colors.blue,
                size: MediaQuery.of(context).size.width / 7,
              ),
            },
            SizedBox(
              width: MediaQuery.of(context).size.width / 5,
            ),
            Icon(
              Icons.moped_outlined,
              color: Colors.red,
              size: MediaQuery.of(context).size.width / 7,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 8,
            ),
            Text(
              "On the way",
              style: GoogleFonts.comfortaa(
                  color: kBuyxBlueColor, fontWeight: FontWeight.w900),
              textScaleFactor: 1,
            )
          ]),
          SizedBox(
            height: 10,
          ),
          Row(children: [
            if (loadingdelivered == true) ...{
              SpinKitPulse(
                size: MediaQuery.of(context).size.width / 7,
                color: kBuyxRedColor,
                duration: Duration(seconds: 2),
              ),
            } else ...{
              Icon(
                Icons.check_circle_outline_outlined,
                color: Colors.blue,
                size: MediaQuery.of(context).size.width / 7,
              ),
            },
            SizedBox(
              width: MediaQuery.of(context).size.width / 5,
            ),
            Icon(
              Icons.assignment_turned_in_outlined,
              color: Colors.pinkAccent,
              size: MediaQuery.of(context).size.width / 7,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 8,
            ),
            Text(
              "Order delivered",
              style: GoogleFonts.comfortaa(
                  color: kBuyxBlueColor, fontWeight: FontWeight.w900),
              textScaleFactor: 1,
            )
          ]),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
