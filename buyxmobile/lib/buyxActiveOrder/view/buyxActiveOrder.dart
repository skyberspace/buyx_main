import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/buyxActiveOrder/view/widgets/activeOrderDetails.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/profilePage/view/widgets/lastOrders/orderDetails.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:rive/rive.dart';

import '../../constants.dart';

class BuyxActiveOrder extends StatefulWidget {
  const BuyxActiveOrder({Key key}) : super(key: key);

  @override
  _BuyxActiveOrderState createState() => _BuyxActiveOrderState();
}

class _BuyxActiveOrderState extends State<BuyxActiveOrder> {
  bool activeOrdersReady = false;
  bool loading = true;

  List<Order> activeOrders;
  getActiveOrder() async {
    try {
      var activeOrderResult = await Api.instance.cart.cartGetActiveOrdersPost();
      activeOrders = activeOrderResult.data;
      activeOrdersReady = true;
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        btnCancel: SizedBox(),
        //title: 'Dialog Title',
        desc: "Connection problem. Please check your internet settings",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          //Navigator.pop(context);
        },
      )..show();
    }
  }

  loadActiveOrders() async {
    loading = true;
    await getActiveOrder();
    if (!mounted) return;
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    loadActiveOrders();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (activeOrders == null)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Active Orders",
              style: GoogleFonts.comfortaa(
                color: Colors.white,
              ),
              textScaleFactor: 1,
            ),
          ],
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: activeOrders.isEmpty || activeOrders == null
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Icon(
                    Icons.shopping_cart_outlined,
                    size: 150,
                    color: kBuyxBlueColor,
                  ),
                ),
                Text(
                  "You have no active order right now",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.comfortaa(
                    fontWeight: FontWeight.w900,
                  ),
                  textScaleFactor: 1,
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            )
          : ListView.separated(
              itemCount: activeOrders.length,
              separatorBuilder: (BuildContext context, int index) => Divider(),
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ActiveOrderDetails(
                          order: activeOrders[index],
                          callback: loadActiveOrders,
                        ),
                      ),
                    );
                  },
                  child: ActiveOrderRow(
                    orderId: activeOrders[index].id,
                    orderDate:
                        "${DateFormat("dd-MM-yyyy kk:mm").format(activeOrders[index].createdAt.toLocal())}",
                    orderAdress: activeOrders[index].orderAddress.addressName,
                    orderPrice: activeOrders[index].total.toString(),
                  ),
                );
              },
            ),
    );
  }
}

class ActiveOrderRow extends StatefulWidget {
  const ActiveOrderRow({
    @required this.orderAdress,
    this.orderDate,
    this.orderPrice,
    this.orderId,
  });

  final String orderDate;
  final String orderAdress;
  final String orderPrice;
  final int orderId;

  @override
  _ActiveOrderRowState createState() => _ActiveOrderRowState();
}

class _ActiveOrderRowState extends State<ActiveOrderRow> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 4, top: 4, bottom: 4),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: Icon(
                    Icons.delivery_dining_rounded,
                    color: kBuyxBlueColor,
                  ),
                )),
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            flex: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                widget.orderDate != null
                    ? Text(
                        widget.orderDate,
                        style: GoogleFonts.comfortaa(
                            color: kBuyxSecondRedColor,
                            fontWeight: FontWeight.w900),
                        textScaleFactor: 1,
                      )
                    : Text(""),
                widget.orderAdress != null
                    ? Text(
                        widget.orderAdress,
                        style: GoogleFonts.comfortaa(
                          color: kBuyxBlueColor,
                          fontWeight: FontWeight.w900,
                        ),
                      )
                    : Text(""),
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(4),
                          topLeft: Radius.circular(4)),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.shopping_basket,
                        color: kBuyxRedColor,
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(4),
                          topRight: Radius.circular(4)),
                      color: kBrightBuyxRedColor,
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(4),
                      child: Center(
                        child: widget.orderAdress != null
                            ? Text(
                                "£" + widget.orderPrice,
                                style: GoogleFonts.comfortaa(
                                    color: kBuyxRedColor,
                                    fontWeight: FontWeight.w900),
                              )
                            : Text(""),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
              flex: 1,
              child: Icon(
                Icons.arrow_forward_ios,
                color: kBuyxBlueColor,
                size: 14,
              )),
        ],
      ),
    );
  }
}
