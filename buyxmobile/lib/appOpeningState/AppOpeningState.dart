import 'dart:collection';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:google_fonts/google_fonts.dart';

class AppOpeningState with ChangeNotifier {
  BannerListBR carouselImages;

  CategoryListBR rootCategories;

  CategoryListBR subCatsData;

  HashMap<int, List<Category>> subcats = HashMap();
  bool buyxProductPageLoading = true;

  BlogEntryListBR campaignResultsBR;

  List<BlogEntry> campaignResults;

  BlogEntryListBR announcementResultsBR;

  List<BlogEntry> announcementResults;

  bool loadingCampaignsAndAnnouncements = true;

  load() async {
    await getCarousel();
    await getCatalogRootCategories();
    notifyListeners();
  }

  getCarousel() async {
    carouselImages = await Api.instance.layout.layoutGetHomeSliderGet();

    //carouselImages.data.first.imagePath
  }

  getCatalogRootCategories() async {
    rootCategories = await Api.instance.catalog.catalogGetRootCategoriesPost();
  }

  /*AuthTokenBR loginResult;
  login() async {
    loginResult = await Api.instance.authApi.authLoginPost(
      loginRequest: LoginRequest(phone: "+445555555555", password: "1234"),
    );
    Api.instance.client
        .addDefaultHeader("Authorization", "Bearer " + loginResult.data.token);
    LoggedInState().setUser(loginResult.data.user);
  }*/

  loadSubCats(List<Category> categories) async {
    buyxProductPageLoading = true;
    notifyListeners();
    for (var item in categories) {
      subCatsData = await Api.instance.catalog.catalogGetSubCategoriesFromPost(
          getSubCategoriesRequest:
              GetSubCategoriesRequest(parentCategoryId: item.id));
      subcats[item.id] = subCatsData.data;
    }
    buyxProductPageLoading = false;
    notifyListeners();
  }

  loadCampaignsAndAnnouncments({BuildContext context}) async {
    try {
      campaignResultsBR = await Api.instance.layout.layoutGetBlogEntriesPost(
          getBlogEntriesRequest:
              GetBlogEntriesRequest(blogType: BlogType.number1));
      campaignResults = campaignResultsBR.data;

      announcementResultsBR = await Api.instance.layout
          .layoutGetBlogEntriesPost(
              getBlogEntriesRequest:
                  GetBlogEntriesRequest(blogType: BlogType.number2));
      announcementResults = announcementResultsBR.data;
      loadingCampaignsAndAnnouncements = false;
      notifyListeners();
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please check your internet settings",
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
        btnCancel: SizedBox(),
      )..show();
    }
  }
}
