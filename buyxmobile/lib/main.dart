import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:buyx_api_main/api.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/appOpeningState/AppOpeningState.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/profilePage/view/widgets/changeInformation/changeInformation.dart';
import 'package:buyx/profilePage/view/widgets/passwordChange/passwordChange.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx/shoppingCartBuyxFood/state/CartBuyxFoodState.dart';
import 'package:buyx/splash/splash.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:provider/provider.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'BuyxPage/view/widgets/BuyxPay.dart';
import 'api.dart';
import 'buyxActiveOrder/view/widgets/activeOrderDetails.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/services.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}

const String delivery_notification_topic = "buyx_delivery_messages";
const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'mobile_notification_channel', // id
  'Buyx Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.max,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> saveTokenToDatabase(String token) async {
  print(token);
  print("token eyy");
  Api.instance.profile.profileSetUserFCMTokenPost(
      setFCMTokenRequest: SetFCMTokenRequest(fcmToken: token));
}

void getFCMToken() async {
  saveTokenToDatabase(await FirebaseMessaging.instance.getToken());
  FirebaseMessaging.instance.onTokenRefresh.listen(saveTokenToDatabase);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );
  //

  //await FirebaseMessaging.instance.subscribeToTopic('buyx_delivery_messages');
  FirebaseMessaging.onMessage.listen((RemoteMessage remoteMessage) {
    flutterLocalNotificationsPlugin.show(
      remoteMessage.notification.hashCode,
      remoteMessage.notification.title,
      remoteMessage.notification.body,
      NotificationDetails(
        iOS: IOSNotificationDetails(),
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channel.description,
          // TODO add a proper drawable resource to android, for now using
          //      one that already exists in example app.
          icon: 'launch_background',
        ),
      ),
    );
  });
  final status = await AppTrackingTransparency.requestTrackingAuthorization();
  var api = Api();
  await api.setAsDefaultInstance();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MultiProvider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primaryColor: Colors.white,
            accentColor: Colors.white,
            scaffoldBackgroundColor: Colors.white),
        home: NavigationBuyx() /*splashScreen()*/ /*BuyxPay()*/,
      ),
      providers: [
        ChangeNotifierProvider(
          create: (context) => CartBuyxState(),
        ),
        /*ChangeNotifierProvider(
          create: (context) => CartBuyxFoodState(),
        ),*/
        ChangeNotifierProvider(
          create: (context) => LoggedInState(),
        ),
        ChangeNotifierProvider(
          create: (context) => AppOpeningState(),
        ),
      ],
    );
  }
}
