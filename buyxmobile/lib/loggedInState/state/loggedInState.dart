import 'package:buyx/main.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_stripe/flutter_stripe.dart';

import '../../api.dart';

class LoggedInState with ChangeNotifier {
  User user;

  AddressBR userDefaultAddress;

  bool get isLoggedIn => user != null;
  final storage = new FlutterSecureStorage();

  setUser(User newUser) async {
    this.user = newUser;
    if (newUser != null) {
      this.userDefaultAddress =
          await Api.instance.profile.profileGetDefaultAddressPost();
      initializePublicStripeKey();
      getFCMToken();
    } else {
      this.userDefaultAddress = null;
    }
    notifyListeners();
  }

  updateUser() async {
    var result = await Api.instance.authApi.authGetUserFromTokenPost();
    this.setUser(result.data.user);
  }

  changed() {
    this.notifyListeners();
  }

  initializePublicStripeKey() async {
    var publicKeyBR = await Api.instance.cart.cartGetPublicStripeTokenPost();
    Stripe.publishableKey = publicKeyBR.data;
  }
}
