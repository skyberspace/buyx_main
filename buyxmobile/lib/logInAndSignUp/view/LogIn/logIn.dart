import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/logInAndSignUp/view/SignUp/otpVerify.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/logInAndSignUp/view/SignUp/signUp.dart';
import 'package:buyx/logInAndSignUp/view/forgotPassword/forgotPassword.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:provider/provider.dart';

class LogInPage extends StatefulWidget {
  @override
  _LogInPageState createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  bool checkedValue = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController controllerPhone = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  String initialCountry = 'GB';
  String number;
  String storageToken = "storageToken";
  bool _obscureText = true;
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Login",
            style: GoogleFonts.comfortaa(
              color: Colors.white,
              fontSize: 24,
            ),
            textScaleFactor: 1,
          ),
          backgroundColor: kBuyxRedColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                /*Expanded(
                  flex: 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Color(0xff4267b2)),
                        ),
                        onPressed: () {},
                        child: Text(
                          "Connect with Facebook",
                          style: GoogleFonts.comfortaa(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),*/
                Expanded(
                  flex: 3,
                  child: Theme(
                    data: ThemeData(primaryColor: kGreyColor),
                    child: Container(
                      margin: EdgeInsets.all(12),
                      child: IntlPhoneField(
                        textAlign: TextAlign.center,
                        style: GoogleFonts.comfortaa(color: Colors.black),
                        decoration: InputDecoration(
                          helperText: "Phone",
                          labelStyle: GoogleFonts.comfortaa(
                              color: Colors.grey.withOpacity(1),
                              fontWeight: FontWeight.w300),
                          counterStyle:
                              GoogleFonts.comfortaa(color: Colors.red),
                          prefixStyle:
                              GoogleFonts.comfortaa(color: Colors.black),
                          labelText: 'Phone Number',
                          border: OutlineInputBorder(
                            borderSide: BorderSide(),
                          ),
                        ),
                        initialCountryCode: 'GB',
                        onChanged: (phone) {
                          this.number = phone.completeNumber;
                          print(phone
                              .completeNumber); // When sending message from otp, use the phone.completeNumber
                        },
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Container(
                    margin: EdgeInsets.all(12),
                    child: TextFormField(
                      obscureText: _obscureText,
                      controller: controllerPassword,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Cant be empty';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        suffixIcon: GestureDetector(
                          child: const Icon(
                            Icons.remove_red_eye_rounded,
                          ),
                          onTap: () {
                            setState(() {
                              _obscureText = !_obscureText;
                            });
                          },
                        ),
                        helperText: 'Password',

                        /*hintText:
                            "The password you enter must be at least 8 characters!",
                        hintStyle: GoogleFonts.comfortaa(
                            fontSize: 14, color: Colors.blue.withOpacity(0.2)),*/
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                              style: BorderStyle.solid, color: kBuyxBlueColor),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(kBuyxRedColor),
                        ),
                        onPressed: () async {
                          try {
                            AuthTokenBR loginResult;
                            loginResult = await Api.instance.authApi
                                .authLoginPost(
                                    loginRequest: LoginRequest(
                                        password: controllerPassword.text,
                                        phone: this.number));
                            if (!loginResult.success &&
                                loginResult.errors.length == 0 &&
                                loginResult.infos.length == 2) {
                              Navigator.push /*AndRemoveUntil*/ (
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      PinCodeVerificationScreen(
                                    this.number,
                                    loginResult.infos[1],
                                  ),
                                ),
                                //(Route<dynamic> route) => false,
                              );
                              return;
                            }
                            if (loginResult.success == true) {
                              Api.instance.client.addDefaultHeader(
                                  "Authorization",
                                  "Bearer " + loginResult.data.token);
                              AuthTokenBR userResult = await Api
                                  .instance.authApi
                                  .authGetUserFromTokenPost();
                              Provider.of<LoggedInState>(context, listen: false)
                                  .setUser(userResult.data.user);

                              await Provider.of<LoggedInState>(context,
                                      listen: false)
                                  .storage
                                  .write(
                                      key: storageToken,
                                      value: userResult.data.token);
                              Provider.of<CartBuyxState>(context, listen: false)
                                  .loadCart(
                                context: context,
                              );
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NavigationBuyx()),
                                (Route<dynamic> route) => false,
                              );
                            } else
                              AwesomeDialog(
                                buttonsTextStyle: GoogleFonts.comfortaa(),
                                context: context,
                                dialogType: DialogType.INFO,
                                animType: AnimType.BOTTOMSLIDE,
                                //title: 'Dialog Title',
                                desc: loginResult.errors[0],
                                btnCancelOnPress: () {},
                                btnOkOnPress: () {
                                  Navigator.pop(context);
                                },
                              )..show();
                          } catch (e) {
                            AwesomeDialog(
                              buttonsTextStyle: GoogleFonts.comfortaa(),
                              context: context,
                              btnCancel: SizedBox(),
                              dialogType: DialogType.INFO,
                              animType: AnimType.BOTTOMSLIDE,
                              //title: 'Dialog Title',
                              desc:
                                  "Connection problem. Please try again later",
                              btnCancelOnPress: () {},
                              btnOkOnPress: () {},
                            )..show();
                          }
                        },
                        child: Text(
                          "Login",
                          style: GoogleFonts.comfortaa(
                            color: Colors.white,
                          ),
                          textScaleFactor: 1,
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ForgotPassword(),
                          ));
                    },
                    child: Text(
                      "Forgot Password",
                      style: GoogleFonts.comfortaa(color: Colors.grey),
                      textScaleFactor: 1,
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: SizedBox(),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.all(12),
                    width: MediaQuery.of(context).size.width,
                    child: OutlinedButton(
                      style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(kBuyxRedColor),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpPage()));
                      },
                      child: Text(
                        "Sign up",
                        style: GoogleFonts.comfortaa(),
                        textScaleFactor: 1,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  /*snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message,),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }*/
}
