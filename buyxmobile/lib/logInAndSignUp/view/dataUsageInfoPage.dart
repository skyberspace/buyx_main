import 'dart:io';

import 'package:file/src/interface/file.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:rive/rive.dart';
import '../../constants.dart';
import 'package:native_pdf_view/native_pdf_view.dart';

class DataUsage extends StatefulWidget {
  DataUsage({Key key}) : super(key: key);

  @override
  _DataUsageState createState() => _DataUsageState();
}

class _DataUsageState extends State<DataUsage> {
  static final int _initialPage = 1;
  int _actualPageNumber = _initialPage, _allPagesCount = 0;
  File result;
  PdfController pdfController;
  load() async {
    result = await DefaultCacheManager().getSingleFile(
        'https://drive.google.com/uc?export=download&id=1TdHeZvD-aLKRPwonn8zdlFk1Y7xfMf2c');
    pdfController = PdfController(
      document: PdfDocument.openFile(result.path),
      initialPage: _initialPage,
    );
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    load();
  }

  bool loading = true;
  @override
  Widget build(BuildContext context) {
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Container(
              alignment: Alignment.center,
              child: Text(
                '$_actualPageNumber/$_allPagesCount',
                style: GoogleFonts.comfortaa(fontSize: 18, color: Colors.white),
                textScaleFactor: 1,
              ),
            ),
          ),
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Privacy Document",
              style: GoogleFonts.comfortaa(
                color: Colors.white,
              ),
              textScaleFactor: 1,
            ),
          ],
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: PdfView(
        controller: pdfController,
        onPageChanged: (page) {
          setState(() {
            _actualPageNumber = page;
          });
        },
        onDocumentLoaded: (document) {
          setState(() {
            _allPagesCount = document.pagesCount;
          });
        },
      ),
    );
  }
}
