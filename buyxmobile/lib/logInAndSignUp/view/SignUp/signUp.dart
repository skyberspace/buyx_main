import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/logInAndSignUp/view/SignUp/otpVerify.dart';
import 'package:buyx/logInAndSignUp/view/dataUsageInfoPage.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:flutter/gestures.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool checkedValue = false;
  bool loading = false;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controllerPhone = TextEditingController();
  final TextEditingController controllerName = TextEditingController();
  final TextEditingController controllerMail = TextEditingController();
  final TextEditingController controllerPassword = TextEditingController();
  final TextEditingController controllerSurname = TextEditingController();
  String initialCountry = 'GB';
  String number;

  StringBR registerAuthTokenResult;

  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Center(
            child: Text(
              "Sign Up",
              style: GoogleFonts.comfortaa(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          actions: [
            Text("          "),
          ],
          backgroundColor: kBuyxRedColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    /*Expanded(
                      flex: 2,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Color(0xff4267b2)),
                            ),
                            onPressed: () {},
                            child: Text(
                              "Facebook ile Bağlan",
                              style: GoogleFonts.comfortaa(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),*/
                    Expanded(
                      flex: 4,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: Theme(
                          data: ThemeData(primaryColor: kGreyColor),
                          child: Container(
                            margin: EdgeInsets.all(12),
                            child: IntlPhoneField(
                              textAlign: TextAlign.center,
                              style: GoogleFonts.comfortaa(
                                  color: Colors.black, fontSize: 11),
                              decoration: InputDecoration(
                                helperText: "Phone",
                                labelStyle: GoogleFonts.comfortaa(
                                    fontSize: 11,
                                    color: Colors.grey.withOpacity(1),
                                    fontWeight: FontWeight.w300),
                                counterStyle:
                                    GoogleFonts.comfortaa(color: Colors.red),
                                prefixStyle:
                                    GoogleFonts.comfortaa(color: Colors.black),
                                labelText: 'Phone Number',
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(),
                                ),
                              ),
                              initialCountryCode: 'GB',
                              onChanged: (phone) {
                                this.number = phone.completeNumber;
                                print(phone
                                    .completeNumber); // When sending message from otp, use the phone.completeNumber
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: TextFormField(
                          controller: controllerPassword,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            helperText: 'Password',
                            hintText:
                                "Must be 8 character with at least one number and one A-z!",
                            hintStyle: GoogleFonts.comfortaa(
                                fontSize: 11, color: Colors.grey),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: TextFormField(
                          controller: controllerName,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            helperText: '',
                            hintText: "Name",
                            hintStyle: GoogleFonts.comfortaa(
                                fontSize: 11, color: Colors.grey),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: TextFormField(
                          controller: controllerSurname,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            helperText: '',
                            hintText: "Surname",
                            hintStyle: GoogleFonts.comfortaa(
                                fontSize: 11, color: Colors.grey),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        child: TextFormField(
                          controller: controllerMail,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Cant be empty';
                            } else if (!RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)) {
                              return 'Please enter a valid E-mail';
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            helperText: '',
                            hintText: "Email",
                            hintStyle: GoogleFonts.comfortaa(
                                fontSize: 11, color: Colors.grey),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                style: BorderStyle.solid,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    /*Expanded(
                      flex: 2,
                      child: CheckboxListTile(
                        checkColor: Colors.white,
                        activeColor: kPurpleColor,
                        title: Text(
                          "I would like to be informed about Buyx's special promotions and opportunities for me.",
                          style: GoogleFonts.comfortaa(fontSize: 12),
                        ),
                        value: checkedValue,
                        onChanged: (newValue) {
                          setState(() {
                            checkedValue = newValue;
                          });
                          print(checkedValue);
                        },
                        controlAffinity: ListTileControlAffinity
                            .leading, //  <-- leading Checkbox
                      ),
                    ),*/
                    Expanded(
                      flex: 2,
                      child: Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text:
                                        "By registering, you agree to receiving marketing emails, calls, notifications, sms and agree to ",
                                    style: GoogleFonts.comfortaa(
                                        color: Colors.black, fontSize: 12)),
                                TextSpan(
                                  text:
                                      "the terms of use and privacy statement.",
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxRedColor, fontSize: 12),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => DataUsage())),
                                ),
                              ],
                            ),
                          )),
                    ),
                    /*Expanded(
                      flex: 2,
                      child: Container(
                          padding: EdgeInsets.all(12),
                          child: RichText(
                            textScaleFactor: 1,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "Click",
                                  style: GoogleFonts.comfortaa(
                                      color: kPurpleColor, fontSize: 12),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DataUsage()));
                                    },
                                ),
                                TextSpan(
                                    text:
                                        " for getting information about personal data usage and collection, ",
                                    style: GoogleFonts.comfortaa(
                                        color: Colors.black, fontSize: 12)),
                              ],
                            ),
                          )),
                    ),*/
                    Expanded(
                      flex: 3,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  kBuyxRedColor),
                            ),
                            onPressed: () async {
                              try {
                                registerAuthTokenResult =
                                    await Api.instance.authApi.authRegisterPost(
                                        registerRequest: RegisterRequest(
                                            email: controllerMail.text,
                                            name: controllerName.text,
                                            password: controllerPassword.text,
                                            phone: this.number,
                                            surname: controllerSurname.text));
                                if (!registerAuthTokenResult.success) {
                                  AwesomeDialog(
                                    buttonsTextStyle: GoogleFonts.comfortaa(),
                                    context: context,
                                    dialogType: DialogType.INFO,
                                    animType: AnimType.BOTTOMSLIDE,
                                    //title: 'Dialog Title',
                                    desc: registerAuthTokenResult.errors[0],
                                    btnCancelOnPress: () {},
                                    btnOkOnPress: () {},
                                  )..show();
                                }
                                if (registerAuthTokenResult.success)
                                  Navigator.push /*AndRemoveUntil*/ (
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          PinCodeVerificationScreen(
                                        this.number,
                                        registerAuthTokenResult.data,
                                      ),
                                    ),
                                    //(Route<dynamic> route) => false,
                                  );
                              } catch (e) {
                                AwesomeDialog(
                                  buttonsTextStyle: GoogleFonts.comfortaa(),
                                  context: context,
                                  dialogType: DialogType.INFO,
                                  animType: AnimType.BOTTOMSLIDE,
                                  //title: 'Dialog Title',
                                  desc:
                                      "Connection problem. Please try again later",
                                  btnCancelOnPress: () {},
                                  btnOkOnPress: () {},
                                )..show();
                              }
                            },
                            child: Text(
                              "Sign Up",
                              style: GoogleFonts.comfortaa(
                                color: Colors.white,
                              ),
                              textScaleFactor: 1,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: SizedBox(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}















/*@override
  Widget build(BuildContext context) {
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Sign Up",
            style: GoogleFonts.comfortaa(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          backgroundColor: kPurpleColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /*Expanded(
                    flex: 2,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Color(0xff4267b2)),
                          ),
                          onPressed: () {},
                          child: Text(
                            "Connect with Facebook",
                            style: GoogleFonts.comfortaa(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),*/
                  Expanded(
                    flex: 3,
                    child: Container(
                      width: MediaQuery.of(context).size.wid,
                      margin: EdgeInsets.only(left: 12, right: 12),
                      child: Theme(
                        data: ThemeData(primaryColor: kGreyColor),
                        child: Container(
                          margin: EdgeInsets.all(12),
                          child: IntlPhoneField(
                            textAlign: TextAlign.center,
                            style: GoogleFonts.comfortaa(color: Colors.white54),
                            decoration: InputDecoration(
                              labelStyle: GoogleFonts.comfortaa(color: Colors.blue[400]),
                              counterStyle: GoogleFonts.comfortaa(color: Colors.red),
                              prefixStyle: GoogleFonts.comfortaa(color: Colors.black),
                              labelText: 'Phone Number',
                              border: OutlineInputBorder(
                                borderSide: BorderSide(),
                              ),
                            ),
                            initialCountryCode: 'GB',
                            onChanged: (phone) {
                              this.number = phone.completeNumber;
                              print(phone
                                  .completeNumber); // When sending message from otp, use the phone.completeNumber
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      margin: EdgeInsets.only(left: 12, right: 12),
                      child: TextFormField(
                        controller: controllerPassword,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Cant be empty';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          helperText: '',
                          hintText:
                              "Girdiğiniz şifre en az 4 karakterden oluşmalı.",
                          hintStyle: GoogleFonts.comfortaa(fontSize: 11, color: kGreyColor),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: kGreyColor),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              style: BorderStyle.solid,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 12, right: 12),
                          child: TextFormField(
                            controller: controllerName,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Cant be empty';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              helperText: '',
                              hintText: "Name",
                              hintStyle:
                                  GoogleFonts.comfortaa(fontSize: 11, color: kGreyColor),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: kGreyColor),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  style: BorderStyle.solid,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 12, right: 12),
                          child: TextFormField(
                            controller: controllerSurname,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Cant be empty';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              helperText: '',
                              hintText: "Surname",
                              hintStyle:
                                  GoogleFonts.comfortaa(fontSize: 11, color: kGreyColor),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: kGreyColor),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                  style: BorderStyle.solid,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      margin: EdgeInsets.only(left: 12, right: 12),
                      child: TextFormField(
                        controller: controllerMail,
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Cant be empty';
                          } else if (!RegExp(
                                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                              .hasMatch(value)) {
                            return 'Please enter a valid E-mail';
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          helperText: '',
                          hintText: "E-Posta",
                          hintStyle: GoogleFonts.comfortaa(fontSize: 11, color: kGreyColor),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: kGreyColor),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              style: BorderStyle.solid,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: CheckboxListTile(
                      checkColor: Colors.white,
                      activeColor: kPurpleColor,
                      title: Text(
                        "Buyx'in bana özel kampanya, tanıtım ve fırsatlarından haberdar olmak istiyorum.",
                        style: GoogleFonts.comfortaa(fontSize: 12),
                      ),
                      value: checkedValue,
                      onChanged: (newValue) {
                        setState(() {
                          checkedValue = newValue;
                        });
                        print(checkedValue);
                      },
                      controlAffinity: ListTileControlAffinity
                          .leading, //  <-- leading Checkbox
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                        padding: EdgeInsets.all(12),
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                  text: "Üye olmakla ",
                                  style: GoogleFonts.comfortaa(
                                      color: Colors.black, fontSize: 12)),
                              TextSpan(
                                text: "Kullanım Koşullarını",
                                style: GoogleFonts.comfortaa(
                                    color: kPurpleColor, fontSize: 12),
                                recognizer: new TapGestureRecognizer()
                                  ..onTap = () => print('Tap Here onTap'),
                              ),
                              TextSpan(
                                  text: " onaylamış olursunuz.",
                                  style: GoogleFonts.comfortaa(
                                      color: Colors.black, fontSize: 12)),
                            ],
                          ),
                        )),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                        padding: EdgeInsets.all(12),
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                  text:
                                      "Kişisel verilerinize dair Aydınlatma Metni için ",
                                  style: GoogleFonts.comfortaa(
                                      color: Colors.black, fontSize: 12)),
                              TextSpan(
                                text: "tıklayınız",
                                style: GoogleFonts.comfortaa(
                                    color: kPurpleColor, fontSize: 12),
                                recognizer: new TapGestureRecognizer()
                                  ..onTap = () => print('Tap Here onTap'),
                              ),
                            ],
                          ),
                        )),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(kPurpleColor),
                          ),
                          onPressed: () async {
                            await Api.instance.authApi.authRegisterPost(
                                registerRequest: RegisterRequest(
                                    email: controllerMail.text,
                                    name: controllerName.text,
                                    password: controllerPassword.text,
                                    phone: this.number,
                                    surname: controllerSurname.text));

                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NavigationBuyx()),
                              (Route<dynamic> route) => false,
                            );
                          },
                          child: Text(
                            "Üye Ol",
                            style: GoogleFonts.comfortaa(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  /*Expanded(
                    flex: 1,
                    child: SizedBox(),
                  ),*/
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

*/