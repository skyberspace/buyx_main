import 'dart:async';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/api.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import "package:flutter_secure_storage/flutter_secure_storage.dart";

class PinCodeVerificationScreen extends StatefulWidget {
  final String phoneNumber;
  final String result;

  PinCodeVerificationScreen(this.phoneNumber, this.result);

  @override
  _PinCodeVerificationScreenState createState() =>
      _PinCodeVerificationScreenState();
}

class _PinCodeVerificationScreenState extends State<PinCodeVerificationScreen> {
  TextEditingController textEditingController = TextEditingController();
  // ..text = "123456";

  // ignore: close_sinks
  StreamController<ErrorAnimationType> errorController;

  bool hasError = false;
  String currentText = "";
  final formKey = GlobalKey<FormState>();
  AuthTokenBR appDataResult;

  String storageToken = "storageToken";

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController.close();

    super.dispose();
  }

  afterVerify() async {
    /*var tokenAuthReslutamk = await Api.instance.orderPreparing
        .orderPreparingGetOrdersForDeliveryPost();*/
    /*return Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) {
        return WarehouseFirst();
      }),
    );*/
    try {
      if (appDataResult.data == null)
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Warning!",
                style: GoogleFonts.comfortaa(color: Colors.redAccent),
              ),
              content: Text(
                /*"You are not authorized to login this app!"*/ appDataResult
                    .errors[0],
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "OK",
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            );
          },
        );
      if (appDataResult.data != null) {
        // ignore: await_only_futures
        await _token();
        //getFCMToken(); TODO
        Provider.of<LoggedInState>(context, listen: false)
            .setUser(appDataResult.data.user);
        await Provider.of<LoggedInState>(context, listen: false)
            .storage
            .write(key: storageToken, value: appDataResult.data.token);
        //globalWarehouseUser = appDataResult.data.user.warehouseUser;
        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) {
          return NavigationBuyx();
        }), (route) => false);
      }
      /*else if (appDataResult.data.user.warehouseUser.warehouseRole ==
          WarehouseRole.number2) {
        // ignore: await_only_futures
        await _token();
        //getFCMToken(); TODO
        globalWarehouseUser = appDataResult.data.user.warehouseUser;
        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) {
          return CourierFirst();
        }), (route) => false);
      } else if (appDataResult.data.user.warehouseUser.warehouseRole.value ==
          WarehouseRole.number0.value) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Warning!",
                style: GoogleFonts.comfortaa(color: Colors.redAccent),
              ),
              content: Text(
                /*"You can not login this app! (SV)"*/ appDataResult.errors[0],
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "OK",
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } else {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Warning!",
                style: GoogleFonts.comfortaa(color: Colors.redAccent),
              ),
              content: Text(
                /*"Please contact the developer team!"*/ appDataResult
                    .errors[0],
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "OK",
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }*/
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  // snackBar Widget

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: () {},
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: <Widget>[
              SizedBox(height: 30),
              Container(
                height: MediaQuery.of(context).size.height / 3,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: Image.asset("assets/phoneOtp.png"),
                ),
              ),
              SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'Phone Number Verification',
                  style: GoogleFonts.comfortaa(
                      fontWeight: FontWeight.bold, fontSize: 22),
                  textAlign: TextAlign.center,
                  textScaleFactor: 1,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
                child: RichText(
                  text: TextSpan(
                      text: "Enter the code sent to ",
                      children: [
                        TextSpan(
                            text: "${widget.phoneNumber}",
                            style: GoogleFonts.comfortaa(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15)),
                      ],
                      style: GoogleFonts.comfortaa(
                          color: Colors.black54, fontSize: 15)),
                  textAlign: TextAlign.center,
                  textScaleFactor: 1,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Form(
                key: formKey,
                child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 30),
                    child: PinCodeTextField(
                      appContext: context,
                      pastedTextStyle: GoogleFonts.comfortaa(
                        color: Colors.green.shade600,
                        fontWeight: FontWeight.bold,
                      ),
                      length: 6,
                      obscureText: true,
                      obscuringCharacter: '*',
                      /*obscuringWidget: Image.asset(
                        "assets/logo.png",
                        fit: BoxFit.contain,
                      ),*/
                      blinkWhenObscuring: true,
                      animationType: AnimationType.fade,
                      validator: (v) {
                        if (v.length < 3) {
                          return "I'm from validator";
                        } else {
                          return null;
                        }
                      },
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderRadius: BorderRadius.circular(5),
                        fieldHeight: 50,
                        fieldWidth: 40,
                        activeFillColor:
                            hasError ? Colors.blue.shade100 : Colors.white,
                      ),
                      cursorColor: Colors.black,
                      animationDuration: Duration(milliseconds: 300),
                      enableActiveFill: true,
                      errorAnimationController: errorController,
                      controller: textEditingController,
                      keyboardType: TextInputType.number,
                      boxShadows: [
                        BoxShadow(
                          offset: Offset(0, 1),
                          color: Colors.black12,
                          blurRadius: 10,
                        )
                      ],
                      onCompleted: (v) {
                        print("Completed");
                      },
                      // onTap: () {
                      //   print("Pressed");
                      // },
                      onChanged: (value) {
                        print(value);
                        setState(() {
                          currentText = value;
                        });
                      },
                      beforeTextPaste: (text) {
                        print("Allowing to paste $text");
                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                        return true;
                      },
                    )),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Text(
                  hasError ? "*Please fill up all the cells properly" : "",
                  style: GoogleFonts.comfortaa(
                      color: Colors.red,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                  textScaleFactor: 1,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  /* Text(
                    "Didn't receive the code? ",
                    style: GoogleFonts.comfortaa(color: Colors.black54, fontSize: 15),
                  ),
                  TextButton(
                      onPressed: () => snackBar("OTP resend!!"),
                      child: Text(
                        "RESEND",
                        style: GoogleFonts.comfortaa(
                            color: Color(0xFF91D3B3),
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ))*/
                ],
              ),
              SizedBox(
                height: 14,
              ),
              Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 16.0, horizontal: 30),
                child: ButtonTheme(
                  height: 50,
                  child: TextButton(
                    onPressed: () async {
                      formKey.currentState.validate();
                      if (currentText.length !=
                          6 /*|| currentText != "123459"*/) {
                        return {
                          errorController.add(ErrorAnimationType
                              .shake), // Triggering error shake animation
                          setState(() {
                            hasError = true;
                          }),
                        };
                      }
                      try {
                        // ignore: await_only_futures
                        await _verify(widget.result);
                      } catch (e) {
                        AwesomeDialog(
                          buttonsTextStyle: GoogleFonts.comfortaa(),
                          context: context,
                          dialogType: DialogType.INFO,
                          animType: AnimType.BOTTOMSLIDE,
                          //title: 'Dialog Title',
                          desc: "Connection problem. Please try again later",
                          btnCancelOnPress: () {},
                          btnOkOnPress: () {
                            Navigator.pop(context);
                          },
                        )..show();
                      }
                      hasError = false;
                      afterVerify();
                      // conditions for validating

                      /*else {
                        setState(
                          () {
                            hasError = false;
                          },
                        );
                      }*/
                    },
                    child: Center(
                        child: Text(
                      "VERIFY".toUpperCase(),
                      style: GoogleFonts.comfortaa(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                      textScaleFactor: 1,
                    )),
                  ),
                ),
                decoration: BoxDecoration(
                    color: Colors.green.shade300,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.green.shade200,
                          offset: Offset(1, -2),
                          blurRadius: 5),
                      BoxShadow(
                          color: Colors.green.shade200,
                          offset: Offset(-1, 2),
                          blurRadius: 5)
                    ]),
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                      child: TextButton(
                    child: Text(/*"Clear"*/ ""),
                    onPressed: () {
                      /*textEditingController.clear();*/
                    },
                  )),
                  Flexible(
                      child: TextButton(
                    child: Text(/*"Set Text"*/ ""),
                    onPressed: () {
                      /*setState(() {
                        textEditingController.text = "123456";
                      });*/
                    },
                  )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  _verify(String serverAnswer) async {
    try {
      appDataResult = await Api.instance.authApi.authRegisterStep2Post(
        OTP: currentText,
        code: serverAnswer,
      );
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  _token() async {
    try {
      await Api.instance.client.addDefaultHeader(
          "Authorization", "Bearer " + appDataResult.data.token);
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }
}
