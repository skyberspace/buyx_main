import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/api.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

import '../../../constants.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final TextEditingController controller = TextEditingController();

  String initialCountry = 'GB';

  String number;

  BooleanBR forgotStep2Result;

  ForgotPasswordStep1ResultBR sendCodeResult;

  TextEditingController controllerNewPassword = TextEditingController();

  TextEditingController controllerOTP = TextEditingController();

  afterVerificationCodeReceived(
      {String resetToken, String newPassword, String otp}) async {
    forgotStep2Result = await Api.instance.authApi.authForgotPasswordStep2Post(
        forgotPasswordStep2Request: ForgotPasswordStep2Request(
      code: resetToken,
      newPassword: newPassword,
      otp: otp,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Forgot Password",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: Material(
        elevation: 10,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              padding: EdgeInsets.only(top: 12, right: 5),
              child: Theme(
                data: ThemeData(primaryColor: Colors.grey),
                child: Container(
                  child: IntlPhoneField(
                    textAlign: TextAlign.center,
                    style: GoogleFonts.comfortaa(color: Colors.black),
                    decoration: InputDecoration(
                      labelStyle: GoogleFonts.comfortaa(color: kBuyxBlueColor),
                      counterStyle: GoogleFonts.comfortaa(color: Colors.red),
                      prefixStyle: GoogleFonts.comfortaa(color: Colors.black),
                      labelText: 'Phone Number',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(),
                      ),
                    ),
                    initialCountryCode: 'GB',
                    onChanged: (phone) {
                      this.number = phone.completeNumber;
                      print(phone
                          .completeNumber); // When sending message from otp, use the phone.completeNumber
                    },
                  ),
                ),
              ),
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(kBuyxRedColor),
              ),
              onPressed: () async {
                sendCodeResult =
                    await Api.instance.authApi.authForgotPasswordStep1Post(
                        forgotPasswordStep1Request: ForgotPasswordStep1Request(
                  phone: this.number,
                ));
                if (sendCodeResult.success) {
                  AwesomeDialog(
                    onDissmissCallback: (value) {
                      setState(() {});
                    },
                    autoHide: Duration(seconds: 2),
                    buttonsTextStyle: GoogleFonts.comfortaa(),
                    context: context,
                    dialogType: DialogType.SUCCES,
                    animType: AnimType.BOTTOMSLIDE,
                    //title: 'Dialog Title',
                    desc: "Verification code sent to your phone",
                    btnCancelOnPress: () {},
                    btnOkOnPress: () {},
                  )..show();
                }
                if (!sendCodeResult.success) {
                  AwesomeDialog(
                    onDissmissCallback: (value) {
                      setState(() {});
                    },
                    autoHide: Duration(seconds: 2),
                    buttonsTextStyle: GoogleFonts.comfortaa(),
                    context: context,
                    dialogType: DialogType.WARNING,
                    animType: AnimType.BOTTOMSLIDE,
                    //title: 'Dialog Title',
                    desc: sendCodeResult.errors[0],
                    btnCancelOnPress: () {},
                    btnOkOnPress: () {},
                  )..show();
                }
              },
              child: Text(
                "Send Code",
                style: GoogleFonts.comfortaa(
                  color: Colors.white,
                ),
                textScaleFactor: 1,
              ),
            ),
            if (sendCodeResult != null && sendCodeResult.success != false)
              Container(
                margin: EdgeInsets.only(left: 12, right: 12),
                child: TextFormField(
                  controller: controllerOTP,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Cant be empty';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    helperText: 'Verification Code',
                    /*hintText:
                        "The password you enter must be at least 8 characters!",
                    hintStyle: GoogleFonts.comfortaa(
                        fontSize: 14, color: Colors.blue.withOpacity(0.2)),*/
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.grey),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                          style: BorderStyle.solid, color: kBuyxBlueColor),
                    ),
                  ),
                ),
              ),
            if (sendCodeResult != null && sendCodeResult.success != false)
              Container(
                margin: EdgeInsets.only(left: 12, right: 12, top: 10),
                child: Center(
                  child: TextFormField(
                    controller: controllerNewPassword,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Cant be empty';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      helperText: 'New Password',
                      /*hintText:
                          "The password you enter must be at least 8 characters!",
                      hintStyle: GoogleFonts.comfortaa(
                          fontSize: 14, color: Colors.blue.withOpacity(0.2)),*/
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                            style: BorderStyle.solid, color: kBuyxBlueColor),
                      ),
                    ),
                  ),
                ),
              ),
            if (sendCodeResult != null && sendCodeResult.success == true)
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(kBuyxRedColor),
                ),
                onPressed: () async {
                  forgotStep2Result = await Api.instance.authApi
                      .authForgotPasswordStep2Post(
                          forgotPasswordStep2Request:
                              ForgotPasswordStep2Request(
                    code: sendCodeResult.data.passwordResetToken,
                    newPassword: controllerNewPassword.text != null
                        ? controllerNewPassword.text
                        : "",
                    otp: controllerOTP.text,
                  ));
                  print("data");
                  if (forgotStep2Result.success) {
                    AwesomeDialog(
                      onDissmissCallback: (value) {
                        Navigator.pop(context);
                      },
                      autoHide: Duration(seconds: 2),
                      buttonsTextStyle: GoogleFonts.comfortaa(),
                      context: context,
                      dialogType: DialogType.SUCCES,
                      animType: AnimType.BOTTOMSLIDE,
                      //title: 'Dialog Title',
                      desc: "Your Password has been reset!",
                      btnCancelOnPress: () {},
                      btnOkOnPress: () {},
                    )..show();
                  }
                  if (!forgotStep2Result.success) {
                    AwesomeDialog(
                      onDissmissCallback: (value) {},
                      autoHide: Duration(seconds: 2),
                      buttonsTextStyle: GoogleFonts.comfortaa(),
                      context: context,
                      dialogType: DialogType.WARNING,
                      animType: AnimType.BOTTOMSLIDE,
                      //title: 'Dialog Title',
                      desc: forgotStep2Result.errors[0],
                      btnCancelOnPress: () {},
                      btnOkOnPress: () {},
                    )..show();
                  }
                },
                child: Text(
                  "Reset Password",
                  style: GoogleFonts.comfortaa(
                    color: Colors.white,
                  ),
                  textScaleFactor: 1,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
