import 'package:buyx_api_main/api.dart';

class Api {
  Api({String baseUrl = "https://buyxapi.skyberspace.com"}) {
    this.client = ApiClient(basePath: baseUrl);
    this.authApi = MainAppAuthApi(this.client);
    this.cart = MainAppCartApi(this.client);
    this.catalog = MainAppCatalogApi(this.client);
    this.profile = MainAppProfileApi(this.client);
    this.cdn = MainAppCdnApi(this.client);
    this.layout = MainAppLayoutApi(this.client);
  }

  setAsDefaultInstance() {
    Api.instance = this;
  }

  ApiClient client;
  MainAppAuthApi authApi;
  MainAppCartApi cart;
  MainAppCatalogApi catalog;
  MainAppProfileApi profile;
  MainAppCdnApi cdn;
  MainAppLayoutApi layout;

  static setInstance(Api api) {
    Api.instance = api;
  }

  static Api instance;
}
