import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/BuyxPage/view/BuyxPage.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:provider/provider.dart';

class splashScreen extends StatefulWidget {
  @override
  _splashScreenState createState() => _splashScreenState();
}

class _splashScreenState extends State<splashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () async {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => NavigationBuyx()),
          (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBuyxRedColor,
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 2,
          height: MediaQuery.of(context).size.width / 2,
          decoration: BoxDecoration(
            color: kBuyxBlueColor,
            shape: BoxShape.circle,
          ),
          child: Center(
              child: Text(
            "Buyx",
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
              color: kBuyxRedColor,
            ),
          )),
        ),
      ),
    );
  }
}
