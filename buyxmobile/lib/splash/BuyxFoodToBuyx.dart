import 'package:flutter/material.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/constants.dart';

class BuyxFoodToBuyxScreen extends StatefulWidget {
  @override
  _BuyxFoodToBuyxScreenState createState() => _BuyxFoodToBuyxScreenState();
}

class _BuyxFoodToBuyxScreenState extends State<BuyxFoodToBuyxScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 1), () async {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => NavigationBuyx()),
        (Route<dynamic> route) => false,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBuyxRedColor,
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 2,
          height: MediaQuery.of(context).size.width / 2,
          decoration: BoxDecoration(
            color: kBuyxBlueColor,
            shape: BoxShape.circle,
          ),
          child: Center(
              child: Text(
            "Buyx",
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: kBuyxRedColor,
            ),
          )),
        ),
      ),
    );
  }
}
