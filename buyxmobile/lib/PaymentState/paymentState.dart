import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';

import '../../api.dart';

class PaymentState with ChangeNotifier {
  User user;

  bool get isLoggedIn => user != null;

  setUser(User newUser) async {
    this.user = newUser;
    notifyListeners();
  }
}
