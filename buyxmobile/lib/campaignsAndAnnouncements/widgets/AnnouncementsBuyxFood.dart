import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';
import 'ReusableCard.dart';
import 'ReusableOpenedCard.dart';
import 'package:animations/animations.dart';

class AnnouncementsGetirYemek extends StatefulWidget {
  @override
  _AnnouncementsGetirYemekState createState() =>
      _AnnouncementsGetirYemekState();
}

class _AnnouncementsGetirYemekState extends State<AnnouncementsGetirYemek> {
  BlogEntryListBR campaignResultsBR;

  List<BlogEntry> campaignResults;

  load() async {
    try {
      campaignResultsBR = await Api.instance.layout.layoutGetBlogEntriesPost(
          getBlogEntriesRequest:
              GetBlogEntriesRequest(blogType: BlogType.number2));
      setState(() {
        campaignResults = campaignResultsBR.data;
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    load();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: campaignResults.length,
                  itemBuilder: (context, index) {
                    return OpenContainer(
                      closedColor: kBackGroundColor,
                      openElevation: 0.0,
                      closedElevation: 0.0,
                      transitionType: ContainerTransitionType.fade,
                      transitionDuration: const Duration(milliseconds: 1000),
                      useRootNavigator: true,
                      closedBuilder:
                          (BuildContext context, VoidCallback openContainer) {
                        // getting datas at index.
                        return ReusableCard(
                          header: campaignResults[index].title,
                          subHeader: campaignResults[index].shortDescription,
                        );
                      },
                      openBuilder: (BuildContext context, VoidCallback _) {
                        return reusableOpenedCard(
                          header: campaignResults[index].title,
                          subHeader: campaignResults[index].shortDescription,
                          conditions: campaignResults[index].description,
                        );
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
