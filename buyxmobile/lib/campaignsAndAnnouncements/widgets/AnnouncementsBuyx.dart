import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/appOpeningState/AppOpeningState.dart';
import 'package:buyx/constants.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';
import 'ReusableCard.dart';
import 'ReusableOpenedCard.dart';
import 'package:animations/animations.dart';

class AnnouncementsBuyx extends StatefulWidget {
  @override
  _AnnouncementsBuyxState createState() => _AnnouncementsBuyxState();
}

class _AnnouncementsBuyxState extends State<AnnouncementsBuyx> {
  @override
  Widget build(BuildContext context) {
    var appOpeningProvider = Provider.of<AppOpeningState>(context);
    if (appOpeningProvider.loadingCampaignsAndAnnouncements == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Scaffold(
      backgroundColor: kBackGroundColor,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: appOpeningProvider.announcementResults.length,
                  itemBuilder: (context, index) {
                    return OpenContainer(
                      closedColor: kBackGroundColor,
                      openElevation: 0.0,
                      closedElevation: 0.0,
                      transitionType: ContainerTransitionType.fade,
                      transitionDuration: const Duration(milliseconds: 1000),
                      useRootNavigator: true,
                      closedBuilder:
                          (BuildContext context, VoidCallback openContainer) {
                        // getting datas at index.
                        return ReusableCard(
                          header: appOpeningProvider
                              .announcementResults[index].title,
                          subHeader: appOpeningProvider
                              .announcementResults[index].shortDescription,
                          imagePath: appOpeningProvider
                              .announcementResults[index].leadingImage,
                        );
                      },
                      openBuilder: (BuildContext context, VoidCallback _) {
                        return reusableOpenedCard(
                            imagePath: appOpeningProvider
                                .announcementResults[index].leadingImage,
                            header: appOpeningProvider
                                .announcementResults[index].title,
                            subHeader: appOpeningProvider
                                .announcementResults[index].shortDescription,
                            conditions: appOpeningProvider
                                .announcementResults[index].description);
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
