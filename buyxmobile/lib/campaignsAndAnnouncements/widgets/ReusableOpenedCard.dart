import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:buyx/campaignsAndAnnouncements/widgets/ReusableCard.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class reusableOpenedCard extends StatelessWidget {
  const reusableOpenedCard(
      {@required this.header, this.subHeader, this.conditions, this.imagePath});

  final String header;
  final String subHeader;
  final String conditions;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: kBuyxRedColor,
        title: Text(
          "Details",
          style: GoogleFonts.comfortaa(color: Colors.white),
          textScaleFactor: 1,
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              ReusableCard(
                imagePath: imagePath,
                header: header,
                subHeader: subHeader,
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Card(
                    child: Html(
                  data: conditions,
                )
                    /*ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: conditions.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                  '- ' + conditions[index],
                                ),
                              ),
                            ],
                          ),
                        );
                      }),*/
                    ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
