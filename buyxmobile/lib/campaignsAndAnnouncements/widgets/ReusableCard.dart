import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class ReusableCard extends StatelessWidget {
  const ReusableCard({@required this.header, this.subHeader, this.imagePath});

  final String header;
  final String subHeader;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 2.5,
      padding: EdgeInsets.all(10),
      child: Card(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 3,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                ),
                margin: EdgeInsets.all(5),
                child: Center(
                  child: FadeInImage.assetNetwork(
                    fit: BoxFit.fitWidth,
                    placeholder: 'assets/placeholder.gif',
                    image: imagePath != null
                        ? Api.instance.client.basePath +
                            "/cdn/image/original" +
                            imagePath
                        : Api.instance.client.basePath +
                            "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: header != null
                    ? Text(
                        header,
                        style: GoogleFonts.comfortaa(
                          color: kBuyxRedColor,
                          fontWeight: FontWeight.w900,
                        ),
                        textScaleFactor: 1,
                      )
                    : Text(""),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: subHeader != null
                    ? Text(
                        subHeader,
                        style: GoogleFonts.comfortaa(
                            color: kBuyxBlueColor, fontWeight: FontWeight.w900),
                        textScaleFactor: 1,
                      )
                    : Text(""),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
