import 'package:flutter/material.dart';
import 'package:buyx/campaignsAndAnnouncements/widgets/AnnouncementsBuyxFood.dart';
import 'package:buyx/campaignsAndAnnouncements/widgets/CampaignsBuyxFood.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class BuyxFoodCampaignNAnnouncementsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: RichText(
            textScaleFactor: 1,
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Buyx",
                  style: GoogleFonts.comfortaa(
                      color: kBuyxBlueColor, fontSize: 30),
                ),
                TextSpan(
                    text: "food",
                    style: GoogleFonts.comfortaa(
                        color: Colors.white, fontSize: 30)),
              ],
            ),
          ),
          backgroundColor: kBuyxRedColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Material(
                elevation: 2,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: TabBar(
                    labelColor: kBuyxRedColor,
                    unselectedLabelColor: Colors.grey,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(
                        color: kBrightBuyxRedColor,
                        borderRadius: BorderRadius.circular(10)),
                    tabs: [
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 25, left: 25),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Promotions",
                            style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold,
                            ),
                            textScaleFactor: 1,
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 25, left: 25),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Announcements",
                            style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold,
                            ),
                            textScaleFactor: 1,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: TabBarView(
                  // Tab Bar View
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    CampaignsGetirYemek(),
                    AnnouncementsGetirYemek(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
