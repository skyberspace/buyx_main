import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/campaignsAndAnnouncements/buyx/widgets/campaignRow.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../constants.dart';

class BuyxFoodPayCampaigns extends StatefulWidget {
  @override
  _BuyxFoodPayCampaignsState createState() => _BuyxFoodPayCampaignsState();
}

class _BuyxFoodPayCampaignsState extends State<BuyxFoodPayCampaigns> {
  BlogEntryListBR campaignResultsBR;

  List<BlogEntry> campaignResults;

  load() async {
    try {
      campaignResultsBR = await Api.instance.layout.layoutGetBlogEntriesPost(
          getBlogEntriesRequest:
              GetBlogEntriesRequest(blogType: BlogType.number1));
      setState(() {
        campaignResults = campaignResultsBR.data;
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  void initState() {
    super.initState();
    load();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          "Promotions",
          style: GoogleFonts.comfortaa(
            color: Colors.white,
          ),
          textScaleFactor: 1,
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Card(
                          child: Icon(
                            Icons.add,
                            color: kBuyxRedColor,
                          ),
                        ),
                      ),
                      Expanded(flex: 1, child: SizedBox()),
                      Expanded(
                          flex: 15,
                          child: Text(
                            'Add Campaign Code',
                            style: GoogleFonts.comfortaa(
                                fontWeight: FontWeight.bold,
                                color: kBuyxRedColor),
                            textScaleFactor: 1,
                          )),
                      Expanded(
                        flex: 3,
                        child: SizedBox(),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: campaignResults.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context, campaignResults[index].title);
                    },
                    child: CampaignRow(
                        //campaign: campaignResults[index],
                        ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      backgroundColor: kBackGroundColor,
    );
  }
}
