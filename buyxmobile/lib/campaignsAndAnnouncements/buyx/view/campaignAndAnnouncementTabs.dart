import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:buyx/appOpeningState/AppOpeningState.dart';
import 'package:buyx/campaignsAndAnnouncements/widgets/AnnouncementsBuyx.dart';
import 'package:buyx/campaignsAndAnnouncements/widgets/CampaignsBuyx.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';

class BuyxCampaignNAnnouncementsTab extends StatefulWidget {
  @override
  _BuyxCampaignNAnnouncementsTabState createState() =>
      _BuyxCampaignNAnnouncementsTabState();
}

class _BuyxCampaignNAnnouncementsTabState
    extends State<BuyxCampaignNAnnouncementsTab> {
  @override
  void initState() {
    super.initState();
    try {
      var appOpeningProvider =
          Provider.of<AppOpeningState>(context, listen: false);
      appOpeningProvider.loadCampaignsAndAnnouncments(context: context);
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please check your internet settings",
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    var appOpeningProvider = Provider.of<AppOpeningState>(context);
    if (appOpeningProvider.loadingCampaignsAndAnnouncements == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Promotions",
                style: GoogleFonts.comfortaa(
                  color: Colors.white,
                ),
                textScaleFactor: 1,
              ),
            ],
          ),
          backgroundColor: kBuyxRedColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Material(
                elevation: 2,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: TabBar(
                    labelColor: kBuyxRedColor,
                    unselectedLabelColor: Colors.grey,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(
                        color: Colors.blueAccent.withOpacity(0.15),
                        borderRadius: BorderRadius.circular(10)),
                    tabs: [
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 25, left: 25),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Promotions",
                            style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold,
                            ),
                            textScaleFactor: 1,
                          ),
                        ),
                      ),
                      Tab(
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, right: 5, left: 5),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Announcements",
                            style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold,
                            ),
                            textScaleFactor: 1,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: TabBarView(
                  // Tab Bar View
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    CampaignsBuyx(),
                    AnnouncementsBuyx(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
