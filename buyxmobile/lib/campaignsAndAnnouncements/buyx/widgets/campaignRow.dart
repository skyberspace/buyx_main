import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/campaignsAndAnnouncements/widgets/ReusableOpenedCard.dart';
import 'package:google_fonts/google_fonts.dart';

class CampaignRow extends StatelessWidget {
  const CampaignRow({
    @required this.campaign,
  });

  final CartCampaign campaign;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
      child: Card(
        child: Padding(
          padding:
              const EdgeInsets.only(top: 15, bottom: 15, left: 8, right: 8),
          child: Row(
            children: [
              /*Expanded(
                flex: 3,
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/placeholder.gif',
                    image: campaign.leadingImage),
              ),*/
              Expanded(flex: 1, child: SizedBox()),
              Expanded(
                flex: 15,
                child: Text(
                  campaign.name,
                  style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                  textScaleFactor: 1,
                ),
              ),
              Expanded(flex: 1, child: SizedBox()),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Discount Amount",
                      style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                      textScaleFactor: 1,
                    ),
                    Text(
                      campaign.discountAmount.toStringAsFixed(2),
                      style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                      textScaleFactor: 1,
                    ),
                  ],
                ),
              ),
              /*Expanded(
                flex: 3,
                child: GestureDetector(
                  onTap: () {
                    /*
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => reusableOpenedCard(
                                header: campaign.title,
                                subHeader: campaign.shortDescription,
                                conditions: campaign.description)));*/
                  },
                  child: Card(
                    child: Icon(
                      Icons.more_vert,
                    ),
                  ),
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}
