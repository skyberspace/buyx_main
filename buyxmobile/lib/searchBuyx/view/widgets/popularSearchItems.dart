import 'package:flutter/material.dart';
import 'package:buyx/BuyxPage/view/widgets/itemsDetailed.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/profilePage/view/widgets/favoriteItems/favoriteItemsPage.dart';
import 'package:google_fonts/google_fonts.dart';

class PopularSearchItem extends StatelessWidget {
  const PopularSearchItem({
    @required this.givenText,
  });
  final String givenText;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => itemsDetailed(itemName: givenText)));
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                givenText,
                style: GoogleFonts.comfortaa(
                  color: kBuyxRedColor,
                ),
                textScaleFactor: 1,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
