import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/BuyxPage/itemTabs/widgets/BuyxItem.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx/shoppingCartBuyx/view/CartPageBuyx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:permission_handler/permission_handler.dart';

class SearchBuyxPage extends StatefulWidget {
  @override
  _SearchBuyxPageState createState() => _SearchBuyxPageState();
}

class _SearchBuyxPageState extends State<SearchBuyxPage> {
  ProductSearchRankedListBR searchResultBR;

  List<ProductSearchRanked> searchResults = [];
  stt.SpeechToText _speech;
  bool _isListening = false;
  String _text = 'Press the button and start speaking';
  double _confidence = 1.0;

  PermissionStatus permissionResulted;

  /*List<PopularSearchItemModal> items = [
    PopularSearchItemModal(name: "yumurta"),
    PopularSearchItemModal(name: "yoğurt"),
    PopularSearchItemModal(name: "kahve"),
    PopularSearchItemModal(name: "peynir"),
    PopularSearchItemModal(name: "cips"),
    PopularSearchItemModal(name: "su"),
    PopularSearchItemModal(name: "dondurma"),
    PopularSearchItemModal(name: "ekmek"),
    PopularSearchItemModal(name: "sucuk"),
  ];*/

  getSearchResults(TextEditingController searchTextCont) async {
    if (searchTextCont.text.length > 1)
      try {
        searchResultBR = await Api.instance.catalog.catalogSearchProductsPost(
            stringFilterRequest:
                StringFilterRequest(filter: searchTextCont.text));
        searchResults = searchResultBR.data;
        setState(() {});
      } catch (e) {
        AwesomeDialog(
          buttonsTextStyle: GoogleFonts.comfortaa(),
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          //title: 'Dialog Title',
          desc: "Connection problem. Please try again later",
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      }
  }

  _listen() async {
    try {
      var permissionResult = await Permission.microphone.status;
      if (permissionResult.isDenied) {
        permissionResulted = await Permission.microphone.request();
      }
      if (permissionResulted.isDenied) return;
      if (!_isListening) {
        bool available = await _speech.initialize(
          onStatus: (val) => print('onStatus: $val'),
          onError: (val) => print('onError: $val'),
        );
        if (available) {
          setState(() => _isListening = true);
          _speech.listen(
            onResult: (val) => setState(() {
              _text = val.recognizedWords;
              if (Platform.isAndroid
                  ? val.finalResult
                  : val.hasConfidenceRating) {
                if (Platform.isIOS) _speech.stop();
                searchTextCont.text = val.recognizedWords;
                getSearchResults(searchTextCont);
              }
              _isListening = false;
              if (Platform.isIOS) {
                if (val.hasConfidenceRating && val.confidence > 0.8) {
                  _confidence = val.confidence;
                } else {
                  if (val.hasConfidenceRating && val.confidence > 80) {
                    _confidence = val.confidence;
                  }
                }
              }
            }),
          );

          /*getSearchResults(searchTextCont);*/
        }
      } else {
        setState(() => _isListening = false);
        _speech.stop();
      }
    } catch (e) {
      /*AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please check your internet connection",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          //Navigator.pop(context);
        },
      )..show();*/
    }
  }

  @override
  void initState() {
    super.initState();
    _speech = stt.SpeechToText();
  }

  TextEditingController searchTextCont = TextEditingController();
  @override
  Widget build(BuildContext context) {
    //var cartProvider = Provider.of<CartBuyxState>(context);
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(),
              Text(
                "Search",
                style: GoogleFonts.comfortaa(color: Colors.white),
                textScaleFactor: 1,
              ),
              Consumer<CartBuyxState>(
                  builder: (context, state, widget) =>
                      state.itemsList.isNotEmpty == true
                          ? GestureDetector(
                              onTap: () async {
                                await Provider.of<CartBuyxState>(context,
                                        listen: false)
                                    .loadCart(context: context);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => CartPageBuyx(),
                                  ),
                                );
                              },
                              child: Container(
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(4),
                                            topLeft: Radius.circular(4)),
                                        color: Colors.white,
                                      ),
                                      child: Center(
                                        child: Icon(
                                          Icons.shopping_basket,
                                          color: kBuyxRedColor,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(4),
                                            topRight: Radius.circular(4)),
                                        color: kBrightBuyxRedColor,
                                      ),
                                      child: Center(
                                        child: Text(
                                          state.sumOfAll.toStringAsFixed(2),
                                          style: GoogleFonts.comfortaa(
                                              color: kBuyxRedColor),
                                          textScaleFactor: 1,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : SizedBox()),
            ],
          ),
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Material(
              elevation: 10,
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 13.5,
                color: Colors.white,
                child: Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Icon(
                          Icons.search,
                          color: kBuyxRedColor,
                        )),
                    Expanded(
                      flex: 8,
                      child: TextField(
                        style: GoogleFonts.comfortaa(),
                        controller: searchTextCont,
                        onChanged: (value) {
                          getSearchResults(searchTextCont);
                        },
                        decoration: InputDecoration(
                          hintStyle: GoogleFonts.comfortaa(),
                          hintText: "Search Products",
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: _listen,
                        child: CircleAvatar(
                            child: Icon(
                                _isListening ? Icons.mic : Icons.mic_none)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            searchResults.isNotEmpty
                ? Material(
                    elevation: 10,
                    child: GridView.count(
                      primary: false,
                      shrinkWrap: true,
                      crossAxisCount: 4,
                      physics: NeverScrollableScrollPhysics(),
                      childAspectRatio: MediaQuery.of(context).size.width /
                          (/*MediaQuery.of(context).size.height / 1*/ 800),
                      children: List.generate(
                        searchResults.length,
                        (index) => BuyxItem(
                          product: searchResults[index].product,
                        ),
                      ),
                    ),
                  )
                : searchTextCont.text == ""
                    ? Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(
                              height: MediaQuery.of(context).size.height / 4,
                            ),
                            Icon(
                              Icons.favorite_rounded,
                              color: kBuyxBlueColor,
                              size: 150,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Browse thousands of products now",
                              style: GoogleFonts.comfortaa(
                                fontSize: 15,
                                fontWeight: FontWeight.w900,
                                color: kBuyxBlueColor,
                              ),
                              textScaleFactor: 1,
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      )
                    : Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(
                              height: MediaQuery.of(context).size.height / 4,
                            ),
                            Icon(
                              Icons.sentiment_dissatisfied,
                              color: kBuyxBlueColor,
                              size: 150,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Ups.. couldn't find it",
                              style: GoogleFonts.comfortaa(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: kBuyxBlueColor,
                              ),
                              textScaleFactor: 1,
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      )

            /*Container(
              padding: EdgeInsets.only(left: 10, top: 30),
              child: Text(
                "Favorite Products",
                style: GoogleFonts.comfortaa(
                  fontWeight: FontWeight.bold,
                  color: Colors.grey,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 10,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Center(
                child: ListView.builder(
                  primary: false,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: cartProvider.favorites.length,
                  itemBuilder: (context, index) {
                    return PopularSearchItem(
                      givenText: cartProvider.favorites[index].product.name,
                    );
                  },
                ),
              ),
            )*/
          ],
        ),
      ),
    );
  }
}
