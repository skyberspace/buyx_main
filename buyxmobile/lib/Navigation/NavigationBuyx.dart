import 'package:flutter/material.dart';
import 'package:buyx/campaignsAndAnnouncements/buyx/view/campaignAndAnnouncementTabs.dart';
import 'package:buyx/BuyxPage/BuyxPage.dart';
import 'package:buyx/profilePage/view/profilePage.dart';
import 'package:buyx/searchBuyx/view/SearchBuyxPage.dart';
import 'package:buyx/splash/BuyxToBuyxFood.dart';
import 'package:buyx/buyxActiveOrder/buyxActiveOrder.dart';
import 'package:google_fonts/google_fonts.dart';

class NavigationBuyx extends StatefulWidget {
  @override
  _NavigationBuyxState createState() => _NavigationBuyxState();
}

class _NavigationBuyxState extends State<NavigationBuyx> {
  void goToBuyxFood() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => NavigationBuyx()));
  }

  int _selectedIndex = 0;
  TextStyle optionStyle =
      GoogleFonts.comfortaa(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    BuyxPage(),
    SearchBuyxPage(),
    BuyxActiveOrder(),
    ProfilePage(),
    BuyxCampaignNAnnouncementsTab(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => BuyxToBuyxFoodScreen()));
        },
        tooltip: 'Increment',
        child: Icon(Icons.filter_tilt_shift),
        elevation: 2.0,
      ),*/
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: SizedBox(
        height: 80,
        child: BottomNavigationBar(
          selectedFontSize: 1,
          unselectedFontSize: 1,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                size: 24,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                size: 24,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.place_outlined,
                size: 24,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.account_box,
                size: 24,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.card_giftcard,
                size: 24,
              ),
              label: '',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Color(0xfffd3525),
          unselectedItemColor: Color(0xff013056),
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
