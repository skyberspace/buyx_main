import 'package:flutter/material.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class LocationAndTVS extends StatefulWidget {
  const LocationAndTVS({
    Key key,
  }) : super(key: key);

  @override
  _LocationAndTVSState createState() => _LocationAndTVSState();
}

class _LocationAndTVSState extends State<LocationAndTVS> {
  @override
  Widget build(BuildContext context) {
    return Consumer<LoggedInState>(
      builder: (context, state, widget) => Stack(
        children: [
          /*Container(
            color: kYellowColor,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 13.5, // 60
            child: Align(
              alignment: Alignment.centerRight,
              child: Container(
                padding: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width / 12.5,
                  top: 5,
                ), // 30 10
                color: kYellowColor,
                child: Column(
                  children: [
                    Text(
                      "TVS",
                      style: GoogleFonts.comfortaa(
                        color: kPurpleColor,
                        fontSize: 12,
                      ),
                    ),
                    /*RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: state.isLoggedIn ? "9" : '', // 9 TVS
                            style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                              color: kPurpleColor,
                            ),
                          ),
                          TextSpan(
                            text: "dk",
                            style: GoogleFonts.comfortaa(
                              color: kPurpleColor,
                            ),
                          ),
                        ],
                      ),
                    ),*/
                  ],
                ),
              ),
            ),
          ),*/
          Container(
            padding: EdgeInsets.only(left: 4, right: 4),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: kBuyxRedColor,
            ),
            child: Container(
              padding: EdgeInsets.only(left: 5),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 17.5, // 60
              decoration: BoxDecoration(
                color: kBuyxBlueColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                  bottomLeft: Radius.circular(30.0),
                  bottomRight: Radius.circular(30.0),
                ),
              ),
              child: (state.userDefaultAddress != null &&
                      state.userDefaultAddress.data != null)
                  ? Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5),
                          child: Icon(
                            Icons.home_outlined,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 5, bottom: 5),
                            child: VerticalDivider()),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.3,
                          child: RichText(
                            softWrap: false,
                            textScaleFactor: 1,
                            overflow: TextOverflow.ellipsis,
                            text: TextSpan(
                              children: [
                                if (state.userDefaultAddress.data != null) ...{
                                  TextSpan(
                                    text: state
                                        .userDefaultAddress.data.addressName,
                                    style: GoogleFonts.comfortaa(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: " " +
                                        state.userDefaultAddress.data
                                            .addressLine1,
                                    style: GoogleFonts.comfortaa(
                                      color: Colors.white,
                                    ),
                                  ),
                                } else ...{
                                  TextSpan(
                                    text: " ",
                                    style: GoogleFonts.comfortaa(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: " ",
                                    style: GoogleFonts.comfortaa(
                                      color: Colors.grey,
                                    ),
                                  ),
                                }
                              ],
                            ),
                          ),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 20,
                          color: Colors.grey,
                        ),
                      ],
                    )
                  : Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5),
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                              'Delivery Address is required to place an order!',
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              style: GoogleFonts.comfortaa(color: Colors.white),
                              textScaleFactor: 1,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 5),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            size: 20,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
