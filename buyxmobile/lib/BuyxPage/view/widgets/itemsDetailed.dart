import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/logInAndSignUp/view/LogIn/logIn.dart';
import 'package:buyx_api_main/api.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:buyx/api.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dots_indicator/dots_indicator.dart';

class itemsDetailed extends StatefulWidget {
  itemsDetailed({
    @required this.itemName,
    this.itemDescription,
    this.itemMoreDescription,
    this.itemPrice,
    this.itemImages,
    this.itemId,
  });

  final String itemName;
  final double itemPrice;
  final String itemDescription;
  final String itemMoreDescription;
  final List<ProductImage> itemImages;
  final int itemId;

  @override
  _itemsDetailedState createState() => _itemsDetailedState();
}

var cartStateProvider;

class _itemsDetailedState extends State<itemsDetailed> {
  CarouselController buttonCarouselController = CarouselController();
  int currentPage = 0;
  int position = 0;
  @override
  Widget build(BuildContext context) {
    /*var item = itemModal(
        priceItem: widget.itemPrice,
        descriptionItem: widget.itemDescription,
        nameItem: widget.itemName,
        numberOfItem: 1);*/
    var cartStateProvider = Provider.of<CartBuyxState>(context);
    try {
      cartStateProvider.getFavorites();
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) => Consumer<CartBuyxState>(
        builder: (context, state, widget) => Scaffold(
          backgroundColor: kBackGroundColor,
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "Product Details",
              style: GoogleFonts.comfortaa(
                color: Colors.white,
              ),
              textScaleFactor: 1,
            ),
            backgroundColor: kBuyxRedColor,
            elevation: 0,
            actions: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: (cartStateProvider.favorites
                          .where((product) =>
                              product.product.id == this.widget.itemId)
                          .isNotEmpty)
                      ? Icon(
                          Icons.favorite,
                          color: kBuyxBlueColor,
                        )
                      : Icon(
                          Icons.favorite_border,
                          color: kBuyxBlueColor,
                        ),
                  onTap: () async {
                    try {
                      var search = cartStateProvider.favorites.where(
                          (product) =>
                              product.product.id == this.widget.itemId);
                      if (search.isNotEmpty) {
                        var removeFavoriteResult = await Api.instance.profile
                            .profileRemoveProductFromFavoritesPost(
                                userFavoriteRequest: UserFavoriteRequest(
                                    favoriteId: search.first.id));
                        cartStateProvider.getFavorites();
                        print("favorilerden silindi");

                        setState(() {});
                      } else if (search.isEmpty) {
                        var addFavoriteResult = await Api.instance.profile
                            .profileAddProductToFavoritesPost(
                                productDetailsRequest: ProductDetailsRequest(
                                    productId: this.widget.itemId));
                        print("favorilere eklendi");
                        cartStateProvider.getFavorites();
                        setState(() {});
                      }
                    } catch (e) {
                      AwesomeDialog(
                        buttonsTextStyle: GoogleFonts.comfortaa(),
                        context: context,
                        dialogType: DialogType.INFO,
                        animType: AnimType.BOTTOMSLIDE,
                        //title: 'Dialog Title',
                        desc: "Connection problem. Please try again later",
                        btnCancelOnPress: () {},
                        btnOkOnPress: () {
                          Navigator.pop(context);
                        },
                      )..show();
                    }
                  },
                ),
              ),
            ],
          ),
          body: Stack(
            children: [
              Container(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Material(
                        elevation: 0,
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(7),
                                  /*border: Border.all(
                                    width: 0,
                                    color: kBrightPurpleColor,
                                  ),*/
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child:
                                      /*FadeInImage.assetNetwork(
                                    placeholder: 'assets/getirDoner.jpeg',
                                    image:  this.widget.itemImages.first,
                                  ),*/
                                      CarouselSlider(
                                    carouselController:
                                        buttonCarouselController,
                                    options: CarouselOptions(
                                      onPageChanged: (int index, reason) {
                                        setState(() {
                                          currentPage = index;
                                        });
                                      },
                                      viewportFraction: 2,
                                      autoPlay:
                                          /*this.widget.itemImages.length > 1
                                              ? true
                                              : false,*/
                                          false,
                                      aspectRatio: 2.0,
                                      enlargeCenterPage: true,
                                    ),
                                    items: this.widget.itemImages.map((i) {
                                      return Builder(
                                        builder: (BuildContext context) {
                                          return Container(
                                              /*width: MediaQuery.of(context)
                                                  .size
                                                  .width,*/
                                              margin: EdgeInsets.symmetric(
                                                  horizontal: 5.0),
                                              child: Center(
                                                  child: Image.network(
                                                //TODO : 150*150 düzeltilecek.
                                                i.path != null
                                                    ? Api.instance.client
                                                            .basePath +
                                                        "/cdn/image/original" +
                                                        i.path
                                                    : Api.instance.client
                                                            .basePath +
                                                        "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                                                fit: BoxFit.contain,
                                              )));
                                        },
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              DotsIndicator(
                                dotsCount: this.widget.itemImages.length,
                                position: currentPage.toDouble(),
                                onTap: (position) {
                                  setState(() {
                                    currentPage = position.toInt();
                                    buttonCarouselController
                                        .jumpToPage(currentPage);
                                  });
                                },
                                decorator: DotsDecorator(
                                  spacing: const EdgeInsets.all(10.0),
                                  activeColor: kBuyxRedColor,
                                  color: kBuyxBlueColor,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              if (this.widget.itemPrice != null) ...{
                                Text(
                                  "£ " +
                                      this.widget.itemPrice.toStringAsFixed(2),
                                  style: GoogleFonts.comfortaa(
                                    color: kBuyxRedColor,
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textScaleFactor: 1,
                                ),
                              } else ...{
                                Text(
                                  " ",
                                  style: GoogleFonts.comfortaa(
                                    color: kBuyxRedColor,
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textScaleFactor: 1,
                                ),
                              },
                              SizedBox(
                                height: 10,
                              ),
                              if (this.widget.itemName != null) ...{
                                Padding(
                                  padding: EdgeInsets.fromLTRB(19, 0, 19, 0),
                                  child: Text(
                                    this.widget.itemName.toString(),
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.comfortaa(
                                      color: kBuyxRedColor,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    textScaleFactor: 1,
                                  ),
                                )
                              } else ...{
                                Text(
                                  " ",
                                  style: GoogleFonts.comfortaa(
                                    color: kBuyxRedColor,
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textScaleFactor: 1,
                                ),
                              },
                              SizedBox(
                                height: 10,
                              ),
                              Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(19, 0, 19, 0),
                                  child: this.widget.itemDescription != null
                                      ? Html(
                                          data: this.widget.itemDescription,
                                        )
                                      : Text("")),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                      ),

                      /*Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          /*'Açıklama'*/ this.widget.itemDescription,
                          style: GoogleFonts.comfortaa(
                              color: kGreyColor, fontWeight: FontWeight.bold),
                        ),
                      ),*/
                      Container(
                        padding: EdgeInsets.all(8),
                        width: double.infinity,
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
                            child: (this.widget.itemMoreDescription != null)
                                ? Html(
                                    data: this.widget.itemMoreDescription,
                                  )
                                : Text(
                                    /*'Çikolata, karamel ve bisküvinin buluşması'*/ "",
                                    textAlign: TextAlign.center,
                                  ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                ),
              ),
              /*SizedBox(
                height: 100,
              ),*/
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: new BoxDecoration(
                    border: new Border.all(
                        width: 0.1,
                        color: Colors
                            .transparent), //color is transparent so that it does nßot blend with the actual color specified
                    color: Colors.white60,
                  ),
                  height: MediaQuery.of(context).size.height / 8,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: 25, bottom: 25, left: 10, right: 10),
                    child: (state.getItemCount(this.widget.itemId) == 0)
                        ? loggedState.isLoggedIn
                            ? GestureDetector(
                                onTap: () {
                                  state.addToCart(this.widget.itemId, context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: kBuyxRedColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Add to Cart',
                                      style: GoogleFonts.comfortaa(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                      textScaleFactor: 1,
                                    ),
                                  ),
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  AwesomeDialog(
                                    buttonsTextStyle: GoogleFonts.comfortaa(),
                                    context: context,
                                    dialogType: DialogType.WARNING,
                                    animType: AnimType.BOTTOMSLIDE,
                                    title: "You must add your address first",
                                    desc:
                                        "To add a product, you must log in from the profile section.",
                                    btnCancelOnPress: () {},
                                    btnOkOnPress: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  LogInPage()));
                                    },
                                  )..show();
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: kBuyxRedColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Add to Cart',
                                      style: GoogleFonts.comfortaa(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                      textScaleFactor: 1,
                                    ),
                                  ),
                                ),
                              )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    state.decrementItem(
                                        this.widget.itemId, context);
                                  },
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                  ),
                                  child: Text(
                                    '-',
                                    style: GoogleFonts.comfortaa(
                                        color: kBuyxRedColor,
                                        fontWeight: FontWeight.bold),
                                    textScaleFactor: 1,
                                  )),
                              ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          kBuyxRedColor),
                                ),
                                onPressed: null,
                                child: state
                                            .getItemCount(this.widget.itemId)
                                            .toString() !=
                                        null
                                    ? Text(
                                        state
                                            .getItemCount(this.widget.itemId)
                                            .toString(),
                                        style: GoogleFonts.comfortaa(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                        textScaleFactor: 1,
                                      )
                                    : Text(""),
                              ),
                              ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                  ),
                                  onPressed: () {
                                    state.addToCart(
                                        this.widget.itemId, context);
                                  },
                                  child: Text(
                                    '+',
                                    style: GoogleFonts.comfortaa(
                                        color: kBuyxRedColor,
                                        fontWeight: FontWeight.bold),
                                    textScaleFactor: 1,
                                  )),
                            ],
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
