import 'package:buyx/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BuyxCard extends StatelessWidget {
  const BuyxCard({
    @required this.givenText,
    this.givenImage,
  });
  final String givenText;
  final String givenImage;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 5.2,
            height: MediaQuery.of(context).size.width / 5.2,
            child: Card(
              elevation: 3,
              clipBehavior: Clip.antiAlias,
              color: kBackGroundColor,
              borderOnForeground: true,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Center(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FadeInImage.assetNetwork(
                        height: MediaQuery.of(context).size.width / 6,
                        width: MediaQuery.of(context).size.width / 6,
                        placeholder: 'assets/placeholder.gif',
                        image: givenImage),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 3,
          ),
          givenText != null
              ? Text(
                  givenText,
                  style: GoogleFonts.comfortaa(
                      color: kBuyxBlueColor,
                      fontSize: 11,
                      fontWeight: FontWeight.w900),
                  textScaleFactor: 1,
                  textAlign: TextAlign.center,
                )
              : Text(""),

          /*Text(
            givenText,
          )*/
        ],
      ),
    );
  }
}
