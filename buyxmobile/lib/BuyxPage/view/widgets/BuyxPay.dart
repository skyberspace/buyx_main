import 'dart:ffi';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/Navigation/NavigationBuyx.dart';
import 'package:buyx/api.dart';
import 'package:buyx/buyxActiveOrder/buyxActiveOrder.dart';
import 'package:buyx_api_main/api.dart';
import 'package:firebase_in_app_messaging/firebase_in_app_messaging.dart';
import 'package:flutter/material.dart';
import 'package:buyx/BuyxPage/view/widgets/paymentWebView.dart';
import 'package:buyx/campaignsAndAnnouncements/buyx/view/BuyxPayCampaigns.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/BuyxPage/view/widgets/LocationAndTVS.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/profilePage/view/widgets/adress/adressPage.dart';
import 'package:buyx/profilePage/view/widgets/paymentMethods.dart/paymentMethods.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BuyxPay extends StatefulWidget {
  @override
  _BuyxPayState createState() => _BuyxPayState();
}

class _BuyxPayState extends State<BuyxPay> {
  bool dontRingBell = false;
  bool iAgree = false;
  CartCampaign campaign;
  bool isCampaignOn = false;
  int billingOption = 1;
  bool loading = true;
  int selectedCampaignId = 0;

  CheckoutDetailsBR checkoutDetailsResult;
  CheckoutDetails checkOut;

  TextEditingController controllerNote = TextEditingController(text: "");

  //Map<String, dynamic> _paymentSheetData;
  Map<String, dynamic> paymentIntentData;

  PaymentSheetDataBR paymentIntentPostResult;

  PaymentSheetData _paymentSheetData;

  OrderBR nonProvisionedResult;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    load(selectedCampaignId);
  }

  load(int selectedCampaignId) async {
    try {
      loading = true;
      checkoutDetailsResult = await Api.instance.cart
          .cartGetCheckoutDetailsPost(
              checkoutDetailsRequest:
                  CheckoutDetailsRequest(campaignId: selectedCampaignId));
      if (checkoutDetailsResult.success) {
        checkOut = checkoutDetailsResult.data;
        setState(() {
          loading = false;
        });
      } else if (!checkoutDetailsResult.success) {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
          AwesomeDialog(
            buttonsTextStyle: GoogleFonts.comfortaa(),
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            //title: 'Dialog Title',
            desc: "Connection problem. Please try again later",
            btnCancelOnPress: () {},
            btnOkOnPress: () {
              Navigator.pop(context);
            },
          )..show();
        });
      }
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Consumer<CartBuyxState>(
      builder: (context, state, widget) => Scaffold(
        backgroundColor: kBackGroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(
            "Checkout",
            style: GoogleFonts.comfortaa(
              color: Colors.white,
            ),
            textScaleFactor: 1,
          ),
          backgroundColor: kBuyxRedColor,
          elevation: 0,
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Add Note',
                      style: GoogleFonts.comfortaa(
                        color: kBuyxBlueColor,
                      ),
                      textScaleFactor: 1,
                    ),
                  ),
                  Material(
                    elevation: 5,
                    child: Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                              controller: controllerNote,
                              onChanged: (value) {
                                print(controllerNote.text);
                              },
                              style: GoogleFonts.comfortaa(),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.only(
                                    left: 15, bottom: 11, top: 11, right: 15),
                                hintText: 'You can write your order note here.',
                                hintStyle: GoogleFonts.comfortaa(
                                  color: Colors.blueGrey.withOpacity(0.8),
                                ),
                              ),
                            ),
                          ),
                          /*Divider(),
                          CheckboxListTile(
                            checkColor: Colors.white,
                            activeColor: kPurpleColor,
                            title: Text(
                              "Zili Çalma",
                              style: GoogleFonts.comfortaa(
                                color: kYellowColor,
                                fontSize: 14,
                              ),
                            ),
                            value: dontRingBell,
                            onChanged: (newValue) {
                              setState(() {
                                dontRingBell = newValue;
                              });
                              print(dontRingBell);
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          ),*/
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Payment method',
                      style: GoogleFonts.comfortaa(
                        color: kBuyxBlueColor,
                      ),
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      padding: EdgeInsets.all(8),
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        children: [
                          if (Provider.of<LoggedInState>(context, listen: false)
                              .user
                              .canPlaceNonProvisionedPaymentMethods) ...{
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  billingOption = 0;
                                });
                              },
                              child: Row(
                                children: [
                                  Material(
                                    elevation: 5,
                                    child: Container(
                                      width: 20,
                                      height: 20,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                      ),
                                      child: billingOption == 0
                                          ? Container(
                                              margin: EdgeInsets.all(4),
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: kBuyxRedColor,
                                              ),
                                            )
                                          : SizedBox(),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Icon(
                                    Icons.account_balance_wallet_rounded,
                                    color: kBuyxBlueColor,
                                  ),
                                  /*Expanded(
                                  flex: 1,
                                  child: SizedBox(),
                                ),*/
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Pay at the door',
                                        style: GoogleFonts.comfortaa(
                                            fontWeight: FontWeight.w900,
                                            color: kBuyxBlueColor),
                                      ),
                                      /*Text(
                                      '432432423' + '*******' + '323',
                                      style: GoogleFonts.comfortaa(
                                        color: Colors.grey,
                                        fontSize: 12,
                                      ),
                                    ),*/
                                    ],
                                  ),
                                  /*Expanded(
                                  flex: 2,
                                  child: SizedBox(),
                                ),
                                OutlinedButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                PaymentMethods()));
                                  },
                                  child: Text(
                                    'Kart Değiştir',
                                    style: GoogleFonts.comfortaa(
                                        fontSize: 12, color: kBuyxRedColor),
                                  ),
                                ),*/
                                ],
                              ),
                            ),
                            Divider(),
                            SizedBox()
                          },
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              setState(() {
                                billingOption = 1;
                              });
                            },
                            child: Row(
                              children: [
                                Material(
                                  elevation: 5,
                                  child: Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                    ),
                                    child: billingOption == 1
                                        ? Container(
                                            margin: EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: kBuyxRedColor,
                                            ),
                                          )
                                        : SizedBox(),
                                  ),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Icon(
                                  Icons.credit_card,
                                  color: kBuyxBlueColor,
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Text(
                                  'Pay with Card',
                                  style: GoogleFonts.comfortaa(
                                      fontWeight: FontWeight.w900,
                                      color: kBuyxBlueColor),
                                ),
                                Expanded(
                                  flex: 6,
                                  child: SizedBox(),
                                ),
                              ],
                            ),
                          ),
                          /*Divider(),
                          Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Card(
                                  child: Icon(
                                    Icons.add,
                                    color: kPurpleColor,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: SizedBox(),
                              ),
                              Text(
                                'BMK Express ile Kart Ekle',
                                style: GoogleFonts.comfortaa(
                                    fontWeight: FontWeight.bold,
                                    color: kPurpleColor),
                              ),
                              Expanded(
                                flex: 2,
                                child: SizedBox(),
                              ),
                            ],
                          ),*/
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Payment Summary',
                      style: GoogleFonts.comfortaa(
                        color: kBuyxBlueColor,
                      ),
                      textScaleFactor: 1,
                    ),
                  ),
                  Material(
                    elevation: 10,
                    child: Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (checkOut.availableCampaigns.isNotEmpty)
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () async {
                                campaign = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => BuyxPayCampaigns(
                                      availableCampaigns:
                                          checkOut.availableCampaigns,
                                    ),
                                  ),
                                );

                                if (campaign != null) {
                                  setState(() {
                                    selectedCampaignId = campaign.id;
                                    load(selectedCampaignId);
                                    isCampaignOn = true;
                                  });
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: !isCampaignOn
                                    ? Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Icon(
                                              Icons.redeem,
                                              color: kBuyxRedColor,
                                            ),
                                          ),
                                          Expanded(
                                            flex: 4,
                                            child: Text(
                                              'Select Promotion',
                                              style: GoogleFonts.comfortaa(
                                                  fontWeight: FontWeight.bold),
                                              textScaleFactor: 1,
                                            ),
                                          ),
                                          Expanded(
                                            flex: 4,
                                            child: SizedBox(),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child:
                                                Icon(Icons.arrow_forward_ios),
                                          )
                                        ],
                                      )
                                    : GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        onTap: () async {
                                          campaign = await Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      BuyxPayCampaigns(
                                                        availableCampaigns: checkOut
                                                            .availableCampaigns,
                                                      )));

                                          if (campaign != null) {
                                            setState(() {
                                              selectedCampaignId = campaign.id;
                                              load(selectedCampaignId);
                                              isCampaignOn = true;
                                            });
                                          }
                                        },
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 1,
                                              child: Icon(
                                                Icons.redeem,
                                                color: kBuyxRedColor,
                                              ),
                                            ),
                                            Expanded(
                                                flex: 4,
                                                child: Text(
                                                  campaign.toString(),
                                                  style: GoogleFonts.comfortaa(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  textScaleFactor: 1,
                                                )),
                                            Expanded(
                                              flex: 1,
                                              child:
                                                  Icon(Icons.arrow_forward_ios),
                                            )
                                          ],
                                        ),
                                      ),
                              ),
                            ),
                          /*Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Card(
                                      child: Icon(
                                        Icons.add,
                                        color: kPurpleColor,
                                      ),
                                    )),
                                Expanded(
                                    flex: 4,
                                    child: Text(
                                      ' Add Invoice Info',
                                      style: GoogleFonts.comfortaa((
                                          fontWeight: FontWeight.bold,
                                          color: kPurpleColor),
                                    )),
                                Expanded(
                                  flex: 5,
                                  child: SizedBox(),
                                ),
                              ],
                            ),
                          ),
                          Divider(),*/
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 16, 8, 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Cart Amount',
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxBlueColor,
                                      fontWeight: FontWeight.w900),
                                  textScaleFactor: 1,
                                ),
                                state.sumOfAll != null
                                    ? Text(
                                        "£ " +
                                            checkOut.cartTotal
                                                .toStringAsFixed(2),
                                        style: GoogleFonts.comfortaa(
                                            color: kBuyxBlueColor,
                                            fontWeight: FontWeight.w900),
                                        textScaleFactor: 1,
                                      )
                                    : Text(""),
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Delivery',
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxBlueColor,
                                      fontWeight: FontWeight.w900),
                                  textScaleFactor: 1,
                                ),
                                Text(
                                  "£ " +
                                      /*'4.99'*/ checkOut.deliveryFee
                                          .toStringAsFixed(2),
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxBlueColor,
                                      fontWeight: FontWeight.w900),
                                  textScaleFactor: 1,
                                ),
                              ],
                            ),
                          ),
                          /*Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Tax',
                                  style: GoogleFonts.comfortaa(
                                      color: kYellowColor,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                ),
                                Text(
                                  '0.50',
                                  style: GoogleFonts.comfortaa(
                                      color: kYellowColor,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                ),
                              ],
                            ),
                          ),*/
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Total Payment',
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxRedColor,
                                      fontWeight: FontWeight.w900),
                                  textScaleFactor: 1,
                                ),
                                state.sumOfAll != null
                                    ? Text(
                                        "£ " +
                                            (/*state.sumOfAll + 0.5 + 4.99*/ checkOut
                                                    .total)
                                                .toStringAsFixed(2),
                                        style: GoogleFonts.comfortaa(
                                            color: kBuyxRedColor,
                                            fontWeight: FontWeight.w900),
                                        textScaleFactor: 1,
                                      )
                                    : Text(""),
                              ],
                            ),
                          ),
                          /* Divider(),
                          CheckboxListTile(
                            checkColor: Colors.white,
                            activeColor: kPurpleColor,
                            title: RichText(
                              textScaleFactor: 1,
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "I have read the ",
                                    style: GoogleFonts.comfortaa(
                                      color: Colors.black,
                                      fontSize: 12,
                                    ),
                                  ),
                                  TextSpan(
                                      text: "distance sales agreement",
                                      style: GoogleFonts.comfortaa(
                                          color: kPurpleColor, fontSize: 12)),
                                  TextSpan(
                                    text: " and accepting it.",
                                    style: GoogleFonts.comfortaa(
                                        color: Colors.black, fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                            value: iAgree,
                            onChanged: (newValue) {
                              setState(() {
                                iAgree = newValue;
                              });
                              print(iAgree);
                            },
                            controlAffinity: ListTileControlAffinity
                                .leading, //  <-- leading Checkbox
                          ),*/
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 8,
                  ),
                  SizedBox(
                    height: 40,
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: GestureDetector(
                onTap: () {},
                child: Container(
                  child: LocationAndTVS(),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: new BoxDecoration(
                  border: new Border.all(
                      width: 0.1,
                      color: Colors
                          .transparent), //color is transparent so that it does nßot blend with the actual color specified
                  color: Colors.white60,
                ),
                height: MediaQuery.of(context).size.height / 8,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 25, bottom: 25, left: 10, right: 10),
                  child: GestureDetector(
                    onTap: () {
                      FirebaseInAppMessaging.instance
                          .setMessagesSuppressed(true);
                      setState(() {
                        loading = true;
                      });
                      paymentArea();
                      FirebaseInAppMessaging.instance
                          .setMessagesSuppressed(false);
                    },
                    child: Row(
                      children: [
                        Expanded(
                          flex: 6,
                          child: Container(
                            decoration: BoxDecoration(
                              color: kBuyxRedColor,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Center(
                              child: Text(
                                'Order now',
                                style: GoogleFonts.comfortaa(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  paymentArea() async {
    _initPaymentSheet();
  }

  //
  Future<void> _initPaymentSheet() async {
    try {
      if (this.billingOption == 1) {
        paymentIntentPostResult =
            await Api.instance.cart.cartCreatePaymentIntentPost(
          createOrderRequest: CreateOrderRequest(
            campaignId: checkOut.selectedCampaign != null
                ? checkOut.selectedCampaign.id
                : 0,
            note: controllerNote.text != null ? controllerNote.text : "No Note",
          ),
        );
        _paymentSheetData = paymentIntentPostResult.data;
      } else if (this.billingOption == 0) {
        nonProvisionedResult = await Api.instance.cart
            .cartPlaceNonProvisionedOrderPost(
                createOrderRequestWithPayment: CreateOrderRequestWithPayment(
                    campaignId: checkOut.selectedCampaign != null
                        ? checkOut.selectedCampaign.id
                        : 0,
                    note: controllerNote.text != null
                        ? controllerNote.text
                        : "No Note",
                    paymentType: PaymentType.number0));
        if (nonProvisionedResult.success) {
          setState(() {
            loading = false;
          });
          Provider.of<CartBuyxState>(context, listen: false).itemsList = [];
          Provider.of<CartBuyxState>(context, listen: false).loadCart();
          return AwesomeDialog(
            onDissmissCallback: (value) {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => NavigationBuyx(),
                  ),
                  (route) => false);
            },
            autoHide: Duration(seconds: 5),
            buttonsTextStyle: GoogleFonts.comfortaa(),
            context: context,
            btnCancel: SizedBox(),
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            //title: 'Dialog Title',
            desc: 'Payment will be at the door',
            btnCancelText: "Go to Home Page",
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      }
      // 1. create payment intent on the server

      if (!paymentIntentPostResult.success) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(paymentIntentPostResult.errors[0]),
          ),
        );
        setState(() {
          loading = false;
        });
        return;
      }
      setState(() {
        loading = false;
      });

      // 2. initialize the payment sheet
      await Stripe.instance.initPaymentSheet(
        paymentSheetParameters: SetupPaymentSheetParameters(
          //applePay: true,
          //googlePay: true,
          style: ThemeMode.light,
          merchantCountryCode: 'GB',
          merchantDisplayName: "Buyx Limited",
          customerId: _paymentSheetData.customerId,
          paymentIntentClientSecret:
              _paymentSheetData.paymentIntentClientSecret,
          customerEphemeralKeySecret:
              _paymentSheetData.customerEphemeralKeySecret,
        ),
      );
      setState(() {});
      _displayPaymentSheet();
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Error: $e' + " initPayment Error")),
      );
    }
  }

  Future<void> _displayPaymentSheet() async {
    try {
      // 3. display the payment sheet.
      await Stripe.instance.presentPaymentSheet(
          parameters: PresentPaymentSheetParameters(
        clientSecret: _paymentSheetData.paymentIntentClientSecret,
        confirmPayment: true,
      ));
      Provider.of<CartBuyxState>(context, listen: false).itemsList = [];
      Provider.of<CartBuyxState>(context, listen: false).loadCart();
      setState(() {
        _paymentSheetData = null;
      });

      AwesomeDialog(
        onDissmissCallback: (value) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => NavigationBuyx(),
              ),
              (route) => false);
        },
        autoHide: Duration(seconds: 3),
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        btnCancel: SizedBox(),
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: 'Payment successfully completed',
        btnCancelText: "Go to Home Page",
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Payment Cancelled'),
        ),
      );
    }
  }
}





/*Future<void> makePayment() async {
    final url = Uri.parse(
        "https://us-central-flutterdemo-c949a.cloudfunctions.net/stripePayment");

    final response =
        await http.get(url, headers: {"Content-Type": "application/json"});
    paymentIntentData = json.decode(response.body);

    await Stripe.instance.initPaymentSheet(
      paymentSheetParameters: SetupPaymentSheetParameters(
        paymentIntentClientSecret:
            paymentIntentData != null ? paymentIntentData["paymentIntent"] : "",
        applePay: true,
        googlePay: true,
        style: ThemeMode.dark,
        merchantCountryCode: "UK",
        merchantDisplayName: "Buyx Limited",
      ),
    );
    setState(() {});
    displaypaymentSheet();
  }

  Future<void> displaypaymentSheet() async {
    try {
      await Stripe.instance.presentPaymentSheet(
        parameters: PresentPaymentSheetParameters(
          clientSecret: paymentIntentData != null
              ? paymentIntentData["paymentIntent"]
              : "",
          confirmPayment: true,
        ),
      );
      setState(() {
        paymentIntentData = null;
      });
      AwesomeDialog(
        autoHide: Duration(seconds: 3),
        btnCancel: Container(),
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Paid Successfully",
        btnOkOnPress: () {},
      )..show();
    } catch (e) {
      print(e);
    }
  }*/






// Flutter Braintree Codes
/* 
//StringBR clientToken = await Api.instance.cart.cartGetPaymentTokenPost();
    var loginProvider = Provider.of<LoggedInState>(context, listen: false);
    var request = BraintreeDropInRequest(
      tokenizationKey: "sandbox_5rdsf9b2_53mr5nvmbc8sb48w",
      collectDeviceData: true,
      cardEnabled: true,
      //requestThreeDSecureVerification: true,
      amount: checkOut.total.toStringAsFixed(2),
      maskSecurityCode: true,
      //clientToken: clientToken.data,
      paypalRequest: BraintreePayPalRequest(
          amount: checkOut.total.toStringAsFixed(2),
          displayName:
              loginProvider.user.name + " " + loginProvider.user.surname),
      googlePaymentRequest: BraintreeGooglePaymentRequest(
          billingAddressRequired: true,
          currencyCode: "GBP",
          googleMerchantID: "",
          totalPrice: checkOut.total.toStringAsFixed(2)),
    );
    BraintreeDropInResult result = await BraintreeDropIn.start(request);
    if (result != null) {
      print(result.paymentMethodNonce.description);
      print(result.paymentMethodNonce.nonce);
      var createOrderResult = await Api.instance.cart.cartCreateOrderPost(
          createOrderRequest: CreateOrderRequest(
              campaignId: selectedCampaignId,
              deviceData: result.deviceData,
              note: controllerNote != null && controllerNote.text != null
                  ? controllerNote.text
                  : "",
              paymentMethodNonce: result.paymentMethodNonce.nonce));
      print(this.controllerNote.text + "dikaaaat");
      print(createOrderResult.success);
      print(createOrderResult.errors[0]);
    }
*/