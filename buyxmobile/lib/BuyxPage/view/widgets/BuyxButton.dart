import 'package:flutter/material.dart';
import 'package:buyx/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class BuyxButton extends StatefulWidget {
  const BuyxButton({
    @required this.givenText,
    this.isTapped,
  });
  final String givenText;
  final bool isTapped;

  @override
  _BuyxButtonState createState() => _BuyxButtonState();
}

class _BuyxButtonState extends State<BuyxButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // ortada küçük durmasın diye yazıldı. Buyx Food eklendiğinde bu satır silinecek.
      padding: EdgeInsets.only(left: 0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: widget.isTapped ? kBuyxRedColor : kBuyxRedColor,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            widget.givenText,
            textAlign: TextAlign.center,
            style: GoogleFonts.comfortaa(
              fontSize: 15,
              letterSpacing: 1,
              color: widget.isTapped ? kBuyxBlueColor : Colors.white,
              fontWeight: FontWeight.w900,
            ),
            textScaleFactor: 1,
          ),
        ),
      ),
    );
  }
}
