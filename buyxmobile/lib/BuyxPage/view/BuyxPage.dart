import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/logInAndSignUp/view/SignUp/otpVerify.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:buyx/Navigation/NavigationBuyxFood.dart';
import 'package:buyx/api.dart';
import 'package:buyx/appOpeningState/AppOpeningState.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/BuyxPage/itemTabs/itemTabs.dart';
import 'package:buyx/BuyxPage/view/widgets/BuyxButton.dart';
import 'package:buyx/logInAndSignUp/loginAndSignUp.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/profilePage/view/widgets/adress/adressPage.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx/shoppingCartBuyx/view/CartPageBuyx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';
import 'widgets/BuyxCard.dart';
import 'widgets/LocationAndTVS.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:math';

class BuyxPage extends StatefulWidget {
  @override
  _BuyxPageState createState() => _BuyxPageState();

  bool isTappedBuyx = false;
}

class _BuyxPageState extends State<BuyxPage> {
  BannerListBR carouselImages;

  CategoryListBR rootCategories;

  bool loading = true;

  /*T getRandomElement<T>(List<T> list) {
    final random = new Random();
    var i = random.nextInt(list.length);
    return list[i];
  }*/

  startLoad() async {
    /*var appOpeningProvider = Provider.of<AppOpeningState>(context);
    await appOpeningProvider.load();
    carouselImages = appOpeningProvider.carouselImages;
    rootCategories = appOpeningProvider.rootCategories;*/

    try {
      if (!Provider.of<LoggedInState>(context, listen: false).isLoggedIn) {
        String token = await Provider.of<LoggedInState>(context, listen: false)
            .storage
            .read(key: "storageToken");
        if (token != null && token != "") {
          getLoginFromStorage(token: token);
        }
      }

      this.carouselImages = await Api.instance.layout.layoutGetHomeSliderGet();

      //carouselImages.data.first.imagePath

      this.rootCategories =
          await Api.instance.catalog.catalogGetRootCategoriesPost();

      if (!mounted) return;
      setState(() {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) {});
        loading = false;
      });
    } catch (e) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        AwesomeDialog(
          buttonsTextStyle: GoogleFonts.comfortaa(),
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          //title: 'Dialog Title',
          desc: "Connection problem. Please check your internet settings",
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
          btnCancel: SizedBox(),
        )..show();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    startLoad();
  }

  int randomInt = Random().nextInt(20);
  @override
  Widget build(BuildContext context) {
    //print(MediaQuery.of(context).size);
    //var loginProvider = Provider.of<LoggedInState>(context);
    if (loading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        toolbarHeight: MediaQuery.of(context).size.height / 18,
        automaticallyImplyLeading: false,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Consumer<CartBuyxState>(
                builder: (context, state, widget) =>
                    state.itemsList.isNotEmpty == true
                        ? SizedBox(
                            width: 75,
                          )
                        : SizedBox(),
              ),
              Container(
                child: Image.asset("assets/logo.png"),
                height: MediaQuery.of(context).size.height / 25,
              ),
              /*Text(
                "Buyx",
                style: GoogleFonts.comfortaa(
                  color: Colors.white,
                  //fontSize: 32,
                ),
              ),*/
              Consumer<CartBuyxState>(
                builder: (context, state, widget) =>
                    state.itemsList.isNotEmpty == true
                        ? GestureDetector(
                            onTap: () async {
                              await Provider.of<CartBuyxState>(context,
                                      listen: false)
                                  .loadCart(context: context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => CartPageBuyx(),
                                ),
                              );
                            },
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(2),
                                          topRight: Radius.circular(2),
                                          bottomLeft: Radius.circular(4),
                                          topLeft: Radius.circular(4)),
                                      color: Colors.white,
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.shopping_basket,
                                        color: kBuyxRedColor,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(4),
                                          topRight: Radius.circular(4)),
                                      color: kBrightBuyxRedColor,
                                    ),
                                    child: Center(
                                      child: state.sumOfAll != null
                                          ? Text(
                                              state.sumOfAll.toStringAsFixed(2),
                                              style: GoogleFonts.comfortaa(
                                                  color: kBuyxRedColor),
                                              textScaleFactor: 1,
                                            )
                                          : Text(""),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : SizedBox(),
              ),
            ],
          ),
        ),
        backgroundColor: kBuyxRedColor,
        elevation: 0,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    color: Colors.transparent,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 16,
                    ),
                  ),
                  CarouselSlider(
                    options: CarouselOptions(
                      viewportFraction: 1,
                      autoPlay: true,
                      aspectRatio: 2.0,
                      enlargeCenterPage: true,
                    ),
                    items: carouselImages.data.map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.symmetric(horizontal: 0.0),
                              child: Center(
                                  child: Image.network(
                                //TODO : 150*150 düzeltilecek.
                                i.imagePath != null
                                    ? Api.instance.client.basePath +
                                        "/cdn/image/original" +
                                        i.imagePath
                                    : Api.instance.client.basePath +
                                        "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                                fit: BoxFit.contain,
                              )));
                        },
                      );
                    }).toList(),
                  ),
                  /*SizedBox(
                    height: MediaQuery.of(context).size.height / 60,
                  ),*/
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      BuyxButton(
                        givenText: 'BuyxWholesale',
                        isTapped: true,
                      ),
                      GestureDetector(
                        onTap: () {
                          return AwesomeDialog(
                            autoHide: Duration(seconds: 3),
                            buttonsTextStyle: GoogleFonts.comfortaa(),
                            context: context,
                            btnCancel: SizedBox(),
                            dialogType: DialogType.INFO,
                            animType: AnimType.BOTTOMSLIDE,
                            //title: 'Dialog Title',
                            desc: "BuyxFood are coming soon!",
                            btnCancelOnPress: () {},
                            btnOkOnPress: () {},
                          )..show();
                        },
                        child: BuyxButton(
                          givenText: 'BuyxFood',
                          isTapped: false,
                        ),
                      ),
                      /*GestureDetector(
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NavigationBuyxFood()),
                              (Route<dynamic> route) => false,
                            );
                          },
                          child: BuyxButton(
                            givenText: 'BuyxFood',
                            isTapped: true,
                          ),
                        ),*/
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: GridView.count(
                      childAspectRatio: MediaQuery.of(context).size.width /
                          (/*MediaQuery.of(context).size.height / 1*/ 420),
                      physics: new NeverScrollableScrollPhysics(),
                      mainAxisSpacing: 5,
                      shrinkWrap: true,
                      crossAxisCount: 4,
                      children:
                          List.generate(rootCategories.data.length, (index) {
                        return GestureDetector(
                          onTap: () {
                            var appOpeningProvider =
                                Provider.of<AppOpeningState>(context,
                                    listen: false);
                            /*appOpeningProvider.loadSubCats(
                                rootCategories.data, index);*/
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => BuyxTabs(
                                    initPos: index,
                                    mainCats: rootCategories.data,
                                  ),
                                ));
                          },
                          child: Container(
                            child: BuyxCard(
                              givenText: rootCategories.data[index].name,
                              givenImage: rootCategories
                                          .data[index].imagePath !=
                                      null
                                  ? Api.instance.client.basePath +
                                      "/cdn/image/150x150" +
                                      rootCategories.data[index].imagePath
                                  : Api.instance.client.basePath +
                                      "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                            ),
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              ),
            ),
            Consumer<LoggedInState>(
              builder: (context, state, widget) => Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: kBuyxRedColor,
                  ),
                  child: GestureDetector(
                      onTap: () {
                        state.isLoggedIn
                            ? Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AdressPage(
                                            /*loginProvider.user*/ state.user)))
                                .then((_) => setState(() {}))
                            : Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LogInPage(),
                                ),
                              );
                        /*showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('You must login first.'),
                                    content: Text(
                                        "To add an address, you must log in from the profile section."),
                                    actions: <Widget>[
                                      TextButton(
                                        child: Text(
                                          "Ok",
                                          style: GoogleFonts.comfortaa(color: Colors.white),
                                        ),
                                        style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  kGreyColor),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );*/
                      },
                      child: LocationAndTVS()),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getLoginFromStorage({String token}) async {
    try {
      Api.instance.client.addDefaultHeader("Authorization", "Bearer " + token);
      AuthTokenBR userResult =
          await Api.instance.authApi.authGetUserFromTokenPost();
      //AuthTokenBR loginResult;
      /*loginResult = await Api.instance.authApi.authLoginPost(
          loginRequest: LoginRequest(password: password, phone: phone));*/
      Provider.of<LoggedInState>(context, listen: false)
          .setUser(userResult.data.user);
      Provider.of<CartBuyxState>(context, listen: false).loadCart(
        context: context,
      );
    } catch (e) {
      AwesomeDialog(
          autoHide: Duration(seconds: 1),
          buttonsTextStyle: GoogleFonts.comfortaa(),
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          //title: 'Dialog Title',
          desc: "Connection problem. Please check your internet settings",
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
          btnCancel: SizedBox())
        ..show();
    }
  }
}
