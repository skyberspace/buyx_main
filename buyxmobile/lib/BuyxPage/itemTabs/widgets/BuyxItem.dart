import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx/logInAndSignUp/view/LogIn/logIn.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:buyx/BuyxPage/itemTabs/itemTabs.dart';
import 'package:buyx/BuyxPage/view/widgets/itemsDetailed.dart';
import 'package:buyx/api.dart';
import 'package:buyx/loggedInState/state/loggedInState.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';

class BuyxItem extends StatefulWidget {
  BuyxItem({
    this.product,
  });

  final Product product;
  @override
  _BuyxItemState createState() => _BuyxItemState();
}

class _BuyxItemState extends State<BuyxItem> {
  String currency = "£";
  bool loading = true;

  ProductBR productData;
  int productCount;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    /*var item = itemModal(
        priceItem: widget.itemPrice,
        descriptionItem: widget.itemDescription,
        nameItem: widget.itemName,
        numberOfItem: widget.itemCount);*/

    return Consumer<LoggedInState>(
      builder: (context, loggedState, widget) =>
          Consumer<CartBuyxState>(builder: (context, state, widget) {
        /*if (state.checkExist(item)) {
          isAdded = true;
          numberAdded = state.getItemCount(item);
        } else {
          isAdded = false;
          numberAdded = 0;
        }*/

        return Container(
          padding: EdgeInsets.all(8),
          child: Stack(
            children: [
              GestureDetector(
                onTap: () async {
                  int count = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => itemsDetailed(
                                itemId: this.widget.product.id,
                                itemMoreDescription:
                                    this.widget.product.description,
                                itemImages: this.widget.product.images,
                                itemName: this.widget.product.name,
                                itemPrice: this.widget.product.price,
                                itemDescription:
                                    this.widget.product.shortDescription,
                              )));
                  //print('this is count ' + count.toString());
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        border: Border.all(
                          width: 1,
                          color: kBrightBuyxRedColor,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FadeInImage.assetNetwork(
                          fit: BoxFit.contain,
                          height: MediaQuery.of(context).size.width / 5,
                          width: MediaQuery.of(context).size.width / 5,
                          placeholder: 'assets/placeholder.gif',
                          image: this.widget.product.images != null
                              ? Api.instance.client.basePath +
                                  "/cdn/image/150x150" +
                                  this.widget.product.images.first.path
                              : Api.instance.client.basePath +
                                  "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    this.widget != null ||
                            this.widget.product != null ||
                            this.widget.product.price != null
                        ? Text(
                            currency +
                                " " +
                                this.widget.product.price.toStringAsFixed(2),
                            style: GoogleFonts.comfortaa(
                                color: kBuyxRedColor,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                            textScaleFactor: 1,
                          )
                        : Text(""),
                    this.widget != null ||
                            this.widget.product != null ||
                            this.widget.product.name != null
                        ? Text(
                            this.widget.product.name,
                            style: GoogleFonts.comfortaa(
                                fontSize: 10,
                                color: kBuyxBlueColor,
                                fontWeight: FontWeight.w900),
                            textAlign: TextAlign.center,
                            textScaleFactor: 1,
                          )
                        : Text(""),
                    /*Text(
                      productDetails.shortDescription,
                      style: TextStyle(
                          color: kGreyColor, fontWeight: FontWeight.bold),
                    ),*/
                  ],
                ),
              ),
              state.getItemCount(this.widget.product.id) > 0
                  ? Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        height: MediaQuery.of(context).size.height / 9.5,
                        width: MediaQuery.of(context).size.width / 9.375,
                        child: Column(
                          children: [
                            Expanded(
                              child: ElevatedButton(
                                onPressed: () {
                                  try {
                                    state.addToCart(
                                        this.widget.product.id, context);
                                  } catch (e) {
                                    AwesomeDialog(
                                      buttonsTextStyle: GoogleFonts.comfortaa(),
                                      context: context,
                                      dialogType: DialogType.INFO,
                                      animType: AnimType.BOTTOMSLIDE,
                                      //title: 'Dialog Title',
                                      desc:
                                          "Connection problem. Please try again later",
                                      btnCancelOnPress: () {},
                                      btnOkOnPress: () {
                                        Navigator.pop(context);
                                      },
                                    )..show();
                                  }
                                },
                                child: Text(
                                  '+',
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxRedColor,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                ),
                              ),
                            ),
                            Expanded(
                              child: ElevatedButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          kBuyxRedColor),
                                ),
                                onPressed: null,
                                child: state.getItemCount(
                                            this.widget.product.id) !=
                                        null
                                    ? Text(
                                        state
                                            .getItemCount(
                                                this.widget.product.id)
                                            .toString(),
                                        style: GoogleFonts.comfortaa(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                        softWrap: false,
                                        textWidthBasis: TextWidthBasis.parent,
                                        textScaleFactor: 1,
                                        overflow: TextOverflow.visible,
                                      )
                                    : Text(""),
                              ),
                            ),
                            Expanded(
                              child: ElevatedButton(
                                onPressed: () {
                                  try {
                                    state.decrementItem(
                                        this.widget.product.id, context);
                                  } catch (e) {
                                    AwesomeDialog(
                                      buttonsTextStyle: GoogleFonts.comfortaa(),
                                      context: context,
                                      dialogType: DialogType.INFO,
                                      animType: AnimType.BOTTOMSLIDE,
                                      //title: 'Dialog Title',
                                      desc:
                                          "Connection problem. Please try again later",
                                      btnCancelOnPress: () {},
                                      btnOkOnPress: () {
                                        Navigator.pop(context);
                                      },
                                    )..show();
                                  }
                                },
                                child: Text(
                                  '-',
                                  style: GoogleFonts.comfortaa(
                                      color: kBuyxRedColor,
                                      fontWeight: FontWeight.bold),
                                  textScaleFactor: 1,
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : loggedState.isLoggedIn
                      ? GestureDetector(
                          onTap: () {
                            try {
                              state.addToCart(this.widget.product.id, context);
                            } on Exception catch (e) {
                              AwesomeDialog(
                                buttonsTextStyle: GoogleFonts.comfortaa(),
                                context: context,
                                dialogType: DialogType.INFO,
                                animType: AnimType.BOTTOMSLIDE,
                                //title: 'Dialog Title',
                                desc:
                                    "Connection problem. Please try again later",
                                btnCancelOnPress: () {},
                                btnOkOnPress: () {
                                  Navigator.pop(context);
                                },
                              )..show();
                            }
                          },
                          child: Align(
                              alignment: Alignment.topRight,
                              child: Card(
                                elevation: 10,
                                child: Icon(
                                  Icons.add,
                                  color: kBuyxRedColor,
                                ),
                              )),
                        )
                      : GestureDetector(
                          onTap: () {
                            AwesomeDialog(
                              buttonsTextStyle: GoogleFonts.comfortaa(),
                              context: context,
                              dialogType: DialogType.WARNING,
                              animType: AnimType.BOTTOMSLIDE,
                              title: 'You must add adress first.',
                              desc:
                                  "To add a product, you must log in from the profile section.",
                              btnCancelOnPress: () {},
                              btnOkOnPress: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LogInPage()));
                              },
                            )..show();
                          },
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Card(
                              elevation: 10,
                              child: Icon(
                                Icons.add,
                                color: kBuyxRedColor,
                              ),
                            ),
                          ),
                        ),
            ],
          ),
        );
      }),
    );
  }
}
