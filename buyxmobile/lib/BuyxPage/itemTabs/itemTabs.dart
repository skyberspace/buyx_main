import 'dart:async';
import 'dart:collection';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:buyx_api_main/api.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';
import 'package:buyx/appOpeningState/AppOpeningState.dart';
import 'package:buyx/constants.dart';
import 'package:buyx/BuyxPage/itemTabs/widgets/BuyxItem.dart';
import 'package:buyx/shoppingCartBuyx/state/cartState.dart';
import 'package:buyx/shoppingCartBuyx/view/CartPageBuyx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:indexed_list_view/indexed_list_view.dart';
import 'package:provider/provider.dart';
import 'package:buyx/api.dart';
import 'package:rive/rive.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class BuyxTabs extends StatefulWidget {
  BuyxTabs({
    this.initPos,
    /*this.tabs*/ this.mainCats,
  });
  final int initPos;
  /*final List<tabModal> tabs*/ List<Category> mainCats;
  @override
  _BuyxTabsState createState() => _BuyxTabsState();
}

class _BuyxTabsState extends State<BuyxTabs> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      try {
        var appOppeningProvider =
            Provider.of<AppOpeningState>(context, listen: false);
        appOppeningProvider.loadSubCats(this.widget.mainCats);
      } catch (e) {
        AwesomeDialog(
          buttonsTextStyle: GoogleFonts.comfortaa(),
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          //title: 'Dialog Title',
          desc: "Connection problem. Please try again later",
          btnCancelOnPress: () {},
          btnOkOnPress: () {
            Navigator.pop(context);
          },
        )..show();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    int initPosition = widget.initPos;
    var appOppeningProvider = Provider.of<AppOpeningState>(context);
    if (appOppeningProvider.buyxProductPageLoading == true)
      return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.darken,
              ),
              image: AssetImage("assets/loadingBackground.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Container(
              child: RiveAnimation.asset('assets/buyxLoadingAnim.riv'),
              width: MediaQuery.of(context).size.width / 1.2,
              //height: MediaQuery.of(context).size.width / 2,
            ),
          ),
        ),
      );
    else if (appOppeningProvider.buyxProductPageLoading == false)
      return Consumer<CartBuyxState>(
        builder: (context, state, widget) => Scaffold(
          backgroundColor: kBackGroundColor,
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
            title: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Text(
                    "Products",
                    style: GoogleFonts.comfortaa(color: Colors.white),
                    textScaleFactor: 1,
                  ),
                  Consumer<CartBuyxState>(
                    builder: (context, state, widget) => state
                                .itemsList.isNotEmpty ==
                            true
                        ? GestureDetector(
                            onTap: () async {
                              await Provider.of<CartBuyxState>(context,
                                      listen: false)
                                  .loadCart(context: context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CartPageBuyx()));
                            },
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(4),
                                          topLeft: Radius.circular(4)),
                                      color: Colors.white,
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.shopping_basket,
                                        color: kBuyxRedColor,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(4),
                                          topRight: Radius.circular(4)),
                                      color: kBrightBuyxRedColor,
                                    ),
                                    child: Center(
                                      child: state.sumOfAll != null
                                          ? Text(
                                              state.sumOfAll.toStringAsFixed(2),
                                              style: GoogleFonts.comfortaa(
                                                  color: kBuyxRedColor),
                                            )
                                          : Text(""),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Text("          "),
                  ),
                ],
              ),
            ),
            backgroundColor: kBuyxRedColor,
            elevation: 0,
          ),
          body: SafeArea(
            bottom: false,
            child: this.widget != null &&
                    this.widget.mainCats != null &&
                    this.widget.mainCats.length != null
                ? CustomTabView(
                    initPosition: initPosition,
                    itemCount: this.widget.mainCats.length,
                    tabBuilder: (context, index) =>
                        Tab(text: this.widget.mainCats[index].name),
                    pageBuilder: (context, index) {
                      return getirTabInside(
                          category: this.widget.mainCats[index]);
                    },
                    onPositionChange: (index) {
                      print('current position: $index');
                      initPosition = index;
                      /*_getirTabInsideState().load();*/
                    },
                    onScroll: (position) => print('$position'),
                  )
                : Text(""),
          ),
        ),
      );
  }
}

/// Implementation

class CustomTabView extends StatefulWidget {
  final int itemCount;
  final IndexedWidgetBuilder tabBuilder;
  final IndexedWidgetBuilder pageBuilder;
  final Widget stub;
  final ValueChanged<int> onPositionChange;
  final ValueChanged<double> onScroll;
  final int initPosition;

  CustomTabView({
    @required this.itemCount,
    @required this.tabBuilder,
    @required this.pageBuilder,
    this.stub,
    this.onPositionChange,
    this.onScroll,
    this.initPosition,
  });

  @override
  _CustomTabsState createState() => _CustomTabsState();
}

class _CustomTabsState extends State<CustomTabView>
    with TickerProviderStateMixin {
  TabController controller;
  int _currentCount;
  int _currentPosition;

  @override
  void initState() {
    _currentPosition = widget.initPosition ?? 0;
    controller = TabController(
      length: widget.itemCount,
      vsync: this,
      initialIndex: _currentPosition,
    );
    controller.addListener(onPositionChange);
    controller.animation.addListener(onScroll);
    _currentCount = widget.itemCount;
    super.initState();
  }

  @override
  void didUpdateWidget(CustomTabView oldWidget) {
    if (_currentCount != widget.itemCount) {
      controller.animation.removeListener(onScroll);
      controller.removeListener(onPositionChange);
      controller.dispose();

      if (widget.initPosition != null) {
        _currentPosition = widget.initPosition;
      }

      if (_currentPosition > widget.itemCount - 1) {
        _currentPosition = widget.itemCount - 1;
        _currentPosition = _currentPosition < 0 ? 0 : _currentPosition;
        if (widget.onPositionChange is ValueChanged<int>) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (mounted) {
              widget.onPositionChange(_currentPosition);
            }
          });
        }
      }

      _currentCount = widget.itemCount;
      setState(() {
        //_getirTabInsideState().loadingProducts = true;
        controller = TabController(
          length: widget.itemCount,
          vsync: this,
          initialIndex: _currentPosition,
        );
        controller.addListener(onPositionChange);
        controller.animation.addListener(onScroll);
      });
    } else if (widget.initPosition != null) {
      controller.animateTo(widget.initPosition);
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.animation.removeListener(onScroll);
    controller.removeListener(onPositionChange);
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.itemCount < 1) return widget.stub ?? Container();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          color: kBuyxSecondRedColor,
          alignment: Alignment.center,
          child: TabBar(
            isScrollable: true,
            controller: controller,
            labelColor: Colors.white,
            unselectedLabelColor: Colors.white,
            indicator: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: kBuyxBlueColor,
                  width: 2,
                ),
              ),
            ),
            tabs: List.generate(
              widget.itemCount,
              (index) => widget.tabBuilder(context, index),
            ),
          ),
        ),
        Expanded(
          child: TabBarView(
            controller: controller,
            children: List.generate(
              widget.itemCount,
              (index) => widget.pageBuilder(context, index),
            ),
          ),
        ),
      ],
    );
  }

  onPositionChange() {
    if (!controller.indexIsChanging) {
      _currentPosition = controller.index;
      if (widget.onPositionChange is ValueChanged<int>) {
        widget.onPositionChange(_currentPosition);
      }
    }
  }

  onScroll() {
    if (widget.onScroll is ValueChanged<double>) {
      widget.onScroll(controller.animation.value);
    }
  }
}

class getirTabInside extends StatefulWidget {
  getirTabInside({@required this.category});
  final Category category;

  @override
  _getirTabInsideState createState() => _getirTabInsideState();
}

class _getirTabInsideState extends State<getirTabInside> {
  var controller = ItemScrollController();
  static int tabIndex = 0;
  //bool loadingProducts = true;

  //Future<CategoryListBR> subCatsData;

  CategoryListBR subCatsData;
  //List<Category> subcats;
  bool ceaseControl = false;

  var subcatListScrollController = ItemScrollController();

  var animateMilliseconds = 300;
  load() async {
    //await loadSubCats();
    //setState(() {});
    try {
      Timer.periodic(new Duration(milliseconds: 50), (timer) {
        if (ceaseControl) return;
        for (var gKey in globalKeyHashMap.entries) {
          var process = gKey.value;
          if (process.currentContext != null && context != null) {
            var appopening =
                Provider.of<AppOpeningState>(context, listen: false);
            RenderBox box = process.currentContext.findRenderObject();
            Offset position = box.localToGlobal(Offset.zero);

            RenderBox appbarRender =
                secondAppbarGlobalKey.currentContext.findRenderObject();

            var startHeight = appbarRender.size.height * 2.5;
            var endHeight = appbarRender.size.height * 6;

            if (position.dy > startHeight && position.dy < endHeight) {
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                var setIndex = gKey.key %
                    appopening.subcats[this.widget.category.id].length;
                if (tabIndex != setIndex && !ceaseControl)
                  setState(() {
                    tabIndex = setIndex;
                    subcatListScrollController.scrollTo(
                        index: tabIndex,
                        duration: Duration(milliseconds: animateMilliseconds));
                  });
              });
            }
          }
        }
      });
    } catch (e) {
      AwesomeDialog(
        buttonsTextStyle: GoogleFonts.comfortaa(),
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        //title: 'Dialog Title',
        desc: "Connection problem. Please try again later",
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.pop(context);
        },
      )..show();
    }
  }

  @override
  void initState() {
    load();
    super.initState();
  }

  GlobalKey secondAppbarGlobalKey = GlobalKey();
  var globalKeyHashMap = HashMap<int, GlobalKey>();
  @override
  Widget build(BuildContext context) {
    var appopening = Provider.of<AppOpeningState>(context);

    if (appopening.subcats[this.widget.category.id] == null) {
      return Scaffold();
    }
    return Scaffold(
      backgroundColor: kBackGroundColor,
      appBar: AppBar(
        key: secondAppbarGlobalKey,
        automaticallyImplyLeading: false,
        title: Container(
          height: 30.0,
          width: MediaQuery.of(context).size.width,
          child: ScrollablePositionedList.separated(
            itemScrollController: this.subcatListScrollController,
            itemCount: appopening.subcats[this.widget.category.id].length,
            separatorBuilder: (BuildContext context, int index) => SizedBox(
              width: 10,
            ),
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  ceaseControl = true;
                  Future.delayed(Duration(milliseconds: 100), () {
                    ceaseControl = false;
                  });
                  setState(() {
                    tabIndex = index;
                    controller.scrollTo(
                        index: index, duration: Duration(milliseconds: 10));
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      setState(() {
                        //
                        subcatListScrollController.scrollTo(
                            index: tabIndex,
                            duration: Duration(
                                milliseconds: this.animateMilliseconds));
                      });
                    });
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(4),
                  decoration: (tabIndex == index)
                      ? BoxDecoration(
                          color: kBuyxRedColor,
                          borderRadius: BorderRadius.circular(5))
                      : BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            width: 2,
                            color: kBrightBuyxRedColor,
                          )),
                  child: Center(
                    child: Text(
                        appopening.subcats[this.widget.category.id][index].name
                            .toString(),
                        style: (tabIndex == index)
                            ? TextStyle(color: Colors.white, fontSize: 14)
                            : TextStyle(color: kBuyxRedColor, fontSize: 14)),
                  ),
                ),
              );
            },
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: ScrollablePositionedList.builder(
        //shrinkWrap: true,
        itemCount: appopening.subcats[this.widget.category.id].length,
        /* maxItemCount:
            subcats.length != 0 ? subcats.length - 1 : subcats.length,*/

        itemScrollController: controller,
        itemBuilder: (context, requestedIndex) {
          int listViewIndex = (requestedIndex %
                  appopening.subcats[this.widget.category.id].length)
              .abs();
          /*WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            setState(() {
              tabIndex = listViewIndex;
            });
          });*/

          GlobalKey key;
          if (globalKeyHashMap.containsKey(requestedIndex)) {
            key = globalKeyHashMap[requestedIndex];
          } else {
            key = GlobalKey();
            globalKeyHashMap[requestedIndex] = key;
          }

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                key: key,
                padding: EdgeInsets.all(8),
                child: appopening.subcats[this.widget.category.id]
                                [listViewIndex] !=
                            null &&
                        appopening
                                .subcats[this.widget.category.id][listViewIndex]
                                .name !=
                            null
                    ? Text(
                        appopening
                            .subcats[this.widget.category.id][listViewIndex]
                            .name
                            .toString(),
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.bold, color: Colors.grey),
                        textScaleFactor: 1,
                      )
                    : Text(""),
              ),
              Material(
                elevation: 10,
                child: GridView.count(
                  primary: false,
                  shrinkWrap: true,
                  crossAxisCount: 4,
                  physics: NeverScrollableScrollPhysics(),
                  childAspectRatio: MediaQuery.of(context).size.width /
                      (MediaQuery.of(context).size.height / 0.9),
                  children: List.generate(
                    appopening.subcats[this.widget.category.id][listViewIndex]
                        .products.length,
                    (index) => BuyxItem(
                        product: appopening
                            .subcats[this.widget.category.id][listViewIndex]
                            .products[index]
                            .product),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              )
            ],
          );
        },
      ),
    );
  }
}
